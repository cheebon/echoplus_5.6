'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	10/04/2008
'	Purpose	    :	SFMS Category List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_FieldForceActy_FieldActyCatList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "FieldActyCatList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.FIELDACTY)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.FIELDACTY
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

                'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
                    btnSave.Visible = False
                End If
                '----------------------------------------------------------------------------------------------------

            Else
                If Master_Row_Count > 0 Then
                    For Each dr As GridViewRow In dgList.Rows
                        Dim btnPreview As Button = CType(dr.Cells(aryDataItem.IndexOf("BTN_PREVIEW")).Controls(0), Button)
                        If btnPreview IsNot Nothing Then
                            btnPreview.CssClass = "cls_button"
                        End If
                    Next
                End If
            End If

            lblErr.Text = ""
           
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            wuc_toolbar.InfoString = ""

            ActivateFirstTab()
            RenewDataBind()

            If wuc_toolbar.TeamCode = "" Then
                wuc_toolbar.InfoString = "Please select the team."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        If wuc_toolbar.TeamCode = "" Then
            wuc_toolbar.InfoString = "Please select the team."
            wuc_toolbar.updatePanel()
            Exit Sub
        End If

        'ViewState("CAT_ID") = ""

        txtCatCode.Text = ""
        txtCatCode.Enabled = True
        txtCatName.Text = ""

        btnSave.Visible = True 'HL: 20080429 (AR)
        ActivateAddTab()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = ""

            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            Dim strSelectedTeamCode As String = ViewState("TEAM_CODE")
            Dim strSelectedCatCode As String = ViewState("CAT_CODE")

            If strSelectedCatCode <> "" Then
                clsFieldActy.UpdateCat(strSelectedTeamCode, strSelectedCatCode, Trim(txtCatName.Text))
                ViewState("TEAM_CODE") = strSelectedTeamCode
                ViewState("CAT_CODE") = strSelectedCatCode
                lblInfo.Text = "The record is successfully saved."
            Else
                Dim DT As DataTable
                DT = clsFieldActy.CreateCat(wuc_toolbar.TeamCode, Trim(txtCatCode.Text), Trim(txtCatName.Text))
                Dim strIsDeleted As String = ""

                Dim isDuplicate As Integer
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Category Code already exists!"
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Category Code is used before, please create a new one!"
                Else
                    ViewState("TEAM_CODE") = wuc_toolbar.TeamCode
                    ViewState("CAT_CODE") = Trim(txtCatCode.Text).ToString

                    'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                    'HL: 20080429 (AR)
                    'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
                    '    ActivateEditTab()
                    'Else
                    '    btnSave.Visible = False
                    'End If

                    ActivateEditTab()
                    If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
                        btnSave.Visible = False
                    End If
                    '----------------------------------------------------------------------------------------------------

                    lblInfo.Text = "The record is successfully created."
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            With (wuc_toolbar)
                DT = clsFieldActy.GetCatList(.TeamCode, .SearchType, .SearchValue)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            'ADD BUTTON EDIT
            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details-------------------------------------------------
            'If Master_Row_Count > 0 AndAlso _
            '    Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 AndAlso _
                Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Delete) Then 'HL: 20080429 (AR)

                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")
            End If

            'ADD BUTTON PREVIEW
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "80"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = ""

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Button
                    dgBtnColumn.ControlStyle.CssClass = "cls_button"
                    dgBtnColumn.ShowSelectButton = True
                    dgBtnColumn.SelectText = "Preview"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_PREVIEW")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldActyCatList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldActyCatList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldActyCatList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldActyCatList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldActyCatList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedTeamCode As String = sender.datakeys(e.NewEditIndex).item("TEAM_CODE")
            Dim strSelectedCatCode As String = sender.datakeys(e.NewEditIndex).item("CAT_CODE")
            Dim DT As DataTable
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy

            DT = clsFieldActy.GetCatDetails(strSelectedTeamCode, strSelectedCatCode)

            If DT.Rows.Count > 0 AndAlso DT.Rows(0)("LOCK_FLAG") = "0" Then
                ViewState("TEAM_CODE") = strSelectedTeamCode
                ViewState("CAT_CODE") = strSelectedCatCode
                txtCatCode.Text = IIf(IsDBNull(DT.Rows(0)("CAT_CODE")), "", DT.Rows(0)("CAT_CODE"))
                txtCatName.Text = IIf(IsDBNull(DT.Rows(0)("CAT_NAME")), "", DT.Rows(0)("CAT_NAME"))

                ActivateEditTab()
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "<script>alert('This record is locked by other user!');</script>", False)
                RenewDataBind()
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 AndAlso Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Delete) Then 'HL: 20080429 (AR)
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            strDeleteMsg = "Are you sure want to delete?"
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        Try
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            Dim strSelectedTeamCode As String = sender.datakeys(e.RowIndex).item("TEAM_CODE")
            Dim strSelectedCatCode As String = sender.datakeys(e.RowIndex).item("CAT_CODE")
            Dim DT As DataTable
            Dim isSubCatExist As Integer

            DT = clsFieldActy.DeleteCat(strSelectedTeamCode, strSelectedCatCode)

            If DT.Rows.Count > 0 Then
                isSubCatExist = DT.Rows(0)("IS_SUB_CAT_EXISTS")
            End If

            If isSubCatExist > 0 Then
                lblMsgPop.Message = "There are " + isSubCatExist.ToString + " sub item(s) under this category!"
                lblMsgPop.Show()
            Else
                RenewDataBind()
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.SelectedIndexChanged
        Try
            Dim strCatcode As String
            strCatcode = sender.SelectedDataKey.Item("CAT_CODE").ToString

            With (wuc_FieldActyCatPreviewPop)
                .TeamCode = wuc_toolbar.TeamCode
                .CatCode = strCatcode
                .BindCatPreview()
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Category List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "Category Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Category Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = True
        TabPanel2.HeaderText = "Sub Category"

        txtCatCode.Enabled = False

        wuc_FieldActySubCatList.TeamCode = wuc_toolbar.TeamCode
        wuc_FieldActySubCatList.CatCode = Trim(txtCatCode.Text)
        wuc_FieldActySubCatList.RenewDataBind()

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)
            If TabPanel2.Visible Then
                aryDgList.Add(wuc_FieldActySubCatList.GetDGSubCatList())
            End If

            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            If TabPanel2.Visible Then
                wuc_FieldActySubCatList.RefreshDataBind()
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    
End Class

Public Class CF_FieldActyCatList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "CAT_CODE"
                strFieldName = "Category Code"
            Case "CAT_NAME"
                strFieldName = "Category Name"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "TEAM_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

