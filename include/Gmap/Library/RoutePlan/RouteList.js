﻿var _RouteList;

function RouteList()
{
    this.Customers = [];
    _RouteList = this;
}
RouteList.prototype.AddCustomer = addcustomer;
RouteList.prototype.Clear = clear;

function addcustomer(_CustCode, _CustName, _Latitude, _Longitude)
{
    var cust = new Customer(_CustCode, _CustName, _Latitude, _Longitude);
    
    _RouteList.Customers.push(cust);
}

function clear()
{
    _RouteList.Customers = [];
}
