﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsCountry
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function CountryList() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_COUNTRY_LIST"
                CountryList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsCountry.CountryList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub Delete(ByVal strCountryId As String, ByVal strUserId As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_COUNTRY_DELETE"
                .addItem("COUNTRY_ID", strCountryId, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                .spDelete()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsCountry.delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CountryDtl(ByVal strCountryId As String, ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_COUNTRY_DETAIL"
                .addItem("COUNTRY_ID", strCountryId, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                CountryDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsCountry.CountryDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function Create(ByVal strCountryCode As String, ByVal strCountryName As String, ByVal strCountryName1 As String, _
                           ByVal strCountryDesc As String, ByVal strCountryDesc1 As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_COUNTRY_CREATE"
                .addItem("country_code", strCountryCode, cor_DB.clsDB.DataType.DBString)
                .addItem("country_name", strCountryName, cor_DB.clsDB.DataType.DBString)
                .addItem("country_name_1", strCountryName1, cor_DB.clsDB.DataType.DBString)
                .addItem("country_desc", strCountryDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("country_desc_1", strCountryDesc1, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBString)
                Create = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsCountry.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub Update(ByVal strCountryID As String, ByVal strCountryCode As String, ByVal strCountryName As String, ByVal strCountryName1 As String, _
                           ByVal strCountryDesc As String, ByVal strCountryDesc1 As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_COUNTRY_UPDATE"
                .addItem("country_id", strCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("country_code", strCountryCode, cor_DB.clsDB.DataType.DBString)
                .addItem("country_name", strCountryName, cor_DB.clsDB.DataType.DBString)
                .addItem("country_name_1", strCountryName1, cor_DB.clsDB.DataType.DBString)
                .addItem("country_desc", strCountryDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("country_desc_1", strCountryDesc1, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsCountry.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
