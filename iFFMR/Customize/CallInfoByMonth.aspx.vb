Imports System.Data

Partial Class iFFMR_Customize_CallInfoByMonth
    Inherits System.Web.UI.Page

    Public Enum Mode As Integer
        Normal = 0
        Export = 1
    End Enum

#Region "Local Variable"
    Dim _reportMode As Mode
    Public Property ReportMode() As Mode
        Get
            Return _reportMode
        End Get
        Set(ByVal value As Mode)
            _reportMode = value
        End Set
    End Property
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_CallInfoByMonth"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property


    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "Customize_CallInfoByMonth.aspx"
        End Get
    End Property
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLINFOBYMONTH)
                .DataBind()
                .Visible = True
            End With


            If Not IsPostBack Then
                'Call Panel
                With wuc_ctrlpanel
                    .SubModuleID = SubModuleType.CALLINFOBYMONTH
                    .DataBind()
                    .Visible = True
                End With

                Dim gotpreviouspage As String
                gotpreviouspage = Request.QueryString("PAGE_INDICATOR")

                LoadDdlPrdSpecialty()

                If Not String.IsNullOrEmpty(gotpreviouspage) Then
                    RefreshDatabinding()
                    wuc_MultiAuthen.PanelCollapese = True
                Else
                    CriteriaCollector = Nothing ' New clsSharedValue    
                    BindDefault()
                End If

            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub


#Region "PRD_SPECIALTY"
    Private Sub LoadDdlPrdSpecialty()
        Try
            ddlSelectedPrdSpecialty.Items.Clear()

            Dim clsSFCallInfo As New rpt_Customize.clsSFCallInfo

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With ddlSelectedPrdSpecialty
                .DataSource = clsSFCallInfo.GetPrdSpecialty(strUserID, strPrincipalID)
                .DataTextField = "PTL_NAME"
                .DataValueField = "PTL_CODE"
                .DataBind()
            End With

            ddlSelectedPrdSpecialty.Items.Add(New ListItem("OTH - Others", "OTH"))

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbPrdSpecialty : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Public ReadOnly Property PTL_Code() As String
        Get
            Return Trim(ddlSelectedPrdSpecialty.SelectedValue)
        End Get
    End Property


#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MultiAuthen.ResetBtn_Click
        Try

            BindDefault()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MultiAuthen.RefreshBtn_Click
        Try

            ViewState("dtCurrentView") = Nothing
            RefreshDatabinding()

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRefresh_Click : " & ex.ToString)
        Finally

        End Try
    End Sub
#End Region

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = Nothing 'CType(ViewState("dtCurrentView"), Data.DataTable)

        Try
            'If dtCurrenttable Is Nothing Then
            dtCurrenttable = GetRecList()

            ' ViewState("dtCurrentView") = dtCurrenttable
            dgList.PageIndex = 0
            ' End If
            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            dgList.DataSource = dvCurrentView
            dgList.PageSize = 26 'PageSize
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SFCALLINFO.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SFCALLINFO.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFCALLINFO.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFCALLINFO.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If aryDataItem.IndexOf("DESC_CODE") >= 0 Then
                        Dim strdescCode As String = Trim(e.Row.Cells(aryDataItem.IndexOf("DESC_CODE")).Text)
                        If strdescCode = "CUST_CLASS_TTL" OrElse strdescCode = "PLAN_CALL_CLASS_TTL" OrElse strdescCode = "ACT_CALL_CLASS_TTL" OrElse strdescCode = "ACHIV_CLASS_TTL" OrElse strdescCode = "AVG_CALL_CUST_CLASS_TTL" OrElse strdescCode = "AVG_CALL_DAY" Then
                            e.Row.CssClass = "GridFooter"
                        End If
                        e.Row.Cells(aryDataItem.IndexOf("DESC_CODE")).Text = CF_SFCALLINFO.GetDisplayColumnName(strdescCode)

                        Dim iIndex As Integer
                        Dim strCellData As String

                        If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DAILYCALLACTY, SubModuleAction.View) Then
                            If strdescCode Like "ACT_CALL_CLASS_*" Then
                                For i As Integer = 1 To 12
                                    iIndex = aryDataItem.IndexOf(CF_SFCALLINFO.GetMonthName(i))
                                    strCellData = DataBinder.Eval(e.Row.DataItem, CF_SFCALLINFO.GetMonthName(i))

                                    'Dim gotpreviouspage As String
                                    'gotpreviouspage = Request.QueryString("PAGE_INDICATOR")

                                    'If Not String.IsNullOrEmpty(gotpreviouspage) Then
                                    e.Row.Cells(iIndex).Text = "<a href=""DailyCallActy.aspx?CLASS=" & strdescCode.Replace("ACT_CALL_CLASS_", "") & _
                                     "&MONTH=" & i & "&SALESREP_CODE=" & CriteriaCollector.SalesrepCode & "&SUB_CAT_CODE=" & PTL_Code & _
                                     "&PAGE_INDICATOR=DAILYCALLACTY" & """ target=""_self"">" & _
                                     strCellData & "</a>"
                                    'Else

                                    'e.Row.Cells(iIndex).Text = "<a href=""DailyCallActy.aspx?CLASS=" & strdescCode.Replace("ACT_CALL_CLASS_", "") & _
                                    '   "&MONTH=" & i & "&SALESREP_CODE=" & wuc_MultiAuthen.SalesrepCode & "&SUB_CAT_CODE=" & PTL_Code & _
                                    '   "&PAGE_INDICATOR=DAILYCALLACTY" & """ target=""_self"">" & _
                                    '   strCellData & "</a>"
                                    'End If

                                Next
                            End If
                        End If
                    End If
                Case DataControlRowType.Footer
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    'Private Function GetRecList() As Data.DataTable
    '    Dim DT As DataTable = Nothing
    '    Try



    '        With wuc_MultiAuthen
    '            DT = GetRecList(.TeamCode, .SalesrepCode, PTL_Code, .SelectedYear)
    '            Session("Year") = .SelectedYear
    '        End With


    '        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
    '    Finally
    '    End Try
    '    Return DT
    'End Function

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .PrincipalID = Session("PRINCIPAL_ID")
                .PrincipalCode = Session("PRINCIPAL_CODE")
                If Page.IsPostBack And ReportMode = Mode.Normal Then
                    .TeamCode = wuc_MultiAuthen.TeamCode
                    .SalesrepCode = wuc_MultiAuthen.SalesrepCode
                    .Year = wuc_MultiAuthen.SelectedYear
                    .PTLCode = PTL_Code
                    Session("Year") = wuc_MultiAuthen.SelectedYear
                End If

            End With

            'CriteriaCollector = CriteriaCollector

            Dim clsSFCALLINFO As New rpt_Customize.clsSFCallInfo
            With clsSFCALLINFO.properties
                .UserID = Session.Item("UserID")
                .PrinID = Session("PRINCIPAL_ID")
                .PrinCode = Session("PRINCIPAL_CODE")
                .Year = CriteriaCollector.Year
                .TeamCode = CriteriaCollector.TeamCode
                .SalesrepCode = CriteriaCollector.SalesrepCode
                .PTLCode = CriteriaCollector.PTLCode
            End With


            DT = clsSFCALLINFO.GetSFCallInfoList

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListPTLCode() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .PrincipalID = Session("PRINCIPAL_ID")
                .PrincipalCode = Session("PRINCIPAL_CODE")
                'If Page.IsPostBack Then
                '.TeamCode = wuc_MultiAuthen.TeamCode
                '.SalesrepCode = wuc_MultiAuthen.SalesrepCode
                '.Year = wuc_MultiAuthen.SelectedYear
                .PTLCode = PTL_Code
                'Session("Year") = wuc_MultiAuthen.SelectedYear
                'End If

            End With

            'CriteriaCollector = CriteriaCollector

            Dim clsSFCALLINFO As New rpt_Customize.clsSFCallInfo
            With clsSFCALLINFO.properties
                .UserID = Session.Item("UserID")
                .PrinID = Session("PRINCIPAL_ID")
                .PrinCode = Session("PRINCIPAL_CODE")
                .Year = CriteriaCollector.Year
                .TeamCode = CriteriaCollector.TeamCode
                .SalesrepCode = CriteriaCollector.SalesrepCode
                .PTLCode = CriteriaCollector.PTLCode
            End With


            DT = clsSFCALLINFO.GetSFCallInfoList

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            ReportMode = Mode.Export
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            ReportMode = Mode.Normal
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

    Private Class CF_SFCALLINFO
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            Select Case ColumnName.ToUpper
                Case "CUST_CLASS_A"
                    strFieldName = "CLASS A CUSTOMER"
                Case "CUST_CLASS_B"
                    strFieldName = "CLASS B CUSTOMER"
                Case "CUST_CLASS_C"
                    strFieldName = "CLASS C CUSTOMER"
                Case "CUST_CLASS_OTH"
                    strFieldName = "CLASS OTHER CUSTOMER"
                Case "CUST_CLASS_TTL"
                    strFieldName = "TOTAL CUSTOMER"
                Case "PLAN_CALL_CLASS_A"
                    strFieldName = "CLASS A PlANNED CALL"
                Case "PLAN_CALL_CLASS_B"
                    strFieldName = "CLASS B PlANNED CALL"
                Case "PLAN_CALL_CLASS_C"
                    strFieldName = "CLASS C PlANNED CALL"
                Case "PLAN_CALL_CLASS_OTH"
                    strFieldName = "CLASS OTHER PlANNED CALL"
                Case "PLAN_CALL_CLASS_TTL"
                    strFieldName = "TOTAL PlANNED CALL"
                Case "ACT_CALL_CLASS_A"
                    strFieldName = "CLASS A CALL ACTIVITIES"
                Case "ACT_CALL_CLASS_B"
                    strFieldName = "CLASS B CALL ACTIVITIES"
                Case "ACT_CALL_CLASS_C"
                    strFieldName = "CLASS C CALL ACTIVITIES"
                Case "ACT_CALL_CLASS_OTH"
                    strFieldName = "CLASS OTH CALL ACTIVITIES"
                Case "ACT_CALL_CLASS_TTL"
                    strFieldName = "TOTAL CALL ACTIVITIES"
                Case "AVG_CALL_DAY"
                    strFieldName = "AVERAGE CALL ACTIVITIES/FIELD DAYS"
                Case "ACHIV_CLASS_A"
                    strFieldName = "PERCENTAGE OF CLASS A ACHIEVED"
                Case "ACHIV_CLASS_B"
                    strFieldName = "PERCENTAGE OF CLASS B ACHIEVED"
                Case "ACHIV_CLASS_C"
                    strFieldName = "PERCENTAGE OF CLASS C ACHIEVED"
                Case "ACHIV_CLASS_OTH"
                    strFieldName = "PERCENTAGE OF CLASS OTH ACHIEVED"
                Case "ACHIV_CLASS_TTL"
                    strFieldName = "PERCENTAGE OF TOTAL ACHIEVED"
                Case "AVG_CALL_CUST_CLASS_A"
                    strFieldName = "AVERAGE CALLS/CLASS A CUSTOMER"
                Case "AVG_CALL_CUST_CLASS_B"
                    strFieldName = "AVERAGE CALLS/CLASS B CUSTOMER"
                Case "AVG_CALL_CUST_CLASS_C"
                    strFieldName = "AVERAGE CALLS/CLASS C CUSTOMER"
                Case "AVG_CALL_CUST_CLASS_OTH"
                    strFieldName = "AVERAGE CALLS/CLASS OTH CUSTOMER"
                Case "AVG_CALL_CUST_CLASS_TTL"
                    strFieldName = "AVERAGE CALLS/CLASS TOTAL CUSTOMER"
                Case "DESC_CODE"
                    strFieldName = "Month"
                Case "AVGTOT"
                    strFieldName = "AVG/TOT"
                Case "TTL"
                    strFieldName = "Total"
                Case "CAP_CODE"
                    strFieldName = "Month"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)

                    If strFieldName = "&NBSP;" Then
                        strFieldName = ""
                    End If
            End Select

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
            Try
                Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

                If strColumnName Like "DESC_NAME" Then
                    FCT = FieldColumntype.InvisibleColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If
                Return FCT
            Catch ex As Exception

            End Try
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strFormatString As String = ""
            Try
                Select Case strColumnName.ToUpper
                    Case "DATE"
                        strFormatString = "{0:yyyy-MM-dd}"
                    Case "QTY"
                        strFormatString = "{0:#,0}"
                    Case Else
                        strFormatString = ""
                End Select
            Catch ex As Exception
            End Try

            Return strFormatString
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            Try
                With CS
                    Dim strColumnName As String = ColumnName.ToUpper
                    .FormatString = GetOutputFormatString(ColumnName)

                    If strColumnName Like "DESC_CODE" Then
                        .HorizontalAlign = HorizontalAlign.Left
                    Else
                        .HorizontalAlign = HorizontalAlign.Right
                    End If

                End With

            Catch ex As Exception

            End Try
            Return CS
        End Function

        Public Shared Function GetMonthName(ByVal iMonth As Integer) As String
            Dim strMonthName As String = ""

            Try
                Select Case iMonth
                    Case 1
                        strMonthName = "JAN"
                    Case 2
                        strMonthName = "FEB"
                    Case 3
                        strMonthName = "MAR"
                    Case 4
                        strMonthName = "APR"
                    Case 5
                        strMonthName = "MAY"
                    Case 6
                        strMonthName = "JUN"
                    Case 7
                        strMonthName = "JUL"
                    Case 8
                        strMonthName = "AUG"
                    Case 9
                        strMonthName = "SEP"
                    Case 10
                        strMonthName = "OCT"
                    Case 11
                        strMonthName = "NOV"
                    Case 12
                        strMonthName = "DEC"
                    Case Else
                        strMonthName = ""
                End Select
            Catch ex As Exception
            End Try

            Return strMonthName

        End Function
    End Class



    Protected Sub ddlSelectedPrdSpecialty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectedPrdSpecialty.SelectedIndexChanged
        Dim dtCurrenttable As Data.DataTable = Nothing 'CType(ViewState("dtCurrentView"), Data.DataTable)

        Try
            'If dtCurrenttable Is Nothing Then
            dtCurrenttable = GetRecListPTLCode()

            ' ViewState("dtCurrentView") = dtCurrenttable
            dgList.PageIndex = 0
            'End If
            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            dgList.DataSource = dvCurrentView
            dgList.PageSize = 26 'PageSize
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try


    End Sub
End Class
