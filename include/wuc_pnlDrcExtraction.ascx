<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_pnlDrcExtraction.ascx.vb" Inherits="include_menu_wuc_DrcExtraction" %>
<table id="tblCtrlPnlGen" cellspacing="0" cellpadding="0" width="98%" border="0"
    style="float: left; padding-left: 10px;">
    <tr align="center">
        <td>
            <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button"
                Width="100%" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="pnlDRCExtraction" runat="server" Width="98%">
                <asp:UpdatePanel ID="UpdatePnlEnquiry" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tr>
                                <td style="width: 45%;">
                                </td>
                                <td style="width: 10%;">
                                </td>
                                <td style="width: 45%;">
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                    <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" />
                                </td>
                                <td valign="middle" align="center">
                                    <div>
                                        <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" /></div>
                                    <div>
                                        <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                            Text="<<" /></div>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                    <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" />
                                </td>
                                <td valign="middle" align="center">
                                    <div>
                                        <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                            Text="<" /></div>
                                    <div>
                                        <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                            Text=">>" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                            Text="<<" /></div>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblPrdGrp" class="cls_label_header">Product Group</span><br>
                                    <asp:ListBox ID="lsbPrdGrp" CssClass="cls_listbox" runat="server" SelectionMode="Multiple">
                                    </asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <div>
                                        <asp:Button ID="lnkAddPrdGrp" runat="server" CssClass="cls_button" Width="35" Text=">" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemovePrdGrp" runat="server" CssClass="cls_button" Width="35"
                                            Text="<" /></div>
                                    <div>
                                        <asp:Button ID="lnkAddAllPrdGrp" runat="server" CssClass="cls_button" Width="35"
                                            Text=">>" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveAllPrdGrp" runat="server" CssClass="cls_button" Width="35"
                                            Text="<<" /></div>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedPrdGrp" class="cls_label_header">Selected Product Group</span><br>
                                    <asp:ListBox ID="lsbSelectedPrdGrp" CssClass="cls_listbox" runat="server" SelectionMode="Multiple">
                                    </asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblGrouping" class="cls_label_header">Grouping Field</span><br>
                                    <asp:ListBox ID="lstGrouping" CssClass="cls_listbox" runat="server" SelectionMode="Multiple">
                                    </asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <div>
                                        <asp:Button ID="lnkAddGrouping" runat="server" CssClass="cls_button" Width="35" Text=">" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveGrouping" runat="server" CssClass="cls_button" Width="35"
                                            Text="<" /></div>
                                    <div>
                                        <asp:Button ID="lnkAddAllGrouping" runat="server" CssClass="cls_button" Width="35"
                                            Text=">>" /></div>
                                    <div>
                                        <asp:Button ID="lnkRemoveAllGrouping" runat="server" CssClass="cls_button" Width="35"
                                            Text="<<" /></div>
                                    <div>
                                        <asp:Button ID="lnkUp" runat="server" CssClass="cls_button" Width="35" Text="Up" /></div>
                                    <div>
                                        <asp:Button ID="lnkDown" runat="server" CssClass="cls_button" Width="35" Text="Down" /></div>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedGrouping" class="cls_label_header">Selected Grouping Field</span><br>
                                    <asp:ListBox ID="lstSelectedGrouping" CssClass="cls_listbox" runat="server" SelectionMode="Multiple">
                                    </asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                    <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search">
                                    </asp:Button>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlDRCExtraction" runat="server" CollapseControlID="btnCollapse"
                ExpandControlID="btnCollapse" TargetControlID="pnlDRCExtraction" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </td>
    </tr>
</table>
