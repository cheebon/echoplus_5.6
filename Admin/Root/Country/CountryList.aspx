﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CountryList.aspx.vb" Inherits="Admin_Root_CountryList" StyleSheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Country List</title>
</head>

<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmCountry" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /><br />            
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    
        <div class="border_std">
        <asp:Button ID="btnCreate" runat="server" Text="Create" CausesValidation="False"
                                                    UseSubmitBehavior="false" Visible="true" /></div>
                                                    <br />
        <div class="border_std">                                            
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>                    
                    <!--Begin Modify Here -->
                    
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                        EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="COUNTRY_ID">
                        <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                        <EmptyDataTemplate>There is no data added.</EmptyDataTemplate>
                        <Columns>
                            <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image" HeaderText="Delete">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="Modify" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="COUNTRY_ID" HeaderText="Country ID" SortExpression="COUNTRY_ID" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="COUNTRY_CODE" HeaderText="Country Code" SortExpression="COUNTRY_CODE" />
                            <asp:BoundField DataField="COUNTRY_NAME" HeaderText="Country Name" SortExpression="COUNTRY_NAME" />
                            <asp:BoundField DataField="COUNTRY_DESC" HeaderText="Country Description" SortExpression="COUNTRY_DESC" />                       
                            
                        </Columns>
                    </ccGV:clsGridView>
                            
                    <!--End Modify Here -->
                </ContentTemplate>
            </asp:UpdatePanel>
         </div>
    </div>
    </form>
</body>
</html>
