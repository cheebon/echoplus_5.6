Imports System.Data

Partial Class CustomerPlanListDetail
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerPlanListDetail"
        End Get
    End Property
    Dim strCustCode, strSalesmanCode, strRouteDate As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

       
            'Call Header
            With wuc_lblheader
            .Title = "Route Plan History"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
            TimerControl1.Enabled = True
            End If
      
    End Sub
    Sub onpageload()

        'Put user code to initialize the page here
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            bindDropDownList(strSalesmanCode, strCustCode)
            bindGridviews()
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindDropDownList(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim obj As New txn_Customer.clsCustInfo
        Dim dt As DataTable
        Try
            dt = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                ddlDate.DataSource = dt
                ddlDate.DataTextField = "route_date"
                ddlDate.DataValueField = "route_date"
                ddlDate.DataBind()
            Else
                ddlDate.Items.Add("None")
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindDropDownList : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub bindGridviews()
        Dim intcount As Integer = 1
        Dim dt As DataTable = Nothing
        Dim dtAddField As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Dim drcurr1(), drcurr2(), drcurr3(), drcurr4(), drcurr5(), drcurr6(), drcurr7(), drcurr8() As DataRow
        Try
            strRouteDate = ddlDate.SelectedValue
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            If ddlDate.SelectedValue <> "None" Then
                dt = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "2", strRouteDate)
                If dt.Rows.Count > 0 Then
                    drcurr1 = dt.Select("DEPT_NAME<>''")
                    If drcurr1.Length > 0 Then
                        buildContDeptDG(dt, intcount)
                        intcount = intcount + 1
                    End If

                    drcurr2 = dt.Select("CONT_CODE<>''")
                    If drcurr2.Length > 0 Then
                        buildContactDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr3 = dt.Select("PRD_GRP_CODE<>''")
                    If drcurr3.Length > 0 Then
                        buildPrdGrpDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr4 = dt.Select("CAT_CODE<>'' AND SUB_CAT_CODE<>''")
                    If drcurr4.Length > 0 Then
                        buildSFMSDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr5 = dt.Select("TITLE_CODE<>''")
                    If drcurr5.Length > 0 Then
                        buildTitleDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr6 = dt.Select("CAMPAIGN_CODE<>''")
                    If drcurr6.Length > 0 Then
                        buildCampaignDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr7 = dt.Select("ACCT_CODE<>''")
                    If drcurr7.Length > 0 Then
                        buildActivityDG(intcount)
                        intcount = intcount + 1
                    End If

                    drcurr8 = dt.Select("ITI_REMARKS<>''")
                    If drcurr8.Length > 0 Then
                        buildRemarksDG(intcount, dt)
                        intcount = intcount + 1
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridviews() : " & ex.ToString)
        End Try
    End Sub

    Sub addGridviewPanel(ByVal intcount As Integer, ByVal dg As cor_CustomCtrl.clsGridView)
        Dim lbl As New Label
        'Dim lit As New Literal
        Try
            'lit.Text = "<br />"
            dg.Width = Unit.Pixel(300)
            lbl.CssClass = "cls_label_header"
            Select Case intcount
                Case 1
                    lbl.Text = "<br />Contact Department"
                    'pnl1.Controls.Add(lit)
                    pnl1.Controls.Add(lbl)
                    pnl1.Controls.Add(dg)
                    pnl1.Visible = True
                Case 2
                    lbl.Text = "<br />Contact"
                    'pnl1.Controls.Add(lit)
                    pnl2.Controls.Add(lbl)
                    pnl2.Controls.Add(dg)
                    pnl2.Visible = True
                Case 3
                    lbl.Text = "<br />Product Group"
                    'pnl1.Controls.Add(lit)
                    pnl3.Controls.Add(lbl)
                    pnl3.Controls.Add(dg)
                    pnl3.Visible = True
                Case 4
                    lbl.Text = "<br />SFMS"
                    'pnl1.Controls.Add(lit)
                    pnl4.Controls.Add(lbl)
                    pnl4.Controls.Add(dg)
                    pnl4.Visible = True
                Case 5
                    lbl.Text = "<br />MSS"
                    'pnl1.Controls.Add(lit)
                    pnl5.Controls.Add(lbl)
                    pnl5.Controls.Add(dg)
                    pnl5.Visible = True
                Case 6

                    lbl.Text = "<br />Promotion Campaign"
                    'pnl1.Controls.Add(lit)
                    pnl6.Controls.Add(lbl)
                    pnl6.Controls.Add(dg)
                    pnl6.Visible = True
                Case 7

                    lbl.Text = "<br />Other Activity"
                    'pnl1.Controls.Add(lit)
                    pnl7.Controls.Add(lbl)
                    pnl7.Controls.Add(dg)
                    pnl7.Visible = True
                Case 8

                    lbl.Text = "<br />Remarks"
                    'pnl1.Controls.Add(lit)
                    pnl8.Controls.Add(lbl)
                    pnl8.Controls.Add(dg)
                    pnl8.Visible = True
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".addGridviewPanel : " & ex.ToString)
        End Try
    End Sub

    Sub buildContDeptDG(ByVal dt As DataTable, ByVal intCount As Integer)
        Dim dbgDept As New BoundField
        Dim dv As New DataView(dt)
        Dim dg1 As New cor_CustomCtrl.clsGridView
        Try
            dg1.AutoGenerateColumns = False
            dbgDept.DataField = "cont_depart"
            dbgDept.HeaderText = "Contact Department"
            dg1.Columns.Add(dbgDept)
            dv.RowFilter = "cont_depart<>''"
            dg1.DataSource = dv
            dg1.DataBind()
            addGridviewPanel(intCount, dg1)

        Catch ex As Exception
            ExceptionMsg(PageName & ".buildContDeptDG : " & ex.ToString)
        End Try
    End Sub

    Sub buildContactDG(ByVal intcount As Integer)
        Dim dbgContCode As New BoundField
        Dim dbgContName As New BoundField
        Dim dg2 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg2.AutoGenerateColumns = False
            dbgContCode.DataField = "contact_cd"
            dbgContCode.HeaderText = "Contact Code"
            dg2.Columns.Add(dbgContCode)

            dbgContName.DataField = "contact_name"
            dbgContName.HeaderText = "Contact Name"
            dg2.Columns.Add(dbgContName)
            dtaddfield = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "3", strRouteDate)

            If dtaddfield.Rows.Count > 0 Then
                dg2.DataSource = dtaddfield
                dg2.DataBind()
                addGridviewPanel(intcount, dg2)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildContactDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildPrdGrpDG(ByVal intcount As Integer)
        Dim dbgPrdGrp As New BoundField
        Dim dbgProdDesc As New BoundField
        Dim dg3 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg3.AutoGenerateColumns = False
            dbgPrdGrp.DataField = "prdgrp_cd"
            dbgPrdGrp.HeaderText = "Product Group"
            dg3.Columns.Add(dbgPrdGrp)

            dbgProdDesc.DataField = "prod_desc"
            dbgProdDesc.HeaderText = "Product Group Desc"
            dg3.Columns.Add(dbgProdDesc)
            dtaddfield = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "4", strRouteDate)

            If dtaddfield.Rows.Count > 0 Then
                dg3.DataSource = dtaddfield
                dg3.DataBind()
                addGridviewPanel(intcount, dg3)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildPrdGrpDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildSFMSDG(ByVal intcount As Integer)
        Dim dbgCatCode As New BoundField
        Dim dbgSubCatCode As New BoundField
        Dim dg4 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg4.AutoGenerateColumns = False
            dbgCatCode.DataField = "cat_cd"
            dbgCatCode.HeaderText = "Category"
            dg4.Columns.Add(dbgCatCode)

            dbgSubCatCode.DataField = "sub_cat_cd"
            dbgSubCatCode.HeaderText = "Sub Category"
            dg4.Columns.Add(dbgSubCatCode)

            Dim dbgDesc As New BoundField
            dbgDesc.DataField = "sfms_desc"
            dbgDesc.HeaderText = "SFMS Desc"
            dg4.Columns.Add(dbgDesc)
            dtAddField = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "5", strRouteDate)

            If dtAddField.Rows.Count > 0 Then
                dg4.DataSource = dtaddfield
                dg4.DataBind()
                addGridviewPanel(intcount, dg4)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildSFMSDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildTitleDG(ByVal intcount As Integer)
        Dim dbgTitleCode As New BoundField
        Dim dbgTitleDesc As New BoundField
        Dim dg5 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg5.AutoGenerateColumns = False
            dbgTitleCode.DataField = "title_cd"
            dbgTitleCode.HeaderText = "Title Code"
            dg5.Columns.Add(dbgTitleCode)

            dbgTitleDesc.DataField = "title_desc"
            dbgTitleDesc.HeaderText = "MSS Title Desc"
            dg5.Columns.Add(dbgTitleDesc)

            dtaddfield = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "6", strRouteDate)

            If dtaddfield.Rows.Count > 0 Then
                dg5.DataSource = dtaddfield
                dg5.DataBind()
                addGridviewPanel(intcount, dg5)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildTitleDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildCampaignDG(ByVal intCount As Integer)
        Dim dbgCampaignCode As New BoundField
        Dim dbgPromoCampDesc As New BoundField
        Dim dbgAgencyDesc As New BoundField
        Dim dg6 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg6.AutoGenerateColumns = False
            dbgCampaignCode.DataField = "campaign_cd"
            dbgCampaignCode.HeaderText = "Campaign Code"
            dg6.Columns.Add(dbgCampaignCode)

            dbgPromoCampDesc.DataField = "campaign_title"
            dbgPromoCampDesc.HeaderText = "Promo Campaign Desc"
            dg6.Columns.Add(dbgPromoCampDesc)

            dbgAgencyDesc.DataField = "agency_desc"
            dbgAgencyDesc.HeaderText = "Agency Desc"
            dg6.Columns.Add(dbgAgencyDesc)

            dtaddfield = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "7", strRouteDate)

            If dtaddfield.Rows.Count > 0 Then
                dg6.DataSource = dtaddfield
                dg6.DataBind()
                addGridviewPanel(intCount, dg6)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildCampaignDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildActivityDG(ByVal intCount As Integer)
        Dim dbgActCode As New BoundField
        Dim dbgActDesc As New BoundField
        Dim dg7 As New cor_CustomCtrl.clsGridView
        Dim obj As New txn_Customer.clsCustInfo
        Dim dtaddfield As DataTable
        Try
            dg7.AutoGenerateColumns = False
            dbgActCode.DataField = "act_cd"
            dbgActCode.HeaderText = "Activities Code"
            dg7.Columns.Add(dbgActCode)

            dbgActDesc.DataField = "act_desc"
            dbgActDesc.HeaderText = "Activities Desc"
            dg7.Columns.Add(dbgActDesc)

            dtaddfield = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "8", strRouteDate)

            If dtaddfield.Rows.Count > 0 Then
                dg7.DataSource = dtaddfield
                dg7.DataBind()
                addGridviewPanel(intCount, dg7)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildActivityDG : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub buildRemarksDG(ByVal intCount As Integer, ByVal dt As DataTable)
        Dim dbgRemark As New BoundField
        Dim dg8 As New cor_CustomCtrl.clsGridView

        Try
            dg8.AutoGenerateColumns = False
            dbgRemark.DataField = "iti_remark"
            dbgRemark.HeaderText = "Remark"
            dg8.Columns.Add(dbgRemark)

            dg8.DataSource = dt
            dg8.DataBind()
            addGridviewPanel(intCount, dg8)
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildRemarksDG : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            clearPanels()
            bindGridviews()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlDate_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Sub clearPanels()
        Try
            pnl1.Controls.Clear()
            pnl2.Controls.Clear()
            pnl3.Controls.Clear()
            pnl4.Controls.Clear()
            pnl5.Controls.Clear()
            pnl6.Controls.Clear()
            pnl7.Controls.Clear()
            pnl8.Controls.Clear()
            pnl1.Visible = False
            pnl2.Visible = False
            pnl3.Visible = False
            pnl4.Visible = False
            pnl5.Visible = False
            pnl6.Visible = False
            pnl7.Visible = False
            pnl8.Visible = False

        Catch ex As Exception
            ExceptionMsg(PageName & ".clearPanels() : " & ex.ToString)
        End Try
    End Sub
End Class



