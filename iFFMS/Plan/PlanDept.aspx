<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanDept.aspx.vb" Inherits="iFFMS_Plan_PlanDept" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Addon DEPT</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglistNewDept');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('dglistSelectedDept');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
            function ValidatedglistNewDeptCheckBoxStates()
   { var dglist = document.getElementById('dglistNewDept');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
        function ValidatedglistSelectedDeptCheckBoxStates()
   { var dglist = document.getElementById('dglistSelectedDept');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('TopBar');HideElement('DetailBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')"
    class="BckgroundInsideContentLayout">
    <form id="frmplanaddondept" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Visible="true"
                            Width="80px" OnClientClick="HideElement('ContentBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')" />
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateSelectedDept" runat="server" UpdateMode="Conditional"
                            RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlSelectedDept" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlSelectedDept_Tick" />
                                <div id="SelectedDept" class="S_DivHeader">
                                    Assigned Department
                                </div>
                                <asp:Panel ID="pSelectedDept" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblSmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckalldelete" value="Check All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(true);" visible="false" style="width: 80px"
                                                validationgroup="deleteDept" runat="server" />
                                            <input type="Button" id="btnuncheckalldelete" value="Uncheck All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(false);" visible="false" style="width: 80px"
                                                validationgroup="deleteDept" runat="server" />
                                            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="cls_button" ValidationGroup="deleteDept"
                                                Visible="false" Width="80px"  OnClientClick="return ValidatedglistSelectedDeptCheckBoxStates()"/>
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistSelectedDept" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="DEPT_NAME">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                 There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DEPT_NAME" HeaderText="Department Name" ReadOnly="True" SortExpression="DEPT_NAME">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSelectedDept" runat="server" TargetControlID="pSelectedDept"
                                    ExpandControlID="SelectedDept" CollapseControlID="SelectedDept">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewDept" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlNewDept" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlNewDept_Tick" />
                                <div id="NewDept" class="S_DivHeader">
                                    Add New Department
                                </div>
                                <asp:Panel ID="PNewDept" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblNmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" validationgroup="newDept" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" validationgroup="newDept" runat="server" />
                                            <asp:Button ID="btnadd" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="newDept"
                                                Visible="false" Width="80px" OnClientClick="return ValidatedglistNewDeptCheckBoxStates()"/>
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistNewDept" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="DEPT_NAME">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DEPT_NAME" HeaderText="Contact code" ReadOnly="True" SortExpression="DEPT_NAME">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewDept" runat="server" TargetControlID="PNewDept"
                                    ExpandControlID="NewDept" CollapseControlID="NewDept">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
