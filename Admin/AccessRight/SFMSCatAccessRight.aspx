﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSCatAccessRight.aspx.vb" Inherits="Admin_AccessRight_SFMSCatAccessRight" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Easy Loader Access Right</title>
     <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        //dgList
        function ValidateCheckBoxStates() {
            var flag = false;
            $("#dgELList").find("input:checkbox[Id*=chkAccessRight]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStates(element) {
            $("#dgELList").find("input:checkbox[Id*=chkAccessRight]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgELList").find("input:checkbox[Id*=chkAccessRight]").each(function() { if (this.checked == false) flag = false; });
            $("#dgELList").find("input:checkbox[Id*=AllCheckBox]").attr('checked', flag);
        }
        //dgList
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->  
<body class="BckgroundInsideContentLayout">
    <form id="frmEasyLoaderAccessRight" runat="server">
    <div>
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="95%" border="0" ALIGN="center">
            <tr><td>&nbsp;</td></tr>
            <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
            <tr>
                <td>
                    <asp:Label ID="lblELName" runat="server" Text="Easy Loader Name :" CssClass="cls_label" ></asp:Label>
                    <asp:TextBox ID="txtELName" runat="server" CssClass="cls_textbox" ></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" />
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>			    
                <td class="BckgroundInsideContentLayout">
                    <ccGV:clsGridView ID="dgELList" runat="server" ShowFooter="false" AllowSorting="True"
                    AutoGenerateColumns="False" Width="100%" FreezeHeader="True" GridHeight="100%"
                    AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                    FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="EL_ID, ACCESSRIGHT">
                         <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="AllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                </HeaderTemplate>
                                <HeaderStyle Width="5%" HorizontalAlign="Left" ></HeaderStyle>
			                    <ItemTemplate>
				                    <asp:CheckBox ID="chkAccessRight" Runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);"></asp:CheckBox>
			                    </ItemTemplate>
                            </asp:TemplateField>                                                              
                            <asp:BoundField DataField="EL_NAME" HeaderText="Easy Loader Name">
                                <HeaderStyle Width="45%"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="EL_DESC" HeaderText="Description">
                                <HeaderStyle Width="50%"></HeaderStyle>
                            </asp:BoundField>
                         </Columns>                                                
                    </ccGV:clsGridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:TextBox id="txtAccessRightID" runat="server" CssClass="cls_textbox" Visible="false"></asp:TextBox>
                    <asp:button id="btnSave" runat="server" CssClass="cls_button" Width="72px" Text="Save" CausesValidation="False"></asp:button>
					<asp:button id="btnCancel" runat="server" CssClass="cls_button" Width="72px" Text="Cancel" CausesValidation="False"></asp:button>
				</td>
            </tr>
        </TABLE>    
    </div>
    </form>
</body>
</html>
