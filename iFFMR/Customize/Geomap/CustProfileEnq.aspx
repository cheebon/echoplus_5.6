﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustProfileEnq.aspx.vb"
    Inherits="iFFMR_Geomap_CustProfileEnqV2" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/MapIconMaker/mapiconmaker.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
    <script type='text/javascript'>
        /// Parameters to play with
        /// Currently hard code color first. 
        var ClassA = "#bc0000";
        var ClassB = "#345bec";
        var ClassC = "#c88c18";
        var ClassD = "#3bba04";
        var Unclassified = "#999a9d";

        //Global variables
        var map;
        var CustName, Address, District, CustGrpName, CustClass, CustType,
            UserID, PrincipalID, PrincipalCode, NetValue, tDate, MtdStart, MtdEnd, YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode;
        var root; //store app root path 
        var Default_Latitude, Default_Longitude, Default_Zoomlevel;

        //Flag Variable
        var CustNameFlag;
        var ContNameFlag;
        var AddressFlag;
        var DistrictFlag;
        var CustGrpFlag;
        var CustClassFlag;
        var CustTypeFlag;
        var MTDFlag;
        var YTDFlag;
        var SKUFlag;
        var IMGFlag;
        var CreditLimitFlag;
        var OutBalFlag;

        var ValidCoordinates = []; //Store valid coordinate

        /***********************************************************
        / Initialiazer
        /
        ************************************************************/
        function Initialize() {

            if (GBrowserIsCompatible()) {
                map = new GMap2(document.getElementById('map_canvas'));

                QueryLocation();

                AddControl();


            }
        }

        function AddControl() {
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
            map.enableScrollWheelZoom();
        }

        function QueryLocation() {
            //map.setCenter(new GLatLng(25.324167, 109.160156), 3);
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {


                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;


                    SetMapDefaultPoint();

                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new GLatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));
            }
        }
        /***********************************************************
        / Method to search customer.
        /
        ************************************************************/
        function SearchCustomerLocation() {
            CustNameFlag = $('#chkCustName').is(':checked') ? 1 : 0;
            ContNameFlag = $('#chkContName').is(':checked') ? 1 : 0;
            AddressFlag = $('#chkAddress').is(':checked') ? 1 : 0;
            DistrictFlag = $('#chkDistrict').is(':checked') ? 1 : 0;
            CustGrpFlag = $('#chkCustGrp').is(':checked') ? 1 : 0;
            CustClassFlag = $('#chkCustClass').is(':checked') ? 1 : 0;
            CustTypeFlag = $('#chkCustType').is(':checked') ? 1 : 0;
            MTDFlag = $('#chkMTD').is(':checked') ? 1 : 0;
            YTDFlag = $('#chkYTD').is(':checked') ? 1 : 0;
            SKUFlag = $('#chkSKU').is(':checked') ? 1 : 0;
            IMGFlag = $('#chkImg').is(':checked') ? 1 : 0;
            CreditLimitFlag = $('#chkCreditLimit').is(':checked') ? 1 : 0;
            OutBalFlag = $('#chkOutBal').is(':checked') ? 1 : 0;

            ws_CustLoc.ReturnCustomerProfile(CustName, Address, District, CustGrpName, CustClass, CustType, NetValue, PrincipalID, PrincipalCode,
                                tDate, CustNameFlag, AddressFlag, DistrictFlag, CustGrpFlag, CustClassFlag, CustTypeFlag,
                                MTDFlag, YTDFlag, SKUFlag, IMGFlag, MtdStart, MtdEnd, YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode, onSuccessLocateCustomerProfile, onFailLocateCustomerProfile);
            ShowMessage("Loading map data.");
        }

        function onSuccessLocateCustomerProfile(result) {
            if (result) {
                if (result.Total > 0) {
                    HideMessage();
                    PlotMap(result);
                    SetCenterMap();
                }
                else //Shit happens
                {

                    ShowMessage("No Record Returned.");

                    QueryLocation();
                }
            }
        }

        function onFailLocateCustomerProfile(ex) {
            //If webservice call fail
            ShowMessage("Error occurred when retrieving data." + ex.get_message());
        }

        function PlotMap(result) {
            var count = 0;

            map.clearOverlays();
            WriteRecordCount(count);

            for (var i = 0; i < result.Total; ++i) {
                //Check coordinate contains value
                if ((result.Rows[i].LATITUDE != undefined && result.Rows[i].LATITUDE != "") && (result.Rows[i].LONGITUDE != undefined && result.Rows[i].LONGITUDE != "")) {
                    //check is numeric
                    if (!isNaN(result.Rows[i].LATITUDE) && !isNaN(result.Rows[i].LONGITUDE)) {
                        var icon = CreateIcon(result.Rows[i].CUST_CLASS);
                        var marker = CreateMarker(result.Rows[i].CUST_CODE, result.Rows[i].CUST_NAME, result.Rows[i].CONT_NAME,
                                                  result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE,
                                                  result.Rows[i].ADDRESS, result.Rows[i].DISTRICT,
                                                  result.Rows[i].CUST_GRP_NAME, result.Rows[i].CUST_TYPE,
                                                  result.Rows[i].MTD_SALES, result.Rows[i].YTD_SALES,
                                                  result.Rows[i].NO_SKU, result.Rows[i].IMG_LOC,
                                                  result.Rows[i].CUST_CLASS, result.Rows[i].CREDITLIMIT, result.Rows[i].OUTBAL, icon);

                        map.addOverlay(marker);
                        WriteRecordCount(++count); //Display records plot on map

                        ValidCoordinates.push(new GLatLng(result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE));
                    }
                    else
                    { ShowMessage("Certain customers have invalid coordinates."); }
                }
                else
                { ShowMessage("Certain customers does not have coordinates information."); }
            }
        }

        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateIcon(Class) {
            var Icon;

            switch (Class) {
                case "A":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassA, label: 'A' });
                    break;
                case "B":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassB, label: 'B' });
                    break;
                case "C":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassC, label: 'C' });
                    break;
                case "D":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassD, label: 'D' });
                    break;
                default:
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: Unclassified, label: 'O' });
                    break;
            }

            return Icon;
        }

        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateMarker(CustCode, CustName, ContName, Latitude, Longitude,
                            Address, District, CustGrpName, CustType,
                            MTD_Sales, YTD_Sales, No_SKU, ImgLoc, Class, CreditLimit, OutBal, Icon) {
            var marker = new GMarker(new GLatLng(Latitude, Longitude), Icon);

            GEvent.addListener(marker, "click", function () {
                var str = "<div class=\"cls_label\">";

                if (CustNameFlag == 1) str += "<span class='map_header_text'>Customer:</span> " + CustName + "<br />";
                if (ContNameFlag == 1) str += "<span class='map_header_text'>Contact:</span> " + ContName + "<br />";
                if (AddressFlag == 1) str += "<span class='map_header_text'>Address:</span> " + Address + "<br />";
                if (DistrictFlag == 1) str += "<span class='map_header_text'>District:</span> " + District + "<br />";
                if (CustGrpFlag == 1) str += "<span class='map_header_text'>Customer Group:</span> " + CustGrpName + "<br />";
                if (CustClassFlag == 1) str += "<span class='map_header_text'>Customer Class:</span> " + GetClassName(Class) + "<br />";
                if (CustTypeFlag == 1) str += "<span class='map_header_text'>Customer Type:</span> " + CustType + "<br />";
                if (CreditLimitFlag == 1) str += "<span class='map_header_text'>Customer Credit Limit:</span> " + CreditLimit + "<br />";
                if (OutBalFlag == 1) str += "<span class='map_header_text'>Customer Outstanding Balance:</span> " + OutBal + "<br />";
                if (MTDFlag == 1) str += "<span class='map_header_text'>MTD Sales:</span> " + MTD_Sales + "<br />";
                if (YTDFlag == 1) str += "<span class='map_header_text'>YTD Sales:</span> " + YTD_Sales + "<br />";
                if (SKUFlag == 1) str += "<span class='map_header_text'>No of SKU:</span> " + No_SKU + "<br />";
                if (IMGFlag == 1) str += "<a href='#' onclick='PopImage(\"" + root + "" + ImgLoc + "\", \"" + CustName + "\", \"" + Address + "\", \"" + CustCode + "\");'>Outlet Photo</a>";




                str += "</div>";

                /// Eye Candy Code. Pan marker to center then only show InfoWindow
                /// Just calling panning and InfoWindow sequentially cannot achieve such effects
                var moveEnd = GEvent.addListener(map, "moveend", function () {
                    marker.openInfoWindowHtml(str);
                    GEvent.removeListener(moveEnd);
                });

                map.panTo(marker.getLatLng());
                //////////////////////////////////////////////////////////////////////////////////
            }
            );

            return marker;
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        function SetCenterMap() {
            if (ValidCoordinates != undefined && ValidCoordinates.length > 0) {
                var iCenter = parseInt(ValidCoordinates.length / 2);

                map.setCenter(ValidCoordinates[iCenter], 12);
            }
        }

        /***********************************************
        / Pop Map and checkresize
        ***********************************************/

        function WrapModalPopup(divTarget, divMask, action, fxCallback) {
            var dglist = document.getElementById("div_dgList");
            $("#hfScrollVal").val(dglist.scrollTop);
            dglist.scrollTop = 0;

            mp_ToggleModalPopup('pnlMapPopUp', 'divModalMask', 'on', SearchCustomerLocation);
            $("#config").hide();
            map.checkResize();
            SetMapDefaultPoint();

        }
        function RestoreScrollTop() {
            //var dglist = document.getElementById("div_dgList");
            //dglist.scrollTop = $("#hfScrollVal").val();
        }
        /***********************************************
        / Show cust image
        ***********************************************/
        function PopImage(ImgLoc, CustName, Address, CustCode) {
            var dglist = document.getElementById("div_dgList");
            $("#hfScrollVal").val(dglist.scrollTop);
            dglist.scrollTop = 0;

            //            mp_ToggleModalPopup("pnlPictureDiv", "divModalMask2", "on"); 


            BindCust(CustName, Address, CustCode);
            LoadImage(ImgLoc);
        }
        function BindCust(CustName, Address, CustCode) {

            $("#hfPicCustCode").val(CustCode);
            $("#hfPicCustName").val(CustName);
            $("#hfPicCustAdd").val(Address);

        }
        function LoadImage(ImgLoc) {
            //$("#CustImg").error(Image_OnErrorHide);

            document.all('btnRefreshPic').click();

            //            resetCustImg();
            //            
            //            $("#CustImg").attr({
            //                src: ImgLoc,
            //                title: "",
            //                alt: "Customer Image"
            //            });
        }
        /***********************************************
        / Hide broken image
        ***********************************************/
        function Image_OnErrorHide() {
            $("#CustImg").hide();
            $("#ImgLoadFailMsg").show();
        }
        function resetCustImg() {
            $("#CustImg").show();
            $("#ImgLoadFailMsg").hide();

            $("#CustImg").attr({
                src: "../../images/indicator.gif",
                title: "",
                alt: "Loading"
            });

            $("#PicDiv_CustName").text("");
            $("#PicDiv_Address").text("");
        }


        $(document).ready(function () {
            ShowVisit();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {

                ShowVisit();

            }
        });

        function ShowVisit() {
            var value = $('#hfVisitStatus').val()
            if (value == "") { $('#Visit').hide(); }
            else {
                if (value == "Show") { $('#Visit').show(); }
                else { $('#Visit').hide(); };
            };
        }

        function SetVisit(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfVisitStatus').val('Hide'); }
            else {
                var value = $('#hfVisitStatus').val();
                if (value == 'Hide') { $('#hfVisitStatus').val('Show'); }
                else { $('#hfVisitStatus').val('Hide'); }
            }
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
            this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
            return this;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="Initialize()" onunload="GUnload()">
    <form id="frmCustProfileEnq" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/DataServices/ws_CustLoc.asmx" />
            <asp:ServiceReference Path="~/DataServices/ws_PrincipalLoc.asmx" />
        </Services>
    </ajaxToolkit:ToolkitScriptManager>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                        <tr>
                            <td>
                                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <%--<asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />--%>
                                        <%--<span style="float:left; width:100%;">--%>
                                        <%-- <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">--%>
                                        <%--<ContentTemplate>--%>
                                        <asp:Panel ID="pnlList" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <center>
                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                Width="98%" FreezeHeader="True" AddEmptyHeaders="0" CellPadding="2" CssClass="Grid"
                                                                EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="" AllowPaging="True"
                                                                DataKeyNames="CUST_CODE" BorderColor="Black" BorderWidth="1" GridBorderColor="Black"
                                                                GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                <FooterStyle CssClass="GridFooter" />
                                                                <HeaderStyle CssClass="GridHeader" />
                                                                <PagerSettings Visible="False" />
                                                                <RowStyle CssClass="GridNormal" />
                                                                <EmptyDataTemplate>
                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                </EmptyDataTemplate>
                                                            </ccGV:clsGridView>
                                                        </center>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--</ContentTemplate>--%>
                                        <%--</ajaxToolkit:TabPanel> 
                                              </ajaxToolkit:TabContainer>--%>
                                        <%--</span>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td style="height: 5px">
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <%--<div id="pnlMapPopUp" class="modalPopup" style="z-index:9000;display: none; padding:10px; width:700px; height:500px; overflow:auto; position:absolute;">--%>
    <div id="pnlMapPopUp" class="modalPopup" style="z-index: 9000; display: none; padding: 10px;
        width: 90%; height: 500px; overflow: auto; position: absolute;">
        <%--<input id="btnClose" type="button" value="Back" class="cls_button" OnClick="mp_ToggleModalPopup('pnlMapPopUp','divModalMask', 'off');" />--%>
        <input id="btnConfig" type="button" value="Config" class="cls_button" onclick="mc_toggleCollapsible('#config');"
            style="float: left" />
        <input id="btnRefresh" type="button" value="Refresh" class="cls_button" onclick="SearchCustomerLocation();"
            style="float: left; margin-left: 3px" />
        <img alt="Close" src="../../../images/ico_close.gif" onclick="RestoreScrollTop();mp_ToggleModalPopup('pnlMapPopUp','divModalMask', 'off');"
            style="float: right; margin-right: 5px" />
        <br />
        <div id="config" style="display: none; width: 300px; background-color: #e8e8e8">
            <table border="0" cellpadding="3" cellspacing="0" class="cls_label">
                <tr>
                    <td style="width: 100px">
                        Customer:
                    </td>
                    <td style="width: 50px">
                        <input id="chkCustName" type="checkbox" checked="checked" />
                    </td>
                    <td style="width: 100px">
                        MTD:
                    </td>
                    <td style="width: 50px">
                        <input id="chkMTD" type="checkbox" checked />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Contact:
                    </td>
                    <td style="width: 50px">
                        <input id="chkContName" type="checkbox" checked="checked" />
                    </td>
                    <td>
                        YTD:
                    </td>
                    <td>
                        <input id="chkYTD" type="checkbox" checked="checked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Address:
                    </td>
                    <td>
                        <input id="chkAddress" type="checkbox" />
                    </td>
                    <td>
                        No of SKU:
                    </td>
                    <td>
                        <input id="chkSKU" type="checkbox" checked="checked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        District:
                    </td>
                    <td>
                        <input id="chkDistrict" type="checkbox" />
                    </td>
                    <td>
                        Image:
                    </td>
                    <td>
                        <input id="chkImg" type="checkbox" checked="checked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Customer Group:
                    </td>
                    <td>
                        <input id="chkCustGrp" type="checkbox" />
                    </td>
                    <td>
                        Credit Limit:
                    </td>
                    <td>
                        <input id="chkCreditLimit" type="checkbox" checked="checked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Customer Class:
                    </td>
                    <td>
                        <input id="chkCustClass" type="checkbox" checked="checked" />
                    </td>
                    <td>
                        Outstanding Balance:
                    </td>
                    <td>
                        <input id="chkOutBal" type="checkbox" checked="checked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Customer Type:
                    </td>
                    <td>
                        <input id="chkCustType" type="checkbox" checked="checked" />
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="position: relative">
            <%--<div style="width:643px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> customers plot on map.</span></div>
            <div id="map_canvas" style="width: 650px; height: 450px; padding: 0px 0px 0px 0px;z-index:20">--%>
            <div style="width: 97%; background-color: #e3e3e3; padding-left: 5px; border-left: solid 1px #989898;
                border-right: solid 1px #989898; border-top: solid 1px #989898">
                <span class="cls_label" id="spnCount" style="font-weight: bold">0</span><span class="cls_label"
                    style="font-weight: bold"> customers plot on map.</span></div>
            <div id="map_canvas" style="width: 98%; height: 450px; padding: 0px 0px 0px 0px;
                z-index: 20">
                <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
                <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>"
                    type="text/javascript"></script>
            </div>
            <div id="Message" class="cls_label" style="display: none; z-index: 30; position: absolute;
                padding: 3px; background: #3C3C3C; color: #ffffff">
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfPicCustCode" runat="server" />
    <asp:HiddenField ID="hfPicCustName" runat="server" />
    <asp:HiddenField ID="hfPicCustAdd" runat="server" />
    <asp:UpdatePanel ID="UpdateVisit" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hfVisitStatus" runat="server" Value="Hide" />
            <div id="Visit" style="width: 50%; float: right; position: absolute;display:none;z-index:10000;" class="modalPopup">
                <div class="shadow">
                    <div class="content" style="width: 100%; height: 100%;">
                        <fieldset>
                            <div id="VisitPnl" style="width: 100%;">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <img src="../../../images/ico_down.jpg" alt='' id='img1' />
                                            <asp:Label ID="lblVistCustDesc" runat="server" Text="Photo" CssClass="cls_label_header"
                                                Style="color: Black"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <img alt="Close" src="../../../images/ico_close.gif" onclick="SetVisit('ForceHide');ShowVisit();"
                                                height="25px" width="25px" style="cursor: pointer;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="ContainerDivBlue">
                                <div id="VisitContent" style="width: 100%;">
                                    <asp:HiddenField ID="hfVisitFilter" runat="server" />
                                    <div id="dVisitFilter" style="width: 98%;">
                                        <%--<div id="pnlPictureDiv" class="modalPopup" style="z-index: 9001; display: none; padding: 10px;
                                            width: 90%; height: 500px; overflow: auto; position: absolute;">--%>
                                        <%--<input id="btnClosePciture" type="button" value="Back" class="cls_button" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" />--%>
                                        <%--  <img alt="Close" src="../../../images/ico_close.gif" onclick="RestoreScrollTop();mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');"
                                                style="float: right; margin-right: 5px" />--%>
                                        <%--   <br />--%>
                                        <span class="cls_label_header">Customer Code: </span>
                                        <%--<span id="PicDiv_CustCode" class="cls_label"> 
                                            </span>--%>
                                        <asp:Label ID="lblCustCode" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                        <br />
                                        <span class="cls_label_header">Customer: </span><%--<span id="PicDiv_CustName" class="cls_label">
                                        </span>--%><asp:Label ID="lblCustName" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                        <br />
                                        <span class="cls_label_header">Address: </span><%--<span id="PicDiv_Address" class="cls_label">
                                        </span>--%><asp:Label ID="lblAddress" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                        <br />
                                        <br />
                                        <asp:Button ID="btnRefreshPic" runat="server" Text="Refresh Pic" style="display:none" />
                                        <asp:Label ID="lblImgLoadFailMsg" runat="server" Text=" Unable to load customer picture. This can be due to no image has been uploaded for
                                                this customer, or the image file has been removed." CssClass="cls_label" Visible="false" /> 
                                        <%-- <img id="CustImg" src="../../../images/indicator.gif" alt='Loading' onerror='Image_OnErrorHide();' />--%>
                                        <asp:Repeater ID="rpCust" runat="server">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfCustCode" runat="server" Value='<%# Eval("CUST_CODE") %>' />
                                                <asp:HiddenField ID="hfCustName" runat="server" Value='<%# Eval("CUST_NAME") %>' />
                                                <asp:HiddenField ID="hfImgLoc" runat="server" Value='<%# Eval("IMG_LOC") %>' />
                                                <asp:Image ID="imgCust" runat="server" ImageUrl="../../../images/indicator.gif"   
                                                    Height="150px" />
                                                </td> </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%--</div>--%>
                                    </div>
                                    <asp:HiddenField ID="hfVisit" runat="server" />
                                </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divModalMask" style="position: absolute; left: 0; top: 0; z-index: 8000;
        display: none; background-color: #fff">
    </div>
    <div id="divModalMask2" style="position: absolute; left: 0; top: 0; z-index: 8000;
        display: none; background-color: #fff">
    </div>
    <asp:HiddenField ID="hfScrollVal" runat="server" />
    </form>
</body>
</html>
