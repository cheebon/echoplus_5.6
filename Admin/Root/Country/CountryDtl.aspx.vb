﻿Option Explicit On

Imports System.Data

Partial Class Admin_Root_CountryDtl
    Inherits System.Web.UI.Page

#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property CountryID() As String
        Get
            Return ViewState("COUNTRY_ID")
        End Get
        Set(ByVal value As String)
            ViewState("COUNTRY_ID") = value
        End Set
    End Property
    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Country"
            If Not IsPostBack Then
                Dim strCountryId As String
                strCountryId = Trim(Request.QueryString("COUNTRY_ID"))
                If IsNothing(strCountryId) Or strCountryId = "" Then
                    CountryID = 0
                    'btnBack.Visible = False
                Else
                    CountryID = strCountryId
                End If

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    Case "CREATE"
                        ChangeMode(MODE.Create)
                    Case "EDIT"
                        ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DTL Button Function Handling"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditDtl.Click
        ChangeMode(MODE.Edit)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("CountryList.aspx", False)
    End Sub

    Protected Sub btnSaveDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDtl.Click
        If ActionMode = MODE.Create Then
            dvCountry.InsertItem(True)
        Else
            dvCountry.UpdateItem(True)
        End If
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDtl.Click
        If ActionMode = MODE.Create Then
            ActionMode = MODE.Normal
            btnBack_Click(sender, Nothing)
        Else
            ChangeMode(MODE.Normal)
        End If
    End Sub
#End Region
#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dvCountry.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dvCountry.DataBind()

            'Call Paging


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsCountry
        Try

            DT = obj.CountryDtl(CountryID, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            Case MODE.Create
                dvCountry.ChangeMode(DetailsViewMode.Insert)
                ActionMode = MODE.Create
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
            Case MODE.Edit
                dvCountry.ChangeMode(DetailsViewMode.Edit)
                ActionMode = MODE.Edit
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
            Case Else 'Normal
                dvCountry.ChangeMode(DetailsViewMode.ReadOnly)
                ActionMode = MODE.Normal
                btnEditDtl.Visible = True
                btnSaveDtl.Visible = False
                btnCancelDtl.Visible = False
        End Select

        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dvCountry_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvCountry.ItemInserting
        Dim obj As New adm_SuperAdmin.clsCountry
        Dim dt As DataTable
        Dim CountryCode, CountryName, CountryName1, CountryDesc, CountryDesc1 As String
        Try
            CountryCode = CType(dvCountry.Rows(0).FindControl("txtCountryCode"), TextBox).Text.Trim
            CountryName = CType(dvCountry.Rows(0).FindControl("txtCountryName"), TextBox).Text.Trim
            CountryName1 = CType(dvCountry.Rows(0).FindControl("txtCountryName1"), TextBox).Text.Trim
            CountryDesc = CType(dvCountry.Rows(0).FindControl("txtCountryDesc"), TextBox).Text.Trim
            CountryDesc1 = CType(dvCountry.Rows(0).FindControl("txtCountryDesc1"), TextBox).Text.Trim
            dt = obj.Create(CountryCode, CountryName, CountryName1, CountryDesc, CountryDesc1, Session("UserID"))

            If dt Is Nothing OrElse dt.Rows.Count = 0 OrElse dt.Rows(0)("isduplicate").ToString = "1" Then
                lblErrDtl.Text = "Country Code already exists."
                Return
            End If

            CountryID = dt.Rows(0)("COUNTRY_ID").ToString
            ChangeMode(MODE.Normal)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dvCountry_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvCountry.ItemUpdating
        Dim obj As New adm_SuperAdmin.clsCountry
        Dim CountryCode, CountryName, CountryName1, CountryDesc, CountryDesc1 As String
        Try
            If Not CountryID Is Nothing Or CountryID <> "" Then
                CountryCode = CType(dvCountry.Rows(0).FindControl("txtCountryCode"), TextBox).Text.Trim
                CountryName = CType(dvCountry.Rows(0).FindControl("txtCountryName"), TextBox).Text.Trim
                CountryName1 = CType(dvCountry.Rows(0).FindControl("txtCountryName1"), TextBox).Text.Trim
                CountryDesc = CType(dvCountry.Rows(0).FindControl("txtCountryDesc"), TextBox).Text.Trim
                CountryDesc1 = CType(dvCountry.Rows(0).FindControl("txtCountryDesc1"), TextBox).Text.Trim

                obj.Update(CountryID, CountryCode, CountryName, CountryName1, CountryDesc, CountryDesc1, Session("UserID"))

                ChangeMode(MODE.Normal)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
