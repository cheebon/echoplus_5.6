﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_dgpaging_v7.ascx.vb" Inherits="Echoplus.wuc_dgpaging_v7" %>
<asp:Panel ID="Panel1" runat="server" DefaultButton="btnGo">
<table id="tbl1" cellspacing="0" cellpadding="0" class="cls_table_pager">
    <tr>
        <td align="right">
            <table id="tbl2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td width="70%" align="left">
                       <div id="RecordCount" runat="server" visible="false"  ><span class="cls_label_header">Record(s) found:</span> 
                       <asp:Label ID="lblRecordCount" runat="server" CssClass="cls_label_header" Text="" /></div>
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblPageSize" runat="server" CssClass ="cls_label_header" Visible="false">Page Size</asp:Label>
                        <asp:DropDownList ID="ddlPageSize" runat="server" CssClass ="cls_dropdownlist" AutoPostBack ="true" Visible="false">
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                            <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td valign="top" nowrap>
                        <asp:Label ID="lblPage" runat="server" CssClass="cls_label_header">Page</asp:Label>&nbsp;
                        <asp:Label ID="lblColon1" runat="server" CssClass="cls_label_header">:</asp:Label>&nbsp;
                        <asp:Label ID="lblTotPage" runat="server" CssClass="cls_label">1 of 2</asp:Label>
                    </td>
                    <td align="center" nowrap>
                        <asp:ImageButton ID="imgPrevious" runat="server" ImageUrl="~/images/ico_previous.gif">
                        </asp:ImageButton>
                        <asp:LinkButton ID="lnkPrevious" runat="server">Prev</asp:LinkButton>&nbsp;
                        <asp:LinkButton ID="lnkNext" runat="server">Next</asp:LinkButton>
                        <asp:ImageButton ID="imgNext" runat="server" ImageUrl="~/images/ico_next.gif"></asp:ImageButton>

                        <%--<script><asp:Literal ID="ltlMsg" runat="server"></asp:Literal></script>--%>

                    </td>
                    <td valign="top" align="right" nowrap style="width: 1%">
                        <asp:Label ID="lblGotoPage" runat="server" CssClass="cls_label_header">Goto</asp:Label>&nbsp;</td>
                    <td valign="top" align="left">
                        <asp:TextBox ID="txtPageNo" runat="server" MaxLength="4" Width="38px" CssClass="cls_textbox"></asp:TextBox></td>
                    <td valign="top" nowrap>
                        <asp:Button ID="btnGo" runat="server" CssClass="cls_button" Text="Go"></asp:Button></td>
                </tr>
                <asp:TextBox ID="txtShowMsgViewerPath" Visible="False" CssClass="cls_textbox" runat="server"></asp:TextBox>
            </table>
        </td>
    </tr>
</table>

</asp:Panel>