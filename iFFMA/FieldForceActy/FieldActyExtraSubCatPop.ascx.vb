Imports System.Data

Partial Class iFFMA_FieldForceActy_FieldActyExtraSubCatPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property CatCode() As String
        Get
            Return Trim(hdCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdCatCode.Value = value
        End Set
    End Property

    Public Property SubCatCode() As String
        Get
            Return Trim(hdSubCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubCatCode.Value = value
        End Set
    End Property

    Public Property ExtraCatCode() As String
        Get
            Return Trim(hdExtraCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdExtraCatCode.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        'SubCatID = ""
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy

            DT = clsFieldActy.GetExtraCatDetails(TeamCode, CatCode, SubCatCode, ExtraCatCode)
            If DT.Rows.Count > 0 Then
                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Show()


            'FieldActyExtraSubCatList1.TeamCode = TeamCode
            'FieldActyExtraSubCatList1.CatCode = CatCode
            'FieldActyExtraSubCatList1.SubCatCode = SubCatCode

            'FieldActyExtraSubCatList1.BindExtrCatCodeList()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

          

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate("SaveExtraSubCat")
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtExtraSubCatCode As TextBox = CType(DetailsView1.FindControl("txtExtraSubCatCode"), TextBox)
            Dim lblExtraSubCatCode As Label = CType(DetailsView1.FindControl("lblExtraSubCatCode"), Label)
            Dim txtExtraSubCatName As TextBox = CType(DetailsView1.FindControl("txtExtraSubCatName"), TextBox)
            Dim chkStatus As CheckBox = CType(DetailsView1.FindControl("chkStatus"), CheckBox)
            Dim chkImplemeted As CheckBox = CType(DetailsView1.FindControl("chkImplemeted"), CheckBox)
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            Dim strStatus As String = If(chkStatus.Checked, "A", "D")
            Dim strImplemeted As String = If(chkImplemeted.Checked, "YES", "NO")
            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsFieldActy.UpdateExtraSubCat(TeamCode, CatCode, SubCatCode, Trim(lblExtraSubCatCode.Text), Trim(txtExtraSubCatName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strStatus, strImplemeted)
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim strIsDeleted As String = ""

                DT = clsFieldActy.CreateExtraSubCat(TeamCode, CatCode, SubCatCode, Trim(txtExtraSubCatCode.Text), Trim(txtExtraSubCatName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strStatus, strImplemeted)
                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Extra Category Code already exists!"
                    ExtraCatCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Extra Category Code is used before, please create a new one!"
                    ExtraCatCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    ExtraCatCode = Trim(txtExtraSubCatCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    ExtraCatCode = Trim(txtExtraSubCatCode.Text)
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view

            'ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    Try
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub txtDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.Load
    '    If SubCatID <> "" Then
    '        Show()
    '    End If
    'End Sub

    Protected Sub imgCal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.imgCal_Click
        Show()
    End Sub


End Class



