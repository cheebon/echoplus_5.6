﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SalesteamSfeKpiPop.ascx.vb" Inherits="iFFMA_Salesteam_SalesteamSfeKpiPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; /*width: 98%*/">
                <fieldset style="padding-left: 10px; width: 100%; box-sizing: border-box;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />

                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" Width="100%"
                        BorderStyle="None" BorderWidth="0px" CellPadding="2" CellSpacing="1"
                        DataKeyNames="SFE_KPI_ID"
                        OnModeChanging="DetailsView1_ModeChanging">
                        <FieldHeaderStyle VerticalAlign="Middle" HorizontalAlign="Left" CssClass="cls_label_header" Width="30%" Wrap="False" />
                        <Fields>
                            <asp:TemplateField HeaderText="SFE KPI">
                                <EditItemTemplate>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlSfeKpi" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <span class="cls_label_header">:</span>
                                    <asp:Label ID="txtDashboardXFieldDesc" runat="server" Text='<%# Bind("SFE_KPI_DESC") %>' CssClass="cls_label" />
                                </ItemTemplate>
                                <InsertItemTemplate>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlSfeKpi" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                    <span class="cls_label_err">*</span>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>

                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                        <center>
                            <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                        </center>
                    </span>
                    <!-- End Customized Content -->

                    <asp:HiddenField ID="hdTeamCode" runat="server" />
                    <asp:HiddenField ID="hdSfeKpiId" runat="server" />
                    <asp:HiddenField ID="hdTotal" runat="server" />
                </fieldset>
                            </ContentTemplate>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server"
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden"
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground"
            DropShadow="True"
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>
