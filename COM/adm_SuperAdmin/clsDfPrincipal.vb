﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsDfPrincipal
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")


    Public Function LoadCountryDDL(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_COUNTRY_DDLLIST"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadCountryDDL = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.LoadCountryDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadPrincipalDDL(ByVal strCountryID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_DDLLIST"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadPrincipalDDL = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDfPrincipal.LoadPrincipalDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function DFPrincipalList(ByVal strCountryID As String, ByVal strPrincipalID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_LIST"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.DFPrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadSalesTeamDDL(ByVal strPrincipalID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_SALESTEAM_LIST"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDfPrincipal.LoadSalesTeamDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function DFPrincipalDtlList(ByVal strPivotcode As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_DTL_LIST"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PIVOT_CODE", strPivotcode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.DFPrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadDFDDL(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_DF_LIST"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.LoadCountryDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function DFPrincipalDtlSave(ByVal strDFID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, _
     ByVal strAltPivotCode As String, ByVal strAltDfCode As String, ByVal strPivotName As String, ByVal strPivotDesc As String, ByVal strStatus As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_DTL_SAVE"
                .addItem("DF_ID", strDFID, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ALT_PIVOT_CODE", strAltPivotCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ALT_DF_CODE", strAltDfCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ALT_PIVOT_NAME", strPivotName, cor_DB.clsDB.DataType.DBString)
                .addItem("ALT_PIVOT_DESC", strPivotDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.DFPrincipalDtlSave" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function RefDFPrincipalDtlList(ByVal strPivotcode As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, ByVal strUserID As String, _
     ByVal strRefCountryID As String, ByVal strRefPrincipalID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFPRINCIPAL_DTL_LIST"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PIVOT_CODE", strPivotcode, cor_DB.clsDB.DataType.DBString)
                .addItem("REF_COUNTRY_ID ", strRefCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("REF_PRINCIPAL_ID", strRefPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsDFPrincipal.RefDFPrincipalDtlList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Exception"

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
