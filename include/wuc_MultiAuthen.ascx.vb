Imports System.Data

Partial Class include_wuc_MultiAuthen
    Inherits System.Web.UI.UserControl
    Public Event ResetBtn_Click As EventHandler
    Public Event RefreshBtn_Click As EventHandler
    Private strTeamCodeList, strSalesrepCodeList As String, strYear As String, lngSubModuleID As String, strProduct As String


    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlExtraction"
        End Get
    End Property

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return strTeamCodeList
        End Get
        Set(ByVal value As String)
            strTeamCodeList = value
        End Set
    End Property

    Public Property SelectedYear() As String
        Get
            Return strYear
        End Get
        Set(ByVal value As String)
            strYear = value
        End Set
    End Property

    Public Property SelectedProduct() As String
        Get
            Return strProduct
        End Get
        Set(ByVal value As String)
            strProduct = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return strSalesrepCodeList
        End Get
        Set(ByVal value As String)
            strSalesrepCodeList = value
        End Set
    End Property

    Public WriteOnly Property PanelCollapese() As Boolean
        Set(ByVal value As Boolean)
            CPE_PnlMultiAuthen.ClientState = value.ToString
            CPE_PnlMultiAuthen.Collapsed = value
        End Set
    End Property

    Public Property SubModuleID() As SubModuleType
        Get
            Return ViewState("lngSubModuleID")
        End Get
        Set(ByVal Value As SubModuleType)
            ViewState("lngSubModuleID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                LoadLsbTeam()
                LoadddlYear()

                If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then
                    pnlProduct.Visible = True
                    LoadPrdDdl()
                Else
                    pnlProduct.Visible = False
                End If

            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub

#Region "Product"

    Private Sub LoadPrdDdl()
        Try

            Dim strTeamList As String
            strTeamList = GetItemsInString(lsbSelectedTeam)

            Dim clsSFActy As New rpt_Customize.clsSFActy
            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strPrincipalCode As String = Session("PRINCIPAL_CODE")

            Dim dt As DataTable = Nothing
            With clsSFActy.properties
                .UserID = strUserID
                .PrinID = strPrincipalID
                .PrinCode = strPrincipalCode
                .TeamCode = strTeamList 
            End With

            dt = clsSFActy.GetSFActyListPrdDdl


            With ddlPrd
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "PRODUCT_NAME"
                .DataValueField = "PRODUCT_CODE" 
                .DataBind()
                .Items.Insert(0, New ListItem("UnSpecified", "UnSpecified"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadPrdDdl : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamAdd.Click
        Try
            If AddToListBox(lsbTeam, lsbSelectedTeam) > 0 Then
                LoadLsbSalesrep()
                If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then 
                    LoadPrdDdl() 
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamAdd_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamRemove.Click
        Try
            If AddToListBox(lsbSelectedTeam, lsbTeam) > 0 Then
                LoadLsbSalesrep()
                If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then
                    LoadPrdDdl() 
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamRemove_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamAddAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            If AddToListBox(lsbTeam, lsbSelectedTeam) > 0 Then
                LoadLsbSalesrep()
                If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then
                    LoadPrdDdl() 
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamAddAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamRemoveAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            If AddToListBox(lsbSelectedTeam, lsbTeam) > 0 Then
                LoadLsbSalesrep()
                If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then
                    LoadPrdDdl() 
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamRemoveAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRAdd.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRAdd_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRRemove.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRRemove_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRAddAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRAddAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRRemoveAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRRemoveAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            LoadLsbTeam()

            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            RaiseEvent ResetBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnReset_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click

        If lsbSelectedSalesrep.Items.Count = 0 Then
            lblInfo.Text = "<BR/>Please select atleast 1 salesrep to cotinue."
            Exit Sub
        Else
            lblInfo.Text = String.Empty
        End If

        TeamCode = Trim(GetItemsInString(lsbSelectedTeam))
        SalesrepCode = Trim(GetItemsInString(lsbSelectedSalesrep))
        SelectedYear = Trim(ddlyear.SelectedValue)
        If SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd Then
            SelectedProduct = Trim(ddlPrd.SelectedValue)
        End If



        RaiseEvent RefreshBtn_Click(sender, e)

        PanelCollapese = True
        'CPE_PnlMultiAuthen.ClientState = "True"
        'CPE_PnlMultiAuthen.Collapsed = True

        UpdatePnlMultiAuthen.Update()
        'Try

        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnRefresh_Click : " & ex.ToString)
        'Finally
        'End Try
    End Sub
#End Region




#Region "YEAR"

    Private Sub LoadddlYear()
        'Build Year
        Dim liThisYear As New ListItem
        liThisYear.Text = Date.Now.Year
        liThisYear.Value = Date.Now.Year
        ddlyear.Items.Add(liThisYear)

        Dim liLastYear As New ListItem
        liLastYear.Text = Date.Now.AddYears(-1).Year
        liLastYear.Value = Date.Now.AddYears(-1).Year
        ddlyear.Items.Add(liLastYear)

        If Not String.IsNullOrEmpty(Session("Year")) Then
            ddlyear.SelectedValue = Session("Year")
        End If

    End Sub
#End Region

#Region "ListBox Action"
    Private Function AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox) As Integer
        Dim iTotal As Integer = lsbFrom.GetSelectedIndices().Length
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        End Try
        Return iTotal
    End Function

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
