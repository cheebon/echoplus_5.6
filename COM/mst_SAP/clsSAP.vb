'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	01/10/2007
'	Purpose	    :	Class to query all the data related to master SAP
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSAP
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Public clsProperties As New mst_SAP.clsProperties.clsSAP
    Public clsRetDtlProperties As New mst_SAP.clsProperties.clsRetDtl
    Public clsRetHdrProperties As New mst_SAP.clsProperties.clsRetHdr
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))
    'Private strSAPID As String

    Sub New()
        'Me.strSAPID = strSAPID
    End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSAP"
        End Get
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Resubmit
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Resubmit()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAPRESUBMIT"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.Resubmit" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Resubmit Trade Deal
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub ResubmitTradeDeal()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_SFA_INSERT_FOC_FT"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("pda_txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.Resubmit" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Function UnBlock
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub UnBlock()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAPUNBLOCK"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("ctn_no", clsProperties.ctn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UnBlock" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub UnBlockSAP()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_UNBLOCK_ORDSAP"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UnBlock" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub SAPUnblock()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAP_UNBLOCK"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.SAPUnblock" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub SAPResubmit()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAP_RESUBMIT"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.SAPUnblock" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub SAPCancel()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAP_CANCEL"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.SAPCancel" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Cancel
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Cancel()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAPCANCEL"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.Cancel" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Resubmit
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub SubmitSAP(ByVal txn_no As String, ByVal txn_timestamp As DateTime)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SUBMITSAP"
                .addItem("TXN_NO", txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_TIMESTAMP", txn_timestamp, cor_DB.clsDB.DataType.DBDatetime)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.SubmitSAP" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateRetHdr()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_UPDATE_RET_HDR"
                .addItem("user_id", clsRetHdrProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsRetHdrProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsRetHdrProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("ref_no", clsRetHdrProperties.ref_no, cor_DB.clsDB.DataType.DBString)
                .addItem("coll_code", clsRetHdrProperties.coll_code, cor_DB.clsDB.DataType.DBString)
                .addItem("ctn_no", clsRetHdrProperties.ctn_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UpdateRetDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub UpdateRetDtl()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_UPDATE_RET_DTL"
                .addItem("user_id", clsRetDtlProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsRetDtlProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsRetDtlProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("line_no", clsRetDtlProperties.line_no, cor_DB.clsDB.DataType.DBString)
                .addItem("prd_code", clsRetDtlProperties.prd_code, cor_DB.clsDB.DataType.DBString)
                .addItem("batch_no", clsRetDtlProperties.batch_no, cor_DB.clsDB.DataType.DBString)
                .addItem("unit_price", clsRetDtlProperties.unit_price, cor_DB.clsDB.DataType.DBString)
                .addItem("reason_code", clsRetDtlProperties.reason_code, cor_DB.clsDB.DataType.DBString)
                .addItem("expired_date", clsRetDtlProperties.expired_date, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UpdateRetDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteRetDtl()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_DELETE_RET_DTL"
                .addItem("user_id", clsRetDtlProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsRetDtlProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsRetDtlProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("line_no", clsRetDtlProperties.line_no, cor_DB.clsDB.DataType.DBString)
                .addItem("prd_code", clsRetDtlProperties.prd_code, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UpdateRetDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub DeleteSAPDTL()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_DELETE_SAP_DTL"
                .addItem("user_id", clsRetDtlProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsRetDtlProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsRetDtlProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("pda_ord_no", clsRetDtlProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("posex", clsRetDtlProperties.line_no, cor_DB.clsDB.DataType.DBString)
                .addItem("matnr", clsRetDtlProperties.prd_code, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UpdateRetDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub Update()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_UPDATE_SAP_HDR"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("billing_no", clsProperties.billing_no, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAP.UpdateRetDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class