Imports System.Data

Partial Class iFFMR_Customize_USRCTRL_wuc_DRCCtrlPnl
    Inherits System.Web.UI.UserControl

    Private lngSubModuleID As SubModuleType
    Public Event NetValue_Changed As EventHandler
    Public Event ExportBtn_Click As EventHandler
    Public Event GenerateBtn_Click As EventHandler

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_DRCCtrlPanel"
        End Get
    End Property
    Public ReadOnly Property CalStartDate() As String
        Get
            Return wuc_txtCalendarRange.DateStart
        End Get
    End Property
    Public ReadOnly Property CalEndDate() As String
        Get
            Return wuc_txtCalendarRange.DateEnd
        End Get
    End Property
    Public ReadOnly Property SelectedSalesRep() As String
        Get
            Return ddlSalesRep.SelectedValue
        End Get
    End Property
    Public ReadOnly Property SelectedProduct() As String
        Get
            Return ddlPrd.SelectedValue
        End Get
    End Property
    Public Property SubModuleID() As SubModuleType
        Get
            Return lngSubModuleID
        End Get
        Set(ByVal Value As SubModuleType)
            lngSubModuleID = Value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Apply for all reports, check for the session
        If Session("UserID") Is Nothing OrElse Session("UserID") = "" Then
            Dim strScript As String = ""
            strScript = "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx") & "?ErrMsg=Session Time Out, Please login again !!!';"
            ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Timeout", strScript, True)
        End If
        'End Session Check

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "IncludedJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/wuc_ctrlPnl.js")
            'BuildQueryYearMonth()
            'GenerateFiledList(lngSubModuleID)
            NetValue_Refresh()
            'FieldConfig_Refresh()
            'BuildReportDefinition()
            'BuildRemarkList()
        End If
        'RefreshDetails(IsPostBack)
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        'End Try

    End Sub

    Public Overloads Sub DataBind()
        imgExport.Visible = True
        imgEnquiryDetails.Visible = False
        'imgRemark.Visible = False
        imgNetValue.Visible = False

        'Ctrl_PnlGeneralInfo_Visible = True

        Select Case lngSubModuleID
            'Sales List
            Case SubModuleType.CUZDRCCUST, SubModuleType.CUZDRCCUSTCLASS, SubModuleType.CUZDRCDISTRICT
                Ctrl_PnlNetValue_Visible = False
                'Ctrl_PnlRemark_Visible = True
                SearchPanelVisibility = True
                imgNetValue.Visible = False
                FillDDL()
            Case Else
                'Ctrl_PnlGeneralInfo_Visible = True
                Ctrl_PnlNetValue_Visible = False
                'Ctrl_PnlRemark_Visible = True
                SearchPanelVisibility = False
                imgExport.Visible = False

        End Select
    End Sub

#Region "Panel Visibility"
    Public WriteOnly Property Ctrl_PnlGeneralInfo_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            'imgGeneralInfo.Visible = boolVisible
            'pnlGeneralInfo.Visible = boolVisible
            'CPE_PnlGeneralInfo.Enabled = boolVisible
        End Set
    End Property

    'Public WriteOnly Property Ctrl_PnlEnquiryDetail_Visible() As Boolean
    '    Set(ByVal boolVisible As Boolean)
    '        imgEnquiryDetails.Visible = boolVisible
    '        Dim strPanelID As String = pnlSalesEnquirySearchDetails.ID
    '        Select Case lngSubModuleID
    '            Case SubModuleType.SALESENQ 'Sales Enquiry
    '                pnlSalesEnquirySearchDetails.Visible = boolVisible
    '                strPanelID = pnlSalesEnquirySearchDetails.ID
    '            Case SubModuleType.CALLENQ 'Call Enquiry
    '                pnlCallEnquirySearchDetails.Visible = boolVisible
    '                strPanelID = pnlCallEnquirySearchDetails.ID
    '            Case SubModuleType.DRCENQ 'DRC Enquiry
    '                pnlDRCEnquirySearchDetails.Visible = boolVisible
    '                strPanelID = pnlDRCEnquirySearchDetails.ID
    '            Case SubModuleType.CHEQENQ 'Coll Cheque Enquiry
    '                pnlCollCheqEnquirySearchDetails.Visible = boolVisible
    '                strPanelID = pnlCollCheqEnquirySearchDetails.ID
    '            Case SubModuleType.COLLENQ
    '                pnlCollEnqSearchDetails.Visible = boolVisible
    '                strPanelID = pnlCollEnqSearchDetails.ID
    '            Case SubModuleType.PAFENQ 'PAF Enquiry
    '                pnlPAFEnquirySearchDetails.Visible = boolVisible
    '                strPanelID = pnlPAFEnquirySearchDetails.ID

    '        End Select
    '        If boolVisible Then
    '            CPE_PnlEnquiryDetail.TargetControlID = strPanelID
    '        End If
    '        CPE_PnlEnquiryDetail.Enabled = boolVisible
    '    End Set
    'End Property

    Public WriteOnly Property Ctrl_PnlNetValue_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            If boolVisible Then
                If Session("NV_Enable") IsNot Nothing AndAlso Session("NV_Enable") = 1 Then
                    imgNetValue.Visible = True
                    pnlNetValue.Visible = True
                    CPE_pnlNetValue.Enabled = True
                End If
            Else
                imgNetValue.Visible = False
                pnlNetValue.Visible = False
                CPE_pnlNetValue.Enabled = False
            End If
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlRemark_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            'imgRemark.Visible = boolVisible
            'Dim strPanelID As String = pnlCallAnalysisByMonthInfo.ID
            'Select Case lngSubModuleID
            '    Case SubModuleType.CABYMTH 'Call Analysis By Month
            '        pnlCallAnalysisByMonthInfo.Visible = boolVisible
            '        strPanelID = pnlCallAnalysisByMonthInfo.ID
            '    Case SubModuleType.CABYDAY 'Call Analysis By Day
            '        pnlCallAnalysisByDayInfo.Visible = boolVisible
            '        strPanelID = pnlCallAnalysisByDayInfo.ID
            'End Select
            'If boolVisible Then
            '    CPE_PnlRemark.TargetControlID = strPanelID
            'End If
            'CPE_PnlRemark.Enabled = boolVisible

            'imgRemark.Visible = boolVisible
            'pnlRemark.Visible = boolVisible
            'CPE_PnlRemark.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property SearchPanelVisibility() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgEnquiryDetails.Visible = boolVisible
            pnlDRCSearch.Visible = boolVisible
            CPE_pnlDRCSearch.Enabled = boolVisible
            CPE_pnlDRCSearch.Collapsed = False
        End Set
    End Property
#End Region

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent GenerateBtn_Click(Me, e)

        CPE_pnlDRCSearch.ClientState = "true"
        CPE_pnlDRCSearch.Collapsed = True
        UpdateCtrlPanel.Update()
    End Sub

#Region "Net Value Control"
    Public Sub NetValue_Refresh()
        Try
            radlNetValue.SelectedValue = Session("NetValue")
        Catch ex As Exception
            ExceptionMsg(ClassName & ".NetValue_Refresh : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnNetValue_refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNetValue_refresh.Click
        Try
            Dim strCurrentValue As String = Session("NetValue")
            If strCurrentValue <> radlNetValue.SelectedValue Then
                Session("NetValue") = radlNetValue.SelectedValue
                'RefreshDetails()
                RaiseEvent NetValue_Changed(Me, e)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnNetValue_refresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region
#Region "pnlRemark"
    'Private Sub BuildReportDefinition()
    '    Try
    '        Dim DT As DataTable = Nothing

    '        Dim clsRDDB As New adm_Glossary.clsGlossary()
    '        DT = clsRDDB.GetReportDefinition(lngSubModuleID, Session("PRINCIPAL_ID"))

    '        dvReportDefinition.DataSource = DT
    '        dvReportDefinition.DataBind()

    '        DT.Dispose()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".BuildReportDefinition : " & ex.ToString)
    '    End Try
    'End Sub

    'Private Sub BuildRemarkList()
    '    Dim DT As DataTable = Nothing
    '    Dim i As Integer
    '    Try

    '        Dim clsGlossaryDB As New adm_Glossary.clsGlossary()
    '        DT = clsGlossaryDB.GetGlossaryList(lngSubModuleID, Session("PRINCIPAL_ID"))

    '        dgRemarkList.Columns.Clear()
    '        'dgRemarkList.RowStyle.BackColor = Nothing
    '        'dgRemarkList.AlternatingRowStyle.BackColor = Nothing

    '        Dim ColumnName As String = ""

    '        For i = 0 To DT.Columns.Count - 1
    '            ColumnName = DT.Columns(i).ColumnName
    '            Dim dgColumn As New BoundField

    '            dgColumn.ReadOnly = True
    '            dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
    '            dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
    '            dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

    '            Dim strFormatString As String = CF_RemarkList.GetOutputFormatString(ColumnName)
    '            If String.IsNullOrEmpty(strFormatString) = False Then
    '                dgColumn.DataFormatString = strFormatString
    '            End If
    '            dgColumn.ItemStyle.HorizontalAlign = CF_RemarkList.ColumnStyle(ColumnName).HorizontalAlign

    '            dgColumn.HtmlEncode = False
    '            dgColumn.HeaderText = CF_RemarkList.GetDisplayColumnName(ColumnName)
    '            dgColumn.DataField = ColumnName
    '            dgRemarkList.Columns.Add(dgColumn)
    '            dgColumn = Nothing
    '        Next

    '        dgRemarkList.DataSource = DT
    '        dgRemarkList.DataBind()

    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".BuildRemarkList : " & ex.ToString)
    '    End Try


    'End Sub
#End Region
#Region "Handle Control Click Event"
    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        RaiseEvent ExportBtn_Click(Me, e)
        'Try
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnExport_Click : " & ex.ToString)
        'End Try
    End Sub
#End Region
#Region "Export"
    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(dgList, Response, customFileName)
    End Sub
    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(aryDgList, Response, customFileName)
    End Sub
    'HL:20080508
    Public Sub ExportToFile(ByRef PrintPanel As Object, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(PrintPanel, Response, customFileName)
    End Sub
#End Region


    Public Sub RefreshDetails(Optional ByVal blnIsPostback As Boolean = False)
        ''lblUserName.Text = HttpContext.Current.Session("UserName")
        'lblCountryName.Text = HttpContext.Current.Session("COUNTRY_NAME")
        'lblPrincipalName.Text = HttpContext.Current.Session("PRINCIPAL_NAME")
        ''lblMapPath.Text = HttpContext.Current.Session("TEAM_NAME")
        ' ''lblTeamName.Text = HttpContext.Current.session("TEAM_NAME") & "|" & HttpContext.Current.session("REGION_NAME")
        ''lblRegionName.Text = HttpContext.Current.Session("REGION_NAME")
        ' ''lblAreaName.Text = HttpContext.Current.Session("currentAreaName")
        ''lblSalesmanName.Text = HttpContext.Current.Session("SALESREP_NAME")
        'lblYearValue.Text = HttpContext.Current.Session("Year")
        'lblMonthValue.Text = HttpContext.Current.Session("Month")
        ''Dim strDT As String = SelectedDateTimeString
        ''SelectedDateTimeString = IIf(String.IsNullOrEmpty(strDT), Session("SelectedDate"), strDT)
        'If blnIsPostback = False Then ReportDateTimeValue = Now
        ''lblModuleName.Text = HttpContext.Current.Session("ModuleName")

        'Dim strValuePath As String = Session("TREE_PATH")
        'Dim strValues(), strName, strvalue As String
        'Dim strMap As New StringBuilder
        'If Not strValuePath = String.Empty Then
        '    strValues = strValuePath.Split("/")
        '    For Each strvalue In strValues
        '        strName = strvalue.Split("@")(0).Replace("_CODE", "_NAME")
        '        strvalue = Session(strName)
        '        If strMap.ToString <> String.Empty Then strMap.Append(" > ")
        '        strMap.Append(strvalue)
        '    Next
        'End If
        'lblMapPath.Text = strMap.ToString
        ''Try
        ''Catch ex As Exception
        ''    ExceptionMsg(ClassName & ".UpdateDetails : " & ex.ToString)
        ''Finally
        ''End Try
    End Sub
    Public Sub UpdateControlPanel()
        UpdateCtrlPanel.Update()
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".UpdateCtrlPanel : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindSalesrep()
        Dim objDRC As New rpt_Customize.clsDRC
        Dim dtSR As DataTable
        Try
            'Get SalesRep List
            dtSR = objDRC.DLLGetSR(Session("UserID"), Session("PRINCIPAL_ID"))

            With ddlSalesRep
                .Items.Clear()
                .DataTextField = "salesrep_name"
                .DataValueField = "salesrep_code"
                .DataSource = dtSR
                .DataBind()
                .Items.Insert(0, New ListItem("Select All", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg("BindSalesrep : " & ex.ToString)
        Finally
            If objDRC IsNot Nothing Then objDRC = Nothing
        End Try
    End Sub
    Private Sub BindProduct()
        Dim objDRC As New rpt_Customize.clsDRC
        Dim dtPrd As DataTable
        Try
            'Get SalesRep List
            dtPrd = objDRC.DLLGetPRD(Session("UserID"), Session("PRINCIPAL_ID"), ddlSalesRep.SelectedValue)

            With ddlPrd
                .Items.Clear()
                .DataSource = dtPrd
                .DataTextField = "prd_name"
                .DataValueField = "prd_code"
                .DataBind()
                .Items.Insert(0, New ListItem("Select All", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg("BindProduct : " & ex.ToString)
        Finally
            If objDRC IsNot Nothing Then objDRC = Nothing
        End Try
    End Sub


    Private Sub FillDDL()
        BindSalesrep()
        BindProduct()
        PanelUpdate()
        'Dim objDRC As rpt_Customize.clsDRC
        'Dim drRow As DataRow
        'Dim dtSR, dtPRD As DataTable

        'Try
        '    'If ddlSalesRepID.Items.Count <= 0 Then
        '    'Get SalesRep List
        '    objDRC = New rpt_Customize.clsDRC
        '    With objDRC
        '        dtSR = .DLLGetSR(Session("UserID"), Session("PRINCIPAL_ID"))
        '        dtPRD = .DLLGetPRD(Session("UserID"), Session("PRINCIPAL_ID"))
        '    End With
        '    'objCommonQuery = Nothing

        '    ddlSalesRep.Items.Clear()
        '    ddlSalesRep.Items.Add(New ListItem("Select All", ""))
        '    If dtSR.Rows.Count > 0 Then
        '        For Each drRow In dtSR.Rows
        '            ddlSalesRep.Items.Add(New ListItem(Trim(drRow("salesrep_name")), Trim(drRow("salesrep_code"))))
        '        Next
        '    End If

        '    ddlPrd.Items.Clear()
        '    ddlPrd.Items.Add(New ListItem("Select All", ""))
        '    If dtPRD.Rows.Count > 0 Then
        '        For Each drRow In dtPRD.Rows
        '            ddlPrd.Items.Add(New ListItem(Trim(drRow("prd_name")), Trim(drRow("prd_code"))))
        '        Next
        '    End If
        '    ddlPrd.SelectedIndex = 0
        '    'End If
        '    objDRC = Nothing
        '    Exit Sub

        'Catch ex As Exception
        '    ExceptionMsg("wuc_paneldrc.DataBind : " & ex.ToString)
        'Finally
        '    'objDRC = Nothing
        'End Try
    End Sub

    Protected Sub ddlSalesRep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesRep.SelectedIndexChanged
        BindProduct()
        PanelUpdate()
    End Sub
    Public Sub PanelUpdate()
        UpdateCtrlPanel.Update()
    End Sub

End Class

Public Class CF_RemarkList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strColumnName As String = ""
        Select Case ColumnName.ToUpper
            Case "GLOSSARY_NAME"
                strColumnName = "Name"
            Case "GLOSSARY_DESC"
                strColumnName = "Description"
            Case Else
                strColumnName = "UNKNOWN"
        End Select

        Return strColumnName
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        Try
            If strNewName Like "*QTY" Then
                strStringFormat = "{0:0}"
            ElseIf strNewName Like "*_AMT" Then
                strStringFormat = "{0:#,0.00}"
            ElseIf strNewName Like "*DATE*" Then
                strStringFormat = "{0:yyyy-MM-dd}"
            Else
                strStringFormat = ""
            End If
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class