Imports System.Data
Imports System.Web

Partial Class CallAnalysisListByStrike
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection

    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_CABYSTRIKE")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_CABYSTRIKE") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CABYSTRIKE"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisListByStrike"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Try

        If Not IsPostBack Then

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CABYSTRIKE) '"Call By Strike"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CABYSTRIKE
                .DataBind()
                .Visible = True
                .blnMakeGroupingFiledDistinct = True
                .intListBoxIndexToFilter = 0
            End With

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                    SortingExpression = .SortExpression
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing
                ChangeReportType()
            End If

            'Session("GroupingValue") = ""
            'Dim strTeamCode As String = Trim(Request.QueryString("TEAM_CODE"))
            'Dim strSalesman As String = Trim(Request.QueryString("SALESREP_CODE"))
            TimerControl1.Enabled = True
        End If
        lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub



#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        ' Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        '    Try
        ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ChangeReportType()
        '    Try

        Dim dt As DataTable
        Dim clsCallQuery As New rpt_CALL.clsCallQuery

        dt = clsCallQuery.GetQuarterlyCallRptType(Session.Item("Year"), Session.Item("Month"), Session.Item("UserID"))
        If dt.Rows.Count > 0 Then
            ddlReportType.SelectedIndex = dt.Rows(0)(0)
        End If


        '----------------------------------------------------------
        'Dim intMonth As Integer = CInt(Session.Item("Month"))
        'If intMonth >= 1 And intMonth <= 3 Then
        '    ddlReportType.SelectedIndex = 0
        'ElseIf intMonth >= 4 And intMonth <= 6 Then
        '    ddlReportType.SelectedIndex = 1
        'ElseIf intMonth >= 7 And intMonth <= 9 Then
        '    ddlReportType.SelectedIndex = 2
        'ElseIf intMonth >= 10 And intMonth <= 12 Then
        '    ddlReportType.SelectedIndex = 3
        'End If
        '----------------------------------------------------------

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        ' Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '  Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        'Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        'Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlpanel.ExportToFile(dgList, "CallProductivity" & ddlReportType.SelectedItem.Text)
        'wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        ' Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
        End If

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        ElseIf dtCurrentTable.Rows.Count = 0 Then
            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        'Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
        Dim aryList As New ArrayList
        '   Try
        For Each DC As DataColumn In dtToBind.Columns
            aryList.Add(DC.ColumnName.ToUpper)
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
        'End Try
        Return aryList
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Dim aryAllColumnName As ArrayList = GenerateAllColumnName(dtToBind)

        '      Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_CABYSTRIKE.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.InvisibleColumn
                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CABYSTRIKE.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SalesList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CABYSTRIKE.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CABYSTRIKE.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        aryDataItem = _aryDataItem
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        ' Try
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '  Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
                    End If
                Next
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format("{0:0}", li.Value)
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        '  Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        '  Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        '  Try
        Dim strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")

        Dim strSalesRepList As String
        strSalesRepList = Session("SALESREP_LIST")

        'Dim strPointerText As String
        'strPointerText = IIf(strTeamCode.Length > 0, "Sales Rep.", "Team")
        'If wuc_ctrlpanel.pnlField_IsPointerExist(0) = False Then
        '    wuc_ctrlpanel.pnlField_InsertPointer(True, 0, strPointerText)
        'End If

        'Dim strGroupField As String
        'If String.IsNullOrEmpty(ViewState("GroupingValue")) Then
        '    strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
        'Else
        '    strGroupField = ViewState("GroupingValue")
        '    ViewState("GroupingValue") = String.Empty
        'End If

        ''select group field to show
        'If Not String.IsNullOrEmpty(strTeamCode) Then
        '    If Not String.IsNullOrEmpty(strRegionCode) Then
        '        'group by salesrep
        '        strGroupField = strGroupField.Replace("@POINTER@", "SALESREP_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")
        '    Else
        '        'Group by Region
        '        strGroupField = strGroupField.Replace("@POINTER@", "REGION_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Region")
        '    End If
        'Else
        '    'group by Team
        '    strGroupField = strGroupField.Replace("@POINTER@", "TEAM_CODE")
        '    wuc_ctrlpanel.pnlField_EditPointerText("Team")
        'End If

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .ReportType = ddlReportType.SelectedIndex
            .Year = strYear
            .Month = strMonth
            .PrincipalCode = strPrincipalCode
            .PrincipalID = strPrincipalID
        End With

        Dim clsCallDB As New rpt_CALL.clsCallQuery
        DT = clsCallDB.GetCallByStrike(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, ddlReportType.SelectedValue, strSalesRepList)
        PreRenderMode(DT)
        'dgList_Init(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_ItemFigureCollector(DT)
        Cal_CustomHeader()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Sub Cal_CustomHeader()
        ' Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            'If strColumnName Like "TARGET_CALLS*" Then
            If strColumnName Like "MONTH_1_*" Then
                If ddlReportType.SelectedValue = 1 Then
                    strColumnName = "JAN"
                ElseIf ddlReportType.SelectedValue = 2 Then
                    strColumnName = "APR"
                ElseIf ddlReportType.SelectedValue = 3 Then
                    strColumnName = "JUL"
                ElseIf ddlReportType.SelectedValue = 4 Then
                    strColumnName = "OCT"
                End If
            ElseIf strColumnName Like "MONTH_2_*" Then
                If ddlReportType.SelectedValue = 1 Then
                    strColumnName = "FEB"
                ElseIf ddlReportType.SelectedValue = 2 Then
                    strColumnName = "MAY"
                ElseIf ddlReportType.SelectedValue = 3 Then
                    strColumnName = "AUG"
                ElseIf ddlReportType.SelectedValue = 4 Then
                    strColumnName = "NOV"
                End If
            ElseIf strColumnName Like "MONTH_3_*" Then
                If ddlReportType.SelectedValue = 1 Then
                    strColumnName = "MAR"
                ElseIf ddlReportType.SelectedValue = 2 Then
                    strColumnName = "JUN"
                ElseIf ddlReportType.SelectedValue = 3 Then
                    strColumnName = "SEPT"
                ElseIf ddlReportType.SelectedValue = 4 Then
                    strColumnName = "DEC"
                End If
            ElseIf strColumnName Like "JAN*" Then
                strColumnName = "JAN"
            ElseIf strColumnName Like "FEB_*" Then
                strColumnName = "FEB"
            ElseIf strColumnName Like "MAR_*" Then
                strColumnName = "MAR"
            ElseIf strColumnName Like "APR_*" Then
                strColumnName = "APR"
            ElseIf strColumnName Like "MAY_*" Then
                strColumnName = "MAY"
            ElseIf strColumnName Like "JUN_*" Then
                strColumnName = "JUN"
            ElseIf strColumnName Like "JUL_*" Then
                strColumnName = "JUL"
            ElseIf strColumnName Like "AUG_*" Then
                strColumnName = "AUG"
            ElseIf strColumnName Like "SEP_*" Then
                strColumnName = "SEP"
            ElseIf strColumnName Like "OCT_*" Then
                strColumnName = "OCT"
            ElseIf strColumnName Like "NOV_*" Then
                strColumnName = "NOV"
            ElseIf strColumnName Like "DEC_*" Then
                strColumnName = "DEC"
            Else
                strColumnName = CF_CABYSTRIKE.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        '   Try
        'licItemFigureCollector = New ListItemCollection
        'Dim strColumnName As String
        'Dim liColumnField As ListItem
        'For Each strColumnName In aryDataItem
        '    strColumnName = strColumnName.ToUpper
        '    Select Case strColumnName
        '        Case "MONTH_1_A", "MONTH_1_B", "MONTH_1_C", "MONTH_1_OTH", _
        '             "MONTH_2_A", "MONTH_2_B", "MONTH_2_C", "MONTH_2_OTH", _
        '             "MONTH_3_A", "MONTH_3_B", "MONTH_3_C", "MONTH_3_OTH", _
        '             "MONTH_1_TTL", "MONTH_2_TTL", "MONTH_3_TTL", "GRAND_TTL"
        '            liColumnField = New ListItem(strColumnName, 0)
        '            licItemFigureCollector.Add(liColumnField)
        '    End Select
        'Next

        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If strColumnName Like "MONTH_1*" OrElse _
             strColumnName Like "MONTH_2*" OrElse _
             strColumnName Like "MONTH_3*" OrElse _
             strColumnName Like "GRAND_TTL*" OrElse _
              strColumnName Like "JAN_*" OrElse _
               strColumnName Like "FEB_*" OrElse _
                strColumnName Like "MAR_*" OrElse _
                 strColumnName Like "APR_*" OrElse _
                  strColumnName Like "MAY_*" OrElse _
                   strColumnName Like "JUN_*" OrElse _
                    strColumnName Like "JUL_*" OrElse _
                     strColumnName Like "AUG_*" OrElse _
                      strColumnName Like "SEP_*" OrElse _
                       strColumnName Like "OCT_*" OrElse _
                        strColumnName Like "NOV_*" OrElse _
                        strColumnName Like "DEC_*" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            SortingExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        '  Try
        SortingExpression = Nothing
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '  Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

End Class

Public Class CF_CABYSTRIKE
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "MONTH_1_A", "MONTH_2_A", "MONTH_3_A", "MONTH_4_A"
                strFieldName = "A"
            Case "MONTH_1_B", "MONTH_2_B", "MONTH_3_B", "MONTH_4_B"
                strFieldName = "B"
            Case "MONTH_1_C", "MONTH_2_C", "MONTH_3_C", "MONTH_4_C"
                strFieldName = "C"
            Case "MONTH_1_OTH", "MONTH_2_OTH", "MONTH_3_OTH", "MONTH_4_OTH"
                strFieldName = "OTH"
            Case "MONTH_1_TTL", "MONTH_2_TTL", "MONTH_3_TTL", "MONTH_4_TTL"
                strFieldName = "Total"
            Case "GRAND_TTL"
                strFieldName = "Grand Total"
            Case "PERIOD_1", "PERIOD_2", "PERIOD_3"
                strFieldName = strColumnName
            Case Else
                If strColumnName Like "*_A" Then
                    strFieldName = "A"
                ElseIf strColumnName Like "*_B" Then
                    strFieldName = "B"
                ElseIf strColumnName Like "*_C" Then
                    strFieldName = "C"
                ElseIf strColumnName Like "*_OTH" Then
                    strFieldName = "OTH"
                ElseIf strColumnName Like "*_TTL" Then
                    strFieldName = "Total"
                Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
                End If

        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '  Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If (strColumnName = "REGION_CODE") OrElse (strColumnName = "TEAM_CODE") Then
            FCT = FieldColumntype.InvisibleColumn
        End If

        Return FCT

        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        ' Try
        Select Case strColumnName.ToUpper

            Case "MONTH_1_A", "MONTH_2_A", "MONTH_3_A", "MONTH_4_A", _
                 "MONTH_1_B", "MONTH_2_B", "MONTH_3_B", "MONTH_4_B", _
                 "MONTH_1_C", "MONTH_2_C", "MONTH_3_C", "MONTH_4_C", _
                 "MONTH_1_OTH", "MONTH_2_OTH", "MONTH_3_OTH", "MONTH_4_OTH", _
                 "MONTH_1_TTL", "MONTH_2_TTL", "MONTH_3_TTL", "MONTH_4_TTL", _
                 "GRAND_TTL"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class


'Public Class CF_CABYSTRIKE
'    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
'        Dim strTblName As String = ""

'        Dim strColumnName As String = ColumnName.ToUpper
'        Select Case strColumnName

'            Case "SALESREP_NAME"
'                strTblName = "Sales Rep."
'            Case "TEAM_CODE"
'                strTblName = "Team"
'            Case "REGION_NAME"
'                strTblName = "Region"
'            Case "MONTH_1_A", "MONTH_2_A", "MONTH_3_A", "MONTH_4_A"
'                strTblName = "A"
'            Case "MONTH_1_B", "MONTH_2_B", "MONTH_3_B", "MONTH_4_B"
'                strTblName = "B"
'            Case "MONTH_1_C", "MONTH_2_C", "MONTH_3_C", "MONTH_4_C"
'                strTblName = "C"
'            Case "MONTH_1_OTH", "MONTH_2_OTH", "MONTH_3_OTH", "MONTH_4_OTH"
'                strTblName = "OTH"
'            Case "MONTH_1_TTL", "MONTH_2_TTL", "MONTH_3_TTL", "MONTH_4_TTL"
'                strTblName = "Total"
'            Case "GRAND_TTL"
'                strTblName = "Grand Total"
'            Case "PERIOD_1", "PERIOD_2", "PERIOD_3"
'                strTblName = strColumnName
'            Case Else
'                strTblName = "UNKNOWN"
'        End Select

'        Return strTblName
'    End Function

'    Public Shared Function GetFieldColumnType(ByVal ColumnName As String) As FieldColumntype
'        Try
'            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
'            Dim strColumnName As String = ColumnName.ToUpper
'            'Select Cat_Code, Category,Sub_Cat,Sub_Category,CustCode,CUST_NAME,CUSTGROUP,CUST_GRP_NAME,
'            Select Case strColumnName
'                Case "SALESREP_CODE", "REGION_NAME", "TEAM_CODE", "PERIOD_1", "PERIOD_2", "PERIOD_3"
'                    FCT = FieldColumntype.InvisibleColumn
'                Case Else
'                    FCT = FieldColumntype.BoundColumn
'            End Select
'            Return FCT
'        Catch ex As Exception

'        End Try
'    End Function

'    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
'        Dim strStringFormat As String = ""
'        Try
'            Select Case strColumnName.ToUpper

'                Case "MONTH_1_A", "MONTH_2_A", "MONTH_3_A", "MONTH_4_A", _
'                     "MONTH_1_B", "MONTH_2_B", "MONTH_3_B", "MONTH_4_B", _
'                     "MONTH_1_C", "MONTH_2_C", "MONTH_3_C", "MONTH_4_C", _
'                     "MONTH_1_OTH", "MONTH_2_OTH", "MONTH_3_OTH", "MONTH_4_OTH", _
'                     "MONTH_1_TTL", "MONTH_2_TTL", "MONTH_3_TTL", "MONTH_4_TTL", _
'                     "GRAND_TTL"
'                    strStringFormat = "{0:#,0}"
'                Case Else
'                    strStringFormat = ""
'            End Select
'        Catch ex As Exception
'        End Try

'        Return strStringFormat
'    End Function

'    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
'        Dim CS As New ColumnStyle
'        Try
'            With CS
'                Dim strColumnName As String = ColumnName.ToUpper
'                .FormatString = GetOutputFormatString(ColumnName)

'                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
'                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
'                OrElse strColumnName Like "TIME_*" Then
'                    .HorizontalAlign = HorizontalAlign.Center
'                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
'                    .HorizontalAlign = HorizontalAlign.Right
'                Else
'                    .HorizontalAlign = HorizontalAlign.Left
'                End If

'            End With

'        Catch ex As Exception

'        End Try
'        Return CS
'    End Function
'End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum