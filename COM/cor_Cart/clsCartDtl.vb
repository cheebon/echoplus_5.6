Imports System.Diagnostics
Imports System.Data
Public Class clsCartDtl
    Private objEventLog As New EventLog("Application", ".", "HIS - .Net Version 2003")
    Private objSession As System.Web.HttpContext
    Private Const mSessionName As String = "CartDT"
    Private mCartDetailsID As Object
    Private mUserID As Object
    Private mCompanyID As Object
    Private mBranchID As Object
    Private mDocID As Object
    Private mDocTypeID As Object
    Private mPrevID As Object
    Private mActionID As Object
    Private mSourceFlag As Object
    Private mQty As Object
    Private mUnitPrice As Object
    Private mfloat1 As Object
    Private mfloat2 As Object
    Private mfloat3 As Object
    Private mfloat4 As Object
    Private mfloat5 As Object
    Private mID1 As Object
    Private mID2 As Object
    Private mID3 As Object
    Private mID4 As Object
    Private mID5 As Object
    Private mID6 As Object
    Private mID7 As Object
    Private mID8 As Object
    Private mID9 As Object
    Private mID10 As Object
    Private mmoney1 As Object
    Private mmoney2 As Object
    Private mmoney3 As Object
    Private mmoney4 As Object
    Private mmoney5 As Object
    Private mflag1 As Object
    Private mflag2 As Object
    Private mflag3 As Object
    Private mflag4 As Object
    Private mflag5 As Object
    Private mflag6 As Object
    Private mflag7 As Object
    Private mflag8 As Object
    Private mflag9 As Object
    Private mflag10 As Object
    Private mField1 As Object
    Private mField2 As Object
    Private mField3 As Object
    Private mField4 As Object
    Private mField5 As Object
    Private mField6 As Object
    Private mField7 As Object
    Private mField8 As Object
    Private mField9 As Object
    Private mField10 As Object
    Private mField11 As Object
    Private mField12 As Object
    Private mField13 As Object
    Private mField14 As Object
    Private mField15 As Object
    Private mField16 As Object
    Private mField17 As Object
    Private mField18 As Object
    Private mField19 As Object
    Private mField20 As Object
    Private mExtended As Object

    Public Property CartDetailsID() As Object
        Get
            Return mCartDetailsID
        End Get
        Set(ByVal Value As Object)
            mCartDetailsID = Value
        End Set
    End Property

    Public Property UserID() As Object
        Get
            Return mUserID
        End Get
        Set(ByVal Value As Object)
            mUserID = Value
        End Set
    End Property

    Public Property CompanyID() As Object
        Get
            Return mCompanyID
        End Get
        Set(ByVal Value As Object)
            mCompanyID = Value
        End Set
    End Property

    Public Property BranchID() As Object
        Get
            Return mBranchID
        End Get
        Set(ByVal Value As Object)
            mBranchID = Value
        End Set
    End Property

    Public Property DocID() As Object
        Get
            Return mDocID
        End Get
        Set(ByVal Value As Object)
            mDocID = Value
        End Set
    End Property

    Public Property DocTypeID() As Object
        Get
            Return mDocTypeID
        End Get
        Set(ByVal Value As Object)
            mDocTypeID = Value
        End Set
    End Property

    Public Property PrevID() As Object
        Get
            Return mPrevID
        End Get
        Set(ByVal Value As Object)
            mPrevID = Value
        End Set
    End Property

    Public Property ActionID() As Object
        Get
            Return mActionID
        End Get
        Set(ByVal Value As Object)
            mActionID = Value
        End Set
    End Property

    Public Property SourceFlag() As Object
        Get
            Return mSourceFlag
        End Get
        Set(ByVal Value As Object)
            mSourceFlag = Value
        End Set
    End Property

    Public Property Qty() As Object
        Get
            Return mQty
        End Get
        Set(ByVal Value As Object)
            mQty = Value
        End Set
    End Property

    Public Property UnitPrice() As Object
        Get
            Return mUnitPrice
        End Get
        Set(ByVal Value As Object)
            mUnitPrice = Value
        End Set
    End Property

    Public Property float1() As Object
        Get
            Return mfloat1
        End Get
        Set(ByVal Value As Object)
            mfloat1 = Value
        End Set
    End Property

    Public Property float2() As Object
        Get
            Return mfloat2
        End Get
        Set(ByVal Value As Object)
            mfloat2 = Value
        End Set
    End Property

    Public Property float3() As Object
        Get
            Return mfloat3
        End Get
        Set(ByVal Value As Object)
            mfloat3 = Value
        End Set
    End Property

    Public Property float4() As Object
        Get
            Return mfloat4
        End Get
        Set(ByVal Value As Object)
            mfloat4 = Value
        End Set
    End Property

    Public Property float5() As Object
        Get
            Return mfloat5
        End Get
        Set(ByVal Value As Object)
            mfloat5 = Value
        End Set
    End Property

    Public Property ID1() As Object
        Get
            Return mID1
        End Get
        Set(ByVal Value As Object)
            mID1 = Value
        End Set
    End Property

    Public Property ID2() As Object
        Get
            Return mID2
        End Get
        Set(ByVal Value As Object)
            mID2 = Value
        End Set
    End Property

    Public Property ID3() As Object
        Get
            Return mID3
        End Get
        Set(ByVal Value As Object)
            mID3 = Value
        End Set
    End Property

    Public Property ID4() As Object
        Get
            Return mID4
        End Get
        Set(ByVal Value As Object)
            mID4 = Value
        End Set
    End Property

    Public Property ID5() As Object
        Get
            Return mID5
        End Get
        Set(ByVal Value As Object)
            mID5 = Value
        End Set
    End Property

    Public Property ID6() As Object
        Get
            Return mID6
        End Get
        Set(ByVal Value As Object)
            mID6 = Value
        End Set
    End Property

    Public Property ID7() As Object
        Get
            Return mID7
        End Get
        Set(ByVal Value As Object)
            mID7 = Value
        End Set
    End Property

    Public Property ID8() As Object
        Get
            Return mID8
        End Get
        Set(ByVal Value As Object)
            mID8 = Value
        End Set
    End Property

    Public Property ID9() As Object
        Get
            Return mID9
        End Get
        Set(ByVal Value As Object)
            mID9 = Value
        End Set
    End Property

    Public Property ID10() As Object
        Get
            Return mID10
        End Get
        Set(ByVal Value As Object)
            mID10 = Value
        End Set
    End Property

    Public Property money1() As Object
        Get
            Return mmoney1
        End Get
        Set(ByVal Value As Object)
            mmoney1 = Value
        End Set
    End Property

    Public Property money2() As Object
        Get
            Return mmoney2
        End Get
        Set(ByVal Value As Object)
            mmoney2 = Value
        End Set
    End Property

    Public Property money3() As Object
        Get
            Return mmoney3
        End Get
        Set(ByVal Value As Object)
            mmoney3 = Value
        End Set
    End Property

    Public Property money4() As Object
        Get
            Return mmoney4
        End Get
        Set(ByVal Value As Object)
            mmoney4 = Value
        End Set
    End Property

    Public Property money5() As Object
        Get
            Return mmoney5
        End Get
        Set(ByVal Value As Object)
            mmoney5 = Value
        End Set
    End Property

    Public Property flag1() As Object
        Get
            Return mflag1
        End Get
        Set(ByVal Value As Object)
            mflag1 = Value
        End Set
    End Property

    Public Property flag2() As Object
        Get
            Return mflag2
        End Get
        Set(ByVal Value As Object)
            mflag2 = Value
        End Set
    End Property

    Public Property flag3() As Object
        Get
            Return mflag3
        End Get
        Set(ByVal Value As Object)
            mflag3 = Value
        End Set
    End Property

    Public Property flag4() As Object
        Get
            Return mflag4
        End Get
        Set(ByVal Value As Object)
            mflag4 = Value
        End Set
    End Property

    Public Property flag5() As Object
        Get
            Return mflag5
        End Get
        Set(ByVal Value As Object)
            mflag5 = Value
        End Set
    End Property

    Public Property flag6() As Object
        Get
            Return mflag6
        End Get
        Set(ByVal Value As Object)
            mflag6 = Value
        End Set
    End Property

    Public Property flag7() As Object
        Get
            Return mflag7
        End Get
        Set(ByVal Value As Object)
            mflag7 = Value
        End Set
    End Property

    Public Property flag8() As Object
        Get
            Return mflag8
        End Get
        Set(ByVal Value As Object)
            mflag8 = Value
        End Set
    End Property

    Public Property flag9() As Object
        Get
            Return mflag9
        End Get
        Set(ByVal Value As Object)
            mflag9 = Value
        End Set
    End Property

    Public Property flag10() As Object
        Get
            Return mflag10
        End Get
        Set(ByVal Value As Object)
            mflag10 = Value
        End Set
    End Property

    Public Property Field1() As Object
        Get
            Return mField1
        End Get
        Set(ByVal Value As Object)
            mField1 = Value
        End Set
    End Property

    Public Property Field2() As Object
        Get
            Return mField2
        End Get
        Set(ByVal Value As Object)
            mField2 = Value
        End Set
    End Property

    Public Property Field3() As Object
        Get
            Return mField3
        End Get
        Set(ByVal Value As Object)
            mField3 = Value
        End Set
    End Property

    Public Property Field4() As Object
        Get
            Return mField4
        End Get
        Set(ByVal Value As Object)
            mField4 = Value
        End Set
    End Property

    Public Property Field5() As Object
        Get
            Return mField5
        End Get
        Set(ByVal Value As Object)
            mField5 = Value
        End Set
    End Property

    Public Property Field6() As Object
        Get
            Return mField6
        End Get
        Set(ByVal Value As Object)
            mField6 = Value
        End Set
    End Property

    Public Property Field7() As Object
        Get
            Return mField7
        End Get
        Set(ByVal Value As Object)
            mField7 = Value
        End Set
    End Property

    Public Property Field8() As Object
        Get
            Return mField8
        End Get
        Set(ByVal Value As Object)
            mField8 = Value
        End Set
    End Property

    Public Property Field9() As Object
        Get
            Return mField9
        End Get
        Set(ByVal Value As Object)
            mField9 = Value
        End Set
    End Property

    Public Property Field10() As Object
        Get
            Return mField10
        End Get
        Set(ByVal Value As Object)
            mField10 = Value
        End Set
    End Property

    Public Property Field11() As Object
        Get
            Return mField11
        End Get
        Set(ByVal Value As Object)
            mField11 = Value
        End Set
    End Property

    Public Property Field12() As Object
        Get
            Return mField12
        End Get
        Set(ByVal Value As Object)
            mField12 = Value
        End Set
    End Property

    Public Property Field13() As Object
        Get
            Return mField13
        End Get
        Set(ByVal Value As Object)
            mField13 = Value
        End Set
    End Property

    Public Property Field14() As Object
        Get
            Return mField14
        End Get
        Set(ByVal Value As Object)
            mField14 = Value
        End Set
    End Property

    Public Property Field15() As Object
        Get
            Return mField15
        End Get
        Set(ByVal Value As Object)
            mField15 = Value
        End Set
    End Property

    Public Property Field16() As Object
        Get
            Return mField16
        End Get
        Set(ByVal Value As Object)
            mField16 = Value
        End Set
    End Property

    Public Property Field17() As Object
        Get
            Return mField17
        End Get
        Set(ByVal Value As Object)
            mField17 = Value
        End Set
    End Property

    Public Property Field18() As Object
        Get
            Return mField18
        End Get
        Set(ByVal Value As Object)
            mField18 = Value
        End Set
    End Property

    Public Property Field19() As Object
        Get
            Return mField19
        End Get
        Set(ByVal Value As Object)
            mField19 = Value
        End Set
    End Property

    Public Property Field20() As Object
        Get
            Return mField20
        End Get
        Set(ByVal Value As Object)
            mField20 = Value
        End Set
    End Property

    Public Property Extended() As Object
        Get
            Return mExtended
        End Get
        Set(ByVal Value As Object)
            mExtended = Value
        End Set
    End Property

    Public Function Populate() As DataTable
        Dim dtTable As New DataTable("Cart")
        Dim i As Integer

        Try
            Dim dcKey As New DataColumn
            dcKey.ColumnName = "CartDetailsID"
            dcKey.DataType = GetType(Long)
            dcKey.AutoIncrement = True
            dcKey.AutoIncrementSeed = 1
            dcKey.AutoIncrementStep = 1
            dtTable.Columns.Add(dcKey)
            dtTable.Constraints.Add("key1", dcKey, True)
            dcKey.Dispose()

            Dim dcUserID As New DataColumn
            dcUserID.ColumnName = "UserID"
            dcUserID.DataType = GetType(Long)
            dtTable.Columns.Add(dcUserID)
            dcUserID.Dispose()

            Dim dcCompanyID As New DataColumn
            dcCompanyID.ColumnName = "CompanyID"
            dcCompanyID.DataType = GetType(Long)
            dtTable.Columns.Add(dcCompanyID)
            dcCompanyID.Dispose()

            Dim dcBranchID As New DataColumn
            dcBranchID.ColumnName = "BranchID"
            dcBranchID.DataType = GetType(Long)
            dtTable.Columns.Add(dcBranchID)
            dcBranchID.Dispose()

            Dim dcDocID As New DataColumn
            dcDocID.ColumnName = "DocID"
            dcDocID.DataType = GetType(Long)
            dtTable.Columns.Add(dcDocID)
            dcDocID.Dispose()

            Dim dcDocTypeID As New DataColumn
            dcDocTypeID.ColumnName = "DocTypeID"
            dcDocTypeID.DataType = GetType(Long)
            dtTable.Columns.Add(dcDocTypeID)
            dcDocTypeID.Dispose()

            Dim dcPrevID As New DataColumn
            dcPrevID.ColumnName = "PrevID"
            dcPrevID.DataType = GetType(Long)
            dtTable.Columns.Add(dcPrevID)
            dcPrevID.Dispose()

            Dim dcActionID As New DataColumn
            dcActionID.ColumnName = "ActionID"
            dcActionID.DataType = GetType(Long)
            dtTable.Columns.Add(dcActionID)
            dcActionID.Dispose()

            Dim dcSourceFlag As New DataColumn
            dcSourceFlag.ColumnName = "SourceFlag"
            dcSourceFlag.DataType = GetType(Integer)
            dtTable.Columns.Add(dcSourceFlag)
            dcSourceFlag.Dispose()

            Dim dcQty As New DataColumn
            dcQty.ColumnName = "Qty"
            dcQty.DataType = GetType(Double)
            dtTable.Columns.Add(dcQty)
            dcQty.Dispose()

            Dim dcUnitPrice As New DataColumn
            dcUnitPrice.ColumnName = "UnitPrice"
            dcUnitPrice.DataType = GetType(Double)
            dtTable.Columns.Add(dcUnitPrice)
            dcUnitPrice.Dispose()

            Dim dcExtended As New DataColumn
            dcExtended.ColumnName = "Extended"
            dcExtended.DataType = GetType(String)
            dtTable.Columns.Add(dcExtended)
            dcExtended.Dispose()

            'Fields
            For i = 1 To 20
                Dim dcField As New DataColumn
                dcField.ColumnName = "Field" & CStr(i)
                dcField.DataType = GetType(String)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'flag
            For i = 1 To 10
                Dim dcField As New DataColumn
                dcField.ColumnName = "flag" & CStr(i)
                dcField.DataType = GetType(Integer)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'ID
            For i = 1 To 10
                Dim dcField As New DataColumn
                dcField.ColumnName = "ID" & CStr(i)
                dcField.DataType = GetType(Long)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'float
            For i = 1 To 5
                Dim dcField As New DataColumn
                dcField.ColumnName = "float" & CStr(i)
                dcField.DataType = GetType(Double)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'money
            For i = 1 To 5
                Dim dcField As New DataColumn
                dcField.ColumnName = "money" & CStr(i)
                dcField.DataType = GetType(String)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable
            Return dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Populate : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        Finally

        End Try
    End Function

    Public Function Create() As Long
        Dim dtTable As DataTable
        Dim drRow As DataRow
        'Dim i As Integer

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drRow = dtTable.NewRow
            drRow = PopulateRow(drRow)

            ' Add the new row to the table.
            dtTable.Rows.Add(drRow)

            Web.HttpContext.Current.Session(mSessionName) = dtTable
            Create = drRow("CartDetailsID")

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Create : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

    Public Sub UpdateActionID()
        Dim dtTable As DataTable
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drRow = dtTable.Rows.Find(mCartDetailsID)

            If Not (drRow Is Nothing) Then
                drRow.BeginEdit()
                drRow("ActionID") = IIf(mActionID = Nothing Or IsDBNull(mActionID), 0, mActionID)
                drRow.EndEdit()
            End If

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.UpdateActionID : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub Update()
        Dim dtTable As DataTable
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drRow = dtTable.Rows.Find(mCartDetailsID)

            If Not (drRow Is Nothing) Then
                drRow.BeginEdit()
                drRow = PopulateRow(drRow)
                drRow.EndEdit()
            End If

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Update : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub Delete()
        Dim dtTable As DataTable
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drCurrRow = dtTable.Select("CartDetailsID=" & mCartDetailsID, "CartDetailsID ASC")

            For Each drRow In drCurrRow
                dtTable.Rows.Remove(drRow)
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Delete : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub Clear()
        Dim dtTable As DataTable
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow
        Dim strSQL As String

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            strSQL = " DocTypeID=" & IIf(mDocTypeID = Nothing Or IsDBNull(mDocTypeID), 0, mDocTypeID)
            drCurrRow = dtTable.Select(strSQL, "CartDetailsID ASC")

            For Each drRow In drCurrRow
                dtTable.Rows.Remove(drRow)
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Clear : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Function Retrieve() As DataTable
        Dim dtTable, dtNewTable As DataTable
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            dtNewTable = dtTable.Copy
            drCurrRow = dtNewTable.Select("CartDetailsID<>" & mCartDetailsID, "CartDetailsID ASC")

            For Each drRow In drCurrRow
                dtNewTable.Rows.Remove(drRow)
            Next

            Retrieve = dtNewTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Retrieve : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

    Private Function PopulateRow(ByVal drRow As DataRow) As DataRow
        Try
            drRow("UserID") = IIf(mUserID = Nothing Or IsDBNull(mUserID), 0, mUserID)
            drRow("CompanyID") = IIf(mCompanyID = Nothing Or IsDBNull(mCompanyID), 0, mCompanyID)
            drRow("BranchID") = IIf(mBranchID = Nothing Or IsDBNull(mBranchID), 0, mBranchID)
            drRow("DocID") = IIf(mDocID = Nothing Or IsDBNull(mDocID), 0, mDocID)
            drRow("DocTypeID") = IIf(mDocTypeID = Nothing Or IsDBNull(mDocTypeID), 0, mDocTypeID)
            drRow("PrevID") = IIf(mPrevID = Nothing Or IsDBNull(mPrevID), 0, mPrevID)
            drRow("ActionID") = IIf(mActionID = Nothing Or IsDBNull(mActionID), 0, mActionID)
            drRow("SourceFlag") = IIf(mSourceFlag = Nothing Or IsDBNull(mSourceFlag), 0, mSourceFlag)
            drRow("Qty") = IIf(mQty = Nothing Or IsDBNull(mQty), 0, mQty)
            drRow("UnitPrice") = IIf(mUnitPrice = Nothing Or IsDBNull(mUnitPrice), 0, mUnitPrice)
            drRow("Extended") = mExtended

            'Fields
            drRow("Field1") = mField1
            drRow("Field2") = mField2
            drRow("Field3") = mField3
            drRow("Field4") = mField4
            drRow("Field5") = mField5
            drRow("Field6") = mField6
            drRow("Field7") = mField7
            drRow("Field8") = mField8
            drRow("Field9") = mField9
            drRow("Field10") = mField10
            drRow("Field11") = mField11
            drRow("Field12") = mField12
            drRow("Field13") = mField13
            drRow("Field14") = mField14
            drRow("Field15") = mField15
            drRow("Field16") = mField16
            drRow("Field17") = mField17
            drRow("Field18") = mField18
            drRow("Field19") = mField19
            drRow("Field20") = mField20

            'flag
            drRow("flag1") = IIf(mflag1 = Nothing Or IsDBNull(mflag1), 0, mflag1)
            drRow("flag2") = IIf(mflag2 = Nothing Or IsDBNull(mflag2), 0, mflag2)
            drRow("flag3") = IIf(mflag3 = Nothing Or IsDBNull(mflag3), 0, mflag3)
            drRow("flag4") = IIf(mflag4 = Nothing Or IsDBNull(mflag4), 0, mflag4)
            drRow("flag5") = IIf(mflag5 = Nothing Or IsDBNull(mflag5), 0, mflag5)
            drRow("flag6") = IIf(mflag6 = Nothing Or IsDBNull(mflag6), 0, mflag6)
            drRow("flag7") = IIf(mflag7 = Nothing Or IsDBNull(mflag7), 0, mflag7)
            drRow("flag8") = IIf(mflag8 = Nothing Or IsDBNull(mflag8), 0, mflag8)
            drRow("flag9") = IIf(mflag9 = Nothing Or IsDBNull(mflag9), 0, mflag9)
            drRow("flag10") = IIf(mflag10 = Nothing Or IsDBNull(mflag10), 0, mflag10)

            'ID
            drRow("ID1") = IIf(mID1 = Nothing Or IsDBNull(mID1), 0, mID1)
            drRow("ID2") = IIf(mID2 = Nothing Or IsDBNull(mID2), 0, mID2)
            drRow("ID3") = IIf(mID3 = Nothing Or IsDBNull(mID3), 0, mID3)
            drRow("ID4") = IIf(mID4 = Nothing Or IsDBNull(mID4), 0, mID4)
            drRow("ID5") = IIf(mID5 = Nothing Or IsDBNull(mID5), 0, mID5)
            drRow("ID6") = IIf(mID6 = Nothing Or IsDBNull(mID6), 0, mID6)
            drRow("ID7") = IIf(mID7 = Nothing Or IsDBNull(mID7), 0, mID7)
            drRow("ID8") = IIf(mID8 = Nothing Or IsDBNull(mID8), 0, mID8)
            drRow("ID9") = IIf(mID9 = Nothing Or IsDBNull(mID9), 0, mID9)
            drRow("ID10") = IIf(mID10 = Nothing Or IsDBNull(mID10), 0, mID10)

            'float
            drRow("float1") = IIf(mfloat1 = Nothing Or IsDBNull(mfloat1), 0, mfloat1)
            drRow("float2") = IIf(mfloat2 = Nothing Or IsDBNull(mfloat2), 0, mfloat2)
            drRow("float3") = IIf(mfloat3 = Nothing Or IsDBNull(mfloat3), 0, mfloat3)
            drRow("float4") = IIf(mfloat4 = Nothing Or IsDBNull(mfloat4), 0, mfloat4)
            drRow("float5") = IIf(mfloat5 = Nothing Or IsDBNull(mfloat5), 0, mfloat5)

            'money
            drRow("money1") = IIf(mmoney1 = Nothing Or IsDBNull(mmoney1), 0, mmoney1)
            drRow("money2") = IIf(mmoney2 = Nothing Or IsDBNull(mmoney2), 0, mmoney2)
            drRow("money3") = IIf(mmoney3 = Nothing Or IsDBNull(mmoney3), 0, mmoney3)
            drRow("money4") = IIf(mmoney4 = Nothing Or IsDBNull(mmoney4), 0, mmoney4)
            drRow("money5") = IIf(mmoney5 = Nothing Or IsDBNull(mmoney5), 0, mmoney5)

            PopulateRow = drRow

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.PopulateRow : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

    Public Function RetrieveByProductGroup(ByVal strPrdGrp As String) As Data.DataTable
        Dim dtTable, dtNewTable As Data.DataTable
        Dim drCurrRow() As Data.DataRow
        Dim drRow As Data.DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            Dim i As Integer = dtTable.Rows.Count
            dtNewTable = dtTable.Copy
            If strPrdGrp = "All" Then
                RetrieveByProductGroup = dtNewTable
            Else
                drCurrRow = dtNewTable.Select("Field12<>'" & strPrdGrp & "'", "Field12 ASC")
                For Each drRow In drCurrRow
                    dtNewTable.Rows.Remove(drRow)
                Next
                RetrieveByProductGroup = dtNewTable
            End If


        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.RetrieveByProductGroup : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

    Public Sub UpdateByField1()
        Dim dtTable As DataTable
        'Dim drRow As DataRow
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            'drRow = dtTable.Rows.Find(mField1)

            drCurrRow = dtTable.Select("Field1='" & mField1 & "'", "")

            For Each drRow In drCurrRow
                drRow.BeginEdit()
                drRow = PopulateRow(drRow)
                drRow.EndEdit()
            Next


            'If Not (drRow Is Nothing) Then
            '    drRow.BeginEdit()
            '    drRow = PopulateRow(drRow)
            '    drRow.EndEdit()
            'End If

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.Update : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try

        'Dim dtTable As Data.DataTable
        'Dim drRow As Data.DataRow = Nothing

        'Try
        '    dtTable = Web.HttpContext.Current.Session(mSessionName)
        '    Dim i As Integer = dtTable.Rows.Count
        '    Dim j As Integer = dtTable.Columns.Count


        '    'drRow = dtTable.Rows.Find(Field1)

        '    ' Dim intfield = Field1
        '    'drRow = dtTable.Rows.Find(dtTable.Select("field1='" & Field1 & "'"))



        '    If Not (drRow Is Nothing) Then
        '        drRow.BeginEdit()
        '        drRow = PopulateRow(drRow)
        '        drRow.EndEdit()

        '    End If

        '    Web.HttpContext.Current.Session(mSessionName) = dtTable

        'Catch ex As Exception
        '    objEventLog.WriteEntry("cor_Cart.clsCartDtl.UpdateByItemCode : " + ex.ToString())
        '    Throw New System.Exception(ex.ToString)
        'End Try
    End Sub

    Public Sub removeByField1(ByVal strItemCode As String)
        Dim dtTable, dtNewTable As Data.DataTable
        Dim drCurrRow() As Data.DataRow
        Dim drRow As Data.DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            Dim i As Integer = dtTable.Rows.Count
            dtNewTable = dtTable.Copy
           
            drCurrRow = dtNewTable.Select("Field1='" & strItemCode & "'", "Field1 ASC")
            For Each drRow In drCurrRow
                dtNewTable.Rows.Remove(drRow)
            Next
            Web.HttpContext.Current.Session(mSessionName) = dtNewTable
        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.removeByField1 : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub removeByField1AndType(ByVal strItemCode As String)
        Dim dtTable, dtNewTable As Data.DataTable
        Dim drCurrRow() As Data.DataRow
        Dim drRow As Data.DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            Dim i As Integer = dtTable.Rows.Count
            dtNewTable = dtTable.Copy

            drCurrRow = dtNewTable.Select("Field1='" & strItemCode & "' AND Field7='TANN'", "Field1 ASC")
            Dim j As Integer = dtNewTable.Select("Field1='" & strItemCode & "' AND Field7='TANN'", "Field1 ASC").Length
            For Each drRow In drCurrRow
                dtNewTable.Rows.Remove(drRow)
            Next
            Dim k As Integer = dtNewTable.Rows.Count
            Web.HttpContext.Current.Session(mSessionName) = dtNewTable
        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.removeByField1AndType : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub UpdateByField1AndType()
        Dim dtTable As DataTable
        'Dim drRow As DataRow
        'Dim drCurrRow() As DataRow
        Dim drRow As DataRow
        Dim strSQl As String = "TRIM(Field7) <> 'TANN'" '"Field1='" & mField1 & "' AND 
        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            'drRow = dtTable.Rows.Find(mField1)
            'drCurrRow = dtTable.Select("Field1='" & mField1 & "'" & " AND Field5 = '138.75'")
            'drCurrRow = dtTable.Select(strSQl)
            Dim i As Integer = dtTable.Rows.Count

            For i = 0 To dtTable.Rows.Count - 1
                If String.IsNullOrEmpty(dtTable.Rows(i)("Field7").ToString) = True Then
                    'If IsDBNull(dtTable.Rows(i)("Field7")) Then
                    'If Trim(dtTable.Rows(i)("Field7")) <> "TANN" Then
                    dtTable.Rows(i).BeginEdit()
                    drRow = PopulateRow(dtTable.Rows(i))
                    dtTable.Rows(i).EndEdit()
                    'End If
                End If
            Next
            'For Each drRow In drCurrRow
            '    drRow.BeginEdit()
            '    drRow = PopulateRow(drRow)
            '    drRow.EndEdit()
            'Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCartDtl.UpdateByField1AndType : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub
End Class
