<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserPwdEdit.aspx.vb" Inherits="Admin_User_UserPwdEdit" %>

<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Change User Password</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script type="text/javascript" src="../../include/jquery-1.4.2.min.js"></script>
     <script type="text/javascript">
    <% = Portal.Policy.WriteClientPolicy() %>
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmUserPwdEdit" method="post" runat="server">
    <table id="tbl1" cellspacing="0" cellpadding="0" width="98%" border="0">
        <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="BckgroundInsideContentLayout">
                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                    <tr>
                        <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                        <td colspan="3">
                            <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                        </td>
                        <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                    </tr>
                    <tr>
                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td valign="top" class="Bckgroundreport">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="Bckgroundreport">
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblLogin" runat="server" CssClass="cls_label_header">Login</asp:Label><asp:Label
                                                        ID="lblMark2" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot2" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:Label ID="lblLoginValue" runat="server" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblUserCode" runat="server" CssClass="cls_label_header">User Code</asp:Label><asp:Label
                                                        ID="lblMark5" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot5" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:Label ID="lblUserCodeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblUserName" runat="server" CssClass="cls_label_header">User Name</asp:Label><asp:Label
                                                        ID="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot6" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:Label ID="lblUserNameValue" runat="server" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblPassword" runat="server" CssClass="cls_label_header">Password</asp:Label><asp:Label
                                                        ID="lblMark8" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot8" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" MaxLength="50" runat="server" Width="110px" TextMode="Password"
                                                        CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblNewPassword" runat="server" CssClass="cls_label_header">New Password</asp:Label><asp:Label
                                                        ID="lblMark3" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot3" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNewPassword" MaxLength="50" runat="server" Width="110px" TextMode="Password"
                                                        CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ControlToValidate="txtNewPassword"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblConfirmedPassword" runat="server" CssClass="cls_label_header">Confirmed Password</asp:Label><asp:Label
                                                        ID="lblMark4" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot4" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtConfirmedPassword" MaxLength="50" runat="server" Width="110px"
                                                        TextMode="Password" CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvConfirmedPassword" runat="server" ControlToValidate="txtConfirmedPassword"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPoorPass" runat="server" ErrorMessage="Minimum 8 alphanumeric character"
                                                        CssClass="cls_validator"  ControlToValidate="txtConfirmedPassword" Display="Dynamic"
                                                        OnServerValidate="PolicyCheck" ClientValidationFunction="PolicyCheck" ></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td align="right">
                                        <asp:TextBox ID="txtUserID" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtOrigPassword" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" CausesValidation="false">
                                        </asp:Button>
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="cls_button" Text="Submit"></asp:Button>
                                    </td>
                                </tr>
                                <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <%'List function called by in-line scripts%>

    <script language="javascript">window.focus();</script>

</body>
</html>
