Imports System.Data

Partial Class CustomerMthSalesHistDetail
    Inherits System.Web.UI.Page



    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerMthSalesHistDetail"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'Call Header
        With wuc_lblheader
            .Title = "Monthly Sales History Detail"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If

    End Sub
    Sub onpageload()
        Try
            Dim strCycleNo, strSalesmanCode, strCustCode As String
            strCycleNo = Request.QueryString("cycleno")
            strSalesmanCode = Session("SALESREP_CODE")
            strCustCode = Session("customercode")
            bindGridView(strSalesmanCode, strCustCode, strCycleNo)

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getMthSalesHistDetailDT(strSalesmanCode, strCustCode, strTxnNo)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
            Else
                dt.Rows.Add(dt.NewRow)
                gridview1.DataSource = dt
                gridview1.DataBind()
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../customer/customermthsaleshist.aspx", False)
    End Sub
End Class



