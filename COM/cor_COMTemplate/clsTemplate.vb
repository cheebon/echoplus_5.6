'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/06/2005
'	Purpose	    :	COM+ Template for Standard Function 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
Option Strict On

Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports System.ComponentModel

Interface Icor_Template
    Function Create() As Long
    Sub Update()
    Sub Delete()
End Interface

'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), 
<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsTemplate
    Inherits ServicedComponent
    Implements Icor_Template

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsTemplate

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Create
    ' Purpose	    :	Create/Insert new record.
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: clsProperties :1,2
    '		            [out]      : FieldID
    '----------------------------------------------------------------------
    <AutoComplete()> _
    Public Function Create() As Long Implements Icor_Template.Create
        Try
            Dim lngTest As Long = clsProperties.FieldID
            'Call Trail class based on system setting

            'Dim obj as Object.clsObjectClass
            'set obj = new Object.clsObjectClass
            'with obj
            '
            'end with
            'set obj = Nothing

            Create = clsProperties.FieldID

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw (New ExceptionMsg(ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	Update/Edit selected record.
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: clsProperties :1,2
    '		            [out]      : -
    '----------------------------------------------------------------------
    <AutoComplete()> _
    Public Sub Update() Implements Icor_Template.Update
        Try
            'Call Trail class based on system setting

            'Dim obj as Object.clsObjectClass
            'set obj = new Object.clsObjectClass
            'with obj
            '
            'end with
            'set obj = Nothing

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw (New ExceptionMsg(ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Delete
    ' Purpose	    :	Delete selected record.
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: clsProperties :1,2
    '		            [out]      : -
    '----------------------------------------------------------------------
    <AutoComplete()> _
    Public Sub Delete() Implements Icor_Template.Delete
        Try
            'Call Trail class based on system setting

            'Dim obj as Object.clsObjectClass
            'set obj = new Object.clsObjectClass
            'with obj
            '
            'end with
            'set obj = Nothing

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw (New ExceptionMsg(ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
