﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSCatAccessRightDtl.aspx.vb"
    Inherits="Admin_SFMSCatAccessright_SFMSCatAccessRightDtl" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EL Accessright Dtl</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script src="../../include/layout.js" type="text/javascript"></script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmELAccessRightDtl" runat="server">
    <fieldset class="" style="width: 98%;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" CombineScripts="False" />
        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
        <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" Title="" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <customToolkit:wuc_lblHeader ID="lblHeader" runat="server" Title="" />
        <asp:UpdatePanel ID="updPnlARDtl" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="ContainerDiv" style="height: 100%">
                    <asp:Button ID="btnEdit" runat="server" CssClass="cls_button" Text="Edit" Visible="False">
                    </asp:Button>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="cls_button" Visible="False"
                        OnClientClick="if (confirm('Are you sure want to delete?') == false) { window.event.returnValue = false; return false; }">
                    </asp:Button>
                    <asp:Button ID="btnViewTray" runat="server" Text="Back" CssClass="cls_button"></asp:Button>
                    <br />
                    <br />
                    <div>
                        <asp:DetailsView ID="dvHdrLeft" runat="server" AutoGenerateRows="False" CellPadding="5"
                            CssClass="DV" Width="450px" meta:resourcekey="dvHdrLeftResource1">
                            <RowStyle CssClass="cls_DV_Row_MST" />
                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                Width="35%" Wrap="False" />
                            <Fields>
                                <asp:BoundField DataField="ACCESSRIGHT_NAME" HeaderText="Name" ReadOnly="True" meta:resourcekey="BoundFieldResource1" />
                                <asp:BoundField DataField="ACCESSRIGHT_NAME_1" HeaderText="Name 1" ReadOnly="True"
                                    meta:resourcekey="BoundFieldResource2" />
                                <asp:BoundField DataField="ACCESSRIGHT_DESC" HeaderText="Description" ReadOnly="True"
                                    meta:resourcekey="BoundFieldResource3" />
                                <asp:BoundField DataField="ACCESSRIGHT_DESC_1" HeaderText="Description 1" ReadOnly="True"
                                    meta:resourcekey="BoundFieldResource4" />
                            </Fields>
                        </asp:DetailsView>
                    </div>
                    <br />
                    <ccGV:clsGridView ID="dgARList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="100%" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        DataKeyNames="cat_code" BorderColor="Black" BorderWidth="1" GridBorderColor="Black"
                        GridBorderWidth="1px" meta:resourcekey="dgARListResource1" RowHighlightColor="AntiqueWhite">
                        <AlternatingRowStyle CssClass="GridAlternate" />
                        <Columns>
                            <asp:BoundField DataField="cat_code" HeaderText="Category Code" ReadOnly="True" SortExpression="cat_code"
                                Visible="True" meta:resourcekey="BoundFieldResource7">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <%--<asp:BoundField DataField="el_code" HeaderText="EL Code" ReadOnly="True" SortExpression="el_code"
                                Visible="False" meta:resourcekey="BoundFieldResource8">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>--%>
                            <asp:BoundField DataField="cat_name" HeaderText="Category" meta:resourcekey="BoundFieldResource9">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <%--<asp:BoundField DataField="el_name" HeaderText="Easy Loader Name" meta:resourcekey="BoundFieldResource10">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="action_string" HeaderText="Action" meta:resourcekey="BoundFieldResource11">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField Visible="False" DataField="action_id" HeaderText="action_id" meta:resourcekey="BoundFieldResource12">
                            </asp:BoundField>--%>
                            <asp:BoundField Visible="False" HeaderText="accessrightdtl_id" meta:resourcekey="BoundFieldResource13">
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle CssClass="GridFooter" />
                        <HeaderStyle CssClass="GridHeader" />
                        <PagerSettings Visible="False" />
                        <RowStyle CssClass="GridNormal" />
                    </ccGV:clsGridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
