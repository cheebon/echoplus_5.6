<%@ Page Language="VB" AutoEventWireup="false" CodeFile="left.aspx.vb" Inherits="iFFMR_SFA_left" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/menu/wuc_Menu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Left Menu</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
<style type="text/css">.TreeLabel {DISPLAY: block;FONT-WEIGHT: bold;WIDTH: 20px;CURSOR: pointer;COLOR: #245edc;BACKGROUND-COLOR: #b8dfff;TEXT-ALIGN: center;FONT-VARIANT: small-caps;height:100%;float:right;position:absolute;top:0px;right:0px;FILTER: progid:DXImageTransform.Microsoft.Gradient(startColorStr= '#FFFFFF' ,endColorStr= '#98BFFF' ,gradientType= '1' );}</style>
<script language="javascript" type="text/javascript">
function ToggleCollapse(){if (self.parent && self.parent.CollapseExpand){self.parent.CollapseExpand();}}
function TreeMenuChanged(){if (self.parent && self.parent.frames['fraContent'].refreshDatabind){self.parent.frames['fraContent'].refreshDatabind();}}
</script>
</head>
<body>
<form id="frmLeftMenu" runat="server">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
<asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
<ContentTemplate>
<customToolkit:wuc_Menu ID="wuc_Menu" runat="server" TreeMenu_Visibility="true" />
</ContentTemplate>
</asp:UpdatePanel>
<div class="TreeLabel" onclick="ToggleCollapse()">
<table class="TreeLabel"><tr><td> O<BR />r<BR />g<BR />a<BR />n<BR />i<BR />z<BR />a<BR />t<BR />i<BR />o<BR />n<BR /><BR />C<BR />h<BR />a<BR />r<BR />t </td></tr></table>
</div>
</form>
</body>
</html>
