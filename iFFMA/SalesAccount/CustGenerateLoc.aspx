<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustGenerateLoc.aspx.vb" Inherits="iFFMA_SalesAccount_CustGenerateLoc" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../include/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../include/jquery.cookie.js"  type="text/javascript"></script>
    <script src="../../include/Gmap/GenerateLoc.js"  type="text/javascript"></script>
    <script src="../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
</head>
<!--#include File="~/include/commonutil.js"-->
 <script  type="text/javascript">
     function toggle(chkall) {
         var GridView = document.getElementById("<%=dgList.ClientID%>");
        for (i = 1; i < GridView.rows.length; i++) {
            GridView.rows[i].cells[GridView.rows[i].cells.length - 1].getElementsByTagName("INPUT")[0].checked = chkall.checked;
        }
     }
     </script>
    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>

<body class="BckgroundInsideContentLayout">
    <form id="frmCustomerList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" >
       
    <Services>

    <asp:ServiceReference path="../../DataServices/ws_CustLoc.asmx" /> 
    </Services>
</ajaxToolkit:ToolkitScriptManager>
<div id="divCountResult" class="cls_label" style=" visibility:hidden; z-index: 30; position: absolute; top:10px;left:10px; padding: 3px; background: #5D7B9D; color: #ffffff"> </div>  
<div id="Message" class="cls_label" style="display: none; z-index: 30; position: absolute; padding: 3px; top:50px; left:10px; background: #5D7B9D; color: #ffffff"> </div>  
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
    <tr align="center">
        <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
            <fieldset class="" style="width: 98%; ">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                    <tr align="left">
                        <td>
                            <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                            <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                            <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                            <asp:Button ID="btnRegenerate" runat="server" CssClass="cls_button" style="margin-left:10px" Text="Regenerate GPS"   OnClientclick="QueryLocation();return false;" />
                            <br />
                            <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <script type="text/javascript">
                                Sys.Application.add_load(init);
                                </script>
                                <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />     
                                <asp:Panel ID="pnlList" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td> 
                                            <center>
                                                <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="" AllowPaging="True" DataKeyNames="CUST_CODE" bordercolor="Black" borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">                          
                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                <FooterStyle CssClass="GridFooter" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <PagerSettings Visible="False" />
                                                <RowStyle CssClass="GridNormal" />
                                                    <Columns>
                                                    <asp:TemplateField  HeaderText="Customer Code" SortExpression="cust_code" >
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblCustCode" runat="server"  Text='<%# Bind("cust_code")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <EditItemTemplate>
                                                        <asp:Label ID="lblCustCode" runat="server"  Text='<%# Bind("cust_code")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField  HeaderText="Customer"  SortExpression="cust_name"  >
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblCustName" runat="server"  Text='<%# Bind("cust_name")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <EditItemTemplate>
                                                        <asp:Label ID="lblCustName" runat="server"  Text='<%# Bind("cust_name")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField  HeaderText="Full Address"  SortExpression="full_add" >
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblFullAddress" runat="server"  Text='<%# Bind("full_add")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <EditItemTemplate>
                                                        <asp:Label ID="lblFullAddress" runat="server"  Text='<%# Bind("full_add")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField  HeaderText="Latitude" SortExpression="latitude" >
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblLatitude" runat="server"  Text='<%# Bind("latitude")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <EditItemTemplate>   
                                                        <asp:TextBox ID="txtLatitude" runat="server"  Text='<%# Bind("latitude")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField  HeaderText="Longitude" SortExpression="Longitude" >
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblLongitude" runat="server"  Text='<%# Bind("Longitude")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <EditItemTemplate>
                                                        <asp:TextBox ID="txtLongitude" runat="server"  Text='<%# Bind("Longitude")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField > 
                                                        <HeaderTemplate> <input type="checkbox" id="chkAll"  onclick="toggle(this)" /></HeaderTemplate>
                                                        <ItemStyle Width="20px" />
                                                        <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelected" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    </Columns>
                                                </ccGV:clsGridView>
                                            </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel> 
                                                      
                                               
                            </ContentTemplate> 
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                            
                       
                </table>
            </fieldset>
        </td>
    </tr>
</table>
      
    </form>
</body>
</html>
