﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BsadminAccDtl.aspx.vb" Inherits="Admin_Root_User_BsadminAccDtl" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BSadmin Detail Page</title>
</head>
<body>
    <form id="frmBSDtl" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />           
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 0px 15px 0px;">
                    <tr>
                        <td>
                            <div class="border_std">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="False"
                                UseSubmitBehavior="false" Width="60px" />
                            </div>
                        </td>
                    </tr>                   
                    <tr class="ajax__tab_portal">
                        <td class="ajax__tab_PNL">
                        <div class="border_std">
                            <asp:DetailsView ID="dvHdr_Left" runat="server" AutoGenerateRows="False" Width="100%"
                                DataKeyNames="USER_ID">                                                                    
                                <Fields>
                                    <asp:TemplateField HeaderText="User ID">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtUserID" runat="server" Enabled="false" Text='<%# Bind("USER_ID") %>' MaxLength="50" />                                        
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtUserID" runat="server" Enabled="false" Text='' MaxLength="50" />                                            
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%# Bind("USER_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Login">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtLogin" runat="server" Enabled="false" Text='<%# Bind("LOGIN") %>' MaxLength="50" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                           <asp:TextBox ID="txtLogin" runat="server" Enabled="false" Text='' MaxLength="50" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLogin" runat="server" Text='<%# Bind("LOGIN") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Code">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtUserCode" runat="server" Text='<%# Bind("USER_CODE") %>' MaxLength="50" Enabled="false" />
                                            <asp:RequiredFieldValidator ID="rfvSubmoduleCode" runat="server" ControlToValidate="txtUserCode"
                                                ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                ValidationGroup="ValidateDtl" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtUserCode" runat="server" Text="" MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfvSubmoduleCode" runat="server" ControlToValidate="txtUserCode"
                                                ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                ValidationGroup="ValidateDtl" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserCode" runat="server" Text='<%# Bind("USER_CODE") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("USER_NAME") %>' MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfvSubmoduleName" runat="server" ControlToValidate="txtUserName"
                                                ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                ValidationGroup="ValidateDtl" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtUserName" runat="server" Text="" MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfvSubmoduleName" runat="server" ControlToValidate="txtUserName"
                                                ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                ValidationGroup="ValidateDtl" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("USER_NAME") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Principal">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPrincipalName" runat="server" Text='<%# Bind("PRINCIPAL_NAME") %>' MaxLength="50" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtPrincipalName" runat="server" Text="" MaxLength="50" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrincipalName" runat="server" Text='<%# Bind("PRINCIPAL_NAME") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCountry" runat="server" Text='<%# Bind("COUNTRY_NAME") %>' MaxLength="100" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="txtCountry" runat="server" Text="" MaxLength="100" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("COUNTRY_NAME") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>        
                        </div>
                        </td>
                    </tr>
                  </table>    
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
