﻿Imports System.Data
Partial Class iFFMR_Customize_CustPrdSalesAnalysis_CustPrdSalesAnalysis
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_CustPrdSalesAnalysis")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_CustPrdSalesAnalysis") = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CustPrdSalesList"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region


#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")


            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CUSTPRDSALESANALYSIS) '"Sales List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CUSTPRDSALESANALYSIS
                .DataBind()
                .Visible = True
            End With
            GroupingValue = String.Empty

            CriteriaCollector = Nothing ' New clsSharedValue
            LoadPrdGrpDDL()
            'wuc_ctrlpanel.pnlField_InsertPointer(True, 0, "Team")
            TimerControl1.Enabled = True

            'Try
            'Catch ex As Exception
            '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
            'End Try
        End If

        lblErr.Text = ""
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerProductSalesList"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        If IsPostBack Then RenewDataBind()
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        GroupingValue = String.Empty
        RenewDataBind()
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub


#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)


        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        '    ViewState("dtCurrentView") = dtCurrentTable
        '    SortingExpression = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_CustPrdSalesAnalysis.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CustPrdSalesAnalysis.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CustPrdSalesAnalysis.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CustPrdSalesAnalysis.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_CustPrdSalesAnalysis.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_CustPrdSalesAnalysis.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                    strUrlFormatString = Nothing 'FormUrlFormatString(0, dtToBind, strUrlFields)
                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.InvisibleColumn

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CustPrdSalesAnalysis.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CustPrdSalesAnalysis.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CustPrdSalesAnalysis.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_CustPrdSalesAnalysis.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_CustPrdSalesAnalysis.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub

    ''' <summary>
    ''' Pass in the datatable, and generate the url format string based on the
    ''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    ''' to form the hyperlink fields need.
    ''' </summary>
    ''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    ''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    ''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    ''' <returns>URLFormatString, as url to pass with parameters</returns>
    ''' <remarks></remarks>
    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Select Case intIndex
            Case 0 'Generate sales info by date links
                Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
                Dim arrFields As New ArrayList 'store field name used in grouping
                Dim licGroupFieldList As New ListItemCollection
                licGroupFieldList.Add(New ListItem("TEAM_CODE", -1))
                licGroupFieldList.Add(New ListItem("REGION_CODE", -1))
                licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
                licGroupFieldList.Add(New ListItem("CHAIN_CODE", -1))
                licGroupFieldList.Add(New ListItem("CHANNEL_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
                licGroupFieldList.Add(New ListItem("PRD_GRP_CODE", -1))
                licGroupFieldList.Add(New ListItem("PRD_CODE", -1))
                licGroupFieldList.Add(New ListItem("SALES_AREA_CODE", -1))
                licGroupFieldList.Add(New ListItem("SHIPTO_CODE", -1))
                licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

                For Each liFieldToFind As ListItem In licGroupFieldList
                    For Each DC As DataColumn In dtToBind.Columns
                        If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
                            liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        End If
                    Next
                Next

                ReDim strUrlFields(arrFields.Count - 1)
                For intIndx As Integer = 0 To arrFields.Count - 1
                    strUrlFields(intIndx) = arrFields.Item(intIndx)
                Next

                Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
                Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value
                Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value

                ' strUrlFormatString = "SalesInfoByDate.aspx?" + _
                ' strbFilterGroup.ToString + _
                'IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
                'IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
                'IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "")

                strUrlFormatString = (" parent.document.getElementById('ContentBarIframe').src='../../iFFMR/Customize/CustSalesAnalysis/CustSalesByDate.aspx?" + _
                 strbFilterGroup.ToString + _
                IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
                IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
                IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "") + "&PAGE_INDICATOR=CUSTSALESANALYSIS'")

                Session("SalesInfoByDate_GroupingField") = arrFields
            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        Return strUrlFormatString
    End Function

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                'For Each TC As TableCell In e.Row.Cells
                '    If TC.Controls.Count > 0 Then
                '        Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                '        If HYL IsNot Nothing Then
                '            Dim strlink As String = HYL.NavigateUrl
                '            HYL.NavigateUrl = "#"
                '            HYL.Target = String.Empty
                '            HYL.Attributes.Add("onclick", strlink + ";resizeLayout2();ScrollTop();")
                '        End If
                '    End If
                'Next
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_CustPrdSalesAnalysis.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next

                If (aryDataItem.IndexOf("MTDVAR")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("MTDVAR")).Text = String.Format(CF_CustPrdSalesAnalysis.GetOutputFormatString("MTDVAR"), (DIVISION(licItemFigureCollector.FindByText("MTD_SALES"), licItemFigureCollector.FindByText("MTD_TGT")) * 100))
                If (aryDataItem.IndexOf("QTDVAR")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("QTDVAR")).Text = String.Format(CF_CustPrdSalesAnalysis.GetOutputFormatString("QTDVAR"), (DIVISION(licItemFigureCollector.FindByText("QTD_SALES"), licItemFigureCollector.FindByText("QTD_TGT")) * 100))
                If (aryDataItem.IndexOf("YTDVAR")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("YTDVAR")).Text = String.Format(CF_CustPrdSalesAnalysis.GetOutputFormatString("YTDVAR"), (DIVISION(licItemFigureCollector.FindByText("YTD_SALES"), licItemFigureCollector.FindByText("YTD_TGT")) * 100))

                Dim dblYTDGROSS As Double = (DIVISION(licItemFigureCollector.FindByText("YTD_SALES"), licItemFigureCollector.FindByText("PYTD_SALES")) * 100)
                dblYTDGROSS = IIf(IsNumeric(licItemFigureCollector.FindByText("PYTD_SALES").Value) AndAlso licItemFigureCollector.FindByText("PYTD_SALES").Value > 0, dblYTDGROSS - 100, 0)
                If (aryDataItem.IndexOf("YTDGROSS")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("YTDGROSS")).Text = String.Format(CF_CustPrdSalesAnalysis.GetOutputFormatString("YTDGROSS"), dblYTDGROSS)

        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Dim intNetValue As Integer
        Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalId = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

        Dim strGroupCode, strGroupName, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupName = strGroupCode.Replace("_CODE", " (Tree)").Replace("_", " ")
        strSalesRepList = Session("SALESREP_LIST")

        Dim strPointerText As String
        strPointerText = strGroupName 'IIf(String.IsNullOrEmpty(strTeamCode), "Team", IIf(String.IsNullOrEmpty(strRegionCode), "Region", "Sales Rep."))
        'Add in salesrep_code when first visit, or when grouping is nothing
        If (IsPostBack = False AndAlso wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False) OrElse _
            (wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow) = 0) Then
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        End If

        Dim strGroupField As String
        'Remark: GroupingValue is just to keep history GroupingValue passed back from previous page.
        'Will be delete after RenewDatabing() excecuted
        If String.IsNullOrEmpty(GroupingValue) Then
            strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue + ",CUST_CODE,PRD_CODE"
        Else
            strGroupField = GroupingValue
            GroupingValue = String.Empty
        End If

        Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        strGroupField = strGroupField.Replace(strPointer, "TREE_" & strGroupCode)
        wuc_ctrlpanel.pnlField_EditPointerText(strGroupName)

        Dim strSearchCustType As String, strSearchCustValue As String, strSearchPrdType As String, strSearchPrdValue As String, strPrdGrpCode As String
        strSearchCustType = rblSearchCustType.SelectedValue
        strSearchCustValue = txtSearchCustValue.Text
        strPrdGrpCode = ddlprdgrp.SelectedValue
        strSearchPrdType = rblSearchPrdType.SelectedValue
        strSearchPrdValue = txtSearchPrdValue.Text

        Dim clsSalesDB As New rpt_Customize.clsCustPrdSalesAnalysis
        Try
            DT = clsSalesDB.GetCustPrdSalesAnalysis(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField, intNetValue, strSearchCustType, strSearchCustValue, strPrdGrpCode, strSearchPrdType, strSearchPrdValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD*" Then
                strColumnName = "MTD Sales"
            ElseIf strColumnName Like "QTD*" Then
                strColumnName = "QTD Sales"
            ElseIf strColumnName Like "YTD*" Then
                If strColumnName Like "YTDGROSS" Then
                    strColumnName = "PYTD Sales"
                Else
                    strColumnName = "YTD Sales"
                End If
            ElseIf strColumnName Like "PYTD*" Then
                strColumnName = "PYTD Sales"
            Else
                strColumnName = CF_CustPrdSalesAnalysis.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "MTD*" OrElse _
             strColumnName Like "QTD*" OrElse _
             strColumnName Like "YTD*" OrElse strColumnName Like "PYTD*") _
             AndAlso Not (strColumnName Like "*VAR" OrElse strColumnName Like "*GROSS") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        RenewDataBind()
    End Sub

    Private Sub LoadPrdGrpDDL()
        Dim DT As DataTable
        Dim clsSalesDB As New rpt_Customize.clsCustPrdSalesAnalysis

        DT = clsSalesDB.GetCustPrdSalesAnalysisPrdGrp(Session.Item("UserID"))
        With ddlprdgrp
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "PRD_GRP_NAME"
            .DataValueField = "PRD_GRP_CODE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With

    End Sub

End Class

Public Class CF_CustPrdSalesAnalysis
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES"
                strFieldName = "Sales"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY"
                strFieldName = "FOC"
            Case "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strFieldName = "QTY"
            Case "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strFieldName = "Target"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strFieldName = "+/-%"
            Case "TREE_PRINCIPAL_NAME"
                strFieldName = "Team(Tree)"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            'Case "MTD_SALES"
            '    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESINFOBYDATE, "'1','8'") Then
            '        FCT = FieldColumntype.HyperlinkColumn
            '    End If

            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                FCT = FieldColumntype.TemplateColumn_Percentage
        End Select
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class