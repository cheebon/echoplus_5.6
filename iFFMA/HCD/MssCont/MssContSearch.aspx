<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MssContSearch.aspx.vb" Inherits="iFFMA_HCD_MssCont_MssContSearch" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mss Contact </title>
    <link href="~/include/DKSH.css" rel="stylesheet" />


<script language="javascript" type="text/javascript">
    function RefreshContentBar()
    {var txtSalesrepCode = document.getElementById("txtSalesrepCode"); var SALESREP_CODE = txtSalesrepCode.value;
    var txtTxnNo = document.getElementById("txtTxnNo"); var TXN_NO = txtTxnNo.value;
    var txtCustCode = document.getElementById("txtCustCode"); var CUST_CODE = txtCustCode.value;
    var ddlstatus = document.getElementById("ddlstatus"); var STATUS = ddlstatus.value;
    var txtContCode = document.getElementById("txtContCode"); var CONT_CODE = txtContCode.value;
     var ddltitlecode = document.getElementById("ddltitlecode"); var TITLE_CODE = ddltitlecode.value;
     var txtTxnDate_txtDateStart = document.getElementById("txtTxnDate_txtDateStart"); var DATE_START = txtTxnDate_txtDateStart.value;
     var txtTxnDate_txtDateEnd = document.getElementById("txtTxnDate_txtDateEnd"); var DATE_END = txtTxnDate_txtDateEnd.value;
    parent.document.getElementById('ContentBarIframe').src="../../iFFMA/HCD/MssCont/MssContList.aspx?SALESREP_CODE=" + SALESREP_CODE + "&TXN_NO=" + TXN_NO+ "&CUST_CODE=" + CUST_CODE + "&STATUS=" + STATUS + "&CONT_CODE=" + CONT_CODE + "&TITLE_CODE=" + TITLE_CODE + "&DATE_START=" + DATE_START + "&DATE_END=" + DATE_END;
    }
</script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frm" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                            align="left">
                            <tr align="left">
                                <td align="left">
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Salesrep Code</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="cls_label_header">Transaction Date</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <customToolkit:wuc_txtCalendarRange ID="txtTxnDate" runat="server" RequiredValidation="false"
                                                            RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Customer Code</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="cls_label_header">Status</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlstatus" runat="server" CssClass="cls_dropdownlist">
                                                            <asp:ListItem Text="All" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="New" Value="NEW"></asp:ListItem>
                                                            <asp:ListItem Text="Updated" Value="UPDATED"></asp:ListItem>
                                                            <asp:ListItem Text="Approved" Value="APPROVED"></asp:ListItem>
                                                        </asp:DropDownList>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Contact Code</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtContCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="cls_label_header">Title Code</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddltitlecode" runat="server" CssClass="cls_dropdownlist">
                                                            <asp:ListItem Text="All" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Add" Value="Add"></asp:ListItem>
                                                            <asp:ListItem Text="Update" Value="Update"></asp:ListItem>
                                                            <asp:ListItem Text="Remove" Value="Remove"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Txn No</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtTxnNo" runat="server" CssClass="cls_textbox" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" align="left">
                                                        <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                                        <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                                            value="Search" class="cls_button" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
