<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustLst.aspx.vb" Inherits="iFFMS_Customer_CustomerList_CustLst"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="uc1" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Listing</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function ClickBtn() {
            __doPostBack('btnrefresh');
            setTimeout('GetScrollPosition();', 100);
        }

        function SetScrollPosition() {
            var dgListQues = document.getElementById('div_gridview1');
            var scrollPosition = dgListQues.scrollTop;
            var hdfScrollPosition = document.getElementById('hdfScrollPosition');
            //alert(scrollPosition);
            if (hdfScrollPosition) {
                hdfScrollPosition.value = scrollPosition;
            }
            dgListQues.scrollTop = 0;
        }
        function GetScrollPosition() {
            var hdfScrollPosition = document.getElementById('hdfScrollPosition');
            if (hdfScrollPosition) {
                var scrollPosition = hdfScrollPosition.value;
            }
            var dgListQues = document.getElementById('div_gridview1');
            //alert(scrollPosition);
            if (scrollPosition) {
                dgListQues.scrollTop = parseInt(scrollPosition);
            }
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ResizeFrameWidth('TopBar','100%');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');HideElement('ContentBar');">
    <form id="frmCustomerListing" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <fieldset class="" style="width: 98%">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                                                width="100%" border="0" style="height: 30px">
                                                <tr align="left" valign="bottom">
                                                    <td>
                                                        <asp:Image ID="imgExpandCollapse" ImageUrl="~/images/ico_Field.gif" runat="server"
                                                            CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pSearchCriteria" Width="100%" runat="server" CssClass="cls_ctrl_panel">
                                                        <table class="cls_panel" cellspacing="0" border="0" cellpadding="1" width="100%">
                                                            <tr align="left" valign="bottom">
                                                                <td valign="top">
                                                                    <span class="cls_label_header">Search By</span>
                                                                </td>
                                                                <td valign="top">
                                                                    <span class="cls_label_header">:</span>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlsearchby" runat="server" CssClass="cls_dropdownlist" Width="144px">
                                                                        <asp:ListItem Text="All" Value="ALL" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="Customer Name" Value="CUST_NAME"></asp:ListItem>
                                                                        <asp:ListItem Text="Customer Code" Value="CUST_CODE"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr align="left" valign="bottom">
                                                                <td valign="top" width="10%">
                                                                    <span class="cls_label_header">Search Value</span>
                                                                </td>
                                                                <td valign="top">
                                                                    <span class="cls_label_header">:</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtsearch" runat="server" CssClass="cls_textbox" Width="200px" MaxLength="100"> </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr align="left" valign="bottom">
                                                                <td>
                                                                    <asp:Button ID="btnCust_Refresh" CssClass="cls_button" runat="server" Text="Search"
                                                                        ValidationGroup="CustSearch" OnClientClick="HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');MaximiseFrameHeight('TopBarIframe');">
                                                                    </asp:Button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPESearchCriteria" runat="server" TargetControlID="pSearchCriteria"
                                                        ExpandControlID="imgExpandCollapse" CollapseControlID="imgExpandCollapse" Collapsed="false">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:Panel ID="PdgList" Width="98%" runat="server">
                                                <asp:UpdatePanel ID="UpdateRoute" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                           <tr align="left" >
                                                        <td valign="top" width="10%">
                                                            <span class="cls_label_header">Route</span>
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlRoute" runat="server" AutoPostBack="True" CssClass="cls_dropdownlist"
                                                                Width="144px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                    <br />
                                                 </ContentTemplate>
                                            </asp:UpdatePanel>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="Bckgroundreport" colspan="3">
                                                            <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                <ContentTemplate>
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                                    </asp:Timer>
                                                                    <div style="display: none;">
                                                                        <asp:Button ID="btnrefresh" runat="server" CssClass="cls_button" Text="Refresh" />
                                                                    </div>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <ccGV:clsGridView ID="gridview1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                    Width="98%" FreezeHeader="True" GridHeight="400px" AddEmptyHeaders="0" CellPadding="2"
                                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                    DataKeyNames="CUST_CODE">
                                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                                    <EmptyDataTemplate>
                                                                                        There is no data to display.</EmptyDataTemplate>
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="IND" HeaderText="Ind" SortExpression="IND">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:BoundField>
                                                                                        <asp:HyperLinkField DataNavigateUrlFields="CUST_CODE" DataNavigateUrlFormatString="#"
                                                                                            DataTextField="CUST_CODE" HeaderText="Customer Code" SortExpression="CUST_CODE">
                                                                                        </asp:HyperLinkField>
                                                                                        <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" SortExpression="CUST_NAME">
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="XFIELD1" HeaderText="XField1" SortExpression="XFIELD1">
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="XFIELD2" HeaderText="XField2" SortExpression="XFIELD2">
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Visit Customer">
                                                                                            <ItemTemplate>
                                                                                                <asp:Image ID="imgButton" runat="server" ImageUrl="../../../images/ico_edit.gif"
                                                                                                    Height='18px' Width='18px'></asp:Image>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--  <asp:ButtonField Text="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                                                            HeaderText="Visit Customer" CommandName="Visit">
                                                                                            <itemstyle horizontalalign="Center" />
                                                                                        </asp:ButtonField>--%>
                                                                                    </Columns>
                                                                                    <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                                                    <FooterStyle CssClass="GridFooter" />
                                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                    <RowStyle CssClass="GridNormal" />
                                                                                </ccGV:clsGridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;
                                            <asp:HiddenField ID="hdfScrollPosition" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
    </form>
</body>
</html>
