﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallRateByBrandSearch.aspx.vb" Inherits="iFFMR_Customize_CallRateByBrand_CallRateByBrandSearch" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <title>Call Rate By Brand Report</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallRateByBrand" runat="server">
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                                align="left">
                                <tr align="left">
                                    <td align="left" colspan="2">
                                        <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                        <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <table>
                                            <tr>
                                                <td>
                                                    <span class="cls_label_header">Year</span>
                                                </td>
                                                <td>
                                                    <span class="cls_label_header">:</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlyear" runat="server" CssClass="cls_dropdownlist" />
                                                </td>
                                                <td>
                                                    <span class="cls_label_header">Month</span>
                                                </td>
                                                <td>
                                                    <span class="cls_label_header">:</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset>
                                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                                            align="left">
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblTeam" class="cls_label_header">Sales Team</span><br />
                                                                <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br />
                                                                <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblChannel" class="cls_label_header">Channel</span><br />
                                                                <asp:ListBox ID="lsbChannel" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddChannel" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveChannel" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllChannel" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllChannel" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedChannel" class="cls_label_header">Selected Channel</span><br />
                                                                <asp:ListBox ID="lsbSelectedChannel" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblSpecialty" class="cls_label_header">Specialty</span><br />
                                                                <asp:ListBox ID="lsbSpecialty" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddSpecialty" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveSpecialty" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllSpecialty" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllSpecialty" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedSpecialty" class="cls_label_header">Selected Specialty</span><br />
                                                                <asp:ListBox ID="lsbSelectedSpecialty" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblCriteria" class="cls_label_header">Criteria</span><br />
                                                                <asp:ListBox ID="lsbCriteria" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddCriteria" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveCriteria" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllCriteria" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllCriteria" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedCriteria" class="cls_label_header">Selected Criteria</span><br />
                                                                <asp:ListBox ID="lsbSelectedCriteria" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        </fieldset>
                                    </td>
                                    <td>
                                        <fieldset>
                                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                                            align="left">
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblSalesrep" class="cls_label_header">Field Force</span><br />
                                                                <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br />
                                                                <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblDistrict" class="cls_label_header">District</span><br />
                                                                <asp:ListBox ID="lsbDistrict" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddDistrict" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveDistrict" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllDistrict" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllDistrict" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedDistrict" class="cls_label_header">Selected District</span><br />
                                                                <asp:ListBox ID="lsbSelectedDistrict" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblCustClass" class="cls_label_header">Customer Class</span><br />
                                                                <asp:ListBox ID="lsbCustClass" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddCustClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveCustClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllCustClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllCustClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedCustClass" class="cls_label_header">Selected Customer Class</span><br />
                                                                <asp:ListBox ID="lsbSelectedCustClass" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="left">
                                                        <tr>
                                                            <td valign="top" align="center" style="width: 45%;">
                                                                <span id="lblPrdClass" class="cls_label_header">Product Class</span><br />
                                                                <asp:ListBox ID="lsbPrdClass" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                            <td valign="middle" align="center" style="width: 10%;">
                                                                <table>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddPrdClass" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemovePrdclass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkAddAllPrdClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button ID="lnkRemoveAllPrdClass" runat="server" CssClass="cls_button" Width="35"
                                                                                Text="<<" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center" style="width: 35%">
                                                                <span id="lblSelectedPrdClass" class="cls_label_header">Selected Product Class</span><br />
                                                                <asp:ListBox ID="lsbSelectedPrdClass" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" />
                                        <%-- <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                        value="Search" class="cls_button" />--%>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate> 
                        </asp:UpdatePanel>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
