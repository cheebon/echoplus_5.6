<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccessRightCreate.aspx.vb" Inherits="Admin_AccessRight_AccessRightCreate" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register Src="../../include/wuc_ddlCountry.ascx" TagName="wuc_ddlCountry" TagPrefix="uc2" %>
<%@ Register Src="../../include/wuc_ddlPrincipal.ascx" TagName="wuc_ddlPrincipal" TagPrefix="uc5" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Creating Access Right</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->  
<body class="BckgroundInsideContentLayout">
    <form id="frmAccessRightCreate" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
    <div>  
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                   <ContentTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
							                        <td width="20%"><asp:label id="lblName" runat="server" CssClass="cls_label_header">Name</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtName" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
								                        <asp:RequiredFieldValidator id="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lblDupError" runat="server" Text="Name already exists!" Visible="false" CssClass="cls_validator"></asp:Label>
								                    </td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblName1" runat="server" CssClass="cls_label_header">Name 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot2" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtName1" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblDesc" runat="server" CssClass="cls_label_header">Description</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtDesc" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300px"></asp:TextBox></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblDesc1" runat="server" CssClass="cls_label_header">Description 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtDesc1" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300px"></asp:TextBox></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCountry" runat="server" CssClass="cls_label_header">Country</asp:label><asp:label id="lblMark8" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><uc2:wuc_ddlCountry ID="Wuc_ddlCountry" runat="server" /></td>
							                    </tr>
							                    <tr>
						                            <td width="20%"><asp:label id="lblPrincipal" runat="server" CssClass="cls_label_header">Principal</asp:label><asp:label id="lblMark9" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><uc5:wuc_ddlPrincipal id="Wuc_ddlPrincipal" runat="server"></uc5:wuc_ddlPrincipal></td>
						                        </tr>	
						                    </table>
				                        </td>
			                        </tr>
			                        <tr><td>&nbsp;</td></tr>
			                        <tr class="Bckgroundreport">
				                        <td>
					                        <%--<table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
					                                <td vAlign="top" width="20%">       
										                <ccGV:clsGridView ID="dgModuleList" runat="server" ShowFooter="false" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                            FreezeRows="0" GridWidth="" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="module_id">
                                                            <Columns>
                                                                <asp:BoundField DataField="module_id" HeaderText="Module ID" ReadOnly="True" SortExpression="module_id" visible="false">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>    
                                                                <asp:TemplateField>
                                                                    <HeaderStyle Width="3%"></HeaderStyle>
													                <ItemTemplate>
														                <asp:CheckBox ID="chkModule" Runat="server" CssClass="cls_checkbox"></asp:CheckBox>
													                </ItemTemplate>
                                                                </asp:TemplateField>
											                    <asp:ButtonField DataTextField="module_name" HeaderText="Module" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" />                                                                                                                                                      
                                                            </Columns>                                                
                                                        </ccGV:clsGridView>									                
					                                </td>
					                                <td vAlign="top" width="50%">
					                                     <ccGV:clsGridView ID="dgSubModuleList" runat="server" ShowFooter="false" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                            FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="submodule_id">
                                                            <Columns>
                                                                <asp:BoundField DataField="module_id" HeaderText="Module ID" ReadOnly="True" SortExpression="module_id" visible="false">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>   
                                                                <asp:BoundField DataField="submodule_id" HeaderText="Sub Module ID" ReadOnly="True" SortExpression="submodule_id" visible="false">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>                                                                   
											                    <asp:ButtonField CommandName="Select" AccessibleHeaderText="Select" text="Select" ButtonType="Link" />                                                                                                                                                      
											                    <asp:BoundField DataField="submodule_name" HeaderText="Sub Module">
												                    <HeaderStyle Width="50%"></HeaderStyle>
											                    </asp:BoundField>
											                    <asp:BoundField DataField="action_string" HeaderText="Action">
												                    <HeaderStyle Width="35%"></HeaderStyle>
											                    </asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="action_id" HeaderText="action_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" HeaderText="accessright_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="country_id" HeaderText="country_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="principal_id" HeaderText="principal_id"></asp:BoundField>
                                                            </Columns>                                                
                                                        </ccGV:clsGridView>			
					                                </td>
					                                <td vAlign="top">
					                                     <table width="100%">
										                    <tr>
											                    <td vAlign="top"><asp:checkboxlist id="chkAction" runat="server" CssClass="cls_listbox"></asp:checkboxlist></td>
											                    <td vAlign="top">
												                    <table width="100%" border="0">
													                    <tr>
														                    <td><asp:button id="btnSelAll" runat="server" CssClass="cls_button" Width="72px" Text="Select All"
																                    CausesValidation="False"></asp:button></td>
													                    </tr>
													                    <tr>
														                    <td><asp:button id="btnClearAll" runat="server" CssClass="cls_button" Width="72px" Text="Clear All"
																                    CausesValidation="False"></asp:button></td>
													                    </tr>
													                    <tr>
														                    <td><asp:button id="btnSet" runat="server" CssClass="cls_button" Width="73px" Text="Set" CausesValidation="False"></asp:button></td>
													                    </tr>
												                    </table>
											                    </td>
										                    </tr>
									                    </table>
					                                </td>
					                            </tr> 
					                        </table>--%> 
					                    </td> 
					                </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                            <asp:TextBox id="txtAccessRightID" runat="server" CssClass="cls_textbox" Visible="false"></asp:TextBox>
				                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:button id="btnReset" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="False" Visible="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
                     </ContentTemplate>
                     </asp:UpdatePanel>
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
    </div>
    </form>
</body>
</html>
