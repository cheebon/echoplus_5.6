﻿Imports System.Data

Partial Class iFFMA_AR_CompBUPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property CompCode() As String
        Get
            Return Trim(hdCompCode.Value)
        End Get
        Set(ByVal value As String)
            hdCompCode.Value = value
        End Set
    End Property


    Public Property BUCode() As String
        Get
            Return Trim(hdBUCode.Value)
        End Get
        Set(ByVal value As String)
            hdBUCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadBUDDL(ByVal DT As DataTable)
        Dim dtBU As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlBUCode As DropDownList = CType(DetailsView1.FindControl("ddlBUCode"), DropDownList)
            If ddlBUCode IsNot Nothing Then
                dtBU = clsCommon.GetBUDDL
                With ddlBUCode
                    .Items.Clear()
                    .DataSource = dtBU.DefaultView
                    .DataTextField = "BU_NAME"
                    .DataValueField = "BU_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("BU_CODE")), "", DT.Rows(0)("BU_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsCompBU As New mst_AR.clsCompBU

            DT = clsCompBU.GetCompBUDetails(CompCode, BUCode)
       
            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadBUDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtCompCode As TextBox = CType(DetailsView1.FindControl("txtCompCode"), TextBox)
            Dim ddlBUCode As DropDownList = CType(DetailsView1.FindControl("ddlBUCode"), DropDownList)
            Dim txtCreConArea As TextBox = CType(DetailsView1.FindControl("txtCreConArea"), TextBox)
            Dim txtBankBACode As TextBox = CType(DetailsView1.FindControl("txtBankBACode"), TextBox)
            Dim txtBankPCCode As TextBox = CType(DetailsView1.FindControl("txtBankPCCode"), TextBox)
            Dim txtBankBizPlaceCode As TextBox = CType(DetailsView1.FindControl("txtBankBizPlaceCode"), TextBox)
            Dim txtARBACode As TextBox = CType(DetailsView1.FindControl("txtARBACode"), TextBox)
            Dim txtARCCCode As TextBox = CType(DetailsView1.FindControl("txtARCCCode"), TextBox)

            Dim clsCompBU As New mst_AR.clsCompBU

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsCompBU.UpdateCompBU(CompCode, BUCode, Trim(txtCreConArea.Text), Trim(txtBankBACode.Text), Trim(txtBankPCCode.Text), Trim(txtBankBizPlaceCode.Text), Trim(txtARBACode.Text), Trim(txtARCCCode.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                DT = clsCompBU.CreateCompBU(Trim(txtCompCode.Text), Trim(ddlBUCode.SelectedValue), Trim(txtCreConArea.Text), Trim(txtBankBACode.Text), Trim(txtBankPCCode.Text), Trim(txtBankBizPlaceCode.Text), Trim(txtARBACode.Text), Trim(txtARCCCode.Text))

                Dim isDuplicate As Integer
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Record already exists!"
                    CompCode = ""
                    BUCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else

                    lblInfo.Text = "The record is successfully created."
                    'BindDetailsView()
                    CompCode = txtCompCode.Text.ToString
                    BUCode = ddlBUCode.SelectedValue.ToString
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class