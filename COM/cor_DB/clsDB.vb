Imports System.Diagnostics
Imports System.Data.SqlClient
Imports cor_DB
Imports System.data
'Imports System.EnterpriseServices
'Imports System.Reflection
'Imports System.Runtime.InteropServices
'Imports System.Runtime.CompilerServices

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), Transaction(TransactionOption.Required)> _
'<Transaction(TransactionOption.Required)> _
Public Class clsDB
    'Inherits ServicedComponent

    Private objEventLog As New EventLog("Application", ".", ".Net Version 2003")
    Private objDB As New cor_DB.clsConnection
    Private mTableName As String
    Private mFieldList As String
    Private mFieldValue As String
    Private mFieldType As String
    Private mFilter As String
    Private mCmdText As String
    
    Enum DataType
        DBInt = 1
        DBString = 2
        DBDatetime = 3
        DBLong = 4
        DBDouble = 5
        DBByte = 6
    End Enum

    'Const DBInt As Integer = 1
    'Const DBString As Integer = 2
    'Const DBDatetime As Integer = 3
    'Const DBLong As Integer = 4
    'Const DBDouble As Integer = 5

    Public Property ConnectionString() As String
        Get
            Return objDB.ConnectionString
        End Get
        Set(ByVal Value As String)
            objDB.ConnectionString = Value
        End Set
    End Property

    Public Property TableName() As String
        Get
            Return mTableName
        End Get
        Set(ByVal Value As String)
            mTableName = Value
        End Set
    End Property

    Public Property CmdText() As String
        Get
            Return mCmdText
        End Get
        Set(ByVal Value As String)
            mCmdText = Value
        End Set
    End Property

    Public Sub addItem(ByVal strFieldName As String, ByVal strFieldValue As String, ByVal intFieldType As Integer, Optional ByVal blnParseExact As Boolean = False)
        Try
            If IsDBNull(strFieldValue) Then strFieldValue = ""
            'If IsNothing(strFieldValue) Then strFieldValue = ""
            If blnParseExact = False Then
                strFieldValue = Replace(strFieldValue, "*", "%")
                strFieldValue = Replace(strFieldValue, "'", "''")
                strFieldValue = Replace(strFieldValue, "~", "&")
                strFieldValue = Replace(strFieldValue, "\", "\\")
            End If
            If mFieldList = "" Then
                mFieldType = intFieldType
                mFieldList = strFieldName
                If intFieldType = DataType.DBString Or intFieldType = DataType.DBDatetime Then
                    If intFieldType = DataType.DBDatetime And mFieldValue = "" Then
                        mFieldValue = "NULL"
                    Else
                        mFieldValue = "'" & strFieldValue & "'"
                    End If
                Else
                    mFieldValue = strFieldValue
                End If
            Else
                mFieldType = mFieldType & "|*" & intFieldType
                mFieldList = mFieldList & "|*" & strFieldName
                If intFieldType = DataType.DBString Or intFieldType = DataType.DBDatetime Then
                    If intFieldType = DataType.DBDatetime And mFieldValue = "" Then
                        mFieldValue = mFieldValue & "|*" & "NULL"
                    Else
                        mFieldValue = mFieldValue & "|*" & "'" & strFieldValue & "'"
                    End If
                Else
                    mFieldValue = mFieldValue & "|*" & strFieldValue
                End If
            End If
            objDB.addInputParam(strFieldName, strFieldValue, intFieldType)

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.addItem : " + ex.ToString))
        End Try
    End Sub

    Public Sub addItemByte(ByVal strFieldName As String, ByVal strFieldValue As Byte(), ByVal intFieldType As Integer, Optional ByVal blnParseExact As Boolean = False)
        Try

            objDB.addInputParamByte(strFieldName, strFieldValue, intFieldType)

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.addItemByte : " + ex.ToString))
        End Try
    End Sub

      
    Public Sub addOutput(ByVal strFieldName As String, Optional ByVal intFieldType As Integer = 2)
        Try
            objDB.addOutputParam(strFieldName, intFieldType)
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.addOutput : " + ex.ToString))
        End Try
    End Sub

    Public Sub addFilter(ByVal strFieldName As String, ByVal strFieldValue As String, ByVal strOperator As String, ByVal intFieldType As Integer)
        Dim strReplaceFieldValue As String

        Try
            strReplaceFieldValue = strFieldValue
            strFieldValue = "@" & strFieldName

            If intFieldType = DataType.DBString Or intFieldType = DataType.DBDatetime Then
                If mFilter = "" Then
                    If intFieldType = DataType.DBDatetime And strFieldValue = "" Then
                        mFilter = strFieldName & " " & strOperator & " " & "NULL"
                    Else
                        mFilter = strFieldName & " " & strOperator & " " & "" & strFieldValue & ""
                    End If
                Else
                    If intFieldType = DataType.DBDatetime And strFieldValue = "" Then
                        mFilter = mFilter & " AND " & strFieldName & " " & strOperator & " " & "NULL"
                    Else
                        mFilter = mFilter & " AND " & strFieldName & " " & strOperator & " " & "" & strFieldValue & ""
                    End If
                End If
            Else
                If mFilter = "" Then
                    mFilter = strFieldName & " " & strOperator & " " & strFieldValue
                Else
                    mFilter = mFilter & " AND " & strFieldName & " " & strOperator & " " & strFieldValue
                End If
            End If
            objDB.addInputParam(strFieldName, strReplaceFieldValue, intFieldType)

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.addFilter : " + ex.ToString))
        End Try
    End Sub

    Private Function InValidSQL() As Boolean
        If mTableName = "" Or mFieldList = "" Then
            InValidSQL = True
        Else
            InValidSQL = False
        End If
    End Function

    '-----------------------------------
    '   Function for Text
    '-----------------------------------

    '<AutoComplete()> _
    Public Function Insert() As Long
        Dim dr As SqlDataReader

        Try
            If InValidSQL() Then
                Exit Function
            End If

            Dim arr As Array
            Dim str As String

            str = ""
            arr = Split(mFieldList, "|*")
            For i As Integer = 0 To UBound(arr)
                str &= "@" & arr(i) & "," '"?,"
            Next
            str = Mid(str, 1, str.Length - 1)

            'objDB.CmdText = "INSERT " & mTableName & "(" & Replace(mFieldList, "|*", ",") & ")" & " VALUES(" & Replace(mFieldValue, "|*", ",") & ")" & ";SELECT @@IDENTITY;"
            objDB.CmdText = "INSERT " & mTableName & "(" & Replace(mFieldList, "|*", ",") & ")" & " VALUES(" & str & ")" & ";SELECT @@IDENTITY;"

            dr = objDB.ExecuteReader

            If dr.Read Then
                Insert = dr.Item(0)
            Else
                Insert = 0
            End If
            dr.Close()

            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.Insert :" + mCmdText + ":" + ex.ToString))
        Finally
            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing
        End Try
    End Function

    '<AutoComplete()> _
    Public Sub Update()
        Dim strSQL As String = ""
        Dim intLoop As Integer
        Dim aryField, aryValue As Array

        Try
            If InValidSQL() Then
                Exit Sub
            End If
            aryField = Split(mFieldList, "|*")
            aryValue = Split(mFieldValue, "|*")
            For intLoop = 0 To UBound(aryField)
                If strSQL = "" Then
                    strSQL = strSQL & aryField(intLoop) & "=" & "@" & aryField(intLoop) 'aryValue(intLoop)
                Else
                    strSQL = strSQL & "," & aryField(intLoop) & "=" & "@" & aryField(intLoop) 'aryValue(intLoop)
                End If
            Next

            strSQL = "UPDATE " & mTableName & " SET " & strSQL
            If mFilter <> "" Then
                strSQL = strSQL & " WHERE " & mFilter
            End If
            objDB.CmdText = strSQL
            objDB.ExecuteNonQuery()

            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.Update : " + mCmdText + ":" + ex.ToString))
        Finally
            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing
        End Try
    End Sub

    '<AutoComplete()> _
    Public Sub Delete()
        Dim strSQL As String

        Try
            If mTableName = "" Then
                Exit Sub
            End If

            strSQL = "DELETE FROM " & mTableName
            If mFilter <> "" Then
                strSQL = strSQL & " WHERE " & mFilter
            End If
            objDB.CmdText = strSQL
            objDB.ExecuteNonQuery()

            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.Delete : " + mCmdText + ":" + ex.ToString))
        Finally
            mTableName = ""
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing
        End Try
    End Sub

    '<AutoComplete()> _
    Public Function Retrieve(ByVal pSQL As String) As DataTable
        Try
            objDB.CmdText = pSQL
            Retrieve = objDB.ExecuteReturnDT()
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.Retrieve : " + mCmdText + ":" + ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '-----------------------------------
    '   Function for Stored Procedure
    '-----------------------------------

    '<AutoComplete()> _
    Public Function spInsert() As Long
        'Dim objDB As New cor_DB.clsConnection
        Dim dr As SqlDataReader
        'Dim intLoop As Integer
        'Dim aryField, aryValue, aryType As Array

        Try
            If mCmdText = "" Or mFieldList = "" Then
                Exit Function
            End If

            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText

                'aryField = Split(mFieldList, "|*")
                'aryValue = Split(mFieldValue, "|*")
                'aryType = Split(mFieldType, "|*")
                'For intLoop = 0 To UBound(aryField)
                '    If Trim(aryType(intLoop)) = "2" Then
                '        aryValue(intLoop) = Mid(aryValue(intLoop), 2, Len(aryValue(intLoop)) - 2)
                '    End If
                '    .addInputParam(aryField(intLoop), aryValue(intLoop), aryType(intLoop))
                'Next
                dr = objDB.ExecuteReader
            End With
            objDB = Nothing

            If dr.Read Then
                spInsert = dr.Item(0)
            Else
                spInsert = 0
            End If
            dr.Close()

            mFieldList = ""
            mFieldValue = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spInsert :" + mCmdText + ":" + ex.ToString))
        Finally
            mFieldList = ""
            mFieldValue = ""
            objDB = Nothing
        End Try
    End Function

    '<AutoComplete()> _
    Public Sub spUpdate()
        'Dim objDB As New cor_DB.clsConnection
        'Dim intLoop As Integer
        'Dim aryField, aryValue, aryType As Array

        Try
            If mCmdText = "" Or mFieldList = "" Then
                Exit Sub
            End If

            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText

                'aryField = Split(mFieldList, "|*")
                'aryValue = Split(mFieldValue, "|*")
                'aryType = Split(mFieldType, "|*")
                'For intLoop = 0 To UBound(aryField)
                '    If Trim(aryType(intLoop)) = "2" Then
                '        aryValue(intLoop) = Mid(aryValue(intLoop), 2, Len(aryValue(intLoop)) - 2)
                '    End If
                '    .addInputParam(aryField(intLoop), aryValue(intLoop), aryType(intLoop))
                'Next
                .ExecuteNonQuery()
            End With
            objDB = Nothing

            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spUpdate : " + mCmdText + ":" + ex.ToString))
        Finally
            mFieldList = ""
            mFieldValue = ""
            mFilter = ""
            objDB = Nothing
        End Try
    End Sub

    '<AutoComplete()> _
    Public Sub spDelete()
        'Dim objDB As New cor_DB.clsConnection
        'Dim intLoop As Integer
        'Dim aryField, aryValue, aryType As Array

        Try
            If mCmdText = "" Or mFieldList = "" Then
                Exit Sub
            End If

            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText

                'aryField = Split(mFieldList, "|*")
                'aryValue = Split(mFieldValue, "|*")
                'aryType = Split(mFieldType, "|*")
                'For intLoop = 0 To UBound(aryField)
                '    .addInputParam(aryField(intLoop), aryValue(intLoop), aryType(intLoop))
                'Next
                .ExecuteNonQuery()
            End With
            objDB = Nothing

            mFieldList = ""
            mFieldValue = ""
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spDelete :" + mCmdText + ":" + ex.ToString))
        Finally
            mFieldList = ""
            mFieldValue = ""
            objDB = Nothing
        End Try
    End Sub

    '<AutoComplete()> _
    Public Function spRetrieve() As DataTable
        'Dim objDB As New cor_DB.clsConnection
        'Dim intLoop As Integer
        'Dim aryField, aryValue, aryType As Array

        Try
            If mCmdText = "" Then
                Return Nothing
                Exit Function
            End If

            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText

                'aryField = Split(mFieldList, "|*")
                'aryValue = Split(mFieldValue, "|*")
                'aryType = Split(mFieldType, "|*")
                'For intLoop = 0 To UBound(aryField)
                '    If Trim(aryType(intLoop)) = "2" Then
                '        aryValue(intLoop) = Mid(aryValue(intLoop), 2, Len(aryValue(intLoop)) - 2)
                '    End If
                '    .addInputParam(aryField(intLoop), aryValue(intLoop), aryType(intLoop))
                'Next
                spRetrieve = objDB.ExecuteReturnDT()
            End With
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spRetrieve :" + mCmdText + ":" + ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function spRetrieveDS() As DataSet
        Try
            If mCmdText = "" Then
                Return Nothing
                Exit Function
            End If
            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText

                'aryField = Split(mFieldList, "|*")
                'aryValue = Split(mFieldValue, "|*")
                'aryType = Split(mFieldType, "|*")
                'For intLoop = 0 To UBound(aryField)
                '    If Trim(aryType(intLoop)) = "2" Then
                '        aryValue(intLoop) = Mid(aryValue(intLoop), 2, Len(aryValue(intLoop)) - 2)
                '    End If
                '    .addInputParam(aryField(intLoop), aryValue(intLoop), aryType(intLoop))
                'Next
                spRetrieveDS = objDB.ExecuteReturnDS()
            End With
            objDB = Nothing
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spRetrieveDS : " + mCmdText + ":" + ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function spRetrieveValue(ByVal strValueName As String) As String

        Try
            If mCmdText = "" Then Return Nothing
            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText


                spRetrieveValue = objDB.ExecuteReturnValue("@" & strValueName)
            End With
            objDB = Nothing

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spRetrieveValue :" + mCmdText + ":" + ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function spRetrieveValue() As String
        Try
            If mCmdText = "" Then Return Nothing
            With objDB
                .CmdType = "StoredProcedure"
                .CmdText = mCmdText
                spRetrieveValue = objDB.ExecuteReturnValue()
            End With
            objDB = Nothing
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsDB.spRetrieveValue :" + mCmdText + ":" + ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Private objEventLog As New EventLog("Application", ".", ".Net Version 2003")

        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'objEventLog.WriteEntry(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = msg
                .Log()
            End With
            objLog = Nothing
        End Sub
    End Class
End Class
