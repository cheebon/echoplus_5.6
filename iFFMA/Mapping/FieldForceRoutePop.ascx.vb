Imports System.Data

Partial Class iFFMA_Mapping_FieldForceRoutePop
    Inherits System.Web.UI.UserControl

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

    Public Event SelectButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

            'Call Paging
            'With wuc_dgRoutePopPaging
            '    '.PageCount = dgRoutePopList.PageCount
            '    '.CurrentPageIndex = dgRoutePopList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not Page.IsPostBack Then
                DataBind()
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "PROPERTY"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property

    Public Property RouteCode() As String
        Get
            Return Trim(hdRouteCode.Value)
        End Get
        Set(ByVal value As String)
            hdRouteCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()

        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Sub ResetPage()
        txtCustCode.Text = ""
        txtCustName.Text = ""
        txtContCode.Text = ""
        txtContName.Text = ""

        updPnlMaintenance.Update()
    End Sub
#End Region

#Region "EVENTS"
    Public Overrides Sub DataBind()

        lblContent.Text = strMessage
        lblTitle.Text = strTitle

        updPnlMaintenance.Update()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    RaiseEvent CloseButton_Click(sender, e)
    'End Sub

    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            lblInfo.Text = ""

            Dim clsFieldForceRoute As New mst_Mapping.clsFieldForceRoute
            Dim strCustCode As String = ""
            Dim strContCode As String = ""

            If dgRoutePopList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                For Each DR As GridViewRow In dgRoutePopList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        strCustCode = dgRoutePopList.DataKeys(i)("CUST_CODE")
                        strContCode = dgRoutePopList.DataKeys(i)("CONT_CODE")

                        clsFieldForceRoute.CreateRoutePlanning(TeamCode, SalesrepCode, strCustCode, strContCode, RouteCode)
                    End If
                    i += 1
                Next
            End If

            If strCustCode = "" Then lblInfo.Text = "Please select at least 1 item to proceed!" : Show() : Exit Sub

            lblInfo.Text = "The selected item(s) was successfully added! Please click on Close button when finish assigning."
            RenewDataBind()

            RaiseEvent SelectButton_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgRoutePopList.PageIndex = 0
        wuc_dgRoutePopPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList(Optional ByVal isDefault As Boolean = False) As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForceRoute As New mst_Mapping.clsFieldForceRoute

            DT = clsFieldForceRoute.GetRoutePlanningExcludeList(txtCustCode.Text, txtCustName.Text, txtContCode.Text, txtContName.Text, txtAddr1.Text, txtAddr2.Text, txtAddr3.Text, txtAddr4.Text, TeamCode, SalesrepCode, RouteCode)
            dgRoutePopList.DataKeyNames = New String() {"CUST_CODE", "CONT_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgRoutePopList"
    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgRoutePopList.PageIndex = 0
            '    wuc_dgRoutePopPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgRoutePopList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(Master_Row_Count > 0, True, False)
                .DataBind()
            End With

            'Call Paging
            With wuc_dgRoutePopPaging
                .PageCount = dgRoutePopList.PageCount
                .CurrentPageIndex = dgRoutePopList.PageIndex
                .RowCount = dvCurrentView.Count
                .DataBind()
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenance.Update()
            RaiseEvent CloseButton_Click(Page, Nothing)
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgRoutePopList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgRoutePopList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            'CHECKBOX
            aryDataItem.Add("chkSelect")
            While dgRoutePopList.Columns.Count > 1
                dgRoutePopList.Columns.RemoveAt(1)
            End While
            dgRoutePopList.Columns(0).HeaderStyle.Width = "25"
            dgRoutePopList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldForceRoutePop.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldForceRoutePop.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldForceRoutePop.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldForceRoutePop.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldForceRoutePop.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgRoutePopList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgRoutePopList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgRoutePopList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgRoutePopList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgRoutePopList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub dgRoutePopList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgRoutePopList.SelectedIndexChanged
    '    Try
    '        lblInfo.Text = ""

    '        Dim strCustCode As String = sender.SelectedDataKey.Item("CUST_CODE").ToString
    '        Dim strContCode As String = sender.SelectedDataKey.Item("CONT_CODE").ToString

    '        Dim clsFieldForceRoute As New mst_Mapping.clsFieldForceRoute

    '        clsFieldForceRoute.CreateRoutePlanning(TeamCode, SalesrepCode, strCustCode, strContCode, RouteCode)

    '        lblInfo.Text = "The selected item(s) was successfully added! Please click on Close button when finish assigning."
    '        RenewDataBind()

    '        RaiseEvent SelectButton_Click(sender, e)

    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try

    'End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgRoutePopPaging.Go_Click
        Try
            dgRoutePopList.PageIndex = CInt(wuc_dgRoutePopPaging.PageNo - 1)

            dgRoutePopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgRoutePopPaging.Previous_Click
        Try
            If dgRoutePopList.PageIndex > 0 Then
                dgRoutePopList.PageIndex = dgRoutePopList.PageIndex - 1
            End If
            wuc_dgRoutePopPaging.PageNo = dgRoutePopList.PageIndex + 1

            dgRoutePopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgRoutePopPaging.Next_Click
        Try
            If dgRoutePopList.PageCount - 1 > dgRoutePopList.PageIndex Then
                dgRoutePopList.PageIndex = dgRoutePopList.PageIndex + 1
            End If
            wuc_dgRoutePopPaging.PageNo = dgRoutePopList.PageIndex + 1

            dgRoutePopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_FieldForceRoutePop
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "ADD_1"
                strFieldName = "Address 1"
            Case "ADD_2"
                strFieldName = "Address 2"
            Case "ADD_3"
                strFieldName = "Address 3"
            Case "ADD_4"
                strFieldName = "Address 4"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            FCT = FieldColumntype.BoundColumn

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*QTY" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" Then
                strFormatString = "{0:#,0.00}"
            ElseIf strColumnName Like "*DBL" Then
                strFormatString = "{0:#,0.0000}"
            ElseIf strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName = "BLANK" Then
                strFormatString = "{0:#,0}"
            Else
                strFormatString = ""
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

