' **********************************************************************************************************
'
'	Author	:	
'	Date	:	08-June-2005
'	Purpose	:	Perform the global function that will use by encryption and decryption
'
'	Revision	: 	
'	---------------------------------------------------------------------------------------------------------
'	|No	|Date Change	|Author		    |Remarks				        |	
'	---------------------------------------------------------------------------------------------------------
'	|1	|               |               |                               |
'	|2	|			    |	    	    |					            |
'	---------------------------------------------------------------------------------------------------------
'
' ***********************************************************************************************************

Option Explicit On 
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Interface Icor_Crypto
    Function RSAAssignParameter(ByVal rsa As RSACryptoServiceProvider) As RSACryptoServiceProvider
    Sub RSAAssignNewKey(ByVal rsa As RSACryptoServiceProvider)
    Function DESTransform(ByVal input() As Byte, ByVal CryptoTransform As ICryptoTransform) As Byte()
End Interface


'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsCrypto
    'Inherits ServicedComponent
    Implements Icor_Crypto

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsCrypto

    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function AssignParameter
    ' Purpose	:	This function will assign the parameter into RSACryptoServiceProvider
    ' Parameters:	[in]  :   
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function RSAAssignParameter(ByVal rsa As RSACryptoServiceProvider) As RSACryptoServiceProvider Implements Icor_Crypto.RSAAssignParameter
        Dim PROVIDER_RSA_FULL As Integer = 1
        Dim CONTAINER_NAME As String = "The.Owner.For.This.Class.Is.Obtech.Pacific.Sdn.Bhd.HardToCrack"
        Dim cspParams As CspParameters

        Try
            cspParams = New CspParameters(PROVIDER_RSA_FULL)
            cspParams.KeyContainerName = CONTAINER_NAME
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore
            cspParams.ProviderName = "Microsoft Strong Cryptographic Provider"
            rsa = New RSACryptoServiceProvider(cspParams)
            Return rsa
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsCrypto.RSAAssignParameter : " + ex.ToString))
        Finally
        End Try
    End Function


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Sub RSAAssignNewKey
    ' Purpose	:	This function will generate a public and private key XML file
    ' Parameters:	[in]  : ByVal rsa As RSACryptoServiceProvider
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub RSAAssignNewKey(ByVal rsa As RSACryptoServiceProvider) Implements Icor_Crypto.RSAAssignNewKey
        Dim writer As StreamWriter
        Dim publicPrivateKeyXML As String
        'Dim publicOnlyKeyXML As String

        Try
            'provide public and private RSA params
            writer = New StreamWriter("C:\privatekey.xml")
            publicPrivateKeyXML = rsa.ToXmlString(True)
            writer.Write(publicPrivateKeyXML)
            writer.Close()

            'provide public only RSA params
            'writer = New StreamWriter("C:\publickey.xml")
            'publicOnlyKeyXML = rsa.ToXmlString(False)
            'writer.Write(publicOnlyKeyXML)
            'writer.Close()
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsCrypto.RSAAssignNewKey : " + ex.ToString))
        Finally
        End Try
    End Sub


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function DESTransform
    ' Purpose	:	This function will assign the parameter into RSACryptoServiceProvider
    ' Parameters:	[in]  : ByVal input() As Byte, ByVal CryptoTransform As ICryptoTransform  
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function DESTransform(ByVal input() As Byte, ByVal CryptoTransform As ICryptoTransform) As Byte() Implements Icor_Crypto.DESTransform
        Dim memStream As MemoryStream
        Dim cryptStream As CryptoStream

        Try
            memStream = New MemoryStream
            cryptStream = New CryptoStream(memStream, CryptoTransform, _
                                           CryptoStreamMode.Write)

            cryptStream.Write(input, 0, input.Length)
            cryptStream.FlushFinalBlock()

            ' Read the memory stream and convert it back into byte array
            memStream.Position = 0
            Dim result(CType(memStream.Length - 1, System.Int32)) As Byte
            memStream.Read(result, 0, CType(result.Length, System.Int32))

            memStream.Close()
            cryptStream.Close()
            Return result
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsCrypto.DESTransform : " + ex.ToString))
        Finally
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
