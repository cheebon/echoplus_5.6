﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAPrint.aspx.vb" Inherits="Admin_TRA_TRAList_TRAPrint" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Monitor</title>
    <script type="text/javascript">
        setTimeout("silentPrint()", 2000);
        //function silentPrint() { document.getElementById('ReportViewer1').ClientController.LoadPrintControl(); return false; }
        //function silentPrint() { window.open(document.getElementById('ReportViewer1').ClientController.m_exportUrlBase + encodeURIComponent('PDF'), '_blank'); }
        function silentPrint() { $find('ReportViewer1').exportReport('PDF'); }  
</script>
    
</head>
<body style="background-color:White;">
    <form id="frmPrint" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="uprptv">
        <p>
            Printing service will be start shortly.<br />
        </p>
        <p>
            <span onclick="silentPrint();" style="cursor: pointer; font-size: small; font-style:italic; color:Gray;">
            If printing pop up does not appear within 5 seconds, kindly click HERE.</span>
        </p>
        <br /> <br />
        <div style="text-align: center; width: 100%;">
            <span onclick="window.close()" style="cursor: pointer; background-color: Maroon; color: yellow; padding: 1px 3px 1px 3px;">Close Window</span>
        </div>
        <br />
        <div style="width: 100%; display: none;">
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" height="0px"  Font-Names="Verdana" 
            Font-Size="8pt"  ProcessingMode="Local"  ShowToolBar="true" AsyncRendering="false">
            </rsweb:ReportViewer>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
