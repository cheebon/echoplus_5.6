﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ctrlPanel_Alt.ascx.vb" Inherits="include_wuc_ctrlPanel_Alt" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_lblDate.ascx" TagName="wuc_lblDate" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td>
            <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                width="100%" border="0" style="height: 30px">
                <tr align="center" valign="bottom">
                    <td>
                        <asp:Image ID="imgTreeMenu" ImageUrl="~/images/ico_treeview.gif" runat="server" CssClass="cls_button"
                            ToolTip="Tree Search" onclick="pnl_TreeMenu_ChangeCollapsibleState();" EnableViewState="false" />
                    </td>
                    <%If imgTreeMenu.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                            CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                    </td>
                    <%If imgGeneralInfo.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgDate" ImageUrl="~/images/ico_calendar.gif" runat="server" CssClass="cls_button"
                            ToolTip="Query Date" EnableViewState="false" />
                    </td>
                    <%If imgDate.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                     <td>
                        <asp:Image ID="imgExpandCollapse" ImageUrl="~/images/ico_Field.gif" runat="server"
                            CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" onclick="ShowHideElement('TopBar')" />
                    </td>
                    <%If imgExpandCollapse.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image13" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                   <td>
                        <asp:Image ID="imgGroupingField" ImageUrl="~/images/ico_Field.gif" runat="server"
                            CssClass="cls_button" ToolTip="Grouping Fields" EnableViewState="false" />
                    </td>
                    <%If imgGroupingField.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image5" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>

                    <td>
                        <asp:Image ID="imgEnquiryDetails" ImageUrl="~/images/ico_Search_Details.gif" runat="server"
                            CssClass="cls_button" ToolTip="Search Details" EnableViewState="false" />
                    </td>
                    <%If imgEnquiryDetails.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image6" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgFieldConfig" runat="server" CssClass="cls_button" ToolTip="Field Configuration"
                            ImageUrl="../images/ico_field_config.gif" EnableViewState="false" />
                    </td>
                    <%If imgFieldConfig.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image8" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgNetValue" runat="server" CssClass="cls_button" ToolTip="Net Value Selection"
                            ImageUrl="../images/ico_money.gif" EnableViewState="false" />
                    </td>
                    <%If imgNetValue.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image9" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgRemark" runat="server" CssClass="cls_button" ToolTip="Report Info"
                            ImageUrl="~/images/ico_header.gif" EnableViewState="false" />
                    </td>
                    <%If imgRemark.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server"
                            CssClass="cls_button" ToolTip="Export" EnableViewState="false" />
                    </td>
                    <%If imgExport.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgPrint" ImageUrl="~/images/ico_printer.gif" runat="server" CssClass="cls_button"
                            ToolTip="Print" onclick="window.print();" EnableViewState="false" />
                    </td>
                    <td style="width: 95%">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdateCtrlPanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr id="All Control Panel ">
                            <td>
                                <asp:Panel ID="pnlRemark" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" border="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td style="width: 45%">
                                                <span class="cls_label_header">Report Definition:</span>
                                            </td>
                                            <td style="width: 45%">
                                                <span class="cls_label_header">Abbreviation:</span>
                                            </td>
                                            <td align="right" style="width: 21px">
                                                <asp:Image ID="imgRemark_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                    CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlRemark')"
                                                    EnableViewState="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:DetailsView ID="dvReportDefinition" runat="server" AutoGenerateRows="False"
                                                    CssClass="Grid" Width="95%" BorderWidth="0px">
                                                    <Fields>
                                                        <asp:BoundField DataField="GLOSSARY_DESC" HeaderText="" ReadOnly="True" HtmlEncode="false" />
                                                    </Fields>
                                                    <RowStyle CssClass="cls_label" />
                                                </asp:DetailsView>
                                            </td>
                                            <td align="center">
                                                <ccGV:clsGridView ID="dgRemarkList" runat="server" ShowFooter="false" ShowHeader="true"
                                                    RowSelectionEnabled="false" AllowSorting="false" AllowPaging="false" AutoGenerateColumns="false"
                                                    Width="95%" FreezeHeader="true" GridHeight="120" BorderStyle="Solid" CssClass="Grid">
                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                    <EmptyDataTemplate>
                                                        There is no data to display.</EmptyDataTemplate>
                                                    <HeaderStyle CssClass="GridRemarkHeader" />
                                                    <RowStyle CssClass="GridRemark" />
                                                    <AlternatingRowStyle CssClass="GridRemark" />
                                                </ccGV:clsGridView>
                                                <br />
                                            </td>
                                            <td style="width: 21px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlRemark" runat="server" CollapseControlID="imgRemark"
                                        ExpandControlID="imgRemark" TargetControlID="pnlRemark" CollapsedSize="0" Collapsed="true"
                                        ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                                <asp:Panel ID="pnlGeneralInfo" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                                                    <tr align="left">
                                                        <td style="width: 8%">
                                                            <span class="cls_label_header">Country</span>
                                                            <td>
                                                                <span class="cls_label_header">:</span>
                                                            </td>
                                                            <td align="left" style="width: 10%">
                                                                <asp:Label ID="lblCountryName" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                                            <td style="width: 8%">
                                                                <span class="cls_label_header">Principal</span>
                                                                <td>
                                                                    <span class="cls_label_header">:</span>
                                                                    <td align="left" style="width: 10%">
                                                                        <asp:Label ID="lblPrincipalName" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                                                    <td style="width: 11%">
                                                                        <span class="cls_label_header">Map</span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">:</span></td>
                                                                    <td align="left" colspan="4">
                                                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                                                    <td align="right">
                                                                        <asp:Image ID="Image10" runat="server" ImageUrl="~/images/ico_close.gif" CssClass="cls_image_Close"
                                                                            onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlGeneralInfo')" EnableViewState="false" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="cls_label_header">Year</span>
                                                        </td>
                                                        <td>
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left">
                                                            <asp:Label ID="lblYearValue" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                                        <td>
                                                            <span class="cls_label_header">Month</span></td>
                                                        <td>
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left">
                                                            <asp:Label ID="lblMonthValue" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                                        <td style="width: 11%">
                                                            <span class="cls_label_header">Selected Date</span></td>
                                                        <td>
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left">
                                                            <customToolkit:wuc_lblDate ID="Wuc_lblSelectedDate" runat="server" ControlType="DateOnly" />
                                                        </td>
                                                        <td style="width: 11%" align="right">
                                                            <span class="cls_label_header">Report</span></td>
                                                        <td style="width: 1%">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td colspan="5" align="left">
                                                            <customToolkit:wuc_lblDate ID="Wuc_lblTimestamp" runat="server" ControlType="DateAndTime" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                                        ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                                <asp:Panel ID="pnlDateQuery" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <span class="cls_label_header">Year</span></td>
                                                        <td>
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlYear" CssClass="cls_dropdownlist" runat="server">
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <span class="cls_label_header">Month</span></td>
                                                        <td>
                                                            <span class="cls_label_header">:</span></td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="ddlMonth" CssClass="cls_dropdownlist" runat="server">
                                                                <asp:ListItem Value="1">Jan</asp:ListItem>
                                                                <asp:ListItem Value="2">Feb</asp:ListItem>
                                                                <asp:ListItem Value="3">Mar</asp:ListItem>
                                                                <asp:ListItem Value="4">Apr</asp:ListItem>
                                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                                <asp:ListItem Value="6">Jun</asp:ListItem>
                                                                <asp:ListItem Value="7">Jul</asp:ListItem>
                                                                <asp:ListItem Value="8">Aug</asp:ListItem>
                                                                <asp:ListItem Value="9">Sept</asp:ListItem>
                                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:Button ID="btnRefreshQueryDate" CssClass="cls_button" runat="server" Text="Refresh"
                                                                EnableViewState="false" /></td>
                                                        <td align="right" width="70%">
                                                            <asp:Image ID="imgDateQuery_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlDateQuery')"
                                                                EnableViewState="false" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlQueryDate" runat="server" CollapseControlID="imgDate"
                                        ExpandControlID="imgDate" TargetControlID="pnlDateQuery" CollapsedSize="0" Collapsed="true"
                                        ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                                <asp:Panel ID="pnlGroupingField" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td valign="top" align="center">
                                                            <span class="cls_label_header">Select</span><br>
                                                            <asp:ListBox ID="lsbGroupField_Hide" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                ></asp:ListBox>
                                                            <asp:HiddenField ID="hdf_Hide" runat="server" />
                                                            <td valign="middle" width="5%">
                                                                <table style="padding-top: 2px">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="btnMoveToRight" type="button" onclick="DL_MoveItems('wuc_ctrlpanel_lsbGroupField_Hide','wuc_ctrlpanel_lsbGroupField_Show')"
                                                                                class="cls_button" style="width: 35px" value=">" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="btnMoveToLeft" type="button" onclick="DL_MoveItems('wuc_ctrlpanel_lsbGroupField_Show','wuc_ctrlpanel_lsbGroupField_Hide')"
                                                                                class="cls_button" style="width: 35px" value="<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="btnMoveAllToRight" type="button" onclick="DL_MoveAllItems('wuc_ctrlpanel_lsbGroupField_Hide','wuc_ctrlpanel_lsbGroupField_Show')"
                                                                                class="cls_button" style="width: 35px" value=">>" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="btnMoveAllToLeft" type="button" onclick="DL_MoveAllItems('wuc_ctrlpanel_lsbGroupField_Show','wuc_ctrlpanel_lsbGroupField_Hide')"
                                                                                class="cls_button" style="width: 35px" value="<<" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="BtnMoveUp" type="button" onclick="DL_MoveUp('wuc_ctrlpanel_lsbGroupField_Show')"
                                                                                class="cls_button" style="width: 35px" value="Up" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input id="BtnMoveDown" type="button" onclick="DL_MoveDown('wuc_ctrlpanel_lsbGroupField_Show')"
                                                                                class="cls_button" style="width: 35px" value="Down" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="center">
                                                                <span class="cls_label_header">Show</span><br>
                                                                <asp:ListBox ID="lsbGroupField_Show" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                    ></asp:ListBox>
                                                                <asp:HiddenField ID="hdf_Show" runat="server" Value="" />
                                                                <td>
                                                                    <td valign="top" align="right">
                                                                        <asp:Image ID="imgGrouping_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                            CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlGroupingField')"
                                                                            EnableViewState="false" />
                                                                    </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnResetField" CssClass="cls_button" runat="server" Text="Reset"
                                                                EnableViewState="false" />
                                                            <asp:Button ID="btnRefresh4" CssClass="cls_button" runat="server" Text="Refresh"
                                                                OnClientClick="FillLsbValue()"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <script id="DualList-Controls" type="text/javascript" language="javascript">
    <!--
    //Created by Alex Chia 2008-03-26
    function DL_MoveItems(source, target){source=(typeof source=="string")? document.getElementById(source): source;if (source.tagName.toLowerCase()!= "select" && source.length < 2)return false;target=(typeof target=="string")? document.getElementById(target): target;if (target.tagName.toLowerCase()!= "select" && target.length < 2)return false;DL_MoveSelectedItems(source,target);return false;}
    function DL_MoveSelectedItems(source,target){if ( source.options.length==0 ){return;}target.options.selectedIndex =-1;var originalIndex=source.options.selectedIndex;while ( source.options.selectedIndex >= 0 ){DL_MoveItem( source.options.selectedIndex, source, target );}if ( originalIndex < source.options.length ){source.options.selectedIndex=originalIndex;} else {source.options.selectedIndex=source.options.length - 1;}}
    function DL_MoveAllItems(source,target){source=(typeof source=="string")? document.getElementById(source): source;if (source.tagName.toLowerCase()!= "select" && source.length < 2)return false;target=(typeof target=="string")? document.getElementById(target): target;if (target.tagName.toLowerCase()!= "select" && target.length < 2)return false;target.options.selectedIndex =-1;	while ( source.options.length > 0 ){DL_MoveItem( 0, source, target );}}
    function DL_MoveItem(itemIndex,source,target){var itemValue=source.options[itemIndex].value;var itemText=source.options[itemIndex].text;var newOption=new Option();newOption.text=itemText;newOption.value=itemValue;source.options.remove(itemIndex);DL_InsertOption( target, newOption);target.options[(target.options.length-1)].selected='selected';}
    function DL_InsertOption( target, option, index ){if ( typeof( index )== "undefined" || index >= target.options.length ){target.options[target.options.length]=option;} else {for( var i=target.options.length; i > index; i-- ){var optionCopy=new Option();optionCopy.text=target.options[i-1].text;optionCopy.value=target.options[i-1].value;target.options[i]=optionCopy;}target.options[index]=option;}}
    function DL_MoveUp(target){target=(typeof target=="string")? document.getElementById(target): target;if (target.tagName.toLowerCase()!= "select" && target.length < 2)return false;var sel=new Array();for (var i=0; i<target.length; i++){if (target[i].selected==true){sel[sel.length]=i;}}for (i in sel){if (sel[i] != 0 && !target[sel[i]-1].selected){var tmp=new Array((document.body.innerHTML ? target[sel[i]-1].innerHTML : target[sel[i]-1].text), target[sel[i]-1].value, target[sel[i]-1].style.color, target[sel[i]-1].style.backgroundColor, target[sel[i]-1].className, target[sel[i]-1].id);if (document.body.innerHTML)target[sel[i]-1].innerHTML=target[sel[i]].innerHTML;else target[sel[i]-1].text=target[sel[i]].text;target[sel[i]-1].value=target[sel[i]].value;target[sel[i]-1].className=target[sel[i]].className;target[sel[i]-1].id=target[sel[i]].id;if (document.body.innerHTML)target[sel[i]].innerHTML=tmp[0];else target[sel[i]].text=tmp[0];target[sel[i]].value=tmp[1];target[sel[i]].className=tmp[4];target[sel[i]].id=tmp[5];target[sel[i]-1].selected=true;target[sel[i]].selected=false;}}}
    function DL_MoveDown(target){target=(typeof target=="string")? document.getElementById(target): target;if (target.tagName.toLowerCase()!= "select" && target.length < 2)return false;var sel=new Array();for (var i=target.length-1; i>-1; i--){if (target[i].selected==true){sel[sel.length]=i;}}for (i in sel){if (sel[i] != target.length-1 && !target[sel[i]+1].selected){var tmp=new Array((document.body.innerHTML ? target[sel[i]+1].innerHTML : target[sel[i]+1].text), target[sel[i]+1].value, target[sel[i]+1].style.color, target[sel[i]+1].style.backgroundColor, target[sel[i]+1].className, target[sel[i]+1].id);if (document.body.innerHTML)target[sel[i]+1].innerHTML=target[sel[i]].innerHTML;else target[sel[i]+1].text=target[sel[i]].text;target[sel[i]+1].value=target[sel[i]].value;target[sel[i]+1].className=target[sel[i]].className;target[sel[i]+1].id=target[sel[i]].id;if (document.body.innerHTML)target[sel[i]].innerHTML=tmp[0];else target[sel[i]].text=tmp[0];target[sel[i]].value=tmp[1];target[sel[i]].className=tmp[4];target[sel[i]].id=tmp[5];target[sel[i]+1].selected=true;target[sel[i]].selected=false;}}}
function FillLsbValue(){if ( !(document.getElementById) ) { return; }lsbHide = document.getElementById('wuc_ctrlpanel_lsbGroupField_Hide') ;hdf = document.getElementById('wuc_ctrlpanel_hdf_Hide') ;hdf.value="";for( var i = 0; i < lsbHide.options.length; i++ ) {var itemValue = lsbHide.options[i].value;var itemText = lsbHide.options[i].text;hdf.value+= itemText + "|" + itemValue + ",";}lsbShow = document.getElementById('wuc_ctrlpanel_lsbGroupField_Show') ;hdf = document.getElementById('wuc_ctrlpanel_hdf_Show') ;hdf.value="";for( var i = 0; i < lsbShow.options.length; i++ ) {var itemValue = lsbShow.options[i].value;var itemText = lsbShow.options[i].text;hdf.value+= itemText + "|" + itemValue + ",";}}        
    -->    
                                    </script>

                                </asp:Panel>
                                <asp:Panel ID="pnlNetValue" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="radlNetValue" runat="server" RepeatDirection="Horizontal"
                                                                CssClass="cls_radiobuttonlist">
                                                                <asp:ListItem Selected="True" Value="1">Net Value 1</asp:ListItem>
                                                                <asp:ListItem Value="2">Net Value 2</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnNetValue_refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                EnableViewState="false" /></td>
                                                        <td align="right" style="width: 70%;">
                                                            <asp:Image ID="imgNetValue_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlNetValue')" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlNetValue" runat="server" CollapseControlID="imgNetValue"
                                        ExpandControlID="imgNetValue" TargetControlID="pnlNetValue" CollapsedSize="0"
                                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                              <%--  <asp:Panel ID="pnlSalesEnquirySearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                    <tr>
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header">Product</span> &nbsp; &nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span> &nbsp;
                                                        </td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <asp:TextBox ID="txtProductNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnProductNoFile" class="cls_button" onclick="SalesEnq_openProduct()"
                                                                            size="22" type="button" value="Search..."  /><br />
                                                                        <asp:TextBox ID="txtProductName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:Image ID="imgSalesEnquiry_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                            CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlSalesEnquirySearch')"
                                                                            EnableViewState="false" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Customer </span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                            &nbsp;<input id="btnShipToNoFile" class="cls_button" onclick="SalesEnq_openCustomer()"
                                                                size="22" type="button" value="Search..."  /><br />
                                                            <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtSalesmanNo" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="btnSalesmanNoFile" class="cls_button" onclick="SalesEnq_openSalesman()"
                                                                size="22" type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtSalesmanName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Invoice</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtInvNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                            <span style="font-size: 8pt">10 digits is required (without zero in front) </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <span class="cls_label_header">Net Value</span>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <span class="cls_label_header">:</span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <asp:RadioButtonList ID="rblNetValue" runat="server" RepeatDirection="Horizontal"
                                                                CssClass="cls_radiobuttonlist">
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr id="SalesEnq_DateRange" valign="top">
                                                          <td valign="baseline" align="left">
                                                            <span class="cls_label_header" style="white-space: nowrap">Txn. Date</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="top" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucSalesEnq_Calendar" runat="server" RequiredValidation="true"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnRefresh5" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="Search"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                                <%--<asp:Panel ID="pnlSalesEnquirySearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td colspan="8">
                                                            <span class="cls_label_header" >Search Criteria</span>
                                                            <td align="right">
                                                                <asp:Image ID="imgSalesEnquiryDet_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false"
                                                                    CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlSalesEnquirySearchDetails')" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%;">
                                                            <span  class="cls_label_header" >Product :</span>
                                                        </td>
                                                        <td align="left" style="width: 22%;">
                                                            <asp:Label ID="lblProductDetails" CssClass="cls_label" runat="server"  EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%;">
                                                        </td>
                                                        <td style="width: 8%;">
                                                            <span  class="cls_label_header" >Customer :</span>
                                                        </td>
                                                        <td align="left" style="width: 22%;">
                                                            <asp:Label ID="lblCustomerDetails" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%;">
                                                        </td>
                                                        <td style="width: 8%;">
                                                            <span class="cls_label_header" >Field Force :</span>
                                                        </td>
                                                        <td align="left" style="width: 22%;">
                                                            <asp:Label ID="lblSalesmanDetails" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="cls_label_header" >Inv No :</span>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblInvoiceDetails" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <span class="cls_label_header" >Net Value :</span>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblNetValueDetails" CssClass="cls_label" runat="server" EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <span class="cls_label_header" >Date :</span>
                                                        </td>
                                                        <td colspan="2">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                        <td align="center" style="width: 22%;">
                                                                            <asp:Label ID="lblDateFromDetails" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                                        </td>
                                                                        <td style="width: 1%; white-space: nowrap;">
                                                                            <td align="center" style="width: 8%;">
                                                                                <span class="cls_label"> to </span>
                                                                            </td>
                                                                            <td style="width: 1%; white-space: nowrap;">
                                                                                <td align="center" style="width: 22%;">
                                                                                    <asp:Label ID="lblDateToDetails" runat="server" CssClass="cls_label" EnableViewState="false">-</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                              <%--  <asp:Panel ID="pnlCallEnquirySearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                    <tr id="CallEnq_Customer">
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header" >Customer</span>
                                                            &nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header" >:<span>
                                                            &nbsp;
                                                        </td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%; height: 48px;">
                                                                        <asp:TextBox ID="txtCallEnq_CustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnCustomer" class="cls_button" onclick="CallEnq_openCustomer()"
                                                                            size="22" type="button" value="Search..."  /><br />
                                                                        <asp:TextBox ID="txtCallEnq_CustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                                    <td align="right" style="height: 48px">
                                                                        <asp:Image ID="imgCallEnquiry_Close" runat="server" ImageUrl="~/images/ico_close.gif"  EnableViewState="false" 
                                                                            CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCallEnquirySearch')" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="CallEnq_Contact">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >Contact</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtCallEnq_ContCode" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="Button2" class="cls_button" onclick="CallEnq_openContact()" size="22"
                                                                 /><br />
                                                            <asp:TextBox ID="txtCallEnq_ContName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CallEnq_Salesman">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtCallEnq_Salesman" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="Button1" class="cls_button" onclick="CallEnq_openSalesman()" size="22"
                                                                type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtCallEnq_SalesmanName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="line-height: 2px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="CallEnq_SDateRange" valign="top">
                                                       <td valign="baseline" align="left">
                                                            <span class="cls_label_header" style="white-space: nowrap">Txn. Date</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucCallEnq_Calendar" runat="server" RequiredValidation="true"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline" style="height: 24px">
                                                                        <asp:Button ID="btnCalEnq_Reset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                                    </td>
                                                                    <td style="height: 24px">
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline" style="height: 24px">
                                                                        <asp:Button ID="btnCallEnq_Refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="CallSearch"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%; height: 24px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                               <%-- <asp:Panel ID="pnlCallEnquirySearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <span class="cls_label_header" >Search Criteria</span>
                                                        </td>
                                                        <td align="right" valign="baseline">
                                                            <asp:Image ID="imgCallEnquiryDet_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCallEnquirySearchDetails')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Customer : </span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCallEnq_Customer" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Contact: </span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCallEnq_Contact" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Field Force : </span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCallEnq_Salesman" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="width: 5%; white-space: nowrap;">
                                                                        <span class="cls_label_header" >Date : </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 8%">
                                                                        <customToolkit:wuc_lblDate ID="WucCallEnq_lblDateFrom" runat="server" ControlType="DateOnly"
                                                                            Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 3%; white-space: nowrap;">
                                                                        <span class="cls_label"> to </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                        <td align="center" style="width: 8%">
                                                                            <customToolkit:wuc_lblDate ID="WucCallEnq_lblDateTo" runat="server" ControlType="DateOnly"
                                                                                Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                             <%--   <asp:Panel ID="pnlDRCEnquirySearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                    <tr id="DrcEnq_Prd">
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header" >Product</span>
                                                            &nbsp; &nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header" >:</span>
                                                            &nbsp;
                                                        </td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <asp:TextBox ID="txtDRCEnq_ProductNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnDRCEnq_ProductNoFile" class="cls_button" onclick="DRCEnq_openProduct()"
                                                                            size="22" type="button" value="Search..." /><br />
                                                                        <asp:TextBox ID="txtDRCEnq_ProductName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:Image ID="imgDRCEnquiry_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                            CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlDRCEnquirySearch')" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="DrcEnq_Customer">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >Customer </spanl></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtDRCEnq_CustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                            &nbsp;<input id="btnDRCEnq_ShipToNoFile" class="cls_button" onclick="DRCEnq_openCustomer()"
                                                                size="22" type="button" value="Search..."  /><br />
                                                            <asp:TextBox ID="txtDRCEnq_CustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr id="DrcEnq_Salesrep">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtDRCEnq_SalesmanNo" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="btnDRCEnq_SalesmanNoFile" class="cls_button" onclick="DRCEnq_openSalesman()"
                                                                size="22" type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtDRCEnq_SalesmanName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="DrcEnq_DateRange" valign="top">
                                                     <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header" >Txn. Date</span>
                                                            &nbsp; &nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header" >:</span>
                                                            &nbsp;
                                                        </td>
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucDrcEnq_Calendar" runat="server" RequiredValidation="true"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnDRCEnq_Reset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnDRCEnq_Refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="DRCSearch"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                               <%-- <asp:Panel ID="pnlDRCEnquirySearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <span class="cls_label_header" >Search Criteria</span>
                                                        </td>
                                                        <td align="right" valign="baseline">
                                                            <asp:Image ID="imgDRCEnquiryDet_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false"
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlDRCEnquirySearchDetails')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Product :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblDRCEnq_ProductDetails" CssClass="cls_label" runat="server"  EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Customer :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblDRCEnq_CustomerDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Field Force :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblDRCEnq_SalesmanDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="width: 5%; white-space: nowrap;">
                                                                        <span class="cls_label_header" >Date : </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 8%">
                                                                        <customToolkit:wuc_lblDate ID="WucDRCEnq_lblDateFrom" runat="server" ControlType="DateOnly"
                                                                            Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 3%; white-space: nowrap;">
                                                                        <span  class="cls_label"> to </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                        <td align="center" style="width: 8%">
                                                                            <customToolkit:wuc_lblDate ID="WucDRCEnq_lblDateTo" runat="server" ControlType="DateOnly"
                                                                                Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                                <%--<asp:Panel ID="pnlCollCheqEnquirySearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                  
                                                    <tr id="CollEnq_Team" valign="top" >
                                                        <td valign="baseline" align="left" colspan="1" style="width:10%">
                                                            <span class="cls_label_header" >Team </span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td> 
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <asp:DropDownList ID="ddlteam" runat="server" CssClass="cls_dropdownlist">
                                                            </asp:DropDownList>
                                                                  <td align="right">
                                                                        <asp:Image ID="imgCollCheqEnquiry_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                            EnableViewState="false" CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCollCheqEnquirySearch')" />
                                                                    </td>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq_Salesrep">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtCollCheqEnq_SalesmanNo" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="btnCollCheqEnq_SalesmanNoFile" class="cls_button" onclick="CollCheqEnq_openSalesman()"
                                                                size="22" type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtCollCheqEnq_SalesmanName" runat="server" CssClass="cls_textbox"
                                                                Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq_Customer">
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header">Customer </span>&nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <asp:TextBox ID="txtCollCheqEnq_CustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnCollCheqEnq_ShipToNoFile" class="cls_button" onclick="CollCheqEnq_openCustomer()"
                                                                            size="22" type="button" value="Search..." /><br />
                                                                        <asp:TextBox ID="txtCollCheqEnq_CustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                              
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr id="CollEnq_CheqNo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Cheque No.</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtCollCheqEnq_CheqNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq_ReceiptNo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Receipt No.</span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtCollCheqEnq_ReceiptNo" runat="server" CssClass="cls_textbox" ></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                            <tr id="CollEnq_TxnDateRange" valign="top">
                                                        <td valign="baseline" align="left" colspan="1">
                                                            <span class="cls_label_header" style="white-space: nowrap">Txn. Date </span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td> 
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucCollEnq_TxnCalendar" runat="server" RequiredValidation="false"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq_DateRange" valign="top">
                                                        <td valign="baseline" align="left" colspan="1" >
                                                            <span class="cls_label_header">Cheque Date </span><span class="cls_mandatory">*</span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td> 
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucCollEnq_Calendar" runat="server" RequiredValidation="true"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                           
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnCollCheqEnq_Reset" CssClass="cls_button" runat="server" Text="Reset">
                                                                        </asp:Button>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnCollCheqEnq_Refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="CollCheqSearch"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                        </tr>
                    </table>
                    </asp:Panel>--%>
                                <%--<asp:Panel ID="pnlCollCheqEnquirySearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <span class="cls_label_header" >Search Criteria</span>
                                                        </td>
                                                        <td align="right" valign="baseline">
                                                            <asp:Image ID="imgCollCheqEnquiryDet_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCollCheqEnquirySearchDetails')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Customer :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollCheqEnq_CustomerDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Field Force :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollCheqEnq_SalesmanDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Cheque No :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollCheqEnq_CheqDetails" CssClass="cls_label" runat="server"  EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="width: 5%; white-space: nowrap;">
                                                                        <span class="cls_label_header" >Date : </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 8%">
                                                                        <customToolkit:wuc_lblDate ID="WucCollCheqEnq_lblDateFrom" runat="server" ControlType="DateOnly"
                                                                            Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 3%; white-space: nowrap;">
                                                                        <span  class="cls_label"> to </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                        <td align="center" style="width: 8%">
                                                                            <customToolkit:wuc_lblDate ID="WucCollCheqEnq_lblDateTo" runat="server" ControlType="DateOnly"
                                                                                Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                              <%--  <asp:Panel ID="pnlCollEnqSearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                    <tr id="CollEnq2_Salesrep">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtCollEnq_SalesrepCode" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="btnCollEnq_SalesrepCode" class="cls_button" onclick="CollEnq_openSalesman()"
                                                                size="22" type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtCollEnq_SalesrepName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq2_Customer">
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header">Customer </span>&nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <asp:TextBox ID="txtCollEnq_CustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnCollEnq_ShipToNoFile" class="cls_button" onclick="CollEnq_openCustomer()"
                                                                            size="22" type="button" value="Search..." /><br />
                                                                        <asp:TextBox ID="txtCollEnq_CustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq2_SONo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" style="white-space:nowrap">Sales Ord No.</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtCollEnq_SONo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq2_InvNo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">Invoice No.</span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtCollEnq_InvNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="CollEnq2_TxnDateRange" valign="top">
                                                        <td valign="baseline" align="left" colspan="1" style="vertical-align:text-top">
                                                            <span class="cls_label_header" style="white-space: nowrap;">Txn. Date </span>
                                                        </td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td valign="baseline" align="left" style="white-space: nowrap" colspan="1">
                                                            <customToolkit:wuc_txtCalendarRange ID="WucCollEnq2_txnDate" runat="server" RequiredValidation="true"
                                                                RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnCollEnq_Reset" CssClass="cls_button" runat="server" Text="Reset">
                                                                        </asp:Button>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnCollEnq_Refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="CollCheqSearch"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                                <asp:Panel ID="pnlCollEnqSearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <span class="cls_label_header">Search Criteria</span>
                                                        </td>
                                                        <td align="right" valign="baseline">
                                                            <asp:Image ID="imgCollEnqDet_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                EnableViewState="false" CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCollEnqSearchDetails')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header">Customer :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollEnq_CustDtl" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header">Field Force :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollEnq_SalesrepDtl" CssClass="cls_label" runat="server" EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header">Sales Ord. No :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblCollEnq_SONo" CssClass="cls_label" runat="server" EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="width: 5%; white-space: nowrap;">
                                                                        <span class="cls_label_header">Date : </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 8%">
                                                                        <customToolkit:wuc_lblDate ID="WucCollEnq_lblDateFrom" runat="server" ControlType="DateOnly"
                                                                            Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    </td>
                                                                    <td align="center" style="width: 3%; white-space: nowrap;">
                                                                        <span class="cls_label">to </span>
                                                                    </td>
                                                                    <td style="width: 1%; white-space: nowrap;">
                                                                    <td align="center" style="width: 8%">
                                                                        <customToolkit:wuc_lblDate ID="WucCollEnq_lblDateTo" runat="server" ControlType="DateOnly"
                                                                            Text="-" DateTimeFormatString="yyyy-MM-dd" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlPAFEnquirySearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                    <tr id="PafEnq_Customer">
                                                        <td valign="top" align="left" style="white-space: nowrap">
                                                            <span class="cls_label_header" >Customer </span>
                                                            &nbsp;
                                                        </td>
                                                        <td valign="top">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td align="left" valign="baseline" style="width: 95%">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="width: 100%">
                                                                        <asp:TextBox ID="txtPAFEnq_CustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                        &nbsp;<input id="btnPAFEnq_ShipToNoFile" class="cls_button" onclick="PAFEnq_openCustomer()"
                                                                            size="22" type="button" value="Search..." /><br />
                                                                        <asp:TextBox ID="txtPAFEnq_CustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:Image ID="imgPAFEnquiry_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                            CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlPAFEnquirySearch')" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="PafEnq_Salesrep">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >Field Force</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td valign="baseline" align="left">
                                                            <asp:TextBox ID="txtPAFEnq_SalesmanNo" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                                            <input id="btnPAFEnq_SalesmanNoFile" class="cls_button" onclick="PAFEnq_openSalesman()"
                                                                size="22" type="button" value="Search..." /><br />
                                                            <asp:TextBox ID="txtPAFEnq_SalesmanName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="PafEnq_PafNo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >PAF No.</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtPAFEnq_PAFNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="PafEnq_SVONo">
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >SVO No.</span></td>
                                                        <td valign="baseline" align="left">
                                                            <span class="cls_label_header" >:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtPAFEnq_SVONo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="baseline" align="left">
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnPAFEnq_Reset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;</td>
                                                                    <td align="left" valign="baseline">
                                                                        <asp:Button ID="btnPAFEnq_Refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                            ValidationGroup="PAFSearch"></asp:Button>
                                                                    </td>
                                                                    <td style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlPAFEnquirySearchDetails" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <sapn class="cls_label_header" >Search Criteria</span>
                                                        </td>
                                                        <td align="right" valign="baseline">
                                                            <asp:Image ID="imgPAFEnquiryDet_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlPAFEnquirySearchDetails')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Customer :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblPAFEnq_CustomerDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >Field Force :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblPAFEnq_SalesmanDetails" CssClass="cls_label" runat="server"  EnableViewState="false">-</asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >PAF No :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblPAFEnq_PAFNoDetails" CssClass="cls_label" runat="server"  EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td style="width: 5%; white-space: nowrap;">
                                                            <span class="cls_label_header" >SVO No :</span>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                        <td align="left" style="width: 18%">
                                                            <asp:Label ID="lblPAFEnq_SVONoDetails" CssClass="cls_label" runat="server"  EnableViewState="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 1%; white-space: nowrap;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCallAnalysisByDayInfo" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <span class="cls_label_header" >W</span>
                                                            <span class="cls_label" >- Working Calendar,</span>
                                                            <span class="cls_label_header">C</span>
                                                            <span class="cls_label" >= Call Days,</span>
                                                            <span class="cls_label_header" >MTD Calls</span>
                                                            <span class="cls_label" >= Month To Date Calls</span></td>
                                                        <td align="right">
                                                            <asp:Image ID="imgCallAnalysisByDayInfo_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCallAnalysisByDayInfo')" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <span class="cls_label_header" >SAT</span>
                                                            <span class="cls_label" >- Saturday,</span>
                                                            <span class="cls_label_header">Sun</span>
                                                            <span class="cls_label" >= Sunday, </span>
                                                            <span class="cls_label_header">AL - Annual Leave</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >al - Annual Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >MC - Medical Leave</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >mc - Medical Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >EL - Emergency Leave</span>
                                                            <span class="cls_label" >,</span
                                                            <span class="cls_label_header">el - Emergency Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >TR - Training</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >tr - Training 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >TV - Travelling</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >tv - Travelling 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >OT - Other</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header">OT - Other</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ot - Other 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >PM - Planned Meeting</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header">pm - Planned Meeting 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header">RL - Replacement Leave</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >rl - Replacement Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ML - Paternity/Maternity Leave</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ml - Paternity/Maternity Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >CL - Compassionate Leave</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >cl - Compassionate Leave 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >UM - Unplanned Meeting</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >um - Unplanned Meeting 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header">CM - Cycle Meeting</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >cm - Cycle Meeting 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >IT - Incentive Trip</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >it - Incentive Trip 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >PH - Public Holiday</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >DS - Delivery of Stock</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ds - Delivery of Stock 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >TS - Team Discussion</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >TS - Team Discussion</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ts - Team Discussion 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >SM - Sales Mgr Briefing</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header">sm - Sales Mgr Briefing 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >FW - Field Work Preparation</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >fw - Field Work Preparation 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >SC - Screening</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >sc - Screening 1/2</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >CH - Coaching</span>
                                                            <span class="cls_label" >,</span>
                                                            <span class="cls_label_header" >ch - Coaching 1/2</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCallAnalysisByMonthInfo" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <span class="cls_label_header" >W</span>
                                                            <span class="cls_label" >- Working Calendar,</span>
                                                            <span class="cls_label_header">C</span>
                                                            <span class="cls_label" >= Call Date,</span>
                                                            <span class="cls_label_header" >YTD</span>
                                                            <span class="cls_label" >= Year To Date,</span>
                                                            <span class="cls_label_header" >PYTD</span>
                                                            <span class="cls_label" >= Previous Year To Date</span>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Image ID="imgCallAnalysisByMonthInfos_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlCallAnalysisByMonthInfo')" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlFieldConfig" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                                    <tr>
                                                        <td align="left" style="white-space: nowrap; width: 35%">
                                                            <asp:RadioButtonList ID="radlFieldConfig" runat="server" RepeatDirection="Horizontal"
                                                                CssClass="cls_radiobuttonlist">
                                                                <asp:ListItem Selected="True" Value="1">Name Only</asp:ListItem>
                                                                <asp:ListItem Value="2">Code Only</asp:ListItem>
                                                                <asp:ListItem Value="3">Code And Name</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td align="left" style="white-space: nowrap">
                                                            <asp:Button ID="btnFieldConfig_refresh" CssClass="cls_button" runat="server" Text="Refresh">
                                                            </asp:Button>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Image ID="imgFieldConfig_Close" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" 
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_ctrlpanel_pnlFieldConfig')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlFieldConfig" runat="server" CollapseControlID="imgFieldConfig"
                                        ExpandControlID="imgFieldConfig" TargetControlID="pnlFieldConfig" CollapsedSize="0"
                                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGroupField" runat="server" CollapseControlID="imgGroupingField"
                        ExpandControlID="imgGroupingField" TargetControlID="pnlGroupingField" CollapsedSize="0"
                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                    </ajaxToolkit:CollapsiblePanelExtender>
                   <%-- <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlEnquiryDetail" runat="server" CollapseControlID="imgEnquiryDetails"
                        ExpandControlID="imgEnquiryDetails" TargetControlID="pnlSalesEnquirySearchDetails"
                        CollapsedSize="0" Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                    </ajaxToolkit:CollapsiblePanelExtender>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="0" class="cls_panel_header">
    <asp:Panel ID="pnlExport" runat="server" Visible="false">
        <tr>
            <td>
                <table class="cls_panel" id="Table1" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlExport" CssClass="cls_dropdownlist" runat="server">
                                            <asp:ListItem Value="0" Selected="True">Excel</asp:ListItem>
                                            <asp:ListItem Value="1">Text</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:Button ID="btnExport" CssClass="cls_button" runat="server" Text="Export"></asp:Button></td>
                                    <td align="right" width="90%">
                                        <asp:ImageButton ID="imgClose_Export" runat="server" ImageUrl="~/images/ico_close.gif" EnableViewState="false" >
                                        </asp:ImageButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </asp:Panel>
</table>
<input type="hidden" runat="server" id="ClientHeight" name="ClientHeight" value="" />