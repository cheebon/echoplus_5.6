<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaveList.aspx.vb" Inherits="iFFMS_Leave_LeaveList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="~/iFFMS/COMMON/wuc_Prdsearch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Leave</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
      <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglist');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
      function CalculateAmt()
      {var txtqty = document.getElementById('txtqty');var lbluomprice = document.getElementById('lbluomprice');var lblamount =  document.getElementById('lblamount');
       var ddllinetype = document.getElementById('ddllinetype'); var amt = (txtqty.value * lbluomprice.value); 
       var strlinetype = ddllinetype.value; 
       if(strlinetype == '.' || strlinetype == ','){lblamount.value = amt.toFixed(2);}else{lblamount.value = 0;}}
       
               function ValidatedglistLeaveCheckBoxStates()
   { var dglist = document.getElementById('dgList');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('No leave selected to submit!'); return false;}     
   }
      </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmLeave" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateLeave" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlLeave" runat="server" Enabled="False" Interval="100" OnTick="TimerControlLeave_Tick" />
                                <div id="LeaveList" class="S_DivHeader">
                                    Last 7 days Leave
                                </div>
                                <asp:Panel ID="pLeave" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <asp:Label ID="lblSmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                        BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" runat="server" />
                                            <asp:Button ID="btnSubmitBatch" runat="server" Text="Submit" CssClass="cls_button"
                                                ValidationGroup="submitLeave" Visible="false" Width="80px" OnClientClick="return ValidatedglistLeaveCheckBoxStates();" />
                                            <asp:Button ID="btndeleteBatch" runat="server" Text="Delete" CssClass="cls_button"
                                                ValidationGroup="deleteLeave" Visible="false" Width="80px" OnClientClick="return ValidatedglistLeaveCheckBoxStates();" />
                                            
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 0px;">
                                        <ccGV:clsGridView ID="dglist" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="REASON_CODE,START_DATE,END_DATE,STATUS">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server" ></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="REASON_CODE" HeaderText="Reason code" ReadOnly="True"
                                                    SortExpression="REASON_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="REASON_NAME" HeaderText="Reason Name" ReadOnly="True"
                                                    SortExpression="REASON_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="START_DATE" HeaderText="Start Date" ReadOnly="True" SortExpression="START_DATE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="END_DATE" HeaderText="End Date" ReadOnly="True" SortExpression="END_DATE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeLeave" runat="server" TargetControlID="pLeave"
                                    ExpandControlID="LeaveList" CollapseControlID="LeaveList">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewLeave" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <div id="NewLeave" class="S_DivHeader">
                                    Add New Leave
                                </div>
                                <asp:Panel ID="PNewLeave" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <asp:Label ID="lblNmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                        BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 0px;
                                        padding-bottom: 10px;">
                                        <table width="60%">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="cls_button" Width="80px"
                                                        ValidationGroup="Leave" />
                                                    <asp:Button ID="btnKIV" runat="server" Text="KIV" CssClass="cls_button" Width="80px"
                                                        ValidationGroup="Leave" />
                                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="cls_button" Width="80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    <span class="cls_label">Reason Code</span>
                                                </td>
                                                <td width="5%">
                                                    <span class="cls_label">: </span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReasonCode" runat="server" CssClass="cls_dropdownlist">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvddlReasonCode" ControlToValidate="ddlReasonCode"
                                                        runat="server" ValidationGroup="Leave" ErrorMessage="Select a reason" CssClass="cls_label_err"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtStartDateVal" runat="server" CssClass="cls_textbox" Visible="false"
                                                        MaxLength="10"></asp:TextBox>
                                                        <asp:TextBox ID="txtEndDateVal" runat="server" CssClass="cls_textbox" Visible="false"
                                                        MaxLength="10"></asp:TextBox> 
                                                    <span class="cls_label">Start Date</span></td>
                                                <td width="5%">
                                                    <span class="cls_label">: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtstartdate" runat="server" CssClass="cls_textbox" ValidationGroup="Leave"
                                                        MaxLength="10"></asp:TextBox>
                                                    <asp:ImageButton ID="imgstartdate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                        CssClass="cls_button" CausesValidation="false" />
                                                    <ajaxToolkit:CalendarExtender ID="cestartdate" TargetControlID="txtstartdate" runat="server"
                                                        PopupButtonID="imgstartdate" Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="CVtxtstartdate" runat="server" Display="Dynamic" ControlToValidate="txtstartdate"
                                                        CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="Leave" />
                                                    <asp:CompareValidator ID="CVtxtstartdate2" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtstartdate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                        ErrorMessage=" Invalid Date!" ValidationGroup="Leave" />
                                                    <asp:CompareValidator ID="CVBackStartDate" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtstartdate" Display="Dynamic" Operator="GreaterThanEqual" Type="Date"
                                                        ErrorMessage=" Cannot more than maximum day allowed." ValidationGroup="Leave" Enabled="false"  ControlToCompare="txtStartDateVal" />
                                                    <asp:CompareValidator ID="CVFutureStartDate" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtstartdate" Display="Dynamic" Operator="LessThanEqual" Type="Date"
                                                        ErrorMessage=" Cannot more than maximum day allowed." ValidationGroup="Leave" Enabled="false" ControlToCompare="txtEndDateVal" />
                                                  <%--  <asp:CompareValidator runat="server" ID="CVtxtstartdate3" ControlToValidate="txtstartdate"
                                                        CssClass="cls_validator" Display="Dynamic" ValueToCompare='<%# DateTime.Now.ToString("d") %>'
                                                        Type="date" Operator="GreaterThanEqual" ErrorMessage="Date less than today" ValidationGroup="Leave" />--%>
                                                    <asp:RequiredFieldValidator ID="rfvtxtstartdate" ControlToValidate="txtstartdate"
                                                        runat="server" ValidationGroup="Leave" ErrorMessage="Enter start date" CssClass="cls_label_err"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                               </tr>
                                               <tr>
                                                <td>
                                                    <span class="cls_label">End Date</span></td>
                                                <td width="5%">
                                                    <span class="cls_label">: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtenddate" runat="server" CssClass="cls_textbox" ValidationGroup="Leave"
                                                        MaxLength="10"></asp:TextBox>
                                                        
                                                    <asp:ImageButton ID="imgenddate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                        CssClass="cls_button" CausesValidation="false" />
                                                    <ajaxToolkit:CalendarExtender ID="cetxtenddate" TargetControlID="txtenddate" runat="server"
                                                        PopupButtonID="imgenddate" Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="cvtxtenddate" runat="server" Display="Dynamic" ControlToValidate="txtenddate"
                                                        CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="Leave" />
                                                    <asp:CompareValidator ID="cvtxtenddate2" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtenddate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                        ErrorMessage=" Invalid Date!" ValidationGroup="Leave" />
                                                   <%-- <asp:CompareValidator runat="server" ID="cvtxtenddate3" ControlToValidate="txtenddate"
                                                        CssClass="cls_validator" Display="Dynamic" ValueToCompare='<%# DateTime.Now.ToString("d") %>'
                                                        Type="date" Operator="GreaterThanEqual" ErrorMessage="Date less than today" ValidationGroup="Leave" />--%>
                                                    <asp:RequiredFieldValidator ID="rfvtxtenddate" ControlToValidate="txtenddate" runat="server"
                                                        ValidationGroup="Leave" ErrorMessage="Enter start date" CssClass="cls_label_err"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                      <asp:CompareValidator runat="server" ID="cvtxtenddate4" ControlToValidate="txtenddate"
                                                        CssClass="cls_validator" Display="Dynamic"  ControlToCompare="txtstartdate"
                                                        Type="date" Operator="GreaterThanEqual" ErrorMessage="Date must not be less that start date" ValidationGroup="Leave" />
                                                    <asp:CompareValidator ID="CVEndFutureDate" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtenddate" Display="Dynamic" Operator="LessThanEqual" Type="Date"
                                                        ErrorMessage=" Cannot more than maximum day allowed." ValidationGroup="Leave" Enabled="false" ControlToCompare="txtEndDateVal"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <span class="cls_label">Remarks</span></td>
                                                <td width="5%" valign="top">
                                                    <span class="cls_label">: </span>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtremarks" runat="server" Width="300px" Rows="10" TextMode="MultiLine"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewLeave" runat="server" TargetControlID="PNewLeave"
                                    ExpandControlID="NewLeave" CollapseControlID="NewLeave">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
