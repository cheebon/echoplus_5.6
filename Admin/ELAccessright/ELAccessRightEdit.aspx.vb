﻿Imports System.Data
Imports Portal
Imports System.Threading
Imports System.Globalization
Imports System.Collections.Generic


''' <summary>
'''  ************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-08-15
'''	Purpose	    :	Easy Loader Accessright Edit
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
''' 
Partial Class Admin_ELAccessright_ELAccessRightEdit
    Inherits System.Web.UI.Page
#Region "Local properties"
    Private AddNewUpdateCBChckTotal, DeleteCBChckTotal, FullRefreshCBChckTotal As Integer 'To keep track how many CheckBoxes checked , DeleteCBChckTotal
    Private AddNewUpdateJS, DeleteJS, FullRefreshJS As New List(Of String) ', DeleteJS

    Protected Property accessright_id() As String
        Get
            If Not ViewState("accessright_id") = Nothing Then
                Return ViewState("accessright_id").ToString()
            Else
                Return "0"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("accessright_id") = value
        End Set
    End Property 'store them in viewstate to perserve their value 
    Protected Property accessright_name() As String
        Get
            Return ViewState("accessright_name")
        End Get
        Set(ByVal value As String)
            ViewState("accessright_name") = value
        End Set
    End Property 
    Protected Property cat_code() As String
        Get
            If Not ViewState("cat_code") = Nothing Then
                Return ViewState("cat_code")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("cat_code") = value
        End Set
    End Property

    Public ReadOnly Property SessionUser() As String
        Get
            Return Portal.UserSession.UserID
        End Get
    End Property

    Public ReadOnly Property SessionUserPrincipal() As String
        Get
            Return Session("PRINCIPAL_ID") 'PortalSession.UserProfile.User.PrincipalID
        End Get
    End Property

    Private Property UrlParams() As String
        Get
            Return ViewState("UrlParams")
        End Get
        Set(ByVal value As String)
            ViewState("UrlParams") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErr.Text = ""
        updPnlErr.Update()
        Dim _ori As String

        Try


            If Not Page.IsPostBack Then
                'lblHeader.Title = "Edit Accessright "
                lblHeader.Title = "Easy Loader V2 Accessright Edit" 'Me.GetLocalResourceObject("PageResource1.Title").ToString

                accessright_id = Request.QueryString("accessright_id")
                _ori = Request.QueryString("ori")
                UrlParams = "search_type=" & Trim(Request.QueryString("search_type")) & "&search_value=" & Trim(Request.QueryString("search_value"))

                If _ori = "edit" Then
                    btnBack.Visible = True
                ElseIf _ori = "create" Then
                    btnDone.Visible = True
                End If

                GetTitle()
                FillARDetails()
                GetModuleList()
                LoadReferenceDDL()
            End If
            btnSave.Visible = False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateARPnl()
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub GetTitle()
        Dim objAccessRightQuery As New adm_ELAccessRight.clsELAccessRightQuery
        Dim dt As DataTable

        With objAccessRightQuery
            .clsQuery.accessright_id = Me.accessright_id
            dt = .GetAccessRightDtl()
        End With

        lblHeader.Title = "Easy Loader V2 Accessright Edit" & " - " & dt.Rows(0)("accessright_name").ToString

    End Sub
#Region "Update Panel"
    Private Sub UpdateErrorPanel()
        updPnlErr.Update()
    End Sub
#End Region

#Region "Private Functions"
    Private Sub GetModuleList()
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_ELAccessRight.clsELAccessRightQuery

        Try
            objAccessRightQuery = New adm_ELAccessRight.clsELAccessRightQuery
            With objAccessRightQuery
                dt = objAccessRightQuery.GetModuleList
            End With

            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            With dgModuleList
                .DataSource = dt
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub 'obtain modules assoc to principal

    Private Sub GetSubModuleList(ByVal cat_code As String, Optional ByVal intPassFlag As Integer = 0, Optional ByVal strAccessrightID As String = "")
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_ELAccessRight.clsELAccessRightQuery

        Try
            objAccessRightQuery = New adm_ELAccessRight.clsELAccessRightQuery
            If intPassFlag = 0 Then
                objAccessRightQuery.clsQuery.accessright_id = accessright_id
            Else
                objAccessRightQuery.clsQuery.accessright_id = strAccessrightID
            End If

            objAccessRightQuery.clsQuery.cat_code = cat_code
            'objAccessRightQuery.clsQuery.principal_id = SessionUserPrincipal
            dt = objAccessRightQuery.GetSubModuleListAction

            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            Else
                btnSave.Visible = True
            End If

            With dgSubModuleList
                .DataSource = dt
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub 'obtain submodules assoc to selected module

    Private Sub ClientAddCBHeader(ByRef gvr As GridViewRow)
        CType(gvr.FindControl("AddNewUpdateAllCheckBox"), CheckBox).Attributes("onclick") = "AddNewUpdateControlAll(this.checked);"
        AddNewUpdateJS.Add(String.Concat("'", CType(gvr.FindControl("AddNewUpdateAllCheckBox"), CheckBox).ClientID, "'"))

        CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).Attributes("onclick") = "DeleteControlAll(this.checked);"
        DeleteJS.Add(String.Concat("'", CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).ClientID, "'"))

        CType(gvr.FindControl("FullRefreshAllCheckBox"), CheckBox).Attributes("onclick") = "FullRefreshControlAll(this.checked);"
        FullRefreshJS.Add(String.Concat("'", CType(gvr.FindControl("FullRefreshAllCheckBox"), CheckBox).ClientID, "'"))

        'CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).Attributes("onclick") = "DeleteControlAll(this.checked);"
        'DeleteJS.Add(String.Concat("'", CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).ClientID, "'"))
    End Sub 'setup header checkbox

    Private Sub CheckHeader()
        Dim AddNewUpdateCBCount, DeleteCBCount, FullRefreshCBCount As Integer ', DeleteCBCount 
        Dim vcb, ccb, ecb, dcb, vHeader, cHeader, eHeader, dHeader As CheckBox

        AddNewUpdateCBCount = 0
        DeleteCBCount = 0
        FullRefreshCBCount = 0
        'DeleteCBCount = 0
        vHeader = CType(dgSubModuleList.HeaderRow.FindControl("AddNewUpdateAllCheckBox"), CheckBox)
        cHeader = CType(dgSubModuleList.HeaderRow.FindControl("DeleteAllCheckBox"), CheckBox)
        eHeader = CType(dgSubModuleList.HeaderRow.FindControl("FullRefreshAllCheckBox"), CheckBox)
        'dHeader = CType(dgSubModuleList.HeaderRow.FindControl("DeleteAllCheckBox"), CheckBox)

        For Each gvr As GridViewRow In dgSubModuleList.Rows
            vcb = CType(gvr.FindControl("AddNewUpdateCheckBox"), CheckBox)
            ccb = CType(gvr.FindControl("DeleteCheckBox"), CheckBox)
            ecb = CType(gvr.FindControl("FullRefreshCheckBox"), CheckBox)
            'dcb = CType(gvr.FindControl("DeleteCheckBox"), CheckBox)

            If vcb.Visible Then AddNewUpdateCBCount = AddNewUpdateCBCount + 1
            If ccb.Visible Then DeleteCBCount = DeleteCBCount + 1
            If ecb.Visible Then FullRefreshCBCount = FullRefreshCBCount + 1
            'If dcb.Visible Then DeleteCBCount = DeleteCBCount + 1
        Next

        'if action is n/a for all submodule, then header checkbox shall disappear
        If AddNewUpdateCBCount = 0 Then
            vHeader.Visible = False
        ElseIf AddNewUpdateCBCount = AddNewUpdateCBChckTotal Then
            vHeader.Checked = True
        End If

        If DeleteCBCount = 0 Then
            cHeader.Visible = False
        ElseIf DeleteCBCount = DeleteCBChckTotal Then
            cHeader.Checked = True
        End If

        If FullRefreshCBCount = 0 Then
            eHeader.Visible = False
        ElseIf FullRefreshCBCount = FullRefreshCBChckTotal Then
            eHeader.Checked = True
        End If

        'If DeleteCBCount = 0 Then
        '    dHeader.Visible = False
        'ElseIf DeleteCBCount = DeleteCBChckTotal Then
        '    dHeader.Checked = True
        'End If
    End Sub 'setup header checkbox checked state in accordance to cb checked state in rows

    Private Sub InjectClientCBArray()
        Dim js As New StringBuilder
        js.Append("<script type='text/javascript'>")
        js.Append(String.Concat("var AddNewUpdateCheckBoxIDs = new Array(", String.Join(",", AddNewUpdateJS.ToArray()), ");"))
        js.Append(String.Concat("var DeleteCheckBoxIDs = new Array(", String.Join(",", DeleteJS.ToArray()), ");"))
        js.Append(String.Concat("var FullRefreshCheckBoxIDs = new Array(", String.Join(",", FullRefreshJS.ToArray()), ");"))
        'js.Append(String.Concat("var DeleteCheckBoxIDs = new Array(", String.Join(",", DeleteJS.ToArray()), ");"))
        js.Append("</script>")

        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CheckJS", js.ToString(), False)
    End Sub 'pass all checkboxes to client site

    Private Sub FillARDetails()
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_ELAccessRight.clsELAccessRightQuery

        Try
            objAccessRightQuery = New adm_ELAccessRight.clsELAccessRightQuery
            With objAccessRightQuery
                .clsQuery.accessright_id = accessright_id
                dt = .GetAccessRightDtl()
            End With

            txtName.Text = Trim(dt.Rows(0)("accessright_name"))
            accessright_name = Trim(dt.Rows(0)("accessright_name"))
            txtName1.Text = Trim(dt.Rows(0)("accessright_name_1"))
            txtDesc.Text = Trim(dt.Rows(0)("accessright_desc"))
            txtDesc1.Text = Trim(dt.Rows(0)("accessright_desc_1"))
            'country_id = Trim(dt.Rows(0)("country_id"))
            'principal_id = Trim(dt.Rows(0)("principal_id"))
            'lblCounName.Text = Trim(dt.Rows(0)("country_name"))
            'lblPrinName.Text = Trim(dt.Rows(0)("principal_name"))

            updPnlARDetail.Update()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try

    End Sub
#End Region
#Region "Update Panel"
    Private Sub UpdateARPnl()
        updPnlAccessright.Update()
    End Sub
    Private Sub UpdateARDtlPnl()
        updPnlARDtl.Update()
    End Sub
#End Region
#Region "Events"
    Protected Sub dgModuleList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgModuleList.Rows(index)
        Dim lb As LinkButton
        Dim strModuleName As String

        'reset variables
        AddNewUpdateCBChckTotal = 0
        DeleteCBChckTotal = 0
        FullRefreshCBChckTotal = 0
        'DeleteCBChckTotal = 0
        AddNewUpdateJS.Clear()
        DeleteJS.Clear()
        FullRefreshJS.Clear()
        'DeleteJS.Clear()

        Try
            Select Case e.CommandName
                Case "Select"
                    lblMesg.Visible = False
                    dgSubModuleList.Visible = True
                    lb = CType(row.Cells(1).Controls(0), LinkButton)
                    strModuleName = Trim(dgModuleList.DataKeys(row.RowIndex).Values(1))
                    'btnSave.Text = Me.GetLocalResourceObject("btnSaveResource1.Text").ToString + " (" & Me.GetLocalResourceObject("lblAccessright.Text").ToString & ")"
                    btnSave.Text = "Save" + " (" & strModuleName & ")"
                    btnSave.Width = btnSave.Text.Length * 8
                    'btnSave.Visible = True
                    cat_code = Trim(dgModuleList.DataKeys(row.RowIndex).Value)
                    GetSubModuleList(cat_code)
            End Select

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateARPnl()
        End Try
    End Sub

    Protected Sub dgSubModuleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim drv As DataRowView
        Dim AddNewUpdate As String
        Dim Delete As String
        Dim FullRefresh As String
        'Dim delete As String
        'Dim cbh As CheckBox
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                ClientAddCBHeader(e.Row)
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)
                AddNewUpdate = drv("AddNewUpdate").ToString()
                Delete = drv("Delete").ToString()
                FullRefresh = drv("FullRefresh").ToString()
                'delete = drv("DELETE").ToString()

                If AddNewUpdate = "2" Then
                    CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).Attributes("onclick") = "ValidateAddNewUpdateHeader(this.checked);"
                    AddNewUpdateJS.Add(String.Concat("'", CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).ClientID, "'"))
                    AddNewUpdateCBChckTotal = AddNewUpdateCBChckTotal + 1
                ElseIf AddNewUpdate = "1" Then
                    CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).Attributes("onclick") = "ValidateAddNewUpdateHeader(this.checked);"
                    AddNewUpdateJS.Add(String.Concat("'", CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("AddNewUpdateCheckBox"), CheckBox).Visible = False
                End If

                If Delete = "2" Then
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                    DeleteCBChckTotal = DeleteCBChckTotal + 1
                ElseIf Delete = "1" Then
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = False
                End If

                If FullRefresh = "2" Then
                    CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).Attributes("onclick") = "ValidateFullRefreshHeader(this.checked);"
                    FullRefreshJS.Add(String.Concat("'", CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).ClientID, "'"))
                    FullRefreshCBChckTotal = FullRefreshCBChckTotal + 1
                ElseIf FullRefresh = "1" Then
                    CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).Attributes("onclick") = "ValidateFullRefreshHeader(this.checked);"
                    FullRefreshJS.Add(String.Concat("'", CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("FullRefreshCheckBox"), CheckBox).Visible = False
                End If

                'If delete = "2" Then
                '    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Checked = True
                '    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                '    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                '    DeleteCBChckTotal = DeleteCBChckTotal + 1
                'ElseIf delete = "1" Then
                '    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = True
                '    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                '    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                'Else
                '    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = False
                'End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objAccessRightDtl As adm_ELAccessRight.clsELAccessrightDtl
        Dim i As Integer
        Dim row As GridViewRow
        Dim vcb, ccb, ecb, dcb As CheckBox
        Dim el_code As String
        Dim vAct, cAct, eAct, dAct As String

        Try
            For i = 0 To dgSubModuleList.Rows.Count - 1
                row = dgSubModuleList.Rows(i)
                vcb = CType(row.FindControl("AddNewUpdateCheckBox"), CheckBox)
                ccb = CType(row.FindControl("DeleteCheckBox"), CheckBox)
                ecb = CType(row.FindControl("FullRefreshCheckBox"), CheckBox)
                ' dcb = CType(row.FindControl("DeleteCheckBox"), CheckBox)

                el_code = dgSubModuleList.DataKeys(i).Value
                If vcb.Visible = True Then
                    vAct = IIf(vcb.Checked, "1", "0")
                Else
                    vAct = -1
                End If
                If ccb.Visible = True Then
                    cAct = IIf(ccb.Checked, "1", "0")
                Else
                    cAct = -1
                End If
                If ecb.Visible = True Then
                    eAct = IIf(ecb.Checked, "1", "0")
                Else
                    eAct = -1
                End If
                'If dcb.Visible = True Then
                '    dAct = IIf(dcb.Checked, "1", "0")
                'Else
                '    dAct = -1
                'End If

                objAccessRightDtl = New adm_ELAccessRight.clsELAccessrightDtl
                With objAccessRightDtl
                    .clsProperties.accessright_id = accessright_id
                    .clsProperties.cat_code = Trim(cat_code)
                    .clsProperties.el_code = Trim(el_code)
                    ' .Alter(vAct, cAct, eAct, dAct, Me.SessionUser)
                    .Alter(vAct, cAct, eAct, Me.SessionUser)
                End With
            Next
            dgSubModuleList.Visible = False
            lblMesg.Text = "Records saved!" 'Me.GetLocalResourceObject("lblMesg.Text").ToString 
            lblMesg.Visible = True
            btnSave.Visible = False

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightDtl = Nothing
            UpdateARPnl()
        End Try
    End Sub

    Protected Sub dgSubModuleList_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckHeader()
        InjectClientCBArray()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDupError.Visible = False
        FillARDetails()
        mdlARPopup.Show()
        UpdateARDtlPnl()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        mdlARPopup.Hide()
        UpdateARDtlPnl()
    End Sub

    Protected Sub btnSaveARDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objAccessRight As adm_ELAccessRight.clsELAccessRight
        Dim objAccessRightQuery As adm_ELAccessRight.clsELAccessRight
        Dim dt As DataTable
        Dim intCnt As Integer


        Try
            'Check AccessRight name exist or not
            If Trim(txtName.Text) <> accessright_name Then
                objAccessRightQuery = New adm_ELAccessRight.clsELAccessRight
                With objAccessRightQuery
                    .clsPpt.accessright_id = "0" 'txtAccessRightID.Text
                    .clsPpt.accessright_name = Trim(txtName.Text)
                    dt = .CheckARDuplicate()
                    intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
                End With
                objAccessRightQuery = Nothing

                If intCnt > 0 Then
                    mdlARPopup.Show()
                    lblDupError.Visible = True
                    Exit Sub
                End If
            End If
            'aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            'aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            'Create record
            objAccessRight = New adm_ELAccessRight.clsELAccessRight
            With objAccessRight
                .clsPpt.accessright_id = accessright_id
                .clsPpt.accessright_name = Trim(txtName.Text)
                .clsPpt.accessright_name_1 = Trim(txtName1.Text)
                .clsPpt.accessright_desc = Trim(txtDesc.Text)
                .clsPpt.accessright_desc_1 = Trim(txtDesc1.Text)
                '.clsPpt.country_id = Convert.ToDouble(country_id)
                '.clsPpt.principal_id = Convert.ToDouble(principal_id)
                .UpdateAR(Me.SessionUser)
            End With

            FillARDetails()
            mdlARPopup.Hide()
            lblDupError.Visible = False

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
            objAccessRight = Nothing

            UpdateARDtlPnl()
        End Try
    End Sub

    Protected Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Response.Redirect("ELAccessRightDtl.aspx?accessright_id=" & accessright_id)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("ELAccessRightList.aspx?" & UrlParams, True)
    End Sub
#End Region

#Region "Reference Functions"
    Private Sub LoadReferenceDDL()
        Dim objAccessRightQuery As adm_ELAccessRight.clsELAccessRightQuery
        Dim dt As DataTable

        Try
            objAccessRightQuery = New adm_ELAccessRight.clsELAccessRightQuery
            With objAccessRightQuery
                .clsQuery.search_type = "ALL"
                .clsQuery.usr_id = Me.SessionUser
                .clsQuery.search_value = "%"
                dt = .GetAccessrightList("ACCESSRIGHT_NAME", "ASC", 1, 20, 1)
            End With

            With ddlAccessright
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "ACCESSRIGHT_NAME"
                .DataValueField = "ACCESSRIGHT_ID"
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRef.Click
        Try
            If ReadyToRef() Then
                LoadRefenceDetails(ddlAccessright.SelectedValue.Trim)
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function ReadyToRef() As Boolean
        If ddlAccessright.SelectedIndex > -1 Then
            If (Not IsNothing(cat_code)) Or cat_code <> 0 Or cat_code <> "" Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Sub LoadRefenceDetails(ByVal strAccessrightID As String)
        Try
            GetSubModuleList(cat_code, 1, strAccessrightID)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
End Class
