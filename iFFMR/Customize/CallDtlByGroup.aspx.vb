﻿Imports System.Data
Partial Class iFFMR_Customize_CallDtlByGroup
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "CallDtlByGroup"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region


#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "CallDtlByGroup"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  Try
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.CALLDTLBYGROUP)
            .DataBind()
            .Visible = True
        End With

        ''Call Paging()
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        '    .DataBind()
        '    .Visible = True
        'End With

        If Not IsPostBack Then
            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLDTLBYGROUP
                .DataBind()
                .Visible = True
            End With
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True


        End If

        lblErr.Text = ""

        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        '  End Try

    End Sub

#Region "EVENT HANDLER"

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        If IsPostBack Then RenewDataBind()
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        RenewDataBind()
    End Sub
#End Region

    '#Region "Paging Control"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = CInt(Wuc_dgpaging.PageNo - 1)

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        ViewState("strSortExpression") = Nothing
        'ViewState("dtCurrentView") = dtCurrentTable
        dgList.PageIndex = 0
        'End If
        PreRenderMode(dtCurrentTable)
        'If dtCurrentTable.Rows.Count = 0 Then
        '    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        'End If

        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.PageSize = 20 'intPageSize
        dgList.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_CallDtlByGroup.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CallDtlByGroup.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CallDtlByGroup.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallDtlByGroup.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallDtlByGroup.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_CallDtlByGroup.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "DESC_CODE"

                    Dim strSalesrepCode As String, strTeamCode As String
                    strTeamCode = Request.QueryString("Team_Code")
                    strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String

                    'strUrlFormatString = ("SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}")
                    strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CallDtlByGroup.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallDtlByGroup.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallDtlByGroup.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagrid.Update()
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

                If aryDataItem.IndexOf("SALESREP_NAME") >= 0 And aryDataItem.IndexOf("CHANNEL_NAME") >= 0 And aryDataItem.IndexOf("TEAM_NAME") >= 0 And aryDataItem.IndexOf("DEPT_NAME") >= 0 Then
                    Dim strCapCode As String = Trim(e.Row.Cells(aryDataItem.IndexOf("SALESREP_NAME")).Text)
                    Dim strCapCode2 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("CHANNEL_NAME")).Text)
                    Dim strCapCode3 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("TEAM_NAME")).Text)
                    Dim strCapCode4 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("DEPT_NAME")).Text)
                    If UCase(strCapCode) Like "*TOTAL" Or UCase(strCapCode2) Like "*TOTAL" Or UCase(strCapCode3) Like "*TOTAL" Or UCase(strCapCode4) Like "*TOTAL" Then
                        e.Row.CssClass = "GridFooter"

                    End If

                ElseIf aryDataItem.IndexOf("CHANNEL_NAME") >= 0 And aryDataItem.IndexOf("TEAM_NAME") >= 0 And aryDataItem.IndexOf("DEPT_NAME") >= 0 Then
                    Dim strCapCode2 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("CHANNEL_NAME")).Text)
                    Dim strCapCode3 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("TEAM_NAME")).Text)
                    Dim strCapCode4 As String = Trim(e.Row.Cells(aryDataItem.IndexOf("DEPT_NAME")).Text)
                    If UCase(strCapCode2) Like "*TOTAL" Or UCase(strCapCode3) Like "*TOTAL" Or UCase(strCapCode4) Like "*TOTAL" Then
                        e.Row.CssClass = "GridFooter"

                    End If

                End If



            Case DataControlRowType.Footer
        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '    If e.Row.RowType = DataControlRowType.Footer Then

    '    End If
    'End Sub


#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsCallDtlByGroup As New rpt_Customize.clsCallDtlByGroup

        Dim strUserID As String, strPrincipalID As String, strPrincipalCode As String, _
         strSalesRepList As String, strGroupCode As String, strYear As String, strMonth As String

     
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strSalesRepList = Session("SALESREP_LIST")
        strGroupCode = Session("NODE_GROUP_NAME")


        DT = clsCallDtlByGroup.GetCallDtlByGroup(strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode, strGroupCode, strSalesRepList)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        ' Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'strb.Append("<table>")
        'strb.Append("<tr><td>")
        'strb.Append(hdfSalesrepName.Value)
        'strb.Append("</td></tr>")
        'strb.Append("</table>")

        Dim pnl As New Panel
        Dim strb As New StringBuilder

        Dim lblInfo As New Label
        strb.Append("<B>Year       : </B>" & Session("YEAR") & "<BR/>")
        ' strb.Append("<B>Sales Team : </B>" & hdfSalesTeamName.Value & "<BR/>")
        'strb.Append("<B>Field Force: </B>" & hdfSalesrepName.Value & "<BR/>")
        strb.Append("<BR/>")

        lblInfo.Text = strb.ToString
        pnl.Controls.Add(lblInfo)
        pnl.Controls.Add(dgList)

        wuc_ctrlpanel.ExportToFile(pnl, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        ' Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        ' ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region
End Class

Public Class CF_CallDtlByGroup
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "GROUPING_ID"
                strFieldName = "-"
            Case "DEPT_CODE"
                strFieldName = "Dept Code"
            Case "DEPT_NAME"
                strFieldName = "Department Name"
            Case "NO_OF_CALLS"
                strFieldName = "No. Of Calls"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If (strColumnName Like "DESC_CODE") OrElse (strColumnName Like "ORD") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf strColumnName = "JAN" OrElse strColumnName = "FEB" OrElse strColumnName = "MAR" _
                     OrElse strColumnName = "APR" OrElse strColumnName = "MAY" OrElse strColumnName = "JUN" _
                     OrElse strColumnName = "JUL" OrElse strColumnName = "AUG" OrElse strColumnName = "SEP" _
                     OrElse strColumnName = "OCT" OrElse strColumnName = "NOV" OrElse strColumnName = "DEC" Then

            If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SRCALLRATEDTL, SubModuleAction.View) Then
                FCT = FieldColumntype.HyperlinkColumn
            End If

        End If

        Return FCT
        'Catch ex As Exception

        ' End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALLS", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUN"
                Return 6
            Case "JUL"
                Return 7
            Case "AUG"
                Return 8
            Case "SEP"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetDayValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        Dim intValue As Integer = 0

        strColumnName = strColumnName.Replace("_CALL", "").Replace("_DEF", "").Replace("DAY_", "")
        'strColumnName = strColumnName.Replace("S", "")
        If Not String.IsNullOrEmpty(strColumnName) AndAlso IsNumeric(strColumnName) Then intValue = CInt(strColumnName)
        Return intValue
        ' Catch ex As Exception
        ' End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        ' Try

        Select Case strNewName
            Case "NO_OF_CALLS"
                strStringFormat = "{0:0}"
            Case Else
                strStringFormat = ""
        End Select

        'If strColumnName.ToUpper Like "DAY_*_DEF" And Not strColumnName.ToUpper Like "DAY_*_CALL" Then
        If strColumnName.ToUpper Like "DAY_*_CALL" And Not strColumnName.ToUpper Like "DAY_*_DEF" Then
            strStringFormat = "{0:0.##}"
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf strColumnName Like "*_NAME" Then
                .HorizontalAlign = HorizontalAlign.Left
                'ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                '    .HorizontalAlign = HorizontalAlign.Left
            ElseIf strColumnName = "NO_OF_CALLS" Then
                .HorizontalAlign = HorizontalAlign.Right
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

    Public Shared Function GetMonthName(ByVal iMonth As Integer) As String
        Dim strMonthName As String = ""

        ' Try
        Select Case iMonth
            Case 1
                strMonthName = "JAN"
            Case 2
                strMonthName = "FEB"
            Case 3
                strMonthName = "MAR"
            Case 4
                strMonthName = "APR"
            Case 5
                strMonthName = "MAY"
            Case 6
                strMonthName = "JUN"
            Case 7
                strMonthName = "JUL"
            Case 8
                strMonthName = "AUG"
            Case 9
                strMonthName = "SEP"
            Case 10
                strMonthName = "OCT"
            Case 11
                strMonthName = "NOV"
            Case 12
                strMonthName = "DEC"
            Case Else
                strMonthName = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strMonthName

    End Function
End Class


