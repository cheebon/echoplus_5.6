﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSetting
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function SettingList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SETTING_LIST"
                .addItem("SEARCH_TYPE", strSearchType, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, cor_DB.clsDB.DataType.DBString)
                SettingList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSetting.SettingList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function SettingDtl(ByVal strSettingID As String, ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SETTING_DTL"
                .addItem("SETTING_ID", strSettingID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                SettingDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSetting.SettingDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function Create(ByVal strPageSize As String, _
                            ByVal FFMAServerIP As String, ByVal FFMAServerName As String, ByVal FFMADBName As String, ByVal FFMALinkServer As String, ByVal FFMAUserID As String, ByVal FFMAPAss As String, _
                            ByVal FFMRServerIP As String, ByVal FFMRServerName As String, ByVal FFMRDBName As String, ByVal FFMRLinkServer As String, ByVal FFMRUserID As String, ByVal FFMRPAss As String, _
                            ByVal FFMSServerIP As String, ByVal FFMSServerName As String, ByVal FFMSDBName As String, ByVal FFMSLinkServer As String, ByVal FFMSUserID As String, ByVal FFMSPAss As String, _
                            ByVal EchoServerIP As String, ByVal EchoServerName As String, ByVal EchoDBName As String, ByVal EchoLinkServer As String, ByVal EchoUserID As String, ByVal EchoPAss As String, _
                            ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SETTING_CREATE"
                .addItem("FFMA_SERVER_IP", FFMAServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMA_SERVER_NAME", FFMAServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_DB_NAME", FFMADBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_LINK_SERVER_NAME", FFMALinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_USER_ID", FFMAUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_USER_PWD", FFMAPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_SERVER_IP", FFMRServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMR_SERVER_NAME", FFMRServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_DB_NAME", FFMRDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_LINK_SERVER_NAME", FFMRLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_USER_ID", FFMRUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_USER_PWD", FFMRPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_SERVER_IP", FFMSServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMS_SERVER_NAME", FFMSServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_DB_NAME", FFMSDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_LINK_SERVER_NAME", FFMSLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_USER_ID", FFMSUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_USER_PWD", FFMSPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_SERVER_IP", EchoServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("ECHOPLUS_SERVER_NAME", EchoServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_DB_NAME", EchoDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_LINK_SERVER_NAME", EchoLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_USER_ID", EchoUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_USER_PWD", EchoPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("PAGE_SIZE", strPageSize, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Create = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSetting.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub Update(ByVal SettingID As String, ByVal strPageSize As String, _
                            ByVal FFMAServerIP As String, ByVal FFMAServerName As String, ByVal FFMADBName As String, ByVal FFMALinkServer As String, ByVal FFMAUserID As String, ByVal FFMAPAss As String, _
                            ByVal FFMRServerIP As String, ByVal FFMRServerName As String, ByVal FFMRDBName As String, ByVal FFMRLinkServer As String, ByVal FFMRUserID As String, ByVal FFMRPAss As String, _
                            ByVal FFMSServerIP As String, ByVal FFMSServerName As String, ByVal FFMSDBName As String, ByVal FFMSLinkServer As String, ByVal FFMSUserID As String, ByVal FFMSPAss As String, _
                            ByVal EchoServerIP As String, ByVal EchoServerName As String, ByVal EchoDBName As String, ByVal EchoLinkServer As String, ByVal EchoUserID As String, ByVal EchoPAss As String, _
                            ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SETTING_UPDATE"
                .addItem("SETTING_ID", SettingID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("FFMA_SERVER_IP", FFMAServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMA_SERVER_NAME", FFMAServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_DB_NAME", FFMADBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_LINK_SERVER_NAME", FFMALinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_USER_ID", FFMAUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMA_USER_PWD", FFMAPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_SERVER_IP", FFMRServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMR_SERVER_NAME", FFMRServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_DB_NAME", FFMRDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_LINK_SERVER_NAME", FFMRLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_USER_ID", FFMRUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMR_USER_PWD", FFMRPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_SERVER_IP", FFMSServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("FFMS_SERVER_NAME", FFMSServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_DB_NAME", FFMSDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_LINK_SERVER_NAME", FFMSLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_USER_ID", FFMSUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("FFMS_USER_PWD", FFMSPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_SERVER_IP", EchoServerIP, cor_DB.clsDB.DataType.DBString, True)
                .addItem("ECHOPLUS_SERVER_NAME", EchoServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_DB_NAME", EchoDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_LINK_SERVER_NAME", EchoLinkServer, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_USER_ID", EchoUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("ECHOPLUS_USER_PWD", EchoPAss, cor_DB.clsDB.DataType.DBString)
                .addItem("PAGE_SIZE", strPageSize, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSetting.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub Delete(ByVal SettingID As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SETTING_DELETE"
                .addItem("SETTING_ID", SettingID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spDelete()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSetting.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
