Imports System.Data
Imports txn_WebActy

Partial Class iFFMS_Customer_CustTxn_TxnMSS
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Public Enum dgcoldtl As Integer
        SUB_CAT_NAME = 2

    End Enum
    Public Enum dgcol As Integer
        SUB_CAT_NAME = 5

    End Enum

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property


    Private Property Master_Row_CountDtl() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountDtl"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountDtl") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustTxnMSS"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     
        LoadTxnDescName()

        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPosition();',100);", True)

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            If Trim(Request.QueryString("titlecode")) = "" Or Trim(Request.QueryString("titlecode")) Is Nothing Then
                TimerControl1.Enabled = True
                pMSSHdr.Visible = False
            Else
                EditKIV()
            End If

        End If
        lblErr.Text = ""

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabindingTitle()
        TimerControl1.Enabled = False
    End Sub

#Region "DGLIST TITLE"
    Public Sub RefreshDatabindingTitle(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionTitle"), String)


        dtCurrentTable = GetRecListTitle()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgListTitle
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With



        UpdateDatagridTitle_Update()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListTitle() As DataTable
        Try
            Dim DT As DataTable = Nothing
            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strSalesrepCode As String
                strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))


                Dim clsMSS As New txn_WebActy.clsMSS
                DT = clsMSS.GetTitlecode(strSalesrepCode)

            End If

            Return DT
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Public Sub UpdateDatagridTitle_Update()



        UpdateTitle.Update()

    End Sub

    Protected Sub dgListTitle_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListTitle.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionTitle")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionTitle") = strSortExpression

        RefreshDatabindingTitle()

    End Sub

    Protected Sub dgListTitle_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgListTitle.RowEditing
        Dim strTitleCode As String = sender.datakeys(e.NewEditIndex).item("TITLE_CODE")
        Dim strTitleName As String = sender.datakeys(e.NewEditIndex).item("TITLE_NAME")
        Dim strSalesrepCode As String = Trim(Request.QueryString("SalesrepCode"))
        Dim DT As DataTable = Nothing
        Dim clsMSS As New txn_WebActy.clsMSS
        DT = clsMSS.GetTRANo(strSalesrepCode)

        LoadTxnNo()
        lbltitlecode.Text = strTitleCode
        lbltitlename.Text = strTitleName
        RefreshDatabindingQues()
        pMSSTitle.Visible = False
        pMSSHdr.Visible = True
        dgListQues.SelectedIndex = -1
        UpdateHdr.Update()
    End Sub

#End Region

#Region "DGLIST QUES"
    Public Sub RefreshDatabindingQues(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionQues"), String)


        dtCurrentTable = GetRecListQues()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgListQues
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With


        UpdateDatagridQues_Update()
        UpdatePage.Update()


    End Sub

    Private Function GetRecListQues() As DataTable
        Try
            Dim DT As DataTable = Nothing


            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strTitleCode As String, strTxnNo As String, strSalesrepCode As String, strSessionId As String
                strTitleCode = Trim(lbltitlecode.Text)
                strTxnNo = Trim(lbltxnno.Text)
                strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                strSessionId = Trim(Request.QueryString("SessionId"))


                Dim clsMSS As New txn_WebActy.clsMSS
                DT = clsMSS.GetQuescode(strTitleCode, strTxnNo, strSalesrepCode, strSessionId)

            End If

            Return DT
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Public Sub UpdateDatagridQues_Update()



        UpdateHdr.Update()

    End Sub

    Protected Sub dgListQues_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListQues.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim strTitleCode As String = lbltitlecode.Text
                Dim strQuesCode As String
                Dim strTxnNo As String = Trim(lbltxnno.Text)
                Dim strSessionId As String = Trim(Request.QueryString("sessionid"))
                strQuesCode = dgListQues.DataKeys(e.Row.RowIndex).Item("QUES_CODE")
                Dim strAction As String = "ResizeFrameWidth('SubDetailBar','50%');ResizeFrameWidth('SubDetailConfBar','50%');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');"
                Dim strPosition As String = "SetScrollPosition();"
                Dim strSelectedindex As String = "javascript:__doPostBack('dgListQues','Select$" & e.Row.RowIndex & "');"
                Dim str3 As String '3 is the column to add attributes the link
                str3 = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMSSQues.aspx?sessionid=" + strSessionId + "&TXNNO=" + strTxnNo + "&TitleCode=" + strTitleCode + "&QuesCode=" + strQuesCode + "';"
                e.Row.Cells(3).Attributes.Add("onclick", str3 + strAction + strPosition + strSelectedindex)
                e.Row.Cells(3).Attributes.Add("style", "cursor:pointer;")

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgListQues_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListQues.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionQues")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionQues") = strSortExpression

        RefreshDatabindingQues()

    End Sub

#End Region

#Region "Header Function"

    Private Sub LoadTxnDescName()
        Try
            Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strvisitid = Trim(Request.QueryString("visitid"))
            strMapPath = "Ondemand > New Transaction > Market Survey"

            Dim dt As DataTable
            Dim clstxncommon As New txn_common.clstxncommon
            dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

            If dt.Rows.Count > 0 Then
                lblFieldForceName.Text = Trim(IIf(IsDBNull(dt.Rows(0)("SALESREP_NAME")), " ", dt.Rows(0)("SALESREP_NAME")))
                lblCustName.Text = Trim(IIf(IsDBNull(dt.Rows(0)("CUST_NAME")), " ", dt.Rows(0)("CUST_NAME")))
                lblContName.Text = Trim(IIf(IsDBNull(dt.Rows(0)("CONT_NAME")), " ", dt.Rows(0)("CONT_NAME")))
                lblVisitId.Text = strvisitid

                lblMapPath.Text = strMapPath
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadTxnNo()
        Try
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DTTXNno As DataTable
                Dim clsMSS As New txn_WebActy.clsMSS

                DTTXNno = clsMSS.GetTRANo(Trim(Request.QueryString("salesrepcode")))
                lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")

            Else
                lbltxnno.Text = strTxnNo
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub Submit(ByVal strTxnstatus As String)
        Try
            Dim strTxnNo = Trim(lbltxnno.Text)
            Dim strCustCode = Trim(Request.QueryString("CustCode"))
            Dim strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            Dim strContCode = Trim(Request.QueryString("ContCode"))
            Dim strVisitId = Trim(Request.QueryString("VisitId"))
            Dim strTitleCode = Trim(lbltitlecode.Text)
            Dim StrSessionId = Trim(Request.QueryString("SessionId"))
            Dim StrGenComment = Trim(txtgencomment.Text)
            Dim strTxnDate = Request.QueryString("datein") + " " + Request.QueryString("timein")

            Dim dt As DataTable
            Dim clsMss As New txn_WebActy.clsMSS
            dt = clsMss.SubmitTMPMSSDetailAndDetailMultiAdv(strTxnNo, strCustCode, strSalesrepCode, strVisitId, strTitleCode, StrGenComment, StrSessionId, strTxnstatus, strContCode, strTxnDate)

            Dim strerror As Integer = dt.Rows(0)("ERROR")

            If strerror = 0 Then
                'lblMsgPop.Message = "You have successfully submitted the transaction!!"
                'lblMsgPop.Show()
            Else
                lblMsgPop.Message = "You request is not successfully!!"
                lblMsgPop.Show()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DeleteDtl()
        Try
            Dim strTxnNo = Trim(lbltxnno.Text)
            Dim StrSessionId = Trim(Request.QueryString("SessionId"))
            Dim clsMss As New txn_WebActy.clsMSS
            clsMss.DelTMPMSSDetailAndDetailMultiAdv(StrSessionId, strTxnNo)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub EditKIV()
        Try
            LoadTxnNo()
            lbltitlecode.Text = Request.QueryString("TitleCode")
            Dim dt As DataTable
            Dim clsMSS As New txn_WebActy.clsMSS

            Dim strTxnNo = Trim(Request.QueryString("TXNNO"))
            Dim strTitleCode = Trim(Request.QueryString("TitleCode"))
            Dim strVisitId = Trim(Request.QueryString("visitid"))
            Dim StrSessionId = Trim(Request.QueryString("sessionid"))
            dt = clsMSS.GetTMPMSSHdrAdv(strTxnNo, strTitleCode, strVisitId, StrSessionId)

            lbltitlename.Text = dt.Rows(0)("TITLE_NAME").ToString
            txtgencomment.Text = dt.Rows(0)("GEN_COMMENT").ToString

            pMSSTitle.Visible = False
            pMSSHdr.Visible = True
            RefreshDatabindingQues()
            btnback.Visible = False
            UpdateHdr.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        DeleteDtl()
        pMSSTitle.Visible = True
        pMSSHdr.Visible = False
        UpdateTitle.Update()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim dt As DataTable

            Dim strTxnNo As String = Trim(lbltxnno.Text)
            Dim strCustCode As String = Trim(Request.QueryString("CustCode"))
            Dim strSalesrepCode As String = Trim(Request.QueryString("SalesrepCode"))
            Dim strVisitId As String = Trim(Request.QueryString("VisitId"))
            Dim strTitleCode As String = Trim(lbltitlecode.Text)
            Dim StrContCode As String = Trim(Request.QueryString("ContCode"))
            Dim strStatus As String = "P"
            Dim StrSessionId As String = Trim(Request.QueryString("sessionid"))

            Dim clsMss As New txn_WebActy.clsMSS
            dt = clsMss.GetTMPMSSHdrdTLAdvValidate(strTxnNo, strTitleCode, strVisitId, StrSessionId)

            If dt.Rows.Count > 0 Then
                Submit("P")
                Session("TxnValid") = "1"
                Response.Redirect("TxnMSSSub.aspx?TxnType=MSS&TXNNO=" + strTxnNo + "&visitid=" + strVisitId + "&SALESREPCODE=" + strSalesrepCode + "&CustCode=" + strCustCode + "&ContCode=" + StrContCode + "&TxnStatus=" + strStatus)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnkiv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnkiv.Click
        Try
            Dim dt As DataTable
            Dim strTxnNo As String = Trim(lbltxnno.Text)
            Dim strCustCode As String = Trim(Request.QueryString("CustCode"))
            Dim strSalesrepCode As String = Trim(Request.QueryString("SalesrepCode"))
            Dim strVisitId As String = Trim(Request.QueryString("VisitId"))
            Dim strTitleCode As String = Trim(lbltitlecode.Text)
            Dim StrContCode As String = Trim(Request.QueryString("ContCode"))
            Dim strStatus As String = "K"
            Dim StrSessionId As String = Trim(Request.QueryString("sessionid"))

            Dim clsMss As New txn_WebActy.clsMSS
            dt = clsMss.GetTMPMSSHdrdTLAdvValidate(strTxnNo, strTitleCode, strVisitId, StrSessionId)

            If dt.Rows.Count > 0 Then
                Submit("K")
                Session("TxnValid") = "1"
                Response.Redirect("TxnMSSSub.aspx?TxnType=MSS&TXNNO=" + strTxnNo + "&visitid=" + strVisitId + "&SALESREPCODE=" + strSalesrepCode + "&CustCode=" + strCustCode + "&ContCode=" + StrContCode + "&TxnStatus=" + strStatus)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnRefreshUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshUp.Click
        Dim index As Integer
        index = dgListQues.SelectedIndex
        dgListQues.SelectedIndex = IIf((index - 1) < 0, 0, (index - 1))
        RefreshDatabindingQues()
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPositionUp();',100);", True)
    End Sub

    Protected Sub btnRefreshDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshDown.Click
        Dim index As Integer
        index = dgListQues.SelectedIndex
        Dim maxRow As Integer = dgListQues.Rows.Count - 1
        dgListQues.SelectedIndex = IIf((index + 1) > maxRow, maxRow, (index + 1))
        RefreshDatabindingQues()
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPositionDown();',100);", True)
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
