<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserCreate.aspx.vb" Inherits="Admin_User_UserCreate" %>
<%@ Register Src="../../include/wuc_ddlUserType.ascx" TagName="wuc_ddlUserType" TagPrefix="uc9" %>
<%@ Register Src="../../include/wuc_ddlSalesRep.ascx" TagName="wuc_ddlSalesRep" TagPrefix="uc8" %>
<%@ Register Src="../../include/wuc_ddlBranch.ascx" TagName="wuc_ddlBranch" TagPrefix="uc7" %>
<%@ Register Src="../../include/wuc_ddlSalesTeam.ascx" TagName="wuc_ddlSalesTeam" TagPrefix="uc6" %>
<%@ Register Src="../../include/wuc_ddlPrincipal.ascx" TagName="wuc_ddlPrincipal" TagPrefix="uc5" %>
<%@ Register Src="../../include/wuc_ddlOperationType.ascx" TagName="wuc_ddlOperationType" TagPrefix="uc4" %>
<%@ Register Src="../../include/wuc_ddlLanguage.ascx" TagName="wuc_ddlLanguage" TagPrefix="uc3" %>
<%@ Register Src="../../include/wuc_ddlCountry.ascx" TagName="wuc_ddlCountry" TagPrefix="uc2" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Creating User</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />   
    <script src="../../include/dkshpolicy.min.js" type="text/javascript"></script>
       <script type="text/javascript">
    <% = Portal.Policy.WriteClientPolicy() %>
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmUserCreate" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:dropdownlist id="ddlStatusID" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="">Select</asp:ListItem>
									                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
									                        <asp:ListItem Value="0">In-Active</asp:ListItem>
								                        </asp:dropdownlist>
								                        <asp:RequiredFieldValidator id="rfvStatusID" runat="server" ControlToValidate="ddlStatusID" ErrorMessage="Please select Status."
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                    </td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblLogin" runat="server" CssClass="cls_label_header">Login</asp:label><asp:label id="lblMark2" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtLogin" MaxLength="20" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvLogin" runat="server" ControlToValidate="txtLogin" ErrorMessage="Login cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>						                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblPassword" runat="server" CssClass="cls_label_header">Password</asp:label><asp:label id="lblMark3" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtPassword" MaxLength="50" Runat="server" Width="110px" TextMode="Password" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                            <td width="20%"><asp:label id="lblConfirmedPassword" runat="server" CssClass="cls_label_header">Confirmed Password</asp:label><asp:label id="lblMark4" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtConfirmedPassword" MaxLength="50" Runat="server" Width="110px" TextMode="Password" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvConfirmedPassword" runat="server" ControlToValidate="txtConfirmedPassword" ErrorMessage="Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
									                      <asp:CustomValidator ID="cvPoorPass" runat="server" ErrorMessage="Minimum 8 alphanumeric character " Display="Dynamic"
                                                        CssClass="cls_validator" ControlToValidate="txtConfirmedPassword" OnServerValidate="PolicyCheck"
                                                        ClientValidationFunction="PolicyCheck"></asp:CustomValidator>   
									                 </td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserCode" runat="server" CssClass="cls_label_header">User Code</asp:label><asp:label id="lblMark5" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtUserCode" MaxLength="20" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvUserCode" runat="server" ControlToValidate="txtUserCode" ErrorMessage="User Code cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserName" runat="server" CssClass="cls_label_header">User Name</asp:label><asp:label id="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtUserName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage=" User Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                            <td width="20%"><asp:label id="lblUserName1" runat="server" CssClass="cls_label_header">User Name 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtUserName1" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserType" runat="server" CssClass="cls_label_header">User Type</asp:label><asp:label id="lblMark24" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot24" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <uc9:wuc_ddlUserType id="Wuc_ddlUserType" runat="server">
                                                        </uc9:wuc_ddlUserType></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCountry" runat="server" CssClass="cls_label_header">Country</asp:label><asp:label id="lblMark8" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <uc2:wuc_ddlCountry ID="Wuc_ddlCountry" runat="server" />
                                                    </td>
						                            <td width="20%"><asp:label id="lblPrincipal" runat="server" CssClass="cls_label_header">Principal</asp:label><asp:label id="lblMark9" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <uc5:wuc_ddlPrincipal id="Wuc_ddlPrincipal" runat="server">
                                                        </uc5:wuc_ddlPrincipal></td>
						                        </tr>	
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblBranch" runat="server" CssClass="cls_label_header">Branch</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot12" runat="server">:</asp:label></td>
							                        <td vAlign="top" colspan="4">
                                                        <uc7:wuc_ddlBranch id="Wuc_ddlBranch" runat="server">
                                                        </uc7:wuc_ddlBranch></td>
						                        </tr>		
						                        <%--<tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblRegion" runat="server" CssClass="cls_label_header">Region</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot10" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:dropdownlist id="ddlRegionID" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="0">None</asp:ListItem>
								                        </asp:dropdownlist></td>
						                        </tr>
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblArea" runat="server" CssClass="cls_label_header">Area</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot11" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:dropdownlist id="ddlAreaID" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="0">None</asp:ListItem>
								                        </asp:dropdownlist></td>
						                        </tr>--%>	
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblSalesTeam" runat="server" CssClass="cls_label_header">Sales Team</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot13" runat="server">:</asp:label></td>
							                        <td vAlign="top">
                                                        <uc6:wuc_ddlSalesTeam id="Wuc_ddlSalesTeam" runat="server">
                                                        </uc6:wuc_ddlSalesTeam></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblSalesRep" runat="server" CssClass="cls_label_header">Sales Representative</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot14" runat="server">:</asp:label></td>
							                        <td vAlign="top">
                                                        <uc8:wuc_ddlSalesRep ID="Wuc_ddlSalesRep" runat="server" />
							                        </td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblLanguage" runat="server" CssClass="cls_label_header">Language</asp:label><asp:label id="lblMark22" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot22" runat="server">:</asp:label></td>
							                        <td vAlign="top" colspan="4">
                                                        <uc3:wuc_ddlLanguage id="Wuc_ddlLanguage" runat="server"></uc3:wuc_ddlLanguage></td>
						                        </tr>
						                        <tr>
							                        <td vAlign="top"><asp:label id="lblEmail" runat="server" CssClass="cls_label_header">Email</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDot15" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:textbox id="txtEmail" MaxLength="100" Runat="server" CssClass="cls_textbox"></asp:textbox><asp:regularexpressionvalidator id="revEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
									                        ErrorMessage="Email must be a valid Email Address format" ControlToValidate="txtEmail" Display="Dynamic" CssClass="cls_validator"></asp:regularexpressionvalidator></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblOperationType" runat="server" CssClass="cls_label_header">Operation Type</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot16" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4">
                                                        <uc4:wuc_ddlOperationType id="Wuc_ddlOperationType" runat="server">
                                                        </uc4:wuc_ddlOperationType></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblNewCustNo" runat="server" CssClass="cls_label_header">New Customer No</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot17" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtNewCustNo" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox></td>
						                            <td width="20%"><asp:label id="lblDummyCustNo" runat="server" CssClass="cls_label_header">Dummy Customer No</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot18" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtDummyCustNo" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblIDPrefix" runat="server" CssClass="cls_label_header">ID Prefix</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot19" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtIDPrefix" MaxLength="10" Runat="server" CssClass="cls_textbox"></asp:textbox></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblPwdExpiry" runat="server" CssClass="cls_label_header">Password Expiry (Days)</asp:label><asp:label id="lblMark23" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot23" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtPwdExpiry" MaxLength="50" CssClass="cls_textbox" Text="<%$ AppSettings:PwdExpiryDays%>" runat="server"> </asp:textbox>
								                        <asp:comparevalidator id="cfvPwdExpiry" runat="server" ErrorMessage="Password Expiry must be numeric" ControlToValidate="txtPwdExpiry"
									                        Type="Double" Operator="DataTypeCheck" Display="Dynamic" CssClass="cls_validator"></asp:comparevalidator>
								                        <asp:RequiredFieldValidator id="rfvPwdExpiry" runat="server" ControlToValidate="txtPwdExpiry" ErrorMessage="Password Expiry cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                        <asp:CompareValidator id="cfvPwdExpiry1" runat="server" Display="Dynamic" ErrorMessage="Password Expiry must be greater or equal to 0."
									                        ControlToValidate="txtPwdExpiry" ValueToCompare="0" Operator="GreaterThanEqual"  CssClass="cls_validator"></asp:CompareValidator></td>
									                <td width="20%"><asp:label id="lblLoginExpiry" runat="server" CssClass="cls_label_header">Login Expiry</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot25" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:CheckBox runat="server" CssClass="cls_checkbox" ID="chkLoginExpiry" /></td>
							                        
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblMaxRetry" runat="server" CssClass="cls_label_header">Max Retry</asp:label><asp:label id="lblMark20" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot20" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtMaxRetry" MaxLength="50" Runat="server" CssClass="cls_textbox" Text="<%$ AppSettings: MaxRetry %>"></asp:textbox>
								                        <asp:comparevalidator id="cfvMaxRetry" runat="server" ErrorMessage="Max Retry must be numeric" ControlToValidate="txtMaxRetry"
									                        Type="Double" Operator="DataTypeCheck" Display="Dynamic" CssClass="cls_validator"></asp:comparevalidator>
								                        <asp:RequiredFieldValidator id="rfvMaxRetry" runat="server" ControlToValidate="txtMaxRetry" ErrorMessage="Max Retry cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                        <asp:CompareValidator id="cfvMaxRetry1" runat="server" Display="Dynamic" ErrorMessage="Max Retry must be greater or equal to 0."
									                        ControlToValidate="txtMaxRetry" ValueToCompare="0" Operator="GreaterThanEqual"  CssClass="cls_validator"></asp:CompareValidator>
							                        </td>
						                        </tr>
						                        <tr>
							                        <td vAlign="top"><asp:label id="lblPageSize" runat="server"  CssClass="cls_label_header">Page Size</asp:label><asp:label id="lblMark21" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDot21" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:textbox id="txtPageSize" MaxLength="4" Runat="server" CssClass="cls_textbox">20</asp:textbox><asp:comparevalidator id="cfvPageSize" runat="server" ErrorMessage="Page Size must be numeric" ControlToValidate="txtPageSize"
									                        Type="Double" Operator="DataTypeCheck" Display="Dynamic" CssClass="cls_validator"></asp:comparevalidator>
								                        <asp:RequiredFieldValidator id="rfvPageSize" runat="server" ControlToValidate="txtPageSize" ErrorMessage="Page Size cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                        <asp:CompareValidator id="cfvPageSize1" runat="server" Display="Dynamic" ErrorMessage="Page Size must be greater or equal to 1."
									                        ControlToValidate="txtPageSize" ValueToCompare="0" Operator="GreaterThan"  CssClass="cls_validator"></asp:CompareValidator></td>
						                        </tr>		
						                        <tr>
						                            <td width="20%"><asp:label id="lblNVFlag" runat="server" CssClass="cls_label_header">Net Value</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot28" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:CheckBox runat="server" CssClass="cls_checkbox" ID="chkNVFlag" /></td>
							                        <td width="20%"><asp:label id="lblDefaultNV" runat="server"  CssClass="cls_label_header">Default Net Value</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot29" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td>
							                            <asp:dropdownlist id="ddlDefaultNV" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
									                        <asp:ListItem Value="2">2</asp:ListItem>
								                        </asp:dropdownlist>
							                        </td>
							                    </tr>	
						                         <tr>
							                        <td width="20%" valign="top"><asp:label id="lblAccessRight" runat="server" CssClass="cls_label_header">Access Rights</asp:label><asp:label id="lblMark26" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%" valign="top"><asp:label id="lblDot26" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <TD style="WIDTH: 326px"><asp:listbox id="lstAccessRightFrom" runat="server" CssClass="cls_listbox" Width="320px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr><td align=center><asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false">></asp:LinkButton></TD></TR>
                                                            <tr><td align=center><asp:LinkButton ID="lnkRemove" runat="server" CausesValidation="false"><</asp:LinkButton></TD></TR>
                                                            <tr><td align=center><asp:LinkButton ID="lnkAddAll" runat="server" CausesValidation="false">>></asp:LinkButton></td></tr>
                                                            <tr><td align=center><asp:LinkButton ID="lnkRemoveAll" runat="server" CausesValidation="false"><<</asp:LinkButton></td></tr>
                                                        </TABLE>
                                                    </TD>
                                                    <td></td>
                                                    <td><asp:listbox id="lstAccessRightTo" runat="server" CssClass="cls_listbox" Width="320px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
						                        </tr>
						                        <TR>
                                                    <TD vAlign=top><asp:label id="lblSR" runat="server" CssClass="cls_label_header">Sales Representatives</asp:label><asp:label id="lblMark27" runat="server" CssClass="cls_mandatory">*</asp:label></TD>
                                                    <td vAlign=top><asp:label id="lblDot27" runat="server" CssClass="cls_label_header">:</asp:label></TD>
                                                   <TD style="WIDTH: 326px"><asp:listbox id="lstSRFrom" runat="server" CssClass="cls_listbox" Width="320px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                                    <td>
                                                        <table width="100%" border="0">
                                                           <tr><td align=center><asp:LinkButton ID="lnkAdd1" runat="server" CausesValidation="false">></asp:LinkButton></TD></TR>
                                                            <tr><td align=center><asp:LinkButton ID="lnkRemove1" runat="server" CausesValidation="false"><</asp:LinkButton></TD></TR>
                                                            <tr><td align=center><asp:LinkButton ID="lnkAddAll1" runat="server" CausesValidation="false">>></asp:LinkButton></td></tr>
                                                            <tr><td align=center><asp:LinkButton ID="lnkRemoveAll1" runat="server" CausesValidation="false"><<</asp:LinkButton></td></tr>
                                                        </TABLE>
                                                    </TD>
                                                    <td></td>
                                                    <td><asp:listbox id="lstSRTo" runat="server" CssClass="cls_listbox" Width="320px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                               </TR>				
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:button id="btnReset" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
