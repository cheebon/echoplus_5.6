<%@ Page Language="vb" AutoEventWireup="false" Inherits="CallAnalysisListByCustomer"
    CodeFile="CallAnalysisListByCustomer.aspx.vb" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblDate" Src="~/include/wuc_lblDate.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CallAnalysisListByCustomer</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/MapIconMaker/mapiconmaker.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/ColorPicker.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/SRVisitedRoutes.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>
    <script type='text/javascript'>
        /// Predefine:
        var NonOriGPSColor = "#00c44e"; //"#530087"; //Get from Cust GPS f63131
        var PhoneInColor = "#325200";
        var PhoneInIndicator = "P";
        var PlotType = { "Direction": 1, "Line": 2 };
        var Default_Latitude;
        var Default_Longitude;
        var Default_Zoomlevel;
        ///

        /// Params
        var UserID;
        var PrincipalID;
        var PrincipalCode;
        var Year;
        var Month;
        var Day;
        var SalesrepCode;
        ///

        var root;

        /// Map variables
        var map;
        var SRRoute;
        var markerIdx = 0;
        var mark = [];
        var gDirection = [];
        var SelectedPlotType = PlotType.Direction;
        var ValidCoordinates = [];
        ///

        /***********************************************************
        / Initialiazer
        /
        ************************************************************/
        function Initialize() {

            if (GBrowserIsCompatible()) {
                map = new GMap2(document.getElementById('map_canvas'));

                QueryLocation();

                AddControl();


            }
        }
        function QueryLocation() {
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {

            if (result) {

                if (result.Total > 0) {

                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;

                    SetMapDefaultPoint();
                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {
                map.setCenter(new GLatLng(Default_Latitude, Default_Longitude), parseInt(Default_Zoomlevel));
            }
        }

        function AddControl() {
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
            map.enableScrollWheelZoom();
        }
        /***********************************************************
        / Method to set required params.
        /
        ************************************************************/
        function SetParam(_UserID, _PrincipalID, _PrincipalCode, _Year, _Month, _Day, _SalesrepCode) {
            UserID = _UserID;
            PrincipalID = _PrincipalID;
            PrincipalCode = _PrincipalCode;
            Year = _Year;
            Month = _Month;
            Day = _Day;
            SalesrepCode = _SalesrepCode;

            toggleMap("on", LocateRoute);
        }

        function LocateRoute() {
            SRRoute = new SRVisitedRoutes();
            markerIdx = 0;
            mark = [];
            gDirection = [];

            /*  Set parameter section */

            var selectedplottype = "Direction" //$("input[@name='PlotTypeRadio']:checked").val();

            /****************************************************************/

            if (UserID && PrincipalID && PrincipalCode && Year && Month && Day && SalesrepCode && selectedplottype) {
                //srl = ReturnSelectedSR(sr);

                //ws_RoutePlan.ReturnSRRoute(srl, startdate, enddate, onSuccessSRRouteList);

                if (selectedplottype == "Line") { SelectedPlotType = PlotType.Line; } else { SelectedPlotType = PlotType.Direction; }

                ws_Route.ReturnSRRoute(UserID, PrincipalID, PrincipalCode, Year, Month, Day, SalesrepCode, onSuccessSRRouteList, onErrorSRRouteList);
                ShowMessage("Loading map data.");
            }
        }
        /***********************************************************
        / Toggle map visibility.
        /
        ************************************************************/
        function toggleMap(flag, CallbackFx) {
            if (flag) {
                switch (flag) {
                    case "on":
                        //$("#pnlPopUp").css("visibility", "visible");
                        //$("#pnlPopUp").show("slow", CallbackFx);
                        mp_ToggleModalPopup('pnlPopUp', 'divModalMask', 'on', CallbackFx);
                        map.checkResize();
                        break;
                    case "off":
                        //$("#pnlPopUp").css("visibility", "hidden");
                        mp_ToggleModalPopup('pnlPopUp', 'divModalMask', 'off');
                        $("#pnlPopUp").hide("slow");
                        break;
                }
            }
        }

        function onErrorSRRouteList(error) {
            ShowMessage("Error has occurred when retrieving data. Please try again later. " + error._message);
        }

        function onSuccessSRRouteList(result) {
            if (result) {
                if (result.Total > 0) {
                    HideMessage();

                    try {
                        ParseVisitedRoute(result);
                        PlotMap();
                        SetCenterMap();
                    }
                    catch (err) {
                        ShowMessage("An error occurred.");
                    }
                }
                else {
                    ShowMessage("No records returned.");
                    SetMapDefaultPoint();
                }

            }
        }
        function ParseVisitedRoute(result) {
            var currentSR;
            var sr;
            var vps;
            var lat;
            var longi;

            currentSR = ""

            for (var i = 0; i < result.Total; ++i) {
                if ((result.Rows[i].IN_LATITUDE != undefined && result.Rows[i].IN_LATITUDE != "") &&
                     (result.Rows[i].IN_LONGITUDE != undefined && result.Rows[i].IN_LONGITUDE != "")) {
                    if (currentSR != result.Rows[i].SALESREP_CODE) //Check is different salesrep
                    {
                        if (sr) //if not first time
                        {
                            SRRoute.Salesreps.push(sr); //Now that salesrep is different, add previous salesrep to collection
                        }

                        sr = new Salesrep(result.Rows[i].SALESREP_CODE, result.Rows[i].SALESREP_NAME); //Then declare new salesrep
                    }

                    lat = result.Rows[i].IN_LATITUDE.substring(0, result.Rows[i].IN_LATITUDE.length - 1);
                    longi = result.Rows[i].IN_LONGITUDE.substring(0, result.Rows[i].IN_LONGITUDE.length - 1);
                    vps = new VisitPath(result.Rows[i].CUST_NAME, result.Rows[i].CUST_CODE, result.Rows[i].CONT_NAME, GetClassName(result.Rows[i].CUST_CLASS), result.Rows[i].VISIT_ID, result.Rows[i].TIME_IN, result.Rows[i].TIME_OUT, result.Rows[i].TIME_SPEND, result.Rows[i].CALL_DATE, lat, longi,
                                        result.Rows[i].VISIT_IND,
                                        result.Rows[i].IMG_LOC,
                                        result.Rows[i].ORI_GPS_IND);

                    sr.VisitPaths.push(vps); //Add visited route to current salesrep route collection        

                    currentSR = result.Rows[i].SALESREP_CODE;
                }
                else {
                    ShowMessage("Certain visits do not have coordinates.");
                }
            }

            ///Don't leave out the last salesrep
            if (sr) {
                SRRoute.Salesreps.push(sr);
            }
            ///
        }

        /************************************************************
        //
        // Function that draw markers and lines on map
        //
        *************************************************************/
        function PlotMap() {
            var colorpicker = new ColorPicker();
            var coordArray;
            var count = 0;

            map.clearOverlays();
            WriteRecordCount(count);

            for (var i = 0; i < SRRoute.Salesreps.length; ++i) {
                color = colorpicker.GetNextColor();
                coordArray = [];

                for (var j = 0; j < SRRoute.Salesreps[i].VisitPaths.length; ++j) {

                    coordArray.push(SRRoute.Salesreps[i].VisitPaths[j].Latitude + "," + SRRoute.Salesreps[i].VisitPaths[j].Longitude);

                    ///Define marker color
                    if (SRRoute.Salesreps[i].VisitPaths[j].VisitInd == PhoneInIndicator) {
                        newIcon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: PhoneInColor, label: "" + (j + 1) + "" });
                    }
                    else if (SRRoute.Salesreps[i].VisitPaths[j].OriGPSInd == "0") {
                        newIcon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: NonOriGPSColor, label: "" + (j + 1) + "" });
                    }
                    else {
                        newIcon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: color, label: "" + (j + 1) + "" });
                    }
                    ///

                    mark[markerIdx] = new GMarker(new GLatLng(SRRoute.Salesreps[i].VisitPaths[j].Latitude, SRRoute.Salesreps[i].VisitPaths[j].Longitude), newIcon);

                    map.addOverlay(mark[markerIdx]);
                    WriteRecordCount(++count); //Display records plot on map

                    GEvent.addListener(mark[markerIdx], "click", (function (markerIdx, i, j) {
                        return function () {
                            var str = "<div class='cls_label'>"
                            str += "<span class='map_header_text'>Sales Rep:</span> " + SRRoute.Salesreps[i].SalesrepCode + " - " + SRRoute.Salesreps[i].SalesrepName + "<br />";
                            str += "<span class='map_header_text'>Location:</span> " + SRRoute.Salesreps[i].VisitPaths[j].CustCode + " - " + SRRoute.Salesreps[i].VisitPaths[j].CustName + "<br />";
                            str += "<span class='map_header_text'>Contact:</span> " + SRRoute.Salesreps[i].VisitPaths[j].ContName + "<br />";
                            str += "<span class='map_header_text'>Customer Class:</span> " + SRRoute.Salesreps[i].VisitPaths[j].Class + "<br />";
                            str += "<span class='map_header_text'>Visit:</span> " + SRRoute.Salesreps[i].VisitPaths[j].VisitID + "<br />";
                            str += "<span class='map_header_text'>Time in/Time out:</span> " + SRRoute.Salesreps[i].VisitPaths[j].Timein + " - " + SRRoute.Salesreps[i].VisitPaths[j].Timeout + "<br />";
                            str += "<span class='map_header_text'>Time Spent:</span> " + SRRoute.Salesreps[i].VisitPaths[j].Timespent + "<br />";
                            str += "<a href='#' onclick='PopImage(\"" + root + "" + SRRoute.Salesreps[i].VisitPaths[j].ImgLoc + "\", \"" + SRRoute.Salesreps[i].VisitPaths[j].CustName + "\",\"" + SRRoute.Salesreps[i].VisitPaths[j].CustCode + "\", \"\");'>Outlet Photo</a><br />";
                            str += "</div>";

                            /// Eye Candy Code. Pan marker to center then only show InfoWindow
                            /// Just calling panning and InfoWindow sequentially cannot achieve such effects
                            var moveEnd = GEvent.addListener(map, "moveend", function () {
                                mark[markerIdx].openInfoWindowHtml(str);
                                GEvent.removeListener(moveEnd);
                            });

                            map.panTo(mark[markerIdx].getLatLng());
                            //////////////////////////////////////////////////////////////////////////////////
                        };
                    }
                        )(markerIdx, i, j)
                        );
                    ++markerIdx;


                }

                /// Draw route methods: Determined by SelectedPlotType set
                switch (SelectedPlotType) {
                    case PlotType.Direction:
                        PlotRouteDirection(i, color, coordArray);
                        break;
                    case PlotType.Line:
                        PlotRoutePolyline(i, color, coordArray);
                        break;
                    default:
                        PlotRoutePolyline(i, color, coordArray);
                }
                ///
            }
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
        function PlotRouteDirection(i, color, coordArray) {
            if (coordArray) {
                gDirection[i] = new GDirections();

                GEvent.addListener(gDirection[i], "load", (function (i, color) {
                    return function () {
                        var _dl = gDirection[i].getPolyline();

                        _dl.color = color;
                        _dl.opacity = 0.7;
                        _dl.weight = 3;
                        map.addOverlay(_dl);
                    }
                }
                )(i, color));

                GEvent.addListener(gDirection[i], "error", (function (i) {
                    return function () {
                        //alert(gDirection[i].getStatus().code);  

                        if (gDirection[i].getStatus().code == 604) {
                            ShowMessage("Google Map returns no route information. Status code: 604");
                        }

                    };
                })(i));

                gDirection[i].loadFromWaypoints(coordArray, { getPolyline: true });
            }

        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        function PlotRoutePolyline(i, color, coordArray) {
            if (coordArray) {
                for (var loop = 1; loop < coordArray.length; ++loop) {
                    var froto = [];
                    var from = coordArray[loop - 1].split(",");
                    var to = coordArray[loop].split(",");

                    froto.push(new GLatLng(from[0], from[1]));
                    froto.push(new GLatLng(to[0], to[1]));

                    var poly = new GPolyline(froto, color, 3);

                    map.addOverlay(poly);
                }
            }
        }

        function SetCenterMap() {
            if (SRRoute != undefined && SRRoute.Salesreps.length > 0) {
                var iCenter = parseInt(SRRoute.Salesreps[0].VisitPaths.length / 2);
                var initLocation = new Array(2);

                initLocation[0] = SRRoute.Salesreps[0].VisitPaths[iCenter].Latitude;
                initLocation[1] = SRRoute.Salesreps[0].VisitPaths[iCenter].Longitude;
                map.setCenter(new GLatLng(initLocation[0], initLocation[1]), 12);
            }

        }

        function ReturnSelectedSR(srbox) {
            var srlist = "";

            for (var i = 0; i < srbox.options.length; ++i) {
                if (srbox.options[i].selected) {
                    if (srlist != "") srlist += ",";

                    srlist += "'" + srbox.options[i].value + "'";
                }
            }
            return srlist;
        }

        /***********************************************
        / Show cust image
        ***********************************************/
        function PopImage(ImgLoc, CustName, CustCode, Address) {
            // mp_ToggleModalPopup("pnlPictureDiv", "divModalMask2", "on");

            BindCust(CustName, Address, CustCode);
            LoadImage(ImgLoc);

        }
        function BindCust(CustName, Address, CustCode) {
            //            $("#PicDiv_CustName").text(CustName);
            //            $("#PicDiv_Address").text(Address);

            $("#hfPicCustCode").val(CustCode);
            $("#hfPicCustName").val(CustName);
            $("#hfPicCustAdd").val(CustName);
        }
        function LoadImage(ImgLoc) {
            //$("#CustImg").error(Image_OnErrorHide);
            //            resetCustImg();

            //            $("#CustImg").attr({
            //                src: ImgLoc,
            //                title: "",
            //                alt: "Customer Image"
            //            });
            document.all('btnRefreshPic').click();
        }

        /***********************************************
        / Hide broken image
        ***********************************************/
        function Image_OnErrorHide() {
            $("#CustImg").hide();
            $("#ImgLoadFailMsg").show();
        }
        function resetCustImg() {
            $("#CustImg").show();
            $("#ImgLoadFailMsg").hide();

            $("#CustImg").attr({
                src: "../../images/indicator.gif",
                title: "",
                alt: "Loading"
            });

            $("#PicDiv_CustName").text("");
            $("#PicDiv_Address").text("");
        }

        $(document).ready(function () {
            ShowVisit();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {

                ShowVisit();

            }
        });

        function ShowVisit() {
            var value = $('#hfVisitStatus').val()
            if (value == "") { $('#Visit').hide(); }
            else {
                if (value == "Show") { $('#Visit').show(); }
                else { $('#Visit').hide(); };
            };
        }

        function SetVisit(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfVisitStatus').val('Hide'); }
            else {
                var value = $('#hfVisitStatus').val();
                if (value == 'Hide') { $('#hfVisitStatus').val('Show'); }
                else { $('#hfVisitStatus').val('Hide'); }
            }
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
            this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
            return this;
        }

    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="Initialize()" onunload="GUnload()">
    <form id="frmCallAnalysisByCustomer" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/DataServices/ws_Route.asmx" />
            <asp:ServiceReference Path="~/DataServices/ws_PrincipalLoc.asmx" />
        </Services>
    </ajaxToolkit:ToolkitScriptManager>
    <%--<AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />--%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" ToolTip="Back To Previous Page."
                                                                                PostBackUrl="~/iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx" />
                                                                            <asp:Button ID="btnDownloadKML" runat="server" CssClass="cls_button" Text="Download as KML"
                                                                                ToolTip="Download the Route as KML file" />
                                                                            <asp:Button ID="btnGPS" runat="server" CssClass="cls_button" Text="View Map" ToolTip="Open Google Map" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Timer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick">
                                                                </asp:Timer>
                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="445" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    RowHighlightColor="AntiqueWhite">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnDownloadKML" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport" style="height: 10px">
                                        <td colspan="3">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <%--<div id="pnlPopUp" class="modalPopup" style="z-index:9000;display: none; padding:10px; width:700px; height:500px; overflow:auto; position:absolute;">--%>
    <div id="pnlPopUp" class="modalPopup" style="z-index: 9000; display: none; padding: 10px;
        width: 90%; height: 500px; overflow: auto; position: absolute;">
        <%--<input id="btnClose" type="image" src="../../../images/ico_close.gif" value="Back"class="cls_button" OnClick="toggleMap('off');" /><br />--%>
        <img alt="Close" src="../../../images/ico_close.gif" onclick="toggleMap('off');"
            style="float: right; margin-right: 5px" /><br />
        <%--<div style="width:643px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> visits plot on map.</span></div>
            <div id="map_canvas" style="width: 650px; height: 450px; padding: 0px 0px 0px 0px;position: relative">   --%>
        <div style="width: 97%; background-color: #e3e3e3; padding-left: 5px; border-left: solid 1px #989898;
            border-right: solid 1px #989898; border-top: solid 1px #989898">
            <span class="cls_label" id="spnCount" style="font-weight: bold">0</span><span class="cls_label"
                style="font-weight: bold"> visits plot on map.</span></div>
        <div id="map_canvas" style="width: 98%; height: 450px; padding: 0px 0px 0px 0px;
            position: relative">
            <%--<script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
            <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>"
                type="text/javascript"></script>
        </div>
        <%--<div id="Message" class="cls_label" style="width:644px; display:none; z-index:30; position:absolute; padding:3px; background:#3C3C3C; color: #ffffff"></div>--%>
        <div id="Message" class="cls_label" style="display: none; z-index: 30; position: absolute;
            padding: 3px; background: #3C3C3C; color: #ffffff">
        </div>
    </div>
    <asp:HiddenField ID="hfPicCustCode" runat="server" />
    <asp:HiddenField ID="hfPicCustName" runat="server" />
    <asp:HiddenField ID="hfPicCustAdd" runat="server" />
    <asp:UpdatePanel ID="UpdateVisit" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hfVisitStatus" runat="server" Value="Hide" />
            <div id="Visit" style="width: 50%; float: right; position: absolute; display: none;
                z-index: 9100" class="modalPopup">
                <div class="shadow">
                    <div class="content" style="width: 100%; height: 100%;">
                        <fieldset>
                            <div id="VisitPnl" style="width: 100%;">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <img src="../../../images/ico_down.jpg" alt='' id='img1' />
                                            <asp:Label ID="lblVistCustDesc" runat="server" Text="Photo" CssClass="cls_label_header"
                                                Style="color: Black"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <img alt="Close" src="../../../images/ico_close.gif" onclick="SetVisit('ForceHide');ShowVisit();"
                                                height="25px" width="25px" style="cursor: pointer;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="ContainerDivBlue">
                                <div id="VisitContent" style="width: 100%;">
                                    <asp:HiddenField ID="hfVisitFilter" runat="server" />
                                    <div id="dVisitFilter" style="width: 98%;">
                                        <span class="cls_label_header">Customer:
                                            <asp:Label ID="lblCustName" runat="server" Text="" CssClass="cls_label"></asp:Label><br />
                                            <br />
                                           <asp:Label ID="lblImgLoadFailMsg" runat="server" Text=" Unable to load customer picture. This can be due to no image has been uploaded for
                                                this customer, or the image file has been removed." CssClass="cls_label" Visible="false" /> 
                                            <asp:Button ID="btnRefreshPic" runat="server" Text="Refresh Pic" Style="display: none" />
                                            <asp:Repeater ID="rpCust" runat="server">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hfCustCode" runat="server" Value='<%# Eval("CUST_CODE") %>' />
                                                    <asp:HiddenField ID="hfCustName" runat="server" Value='<%# Eval("CUST_NAME") %>' />
                                                    <asp:HiddenField ID="hfImgLoc" runat="server" Value='<%# Eval("IMG_LOC") %>' />
                                                    <asp:Image ID="imgCust" runat="server" ImageUrl="../../../../images/indicator.gif"  
                                                        Height="150px" />
                                                    </td> </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                    </div>
                                    <asp:HiddenField ID="hfVisit" runat="server" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--  <div id="pnlPictureDiv" class="modalPopup" style="z-index:9001;display: none; padding:10px; width:90%; height:500px; overflow:auto; position:absolute;">
        <input id="btnClosePciture" type="button" value="Back" class="cls_button" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" />
        <br />
        <span class="cls_label_header">Customer: </span><span id="PicDiv_CustName" class="cls_label"></span><br />
        <br />
        
        <div id="ImgLoadFailMsg" class="cls_label" style="display:none">Unable to load customer picture. This can be due to no image has been uploaded for this customer, or the image file has been removed. </div>
        <img id="CustImg" src="../../../images/indicator.gif" alt='Loading' onerror='Image_OnErrorHide();' />
    </div>--%>
    <div id="divModalMask2" style="position: absolute; left: 0; top: 0; z-index: 8000;
        display: none; background-color: #fff">
    </div>
    <div id="divModalMask" style="position: absolute; left: 0; top: 0; z-index: 8000;
        display: none; background-color: #fff">
    </div>
    </form>
</body>
</html>
