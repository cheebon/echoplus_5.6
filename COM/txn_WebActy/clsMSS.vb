Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsMSS

    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMSS"
        End Get
    End Property

#Region "TXN_NO"

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_MSS_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "DDL/GRID"

    Public Function GetTitlecode(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_MSS_TTLCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetQuescode(ByVal strTitleCode As String, ByVal strTxnNo As String, ByVal strSalesrepCode As String, ByVal strSessionId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_MSS_QUESCODE"
            .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
            .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "MSS SUB QUES ADV"
    Public Function GetMSSQuesAdv(ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_MSS_QUES_ADV"
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvCriteria(ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_MSS_QUES_ADV_CRITERIA"
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvNext(ByVal strTitleCode As String, ByVal strQuesCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_MSS_QUES_ADV_NEXT"
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvPrevious(ByVal strTitleCode As String, ByVal strQuesCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_MSS_QUES_ADV_PREVIOUS"
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "Detail"

    Public Sub InstTMPMSSDetails(ByVal strSessionId As String, _
    ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaCode As String, _
  ByVal strExpec As String, ByVal strDateVal As String, ByVal strOptVal As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_MSS_DTL_ADV_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
            .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
            .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
            .addItem("CRITERIA_CODE", strCriteriaCode, clsDB.DataType.DBString)
            .addItem("EXPEC", strExpec, clsDB.DataType.DBString)
            .addItem("DATE_VAL", strDateVal, clsDB.DataType.DBString)
            .addItem("OPT_VAL", strOptVal, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub InstTMPMSSDetailsMulti(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, _
    ByVal strSubQuesCode As String, ByVal strCriteriaCode As String, ByVal strExpec As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_MSS_DTLMULTI_ADV_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
            .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
            .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
            .addItem("CRITERIA_CODE", strCriteriaCode, clsDB.DataType.DBString)
            .addItem("EXPEC", strExpec, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Function GetTMPMSSDetailAdv(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
     ByVal strSessionId As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_DTL_ADV"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTMPMSSDetailMultiAdv(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
    ByVal strCriteriaCode As String, ByVal strSessionId As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_DTLMULTI_ADV"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE ", strCriteriaCode, clsDB.DataType.DBString)
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function DelTMPMSSDetailAndDetailMultiAdv(byval strSessionId as String, byval strTxnNo as String ) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_DTL_DELETE"
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

    End Function
#End Region

#Region "SUBMIT HDR DTL"

    Public Function SubmitTMPMSSDetailAndDetailMultiAdv(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strSalesrepCode As String, ByVal strVisitId As String, _
    ByVal strTitleCode As String, ByVal strGenComment As String, ByVal strSessionId As String, ByVal strTxnstatus As String, ByVal strContCode As String, _
    ByVal strTxnDate As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_HDR_DTL_SUBMIT"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("GEN_COMMENT", strGenComment, clsDB.DataType.DBString)
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("TXN_STATUS", strTxnstatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

    End Function

    Public Function GetTMPMSSHdrAdv(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strVisitId As String, _
     ByVal strSessionId As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_HDR_ADV"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTMPMSSHdrdTLAdvValidate(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strVisitId As String, _
 ByVal strSessionId As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_MSS_DTL_DTLMULTI_ADV_VALIDATE"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
                .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
