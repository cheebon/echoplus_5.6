<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MssContDtlSearch.aspx.vb"
    Inherits="iFFMA_HCD_MssContDtl_MssContDtlSearch" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mss Contact Details</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
   function RefreshContentBar()
    {var txtSpeciality = document.getElementById("txtSpeciality"); var strSpeciality = txtSpeciality.value;
    var txtCustName = document.getElementById("txtCustName"); var strCustName = txtCustName.value;
    var txtposition = document.getElementById("txtposition"); var strPosition = txtposition.value;
    var txtContName = document.getElementById("txtContName"); var strContname = txtContName.value;
    var txtdepartment = document.getElementById("txtdepartment"); var strDepartment = txtdepartment.value;
     var txtClass = document.getElementById("txtClass"); var strClass = txtClass.value;
     var txtHospRankCode = document.getElementById("txtHospRankCode"); var strHospRankCode = txtHospRankCode.value;
    parent.document.getElementById('ContentBarIframe').src="../../iFFMA/HCD/MssContDtl/MssContDtlList.aspx?SPECIALITY=" + strSpeciality + "&CUST_NAME=" + strCustName+ "&POSITION=" + strPosition + "&CONT_NAME=" + strContname + "&DEPARTMENT=" + strDepartment + "&CLASS=" + strClass + "&HOSP_RANK_CODE=" + strHospRankCode;
    }
</script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frm" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                            align="left">
                            <tr align="left">
                                <td align="left">
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td width="20%">
                                                        <span class="cls_label_header">Specialty Name</span></td>
                                                    <td width="1px">
                                                        <span class="cls_label_header">:</span></td>
                                                    <td width="30%" >
                                                        <asp:TextBox ID="txtSpeciality" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td width="20%">
                                                        <span class="cls_label_header">Customer Name</span></td>
                                                    <td width="1px">
                                                        <span class="cls_label_header">:</span></td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Position</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtposition" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="cls_label_header">Contact Name</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtContName" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Department</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtdepartment" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="cls_label_header">Classification</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtClass" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="cls_label_header">Hospital Ranking Code</span></td>
                                                    <td>
                                                        <span class="cls_label_header">:</span></td>
                                                    <td>
                                                        <asp:TextBox ID="txtHospRankCode" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="100"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" align="left">
                                                        <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                                        <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                                            value="Search" class="cls_button" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
