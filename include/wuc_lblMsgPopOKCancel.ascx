<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_lblMsgPopOKCancel.ascx.vb" Inherits="include_wuc_lblMsgPopOKCancel" %>
<asp:UpdatePanel ID="updPnlMsgPopOKCancel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPopOKCancel" runat="server" Style="display: none; width: 400px; padding: 15px"
            CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" />
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 98%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <center>
                    
                        <asp:Button ID="editBox_OK" runat="server" Text="OK" cssclass="cls_button" OnClick="editBox_OK_Click" width="60px" />
                        <asp:Button ID="editBox_Cancel" runat="server" Text="Cancel" cssclass="cls_button" OnClick="editBox_Cancel_Click" width="60px"/>

                    </center>
                </fieldset>
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender 
            ID="ModalPopupMessageOKCancel" runat="server" 
            BehaviorID="programmaticModalPopupBehavior"
            TargetControlID="btnHidden" 
            PopupControlID="pnlMsgPopOKCancel" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>