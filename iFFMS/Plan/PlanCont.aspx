<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanCont.aspx.vb" Inherits="iFFMS_Plan_PlanCont" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Addon Contact</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglistNewCont');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('dglistSelectedCont');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
          function ValidatedglistNewContCheckBoxStates()
   { var dglist = document.getElementById('dglistNewCont');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
        function ValidatedglistSelectedContCheckBoxStates()
   { var dglist = document.getElementById('dglistSelectedCont');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('TopBar');HideElement('DetailBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')"
    class="BckgroundInsideContentLayout">
    <form id="frmplanaddoncont" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" 
                         Visible="true" Width="80px" OnClientClick="HideElement('ContentBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')" />
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateSelectedCont" runat="server" UpdateMode="Conditional"
                            RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlSelectedCont" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlSelectedCont_Tick" />
                                <div id="SelectedCont" class="S_DivHeader">
                                    Assigned Contact
                                </div>
                                <asp:Panel ID="pSelectedCont" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                     <asp:Label ID="lblSmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckalldelete" value="Check All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(true);" visible="false" style="width: 80px"
                                                validationgroup="deleteCont" runat="server" />
                                            <input type="Button" id="btnuncheckalldelete" value="Uncheck All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(false);" visible="false" style="width: 80px"
                                                validationgroup="deleteCont" runat="server" />
                                            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="cls_button" ValidationGroup="deleteCont"
                                                Visible="false" Width="80px"  OnClientClick="return ValidatedglistSelectedContCheckBoxStates()"/>
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistSelectedCont" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CONT_CODE">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CONT_CODE" HeaderText="Contact code" ReadOnly="True" SortExpression="CONT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True" SortExpression="CONT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SPECIALITY_NAME" HeaderText="Specialty" ReadOnly="True"
                                                    SortExpression="SPECIALITY_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="POSITION_NAME" HeaderText="Position" ReadOnly="True" SortExpression="POSITION_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DEPT_NAME" HeaderText="Department" ReadOnly="True" SortExpression="DEPT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSelectedCont" runat="server" TargetControlID="pSelectedCont"
                                    ExpandControlID="SelectedCont" CollapseControlID="SelectedCont">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewCont" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlNewCont" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlNewCont_Tick" />
                                <div id="NewCont" class="S_DivHeader">
                                    Add New Contact
                                </div>
                                <asp:Panel ID="PNewCont" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblNmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" validationgroup="newCont" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" validationgroup="newCont" runat="server" />
                                            <asp:Button ID="btnadd" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="newCont"
                                                Visible="false" Width="80px" OnClientClick="return ValidatedglistNewContCheckBoxStates()" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistNewCont" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CONT_CODE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CONT_CODE" HeaderText="Contact code" ReadOnly="True" SortExpression="CONT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True" SortExpression="CONT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SPECIALITY_NAME" HeaderText="Specialty" ReadOnly="True"
                                                    SortExpression="SPECIALITY_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="POSITION_NAME" HeaderText="Position" ReadOnly="True" SortExpression="POSITION_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DEPT_NAME" HeaderText="Department" ReadOnly="True" SortExpression="DEPT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewCont" runat="server" TargetControlID="PNewCont"
                                    ExpandControlID="NewCont" CollapseControlID="NewCont">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
