'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	8/12/2006
'	Purpose	    :	Class to build datatables for getting and storing prePlan details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

'Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsMessage
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMessage"
        End Get
    End Property


    Function GetInbox(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEINBOX"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetInbox :" & ex.Message))
        End Try
    End Function

    Function GetOutbox(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEOUTBOX"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetInbox :" & ex.Message))
        End Try
    End Function

    Function GetDeletedbox(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEDELETED"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetInbox :" & ex.Message))
        End Try
    End Function

#Region "TXN_NO"

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_MSG_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "INSERT/DELETE"

    Public Sub InstMSG(ByVal strMsgCode As String, ByVal strReceiverName As String, ByVal strSubject As String, ByVal strContent As String, _
        ByVal strTxnStatus As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_OUTMESSAGE_CREATE"
            .addItem("MSG_CODE ", strMsgCode, clsDB.DataType.DBString)
            .addItem("RECEIVER_NAME", strReceiverName, clsDB.DataType.DBString)
            .addItem("SUBJECT", strSubject, clsDB.DataType.DBString)
            .addItem("CONTENT", strContent, clsDB.DataType.DBString)
            .addItem("TXN_STATUS", strTxnStatus, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelMSGOutBox(ByVal strMsgCode As String, ByVal strReceiverName As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_OUTMESSAGE_DELETE"
            .addItem("MSG_CODE", strMsgCode, clsDB.DataType.DBString)
            .addItem("RECEIVER_NAME", strReceiverName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelMSGInbox(ByVal strMsgCode As String, ByVal strReceiverName As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_MSTMESSAGE_DELETE"
            .addItem("MSG_CODE", strMsgCode, clsDB.DataType.DBString)
            .addItem("SENDER_ID", strReceiverName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub
#End Region

#Region "OLDSPP"
    Function getInboxDT(ByVal strSalesmanCode As String, ByVal strType As String, Optional ByVal strMsgID As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEINBOX"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", strType, 2)
                .addItem("msgid", strMsgID, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getInboxDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getOutboxDT(ByVal strSalesmanCode As String, ByVal strType As String, Optional ByVal strMsgID As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEOUTBOX"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", strType, 2)
                .addItem("msgid", strMsgID, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOutboxDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Sub insertReply(ByVal strMsgCode As String, ByVal strSalesmanCode As String, ByVal strReceiver As String, ByVal strSubject As String, ByVal strContent As String, ByVal dtmDate As DateTime)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEREPLY"
                .addItem("msgcode", strMsgCode, 2)
                .addItem("userid", strSalesmanCode, 2)
                .addItem("receiver", strReceiver, 2)
                .addItem("subject", strSubject, 2)
                .addItem("content", strContent, 2)
                .addItem("date", dtmDate, 3)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertReply :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub deleteMsg(ByVal strMsgCode As String, ByVal strType As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MESSAGEDELETE"
                .addItem("msgcode", strMsgCode, 2)
                .addItem("type", strType, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertReply :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub
#End Region

#Region "Exception"

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
