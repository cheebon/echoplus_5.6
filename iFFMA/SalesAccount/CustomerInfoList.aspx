﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerInfoList.aspx.vb"
    Inherits="iFFMA_SalesAccount_CustomerInfo" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerPriceGrpList" Src="CustomerPriceGrpList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerContList" Src="CustomerContList.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Info</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmcustinfo" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                        <tr align="left">
                            <td>
                                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                <br />
                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                        <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                            ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlList" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <center>
                                                                        <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="SALESREP_CODE,CUST_CODE">
                                                                            <%--<Columns>
                                                                            <asp:BoundField DataField="SALESREP_CODE" HeaderText="Field Force Code" ReadOnly="True" SortExpression="SALESREP_CODE">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SALESREP_NAME" HeaderText="Field Force Name" ReadOnly="True" SortExpression="SALESREP_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True" SortExpression="CUST_CODE">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True" SortExpression="CUST_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="XFIELD1">
                                                                                <itemtemplate>
                                                                                    <asp:TextBox ID="txtXFIELD1" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("XFIELD1") %>' Width="150px" MaxLength="100" />
                                                                                <itemstyle horizontalalign="Left" />                                                             
                                                                                </itemtemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="XFIELD2">
                                                                                <itemtemplate>
                                                                                    <asp:TextBox ID="txtXFIELD2" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("XFIELD2") %>' Width="150px" MaxLength="100" />
                                                                                <itemstyle horizontalalign="Left" />                                                             
                                                                                </itemtemplate>
                                                                            </asp:TemplateField> 
                                                                            <asp:TemplateField HeaderText="XFIELD3">
                                                                                <itemtemplate>
                                                                                    <asp:TextBox ID="txtXFIELD3" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("XFIELD3") %>' Width="150px" MaxLength="100" />
                                                                                <itemstyle horizontalalign="Left" />                                                             
                                                                                </itemtemplate>
                                                                            </asp:TemplateField> 
                                                                            <asp:TemplateField HeaderText="XFIELD4">
                                                                                <itemtemplate>
                                                                                    <asp:TextBox ID="txtXFIELD4" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("XFIELD4") %>' Width="150px" MaxLength="100" />
                                                                                <itemstyle horizontalalign="Left" />                                                             
                                                                                </itemtemplate>
                                                                            </asp:TemplateField> 
                                                                            <asp:TemplateField HeaderText="REMARKS">
                                                                                <itemtemplate>
                                                                                    <asp:TextBox ID="txtREMARKS" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("REMARKS") %>' Width="150px" MaxLength="100" />
                                                                                <itemstyle horizontalalign="Left" />                                                             
                                                                                </itemtemplate>
                                                                            </asp:TemplateField>  
                                                                            </Columns>--%>
                                                                        </ccGV:clsGridView>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td><span class="cls_label_header">Salesrep Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:label ID="lblSalesrepCode" runat="server" CssClass="cls_label" Width="200px" /></td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td><span class="cls_label_header">Salesrep Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:label ID="lblSalesrepName" runat="server" CssClass="cls_label" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:label ID="lblCustCode" runat="server" CssClass="cls_label" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:label ID="lblCustName" runat="server" CssClass="cls_label"  Width="200px" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">XFIELD1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtXFIELD1" runat="server" CssClass="cls_textbox" MaxLength="100" Width="250px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">XFIELD2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtXFIELD2" runat="server" CssClass="cls_textbox" MaxLength="100" Width="250px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">XFIELD3</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtXFIELD3" runat="server" CssClass="cls_textbox" MaxLength="100" Width="250px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">XFIELD4</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtXFIELD4" runat="server" CssClass="cls_textbox" MaxLength="100" Width="250px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Remarks</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtREMARKS" runat="server" CssClass="cls_textbox" MaxLength="250" Width="400px" /></td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td style="height: 5px">
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
