'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	15/09/2005
'	Purpose	    :	Class to make salesman table query
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSFMSExtract
    Implements IDisposable
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    '**************************************************************************
    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFMSExtract"

        End Get
    End Property

    Public Function GetSFMSCatList(ByVal strTeamCode As String) As DataTable
        Try
            Dim clsSalesTeamDB As New clsDB

            With clsSalesTeamDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMSCATLIST"
                .addItem("TeamCode", strTeamCode, 2, True)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSCatList :" & ex.Message))
        Finally
        End Try
    End Function

    Public Function GetSFMSSubCatList(ByVal strTeamCode As String) As DataTable
        Try
            Dim clsSalesTeamDB As New clsDB

            With clsSalesTeamDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMSSUBCATLIST"
                .addItem("TeamCode", strTeamCode, 2, True)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSSubCatList :" & ex.Message))
        Finally
        End Try
    End Function

    Public Function GetSFMSExtractList(ByVal strDateStart As String, ByVal strDateEnd As String, ByVal strTeamCode As String, ByVal strSalesmanCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Try
            Dim clsSalesTeamDB As New clsDB

            With clsSalesTeamDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMSEXTRACT"
                .addItem("DateStart", strDateStart, 2)
                .addItem("DateEnd", strDateEnd, 2)
                .addItem("TeamCode", strTeamCode, 2, True)
                .addItem("SalesmanCode", strSalesmanCode, 2, True)
                .addItem("CatCode", strCatCode, 2, True)
                .addItem("SubCatCode", strSubCatCode, 2, True)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractList :" & ex.Message))
        Finally
        End Try
    End Function


    '**************************************************************************
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
