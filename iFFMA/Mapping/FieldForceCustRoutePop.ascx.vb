Imports System.Data

Partial Class iFFMA_Mapping_FieldForceCustRoutePop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(lblSalesrepCode.Text)
        End Get
        Set(ByVal value As String)
            lblSalesrepCode.Text = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(lblCustCode.Text)
        End Get
        Set(ByVal value As String)
            lblCustCode.Text = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(lblContCode.Text)
        End Get
        Set(ByVal value As String)
            lblContCode.Text = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub
#End Region


#Region "LISTBOX"
    Private Sub BindLsb()
        Dim dtRoute As DataTable = Nothing
        Dim dtSelected As DataTable = Nothing
        Dim dvRoute, dvSelected As DataView
        Dim strFilter As String = ""

        Try
            lsbRoute.Items.Clear()
            lsbSelected.Items.Clear()

            Dim clsFieldForceRoute As New mst_Mapping.clsFieldForceRoute
            Dim clsCommon As New mst_Common.clsDDL

            dtSelected = clsFieldForceRoute.GetRoutePlanningList(TeamCode, SalesrepCode, "", CustCode, ContCode)
            dvSelected = dtSelected.DefaultView
            lsbSelected.DataSource = dvSelected
            lsbSelected.DataTextField = "ROUTE_NAME"
            lsbSelected.DataValueField = "ROUTE_CODE"
            lsbSelected.DataBind()

            dtRoute = clsCommon.GetRouteDDL(TeamCode)
            dvRoute = dtRoute.DefaultView

            If dtSelected.Rows.Count > 0 Then
                Dim drWhs As DataRow() = dtSelected.Select()
                strFilter = "ROUTE_CODE NOT IN (" & GetFilterString(drWhs, "ROUTE_CODE") & ")"

                dvRoute = New DataView(dtRoute, strFilter, "", DataViewRowState.CurrentRows)
            End If

            lsbRoute.DataSource = dvRoute
            lsbRoute.DataTextField = "ROUTE_NAME"
            lsbRoute.DataValueField = "ROUTE_CODE"
            lsbRoute.DataBind()


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetFilterString(ByRef drRows() As DataRow, ByVal strColumnName As String) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList

        For Each ROW As Data.DataRow In drRows
            If aryList.IndexOf(Trim(ROW(strColumnName))) < 0 Then
                sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(ROW(strColumnName)) & "'")
                aryList.Add(Trim(ROW(strColumnName)))
            End If
        Next
        Return sbString.ToString
    End Function

    Protected Sub lnkAddRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddRoute.Click
        Try
            AddToListBox(lsbRoute, lsbSelected)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveRoute.Click
        Try
            AddToListBox(lsbSelected, lsbRoute)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllRoute.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbRoute.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbRoute, lsbSelected)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllRoute.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelected.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelected, lsbRoute)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next

                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)

        lsbRoute.Enabled = False
        'lsbSelected.Enabled = False
        lnkAddRoute.Enabled = False
        lnkRemoveRoute.Enabled = False
        lnkAddAllRoute.Enabled = False
        lnkRemoveAllRoute.Enabled = False

    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)

        lsbRoute.Items.Clear()
        lsbSelected.Items.Clear()

        lsbRoute.Enabled = True
        'lsbSelected.Enabled = True
        lnkAddRoute.Enabled = True
        lnkRemoveRoute.Enabled = True
        lnkAddAllRoute.Enabled = True
        lnkRemoveAllRoute.Enabled = True
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust

            DT = clsFieldForceCust.GetFieldForceCustVisitFreqDtl(TeamCode, SalesrepCode, CustCode, ContCode)
            If DT.Rows.Count > 0 Then
                
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            BindDetailsView2()
            BindLsb()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub BindDetailsView2()
        Try
            Dim DT As DataTable
            Dim dvDetailView2 As DataView
            Dim clsBCP As New adm_bcp.clsBCP

            DT = clsBCP.GetFieldForceBCPTemplate(TeamCode, SalesrepCode, CustCode, ContCode)
            If DT.Rows.Count > 0 Then

            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView2 = DT.DefaultView
            DetailsView2.DataSource = dvDetailView2
            DetailsView2.DataBind()

            If DT IsNot Nothing Then
                LoadBCPTemplate(DT)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadBCPTemplate(ByVal dt As DataTable)
        Dim dtBCP As DataTable
        Dim clsBCP As New adm_bcp.clsBCP
        Try
            Dim ddlBCP As DropDownList = CType(DetailsView2.FindControl("ddlBCPTemplate"), DropDownList)
            If ddlBCP IsNot Nothing Then
                dtBCP = clsBCP.GetBCPTemplateDDL
                With ddlBCP
                    .Items.Clear()
                    .DataSource = dtBCP.DefaultView
                    .DataTextField = "TEMPLATE_DESC"
                    .DataValueField = "BCP_TEMPLATE_ID"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(dt.Rows(0)("BCP_TEMPLATE_ID")), "", dt.Rows(0)("BCP_TEMPLATE_ID"))
                End With
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub

    Public Sub ResetPage()
        'lblDate.Text = ""
        'lblFrom.Text = ""
        'lblRecipients.Text = ""
        'lblSubject.Text = ""
        'lblMsgContent.Text = ""

        'ddlTeam.SelectedIndex = 0
        'txtSubject.Text = ""
        'txtMsgContent.Text = ""
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtVisitFreq As TextBox = CType(DetailsView1.FindControl("txtVisitFreq"), TextBox)
            txtVisitFreq.Text = IIf(Trim(txtVisitFreq.Text) = "", 0, Trim(txtVisitFreq.Text))

            Dim strRouteList As String = GetItemsInString(lsbSelected)

            Dim clsFieldForceRoute As New mst_Mapping.clsFieldForceRoute

            Dim ddlBCPTemplate As DropDownList = CType(DetailsView2.FindControl("ddlBCPTemplate"), DropDownList)

            clsFieldForceRoute.CreateRoutePlanningByCust(strRouteList, Trim(txtVisitFreq.Text), TeamCode, SalesrepCode, CustCode, ContCode, ddlBCPTemplate.SelectedValue)
            lblInfo.Text = "The record is successfully saved."
            LoadDvViewMode()

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

