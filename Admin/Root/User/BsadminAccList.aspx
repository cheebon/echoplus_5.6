﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BsadminAccList.aspx.vb" Inherits="Admin_Root_User_BsadminAccList" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bsadmin list</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmBsadminList" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />           
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>                    
                    <!--Begin Modify Here -->
                    
                    <div class="border_std">
                        <asp:Button ID="btnCreate" runat="server" Text="Create" CausesValidation="False"
                                                        UseSubmitBehavior="false" Visible="true" />
                    </div>
                    <div class="border_std">
                    <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblCountry" runat="server" Text="Country "></asp:Label></td><td>
                        <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" Width="200px">                                
                            </asp:DropDownList>
                            </td>
                    </tr>                     
                    <tr>
                        <td>
                            <asp:Label ID="lblPrincipal" runat="server" Text="Principal "></asp:Label></td><td>
                                <asp:DropDownList ID="ddlPrincipal" runat="server" AutoPostBack="false" Width="200px">                                
                            </asp:DropDownList></td>
                    </tr>                
                   
                    </table>
                   
                    <asp:Button ID="btnSearch" runat="server" Text="Search" />
                </div>
                <br />
                <div class="border_std"> 
                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="false"></customToolkit:wuc_dgpaging>
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                        EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        AllowPaging="true" PagerSettings-Visible="false" DataKeyNames="USER_ID">
                        <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                        <EmptyDataTemplate>Use search to locate BSADMIN records.</EmptyDataTemplate>
                        <Columns>        
                            <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image" HeaderText="Delete">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>                    
                            <%--<asp:ButtonField CommandName="Modify" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>--%>
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="USER_ID" HeaderText="User ID" SortExpression="USER_ID" />
                            <asp:BoundField DataField="COUNTRY_CODE" HeaderText="Country" SortExpression="COUNTRY_CODE" />
                            <asp:BoundField DataField="PRINCIPAL_CODE" HeaderText="Principal" SortExpression="PRINCIPAL_CODE" />                            
                            <asp:BoundField DataField="LOGIN" HeaderText="Login" SortExpression="USER_ID" />
                            <asp:BoundField DataField="USER_CODE" HeaderText="User Code" SortExpression="USER_CODE" />
                            <asp:BoundField DataField="USER_NAME" HeaderText="User Name" SortExpression="USER_NAME" />
                        </Columns>
                    </ccGV:clsGridView>
                </div>           
                    <!--End Modify Here -->
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
