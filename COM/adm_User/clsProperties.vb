'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	27/10/2006
'	Purpose	    :	Class User for Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel
Imports system.Web.UI.WebControls

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsUser
        Public user_id As Double
        Public user_type_id As Double
        Public principal_id As Double
        Public country_id As Double
        Public salesteam_id As Double
        Public salesteam_code As String
        Public salesrep_id As Double
        Public salesrepgrp_id As Double
        Public salesrep_code As String
        Public region_id As Double
        Public region_code As String
        Public area_id As Double
        Public area_code As String
        Public language_id As Double
        Public branch_id As Double
        Public branch_code As String
        Public login As String
        Public pwd_flag As Integer
        Public pwd As String
        Public user_code As String
        Public user_name As String
        Public user_name_1 As String
        Public email As String
        Public pwd_expiry As Integer
        Public pwd_expiry_rmd As Integer
        Public login_expiry_flag As Integer
        Public max_retry As Integer
        Public page_size As Integer
        Public nv_flag As Integer
        Public default_nv As Integer
        Public operation_type_id As Double
        Public operation_type_code As String
        Public new_cust_no As String
        Public dummy_cust_no As String
        Public id_prefix As String
        Public pwd_changed_date As DateTime
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
        Public active_flag As Integer
        Public login_cnt As Integer
        Public liAR As ListItemCollection 'DataTable
        Public liSR As ListItemCollection 'DataTable
        Public liSRG As ListItemCollection
    End Class

    Public Class clsUserAccessRight
        Public useraccessright_id As Double
        Public user_id As Double
        Public user_code As String
        Public accessright_id As Double
        Public country_id As Double
        Public principal_id As Double
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
    End Class

    Public Class clsUserSalesRep
        Public usersalesrep_id As Double
        Public user_id As Double
        Public user_code As String
        Public salesrep_id As Double
        Public salesrep_code As String
        Public salesteam_id As Double
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
    End Class
    Public Class clsUserSalesRepGrp
        Public salesrepgrp_id As Double
        Public user_id As Double
        Public user_code As String
        Public salesrep_id As Double
        Public salesrep_code As String
        Public salesteam_id As Double
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
    End Class
    Public Class clsUserQuery
        Public user_id As Double
        Public login As String
        Public pwd As String
        Public user_code As String
        Public search_type As String
        Public search_value As String
        Public search_value_1 As String
        Public delete_flag As Integer
    End Class

    Public Class clsUserAgency
        Public strPrincipalId As String
        Public strUserId As String
        Public strSessionUserId As String
        Public strAgencyList As String
        Public strOption As String
    End Class

End Class
