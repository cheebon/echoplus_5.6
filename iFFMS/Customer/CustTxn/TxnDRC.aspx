<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnDRC.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnDRC" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="~/iFFMS/COMMON/wuc_Prdsearch.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Transaction DRC</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    
        function CalAmt(element)//element is the textbox
        {
            var txtPO = element;
            var lblId = txtPO.id.replace("txtPO", "lblPOamt");
            var lblPOAmt = document.getElementById(lblId);
            var Row = element.parentNode.parentNode.rowIndex;
            var dglist = document.getElementById('dglistdtl'); //diglistdtl
            var price, qty;
            price = parseFloat(dglist.rows[Row].cells[7].innerHTML.replace(',', '')); //cell 6 is the uom price
            qty = parseFloat(txtPO.value);
            var Total = parseFloat(price * qty);
            if (isNaN(Total)) Total = 0.00;
            var POamt = Total.toFixed(2); //set the po amount 
            if (lblId)
            { lblPOAmt.innerHTML = POamt; }
            else
            { alert('cannot find'); }
        }
            
        function CalAmtSum(element)//element is the textbox
        {
            var txtPO = element;
            var lblId = txtPO.id.replace("txtSumPO", "lblSumPOamt");
            var lblPOAmt = document.getElementById(lblId);
            var Row = element.parentNode.parentNode.rowIndex;
            var dglist = document.getElementById('dglist'); //dglist for summary
            var price, qty;
            price = parseFloat(dglist.rows[Row].cells[9].innerHTML.replace(',', '')); //cell 9 is the uom price
            qty = parseFloat(txtPO.value);
            var Total = parseFloat(price * qty);
            if (isNaN(Total)) Total = 0.00;
            var POamt = Total.toFixed(2); //set the po amount
            if (lblId)
            { lblPOAmt.innerHTML = POamt; }
            else
            { alert('cannot find'); }
        }

        //dgList
        function ValidateCheckBoxStates() {
            var flag = false;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStates(element) {
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this.checked == false) flag = false; });
            $("#dgList").find("input:checkbox[Id*=DeleteAllCheckBox]").attr('checked', flag);
        }
        //dgList
        //dglistdtl
        function ValidateCheckBoxStatesDtl() {
            var flag = false;
            $("#dglistdtl").find("input:checkbox[Id*=chkSelection]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStatesDtl(element) {
            $("#dglistdtl").find("input:checkbox[Id*=chkSelection]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStatesDtl(element) {
            var flag = true;
            $("#dglistdtl").find("input:checkbox[Id*=chkSelection]").each(function() { if (this.checked == false) flag = false; });
            $("#dglistdtl").find("input:checkbox[Id*=AllCheckBox]").attr('checked', flag);
        }
        //dglistdtl
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmtxndrc" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <fieldset class="" style="width: 98%;">
        <div id="title">
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div id="txnDRC">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="Proce" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                        background-color: Gray">
                        <span style="float: right;">
                            <asp:Button ID="btnsubmit" runat="server" Text="Proceed >>" CssClass="cls_button"
                                Width="80px" />
                        </span>
                    </div>
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <asp:UpdatePanel ID="UpdateHdr" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <div id="dheader" class="S_DivHeader">
                                    General Information
                                </div>
                                <asp:Panel ID="pDRCHdr" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                        <table class="cls_form_table">
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Txn No:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpepDRCHdr" runat="server" TargetControlID="pDRCHdr"
                                    ExpandControlID="dheader" CollapseControlID="dheader">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateDtl" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <div id="ddtl" class="S_DivHeader">
                                    Detail Information - Add New
                                </div>
                                <asp:Panel ID="pDRCDtl" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 98%; padding-left: 5px;">
                                        <asp:Button ID="btnsavedtl" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="DRCDtl"
                                            Width="80px" OnClientClick="return ValidateCheckBoxStatesDtl();" />
                                    </div>
                                    <div style="width: 98%; padding-left: 5px;">
                                        <span class="cls_label_header">Product Group :
                                            <asp:DropDownList ID="ddlprdgrpcode" runat="server" CssClass="cls_dropdownlist" Width="200px"
                                                ValidationGroup="DRCDtl" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </span>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistdtl" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="PRD_CODE">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="AllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStatesDtl(this);" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelection" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStatesDtl(this);" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" ReadOnly="True" SortExpression="PRD_NAME">
                                                    <ItemStyle HorizontalAlign="left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="OPEN_BAL_QTY" HeaderText="Open Bal. Qty." ReadOnly="True"
                                                    DataFormatString="{0:#,0}" SortExpression="OPEN_BAL_QTY">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMS" HeaderText="AMS" ReadOnly="True" DataFormatString="{0:#,0}"
                                                    SortExpression="AMS">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ICO" HeaderText="ICO" ReadOnly="True" DataFormatString="{0:#,0}"
                                                    SortExpression="ICO">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UOM_CODE" HeaderText="UOM" ReadOnly="True" SortExpression="UOM_CODE">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UOM_PRICE" HeaderText="UOM Price" ReadOnly="True" DataFormatString="{0:#,0.00}"
                                                    SortExpression="UOM_PRICE">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Shelf">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtShelf" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text=""> </asp:TextBox>
                                                        <asp:CompareValidator ID="cvtxtShelf" runat="server" ControlToValidate="txtShelf"
                                                            ValidationGroup="DRCDtl" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                            ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Store">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStore" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text=""> </asp:TextBox>
                                                        <asp:CompareValidator ID="cvtxtStore" runat="server" ControlToValidate="txtStore"
                                                            ValidationGroup="DRCDtl" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                            ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PO">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPO" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text="" onchange='CalAmt(this)'> </asp:TextBox>
                                                        <asp:CompareValidator ID="cvtxtPO" runat="server" ControlToValidate="txtPO" ValidationGroup="DRCDtl"
                                                            Display="Dynamic" Operator="DataTypeCheck" Type="Double" ErrorMessage="Enter numeric only"
                                                            CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PO Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPOamt" runat="server" Width="100px" CssClass="cls_label" Text="0"> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDRCDtl" runat="server" TargetControlID="pDRCDtl"
                                    ExpandControlID="ddtl" CollapseControlID="ddtl">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <div id="Div1" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                                    background-color: Gray">
                                    <span style="float: right;">
                                        <asp:Button ID="btnProceeed" runat="server" Text="Proceed >>" CssClass="cls_button"
                                            Width="80px" />
                                    </span>
                                </div>
                                <div id="divsumm" class="S_DivHeader">
                                    Detail Information - View/Delete/Edit
                                </div>
                                <asp:Panel ID="pgridview" runat="server" CssClass="cls_panel_header" Width="99.8%">
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <asp:Button ID="btndeletesum" runat="server" Text="Delete" CssClass="cls_button"
                                                Visible="false" Width="80px" CausesValidation="false" ValidationGroup="DRCSum"
                                                OnClientClick="return  ValidateCheckBoxStates();" />
                                            <asp:Button ID="btnsavesum" runat="server" Text="Save" CssClass="cls_button" ValidationGroup="DRCSum"
                                                Visible="false" Width="80px" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="100%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="PRD_CODE,LINE_NO">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="DeleteAllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkdelete" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="LINE_NO" HeaderText="Line No" ReadOnly="True" SortExpression="LINE_NO">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" ReadOnly="True" SortExpression="PRD_NAME">
                                                    <ItemStyle HorizontalAlign="left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TXN_STATUS" HeaderText="Txn Status" ReadOnly="True" SortExpression="PRD_NAME">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="OPEN_BAL_QTY" HeaderText="Open Bal. Qty." ReadOnly="True"
                                                    DataFormatString="{0:#,0}" SortExpression="OPEN_BAL_QTY">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMS" HeaderText="AMS" ReadOnly="True" DataFormatString="{0:#,0}"
                                                    SortExpression="AMS">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ICO" HeaderText="ICO" ReadOnly="True" DataFormatString="{0:#,0}"
                                                    SortExpression="ICO">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UOM_CODE" HeaderText="UOM" ReadOnly="True" SortExpression="UOM_CODE">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UOM_PRICE" HeaderText="UOM Price" ReadOnly="True" DataFormatString="{0:#,0.00}"
                                                    SortExpression="UOM_PRICE">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CUR_STK_QTY" HeaderText="Cur. Stk Qty. " ReadOnly="True"
                                                    DataFormatString="{0:#,0}" SortExpression="CUR_STK_QTY">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="OFF_TAKE_QTY" HeaderText="Off Take Qty." ReadOnly="True"
                                                    DataFormatString="{0:#,0}" SortExpression="OFF_TAKE_QTY">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STK_TRF_QTY" HeaderText="Stk. Trf Qty." ReadOnly="True"
                                                    DataFormatString="{0:#,0}" SortExpression="STK_TRF_QTY">
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Shelf">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSumShelf" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text='<%#Bind("SHELF_QTY") %>'> </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSumShelf" runat="server" ErrorMessage="Enter quantity"
                                                            ControlToValidate="txtSumShelf" Display="Dynamic" CssClass="cls_label_header"
                                                            ValidationGroup="DRCSum">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="cvtxtSumShelf" runat="server" ControlToValidate="txtSumShelf"
                                                            ValidationGroup="DRCSum" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                            ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Store">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSumStore" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text='<%#Bind("STORE_QTY") %>'> </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSumStore" runat="server" ErrorMessage="Enter quantity"
                                                            ControlToValidate="txtSumStore" Display="Dynamic" CssClass="cls_label_header"
                                                            ValidationGroup="DRCSum">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="cvtxtSumStore" runat="server" ControlToValidate="txtSumStore"
                                                            ValidationGroup="DRCSum" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                            ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PO">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSumPO" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                            Style="text-align: right; border-style: ridge;" Text='<%#Bind("PROPOSED_ORD_QTY") %>'
                                                            onchange='CalAmtSum(this)'> </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSumPO" runat="server" ErrorMessage="Enter quantity"
                                                            ControlToValidate="txtSumPO" Display="Dynamic" CssClass="cls_label_header" ValidationGroup="DRCSum">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="cvtxtSumPO" runat="server" ControlToValidate="txtSumPO"
                                                            ValidationGroup="DRCSum" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                            ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                        </asp:CompareValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PO Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSumPOamt" runat="server" Width="100px" CssClass="cls_label" Text='<%#Bind("POAMT") %>' > </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="right"  />
                                                </asp:TemplateField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="spepgridview" runat="server" TargetControlID="pgridview"
                                    ExpandControlID="divsumm" CollapseControlID="divsumm">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>
</body>
</html>
