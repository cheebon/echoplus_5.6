'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	07/08/2008
'	Purpose	    :	New Equipment List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_Services_NewEqpList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "NewEqpList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.NEWEQPLIST)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.NEWEQPLIST
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

            End If

            If wuc_toolbar.ViewType = "1" Then
                btnArchiveSelected.Visible = False
            Else
                btnArchiveSelected.Visible = True
            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
            'If Page.IsValid = False Then
            '    Exit Sub
            'Else
            '    RenewDataBind()
            'End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnArchiveSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchiveSelected.Click
        Try
            If dgList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim strSelectedTxnNo As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSelectedTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                            MoveToArchive(strSelectedTxnNo)
                        End If
                    End If
                    i += 1
                Next

                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnArchive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchive.Click
        Try
            Dim strSelectedTxnNo As String = lblTxnNo.Text
            MoveToArchive(strSelectedTxnNo)

            pnlBtnArchive.Visible = False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080526
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsNewEqp As New mst_Services.clsNewEqp
            With (wuc_toolbar)
                DT = clsNewEqp.GetNewEqpList(.SearchType, .SearchValue, .ViewType)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            'CHECKBOX
            'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, SubModuleAction.Delete) Then
            aryDataItem.Add("chkSelect")
            While dgList.Columns.Count > 1
                dgList.Columns.RemoveAt(1)
            End While
            dgList.Columns(0).HeaderStyle.Width = "25"
            dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Else
            'dgList.Columns.Clear()
            'End If

            'ADD BUTTON VIEW DETAIL
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "View Detail"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='View Detail' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_VIEW_DTL")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_NewEqpList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_NewEqpList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_NewEqpList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_NewEqpList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_NewEqpList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, SubModuleAction.Delete) Then 'HL: 20080429 (AR)
            If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                If chkAll Is Nothing Then
                    chkAll = New CheckBox
                    chkAll.ID = "chkSelectAll"
                    e.Row.Cells(0).Controls.Add(chkAll)
                End If
                chkAll.Attributes.Add("onClick", "SelectAllRows(this)")
                chkAll.ToolTip = "Click to toggle the selection of ALL rows"
            ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
                chk.Attributes.Add("onClick", "SelectRow(this)")
            End If
            'End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedTxnNo As String = sender.datakeys(e.NewEditIndex).item("TXN_NO")
            Dim DT As DataTable
            Dim clsNewEqp As New mst_Services.clsNewEqp
            Dim strArchiveFlag As String

            DT = clsNewEqp.GetNewEqpDetails(strSelectedTxnNo)

            If DT.Rows.Count > 0 Then
                lblTxnNo.Text = IIf(IsDBNull(DT.Rows(0)("TXN_NO")), "", DT.Rows(0)("TXN_NO"))
                'lblTxnStatus.Text = IIf(IsDBNull(DT.Rows(0)("TXN_STATUS")), "", DT.Rows(0)("TXN_STATUS"))
                lblTechnicianCode.Text = IIf(IsDBNull(DT.Rows(0)("TECHNICIAN_CODE")), "", DT.Rows(0)("TECHNICIAN_CODE"))
                lblTechnicianName.Text = IIf(IsDBNull(DT.Rows(0)("TECHNICIAN_NAME")), "", DT.Rows(0)("TECHNICIAN_NAME"))
                lblCustCode.Text = IIf(IsDBNull(DT.Rows(0)("CUST_CODE")), "", DT.Rows(0)("CUST_CODE"))
                lblCustName.Text = IIf(IsDBNull(DT.Rows(0)("CUST_NAME")), "", DT.Rows(0)("CUST_NAME"))
                lblContCode.Text = IIf(IsDBNull(DT.Rows(0)("CONT_CODE")), "", DT.Rows(0)("CONT_CODE"))
                lblContName.Text = IIf(IsDBNull(DT.Rows(0)("CONT_NAME")), "", DT.Rows(0)("CONT_NAME"))
                lblManufacturer.Text = IIf(IsDBNull(DT.Rows(0)("MANUFACTURER")), "", DT.Rows(0)("MANUFACTURER"))
                lblModel.text = IIf(IsDBNull(DT.Rows(0)("MODEL")), "", DT.Rows(0)("MODEL"))
                lblManufacturerPartNo.Text = IIf(IsDBNull(DT.Rows(0)("MANUFACTURER_PART_NO")), "", DT.Rows(0)("MANUFACTURER_PART_NO"))
                lblSerialNo.Text = IIf(IsDBNull(DT.Rows(0)("SERIAL_NO")), "", DT.Rows(0)("SERIAL_NO"))
                lblLocation.Text = IIf(IsDBNull(DT.Rows(0)("LOCATION")), "", DT.Rows(0)("LOCATION"))
                lblMaintPlant.Text = IIf(IsDBNull(DT.Rows(0)("MAINT_PLANT")), "", DT.Rows(0)("MAINT_PLANT"))
                lblWorkcenterCode.Text = IIf(IsDBNull(DT.Rows(0)("WORKCENTER_CODE")), "", DT.Rows(0)("WORKCENTER_CODE"))
                lblCustWarrantyFromDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("CUST_WARRANTY_FROM_DATE")), "", DT.Rows(0)("CUST_WARRANTY_FROM_DATE")))
                lblCustWarrantyToDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("CUST_WARRANTY_TO_DATE")), "", DT.Rows(0)("CUST_WARRANTY_TO_DATE")))
                lblVendorWarrantyFromDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("VENDOR_WARRANTY_FROM_DATE")), "", DT.Rows(0)("VENDOR_WARRANTY_FROM_DATE")))
                lblVendorWarrantyToDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("VENDOR_WARRANTY_TO_DATE")), "", DT.Rows(0)("VENDOR_WARRANTY_TO_DATE")))
                lblSoldtoCode.Text = IIf(IsDBNull(DT.Rows(0)("SOLDTO_CODE")), "", DT.Rows(0)("SOLDTO_CODE"))
                lblShiptoCode.Text = IIf(IsDBNull(DT.Rows(0)("SHIPTO_CODE")), "", DT.Rows(0)("SHIPTO_CODE"))
                lblABCInd.Text = IIf(IsDBNull(DT.Rows(0)("ABC_IND")), "", DT.Rows(0)("ABC_IND"))
                lblClass.Text = IIf(IsDBNull(DT.Rows(0)("CLASS")), "", DT.Rows(0)("CLASS"))
                lblRemark.Text = IIf(IsDBNull(DT.Rows(0)("REMARK")), "", DT.Rows(0)("REMARK"))
                lblTxnTimestamp.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("TXN_TIMESTAMP")), "", DT.Rows(0)("TXN_TIMESTAMP")))
                lblErrDate.Text = IIf(IsDBNull(DT.Rows(0)("ERR_DATE")), "", DT.Rows(0)("ERR_DATE"))
                lblErrFlag.Text = IIf(IsDBNull(DT.Rows(0)("ERR_FLAG")), "", DT.Rows(0)("ERR_FLAG"))
                lblErrDesc.Text = IIf(IsDBNull(DT.Rows(0)("ERR_DESC")), "", DT.Rows(0)("ERR_DESC"))

                strArchiveFlag = IIf(IsDBNull(DT.Rows(0)("ARCHIVE_FLAG")), "", DT.Rows(0)("ARCHIVE_FLAG"))
                If strArchiveFlag = "1" Then
                    pnlBtnArchive.Visible = False
                Else
                    pnlBtnArchive.Visible = True
                End If

            End If

            ActivateDtlTab()
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Protected Sub MoveToArchive(ByVal strTxnNO As String)
        Try
            Dim clsNewEqp As New mst_Services.clsNewEqp
            clsNewEqp.UpdateNewEqp(strTxnNO)

            wuc_lblMsgPop.Message = "The record(s) have beed successfully moved to archive!"
            wuc_lblMsgPop.Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function

    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "New Equipment List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateDtlTab()
        TabPanel1.HeaderText = "New Equipment Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            wuc_toolbar.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_NewEqpList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn. No."
            Case "TXN_STATUS"
                strFieldName = "Txn. Status"
            Case "MANUFACTURER_CODE"
                strFieldName = "Manufacturer Code"
            Case "MANUFACTURER_NAME"
                strFieldName = "Manufacturer Name"
            Case "MODEL"
                strFieldName = "Model"
            Case "MANUFACTURER_PART_NO"
                strFieldName = "Manufacturer Part No."
            Case "SERIAL_NO"
                strFieldName = "Serial No."
            Case "LOCATION"
                strFieldName = "Location"
            Case "MAINT_PLANT"
                strFieldName = "Maint Plant"
            Case "WORKCENTER_CODE"
                strFieldName = "Workcenter Code"
            Case "CUST_WARRANTY_FROM_DATE"
                strFieldName = "Customer Warranty From"
            Case "CUST_WARRANTY_TO_DATE"
                strFieldName = "Customer Warranty To"
            Case "VENDOR_WARRANTY_FROM_DATE"
                strFieldName = "Vendor Warranty From"
            Case "VENDOR_WARRANTY_TO_DATE"
                strFieldName = "Vendor Warranty To"
            Case "SOLDTO_CODE"
                strFieldName = "Soldto Code"
            Case "ABC_IND"
                strFieldName = "ABC Ind"
            Case "REMARK"
                strFieldName = "Remark"
            Case "TXN_DATE"
                strFieldName = "Txn Date"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

