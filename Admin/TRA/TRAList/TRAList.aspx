﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAList.aspx.vb" Inherits="iFFMA_TRA_TRAList_TRAList" %>

<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<%@ Register src="../../../include/wuc_txtCalendarRange.ascx" tagname="wuc_txtCalendarRange" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

        window.onresize = function() { resizeLayout3('div_dgList', 'ContentBarIframe', 135); }
        function getSelectedCriteria() { var frm = self.parent.document.frames[0]; if (frm) { var hdf = document.getElementById('hdfSalesrepName'); var spn_ori = frm.document.getElementById('hdnSalesrepName'); if (hdf && spn_ori) { hdf.value = spn_ori.innerHTML; } hdf = document.getElementById('hdfSalesTeamName'); spn_ori = frm.document.getElementById('hdnSalesTeamName'); if (hdf && spn_ori) { hdf.value = spn_ori.innerHTML; } } }
        setTimeout("getSelectedCriteria()", 1000);
        function ShowElement(element) { var TopDiv = self.parent.document.getElementById(element); TopDiv.style.display = ''; }


        function ChangeAllCheckBoxStates(element) {
            $("#dgList").find("input:checkbox[Id*=chkSelection]").each(function () { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgList").find("input:checkbox[Id*=chkSelection]").each(function () { if (this.checked == false) flag = false; });
            $("#dgList").find("input:checkbox[Id*=chkAllSelection]").attr('checked', flag);
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ShowElement('ContentBar');resetSize('div_dgList','ContentBarIframe');MaximiseFrameHeight('ContentBarIframe');HideElement('DetailBar');">
    <form id="frmTRAList" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                <tr>
                                    <td colspan="3">
                                        <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td colspan="3">
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="Bckgroundreport" align="left">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellpadding="1" cellspacing="1" width="98%" class="cls_table">
                                                        <tr>
                                                            <td width="10%" valign="top">
                                                                <asp:Label ID="lblTeam" runat="server" CssClass="cls_label_header">Team</asp:Label>
                                                            </td>
                                                            <td width="10%" valign="top">
                                                                <asp:Label ID="lblSearch" runat="server" CssClass="cls_label_header">Search By</asp:Label>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:Label ID="lblSearchValue" runat="server" CssClass="cls_label_header"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="False">
                                                                    <asp:ListItem Value="ALL">All</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
                                                                    <asp:ListItem Value="ALL">All</asp:ListItem>
                                                                    <asp:ListItem Value="TXN_NO">Txn No</asp:ListItem>
                                                                    <%-- <asp:ListItem Value="TXN_DATE">Txn Date</asp:ListItem>
                                                                    <asp:ListItem Value="TXN_STATUS">Txn Status</asp:ListItem>--%>
                                                                    <asp:ListItem Value="SALES_OFFICE">Sales Office</asp:ListItem>
                                                                    <asp:ListItem Value="SALESREP_CODE">Salesrep Code</asp:ListItem>
                                                                    <asp:ListItem Value="PRD_GRP_CODE">Prd Grp Code</asp:ListItem>
                                                                     <asp:ListItem Value="CUST_CODE">Customer Code</asp:ListItem> 
                                                                      <asp:ListItem Value="PRD_GRP_NAME">Prd Grp Name</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td valign="top" nowrap="nowrap">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="550px">
                                                                    <tr>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:DropDownList ID="ddlList" runat="server" CssClass="cls_dropdownlist">
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                            <customToolkit:wuc_txtDate ID="Wuc_txtDate1" runat="server" ControlType="DateOnly" />
                                                                        </td>
                                                                        <td width="1%" valign="top">
                                                                            <asp:Label ID="lblTo" runat="server" CssClass="cls_label_header">To</asp:Label>
                                                                        </td>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:TextBox ID="txtSearchValue1" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                            <customToolkit:wuc_txtDate ID="Wuc_txtDate2" runat="server" ControlType="DateOnly" />
                                                                        </td>
                                                                        <td valign="top">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table>
                                                                    <tr>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:Label ID="lblTxnStatus" runat="server" Text="Txn Status" CssClass="cls_label_header"></asp:Label>
                                                                        </td>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:Label ID="lblPrintStatusFrom" runat="server" Text="Print Status" CssClass="cls_label_header"></asp:Label>
                                                                        </td>
                                                                       <%-- <td nowrap="nowrap" valign="top">
                                                                            <asp:Label ID="lblPrintStatusTo" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                        </td>--%>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:Label ID="lblTxnDateFrom" runat="server" Text="Txn Date" CssClass="cls_label_header"></asp:Label>
                                                                        </td> 
                                                                        <td nowrap="nowrap" valign="top">
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:DropDownList ID="ddltxnStatus" runat="server" CssClass="cls_dropdownlist" ValidationGroup="search" >
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:DropDownList ID="ddlprintstatus" runat="server" CssClass="cls_dropdownlist"> 
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                      <%--  <td nowrap="nowrap" valign="top">
                                                                            <asp:TextBox ID="txtPrintStatusFrom" runat="server" CssClass="cls_textbox" ValidationGroup="search"  Width="25px" ></asp:TextBox>
                                                                        </td>
                                                                        <td nowrap="nowrap" valign="top">
                                                                            <asp:TextBox ID="txtPrintStatusTo" runat="server" CssClass="cls_textbox" ValidationGroup="search"  Width="25px" ></asp:TextBox>
                                                                        </td>--%>
                                                                        <td nowrap="nowrap" valign="top" rowspan="2">
                                                                           <%-- <asp:TextBox ID="txtDateFrom" runat="server" CssClass="cls_textbox"></asp:TextBox>--%> 
                                                                            <uc2:wuc_txtCalendarRange ID="wuc_txtCalendarRange1"  runat="server" RequiredValidation="true" RequiredValidationGroup="search"  /> 
                                                                        </td> 
                                                                        <td nowrap="nowrap" valign="top">
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" ValidationGroup="search" ></asp:Button>
                                                                            <asp:Button ID="btnPrint" runat="server" Text="Print Selected Batch" CssClass=" cls_button"
                                                                                Visible="false" />
                                                                            <cc1:ConfirmButtonExtender ID="cbebtnPrint" runat="server" TargetControlID="btnPrint"
                                                                                ConfirmText="Do you want to Print selected?" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="Bckgroundreport">
                                                <td colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Bckgroundreport" colspan="4">
                                                    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress2" runat="server" />
                                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                        <ContentTemplate>
                                                            <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>
                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="TXN_NO" BorderColor="Black"
                                                                BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                                <Columns>
                                                                    <asp:TemplateField ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgApprove" CommandName="Approve" runat="server" ImageUrl="~/images/ico_UnBlock.gif"
                                                                                CommandArgument="<%# CType(Container, GridViewRow).RowIndex %>" CausesValidation="False"
                                                                                AlternateText="Approve" ImageAlign="Middle" Visible="False"></asp:ImageButton>
                                                                            <cc1:ConfirmButtonExtender ID="cbeApprove" runat="server" TargetControlID="imgApprove"
                                                                                ConfirmText="Do you want to approve?" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgReject" CommandName="Reject" runat="server" ImageUrl="~/images/icoCancel.gif"
                                                                                CommandArgument="<%# CType(Container, GridViewRow).RowIndex %>" CausesValidation="False"
                                                                                AlternateText="Reject" ImageAlign="Middle" Visible="False"></asp:ImageButton>
                                                                            <cc1:ConfirmButtonExtender ID="cbeReject" runat="server" TargetControlID="imgReject"
                                                                                ConfirmText="Do you want to reject?" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ShowHeader="False">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgPrint" CommandName="Print" runat="server" ImageUrl="~/images/ico_printer.gif"
                                                                                CommandArgument="<%# CType(Container, GridViewRow).RowIndex %>" CausesValidation="False"
                                                                                AlternateText="Print" ImageAlign="Middle" Visible="False"></asp:ImageButton>
                                                                            <cc1:ConfirmButtonExtender ID="cbePrint" runat="server" TargetControlID="imgPrint"
                                                                                ConfirmText="Do you want to Print?" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField  >
                                                                       <HeaderTemplate>
                                                                            <%--  <img alt="Print" src="../../../images/ico_printer.gif"  />--%>
                                                                            <asp:Label ID="lblHdrPrint" runat="server" Text="Print" ></asp:Label>
                                                                            <asp:CheckBox ID="chkAllSelection" runat="server" CssClass="cls_checkbox" onclick="ChangeAllCheckBoxStates(this);" />
                                                                        </HeaderTemplate>
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelection" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="PRD_GRP_CODE" HeaderText="Prd. Grp. Code" ReadOnly="True"
                                                                        SortExpression="PRD_GRP_CODE">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="PRD_GRP_NAME" HeaderText="Prd. Grp. Name" ReadOnly="True"
                                                                        SortExpression="PRD_GRP_NAME">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:HyperLinkField DataNavigateUrlFields="txn_no" DataNavigateUrlFormatString="#"
                                                                        DataTextField="txn_no" HeaderText="Txn No" SortExpression="txn_no"></asp:HyperLinkField>
                                                                    <asp:BoundField DataField="txn_status" HeaderText="Txn Status" ReadOnly="True" SortExpression="txn_status">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="print_no" HeaderText="Printing Status" ReadOnly="True"
                                                                        SortExpression="print_no">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="txn_date" HeaderText="Txn Date" ReadOnly="True" SortExpression="txn_date"
                                                                        DataFormatString="{0:yyyy-MM-dd}">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ref_no" HeaderText="Purchase order No. (Customer return document No.) "
                                                                        ReadOnly="True" SortExpression="ref_no">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="salesrep_code" HeaderText="Salesrep Code" ReadOnly="True"
                                                                        SortExpression="salesrep_code">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="salesrep_name" HeaderText="Salesrep Name" ReadOnly="True"
                                                                        SortExpression="salesrep_name">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="cust_code" HeaderText="Ship-to (Customer ID)" ReadOnly="True"
                                                                        SortExpression="cust_code">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="cust_name" HeaderText="Customer Name" ReadOnly="True"
                                                                        SortExpression="cust_name">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="address" HeaderText="Ship-to Address" ReadOnly="True"
                                                                        SortExpression="address">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="sales_office" HeaderText="Sales Office" ReadOnly="True"
                                                                        SortExpression="sales_office">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ret_type" HeaderText="Storage before return DK warehouse (for transport pick up purpose) "
                                                                        ReadOnly="True" SortExpression="ret_type">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="manager_name" HeaderText="Approval Level–District Sales Manager/Nation Sales Manager/Marketing "
                                                                        ReadOnly="True" SortExpression="manager_name">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="changed_date" HeaderText="Approved/ Rejected Date " ReadOnly="True"
                                                                        DataFormatString="{0:yyyy-MM-dd}" SortExpression="changed_date">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <FooterStyle CssClass="GridFooter" />
                                                                <HeaderStyle CssClass="GridHeader" />
                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                <RowStyle CssClass="GridNormal" />
                                                                <PagerSettings Visible="False" />
                                                            </ccGV:clsGridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr class="Bckgroundreport">
                                                <td align="right" colspan="3">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--  <div id="PrintBar" style="display: inline; overflow: hidden; margin: 0; border: 0; padding: 0;">
                            <iframe id="PrintBarIframe" frameborder="0" marginwidth="0" marginheight="0" src=""
                                width="100%" scrolling="auto" height="500" style="border: 0; position: relative;
                                top: 0px;"></iframe>
                             </div>--%>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
