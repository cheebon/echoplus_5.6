'***********************************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/06/2005
'	Purpose	    :	Class XML Reader 
'
'	Revision	: 	
' -----------------------------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			                        |	
' -----------------------------------------------------------------------------------------------
' |1	    |           	|                   | 	                                            |
' |2	    |               |                   |                                               |
' -----------------------------------------------------------------------------------------------
'************************************************************************************************


Option Explicit On 
'Option Strict On
'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.xml


Interface IcorOB_ReadXml
    Function GetXmlValueByAttribute(ByVal strPath As String, ByVal strElementName As String, ByVal strAttributeName As String, Optional ByVal strElementName2 As String = "", Optional ByVal strAttributeName2 As String = "") As DataTable
    Function GetXmlValueByArrayAttribute(ByVal strPath As String, ByVal strElementName As String, ByVal arrAttributeName As ArrayList) As DataTable
    Function PopulateRow(ByVal drRow As DataRow, ByVal arrAttributeName As ArrayList, ByVal arrTemp As ArrayList) As DataRow
End Interface


'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsReadXml
    'Inherits ServicedComponent
    Implements IcorOB_ReadXml

    Private handle As IntPtr                ' Pointer to an external unmanaged resource.
    Private Components As Component         ' Other managed resource this class uses.
    Private disposed As Boolean = False     ' Track whether Dispose has been called.
    'Public clsProperties As New clsProperties.clsReadXml


    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetXmlValueByAttribute
    ' Purpose	    :   Get Attribute Value
    ' Parameters    :	[in] sender: strPath,strElementName,strAttribute
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetXmlValueByAttribute(ByVal strPath As String, ByVal strElementName As String, ByVal strAttributeName As String, Optional ByVal strElementName2 As String = "", Optional ByVal strAttributeName2 As String = "") As DataTable Implements IcorOB_ReadXml.GetXmlValueByAttribute
        Dim xmlReader As XmlReader
        Dim dt As DataTable
        Dim drRow1, drRow2 As DataRow
        Dim dColumn1, dColumn2 As DataColumn

        'Try
        'Call Trail class based on system setting
        'objTrace = New ClsTrace
        'objTrace.Trace(0)
        dt = New DataTable("Table")

        dColumn1 = New DataColumn(strAttributeName)
        dColumn1.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dColumn1)
        dColumn1.Dispose()

        xmlReader = New XmlTextReader(strPath)
        Do While (xmlReader.Read())
            If Trim(xmlReader.NodeType) = Trim(XmlNodeType.Element) Then
                If xmlReader.Name = strElementName Then
                    drRow1 = dt.NewRow()
                    drRow1.Item(strAttributeName) = IIf(xmlReader.GetAttribute(strAttributeName) <> Nothing, xmlReader.GetAttribute(strAttributeName), "")
                    dt.Rows.Add(drRow1)
                End If
            End If
        Loop

        If strElementName2 <> "" And strAttributeName2 <> "" Then
            xmlReader = New XmlTextReader(strPath)
            dColumn2 = New DataColumn(strAttributeName2)
            dColumn2.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dColumn2)
            dColumn2.Dispose()
            Dim i As Integer
            Do While (xmlReader.Read())
                If Trim(xmlReader.NodeType) = Trim(XmlNodeType.Element) Then
                    If xmlReader.Name = strElementName2 Then
                        If Trim(xmlReader.NodeType) = Trim(XmlNodeType.Element) Then
                            If xmlReader.Name = strElementName2 Then
                                drRow2 = dt.Rows(i)
                                drRow2.Item(strAttributeName2) = IIf(xmlReader.GetAttribute(strAttributeName2) <> Nothing, xmlReader.GetAttribute(strAttributeName2), "")
                                i = i + 1
                            End If
                        End If
                    End If
                End If
            Loop
        End If
        GetXmlValueByAttribute = dt
        xmlReader.Close()
        xmlReader = Nothing
        dt = Nothing
        Exit Function
        'Catch ex As Exception
        '    Throw (New ExceptionMsg("cor_Xml.clsWriteXml.GetXmlValueByAttribute : " & ex.ToString))
        'Finally
        '    xmlReader = Nothing
        '    dt = Nothing
        'End Try
    End Function


    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetXmlValueByArrayAttribute
    ' Purpose	    :   Get All Attribute Value
    ' Parameters    :	[in]  ByVal strPath As String, ByVal strElementName As String, ByVal arrAttributeName As ArrayList
    '		            [out] Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetXmlValueByArrayAttribute(ByVal strPath As String, ByVal strElementName As String, ByVal arrAttributeName As ArrayList) As DataTable Implements IcorOB_ReadXml.GetXmlValueByArrayAttribute
        Dim xmlReader As New XmlTextReader(strPath)
        Dim dt As DataTable
        Dim drRow As DataRow
        Dim dColumn As DataColumn
        Dim intCount As Integer
        Dim arrTemp As New ArrayList
        Dim strTemp As String
        'Dim blnSet As Boolean

        Try
            'Call Trail class based on system setting
            'objTrace = New ClsTrace
            'objTrace.Trace(0)

            dt = New DataTable("Table")

            For intCount = 0 To arrAttributeName.Count - 1
                dColumn = New DataColumn(arrAttributeName.Item(intCount))
                dColumn.DataType = System.Type.GetType("System.String")
                dt.Columns.Add(dColumn)
                dColumn.Dispose()
            Next
            Do While (xmlReader.Read())
                If Trim(xmlReader.NodeType) = Trim(XmlNodeType.Element) Then
                    If xmlReader.Name = strElementName Then
                        drRow = dt.NewRow()
                        For intCount = 0 To arrAttributeName.Count - 1
                            strTemp = IIf(xmlReader.GetAttribute(arrAttributeName.Item(intCount)) <> Nothing, xmlReader.GetAttribute(arrAttributeName.Item(intCount)), "")
                            arrTemp.Add(strTemp)
                        Next
                        drRow = PopulateRow(drRow, arrAttributeName, arrTemp)
                        dt.Rows.Add(drRow)
                        arrTemp = Nothing
                        arrTemp = New ArrayList
                    End If
                End If
            Loop


            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            'Throw (New ExceptionMsg("cor_Xml.clsReadXML.GetXmlValueByArrayAttribute : " + ex.ToString))
        Finally
            'set obj = Nothing
            'objTrace = Nothing
            xmlReader.Close()
            xmlReader = Nothing
            dt = Nothing
        End Try
        Return dt
    End Function


    Public Function GetSingleXMLValueByAttribute(ByVal strPath As String, ByVal strElementName As String, ByVal strAttributeName As String) As DataTable
        Dim xmlReader As XmlReader
        Dim dt As DataTable
        Dim drRow1 As DataRow
        Dim dColumn1 As DataColumn

        Try
            dt = New DataTable("Table")

            dColumn1 = New DataColumn(strAttributeName)
            dColumn1.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dColumn1)
            dColumn1.Dispose()

            xmlReader = New XmlTextReader(strPath)
            Do While (xmlReader.Read())
                If Trim(xmlReader.NodeType) = Trim(XmlNodeType.Element) Then
                    If xmlReader.Name = strElementName Then
                        If xmlReader.GetAttribute(strAttributeName) <> Nothing Then
                            drRow1 = dt.NewRow()
                            drRow1.Item(strAttributeName) = xmlReader.GetAttribute(strAttributeName)
                            dt.Rows.Add(drRow1)
                            Exit Do
                        End If
                    End If
                End If
            Loop
            GetSingleXMLValueByAttribute = dt
            xmlReader.Close()
            xmlReader = Nothing
            dt = Nothing
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_Xml.clsReadXML.GetSingleXMLValueByAttribute : " + ex.ToString))
        End Try
    End Function


    Public Function GetXmlByElement(ByVal dt As DataTable, ByVal strFieldName As String, ByVal strElementName As String, ByVal strFullPath As String) As DataTable
        Try
            'Language DataTable Fields
            Dim dcField As New DataColumn
            dcField.ColumnName = strFieldName
            dcField.DataType = GetType(String)
            dt.Columns.Add(dcField)
            dcField.Dispose()

            Dim xmlReader As XmlTextReader
            xmlReader = New XmlTextReader(strFullPath)

            If dt.Columns.Count = 1 Then

                'Since there is only one column and this column is blank
                'We need to add row 
                Do While (xmlReader.Read())
                    If xmlReader.Name = strElementName Then
                        Dim drRow As DataRow = dt.NewRow
                        drRow(strFieldName) = xmlReader.ReadString()
                        dt.Rows.Add(drRow)
                    End If
                Loop
                'Return dt

            Else
                'Assuming the first column has already been created with values inserted
                'The second column will be blank, so we need to add text into cells NOT rows
                Dim i As Integer = 0
                Do While (xmlReader.Read())
                    If xmlReader.Name = strElementName Then
                        dt.Rows(i)(strFieldName) = xmlReader.ReadString()
                        i = i + 1
                    End If
                Loop
                'Return dt

            End If


        Catch ex As Exception
        End Try
        Return dt
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function PopulateRow
    ' Purpose	    :   Get Attribute Value
    ' Parameters    :	[in]  ByVal drRow As DataRow, ByVal arrAttributeName As ArrayList, ByVal arrTemp As ArrayList
    '		            [out] Datarow
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Private Function PopulateRow(ByVal drRow As DataRow, ByVal arrAttributeName As ArrayList, ByVal arrTemp As ArrayList) As DataRow Implements IcorOB_ReadXml.PopulateRow
        Dim intCount As Integer
        
        Try
            'Call Trail class based on system setting
            'objTrace = New ClsTrace
            'objTrace.Trace(0)

            For intCount = 0 To arrAttributeName.Count - 1
                drRow(arrAttributeName.Item(intCount)) = arrTemp.Item(intCount)
            Next

            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            'Throw (New ExceptionMsg("cor_Xml.clsReadXML.PopulateRow : " + ex.ToString))
        Finally
            'set obj = Nothing
            'objTrace = Nothing
        End Try
        Return drRow
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            ''Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class


