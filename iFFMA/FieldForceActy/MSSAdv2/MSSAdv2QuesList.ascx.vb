Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdv2QuesList
    Inherits System.Web.UI.UserControl

    Public Event ViewSubQues_Click As EventHandler
    Public Event DeleteQues_Click As EventHandler
    Public Event SaveQues_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        'Call Paging()
        'With wuc_dgQuesPaging
        '    '.PageCount = dgQuesList.PageCount
        '    '.CurrentPageIndex = dgQuesList.PageIndex
        '    .DataBind()
        '    .Visible = True
        'End With

        If IsPostBack Then
            If Master_Row_Count > 0 Then
                For Each dr As GridViewRow In dgQuesList.Rows
                    Dim btnPreview As Button = CType(dr.Cells(aryDataItem.IndexOf("BTN_PREVIEW")).Controls(0), Button)
                    If btnPreview IsNot Nothing Then
                        btnPreview.CssClass = "cls_button"
                    End If
                Next
            End If
        End If


        'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Edit) Then
            btnAdd.Visible = False
            btnArrangeSeq.Visible = False
        End If
        '----------------------------------------------------------------------------------------------------
    End Sub

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgQuesList.PageIndex = 0
        wuc_dgQuesPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Public Sub UpdateHdrInfo()
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2
            DT = clsMSSAdv.MSSUpdateHdrInfo(TeamCode, TitleCode, "", "")

            If DT.Rows.Count > 0 Then
                lblTeamCode.Text = DT.Rows(0)("TEAM_NAME").ToString + " (" + TeamCode + ")"
                lblTitleCode.Text = DT.Rows(0)("TITLE_NAME").ToString + " (" + TitleCode + ")"
            Else
                lblTeamCode.Text = TeamCode
                lblTitleCode.Text = TitleCode
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            DT = clsMSSAdv.GetMSSQuesAdvList(TeamCode, TitleCode)
            dgQuesList.DataKeyNames = New String() {"QUES_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgQuesList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            UpdateHdrInfo()

            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgQuesList.PageIndex = 0
            '    wuc_dgQuesPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgQuesList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgQuesPaging
                .PageCount = dgQuesList.PageCount
                .CurrentPageIndex = dgQuesList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

            btnArrangeSeq.Visible = IIf(Master_Row_Count > 0, True, False)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlQues.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgQuesList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgQuesList.Columns.Clear()

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgQuesList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgQuesList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")
            End If

            'ADD BUTTON PREVIEW
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "80"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = ""

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Button
                    dgBtnColumn.ControlStyle.CssClass = "cls_button"
                    dgBtnColumn.ShowSelectButton = True
                    dgBtnColumn.SelectText = "Preview"
                    dgBtnColumn.Visible = True
                End If

                dgQuesList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_PREVIEW")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MSSAdvQuesList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_MSSAdvQuesList.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_MSSAdvQuesList.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSAdvQuesList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSAdvQuesList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSAdvQuesList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = ""
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgQuesList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MSSAdvQuesList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSAdvQuesList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSAdvQuesList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSAdvQuesList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgQuesList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgQuesList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgQuesList.RowEditing
        Try
            Dim strSelectedQuesCode As String = sender.datakeys(e.NewEditIndex).item("QUES_CODE")

            With (wuc_MSSAdv2QuesPop)
                .TeamCode = TeamCode
                .TitleCode = TitleCode
                .QuesCode = strSelectedQuesCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgQuesList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgQuesList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            strDeleteMsg = "Are you sure want to delete?"
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"

                            'HL: 20120702 - Due to BDF, display delete button according to accessright----------------------------
                            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Delete) Then
                                btnDelete.Visible = False
                            End If
                            '----------------------------------------------------------------------------------------------------
                        End If

                        Dim TC As TableCell
                        TC = e.Row.Cells(aryDataItem.IndexOf("QUES_CODE"))
                        TC.Attributes.Add("onclick", "javascript:__doPostBack('" & dgQuesList.UniqueID & "','Select$" & e.Row.RowIndex & "')")
                        TC.Attributes.Add("style", "cursor:pointer;")

                        TC = e.Row.Cells(aryDataItem.IndexOf("BTN_PREVIEW"))
                        TC.Attributes.Add("onclick", "javascript:__doPostBack('" & dgQuesList.UniqueID & "','Preview$" & e.Row.RowIndex & "')")

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgQuesList.RowDeleting
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            Dim strQuesCode As String
            strQuesCode = IIf(sender.datakeys(e.RowIndex).item("QUES_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("QUES_CODE"))

            clsMSSAdv.DeleteMSSQuesAdv(TeamCode, TitleCode, strQuesCode)

            RenewDataBind()

            RaiseEvent DeleteQues_Click(sender, e)
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgQuesList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgQuesList.SelectedIndexChanged
        Try
            RaiseEvent ViewSubQues_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgQuesList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgQuesList.RowCommand
        Dim index As Integer
        Dim strQuesCode As String

        Try
            Select Case e.CommandName
                Case "Preview"
                    index = Convert.ToInt32(e.CommandArgument)
                    strQuesCode = dgQuesList.DataKeys(index).Item("QUES_CODE")

                    'HL: 20120515 - Due to BDF, enhance MSS to unlimited sub ques---------------------------------
                    'If Session("PRINCIPAL_ID") = 5 Or Session("PRINCIPAL_ID") = 81 Then
                    With (wuc_MSSAdv2QuesPreview2Pop)
                        .TeamCode = TeamCode
                        .TitleCode = TitleCode
                        .QuesCode = strQuesCode
                        .BindQuesPreview()
                    End With
                    'Else
                    'With (wuc_MSSAdv2QuesPreviewPop)
                    '    .TeamCode = TeamCode
                    '    .TitleCode = TitleCode
                    '    .QuesCode = strQuesCode
                    '    .BindQuesPreview()
                    'End With
                    'End If

            End Select

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Function GetdgQuesList() As GridView
        Dim dgQuesListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgQuesList.AllowSorting
            Dim blnAllowPaging As Boolean = dgQuesList.AllowPaging

            dgQuesList.AllowSorting = False
            dgQuesList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgQuesListToExport = dgQuesList

            dgQuesList.AllowPaging = blnAllowPaging
            dgQuesList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgQuesListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesPaging.Go_Click
        Try
            dgQuesList.PageIndex = CInt(wuc_dgQuesPaging.PageNo - 1)

            dgQuesList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesPaging.Previous_Click
        Try
            If dgQuesList.PageIndex > 0 Then
                dgQuesList.PageIndex = dgQuesList.PageIndex - 1
            End If
            wuc_dgQuesPaging.PageNo = dgQuesList.PageIndex + 1

            dgQuesList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesPaging.Next_Click
        Try
            If dgQuesList.PageCount - 1 > dgQuesList.PageIndex Then
                dgQuesList.PageIndex = dgQuesList.PageIndex + 1
            End If
            wuc_dgQuesPaging.PageNo = dgQuesList.PageIndex + 1

            dgQuesList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        With (wuc_MSSAdv2QuesPop)
            .TeamCode = TeamCode
            .TitleCode = TitleCode
            .QuesCode = ""
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub btnArrangeSeq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArrangeSeq.Click
        With (wuc_MSSAdv2QuesSeqPop)
            .TeamCode = TeamCode
            .TitleCode = TitleCode
            .BindQuesList()
        End With
    End Sub

    Protected Sub btnMSSAdvQuesPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MSSAdv2QuesPop.SaveButton_Click
        RenewDataBind()
        RaiseEvent SaveQues_Click(sender, e)
    End Sub

    Protected Sub btnMSSAdvQuesSeqPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MSSAdv2QuesSeqPop.SaveButton_Click
        RenewDataBind()
    End Sub
#End Region

#Region "COMMON FUNCTION"

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_MSSAdvQuesList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "SEQ"
                strFieldName = "Sequence"
            Case "QUES_CODE"
                strFieldName = "Question Code"
            Case "QUES_NAME"
                strFieldName = "Question Name"
            Case "LVL_IND"
                strFieldName = "Next Level Indicator"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "QUES_CODE" Then
                FCT = FieldColumntype.HyperlinkColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
