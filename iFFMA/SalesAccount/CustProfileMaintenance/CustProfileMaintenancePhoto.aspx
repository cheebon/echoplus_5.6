﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustProfileMaintenancePhoto.aspx.vb" Inherits="iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenancePhoto" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Profile Maintenance - Photo</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>    
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="padding: 10px 0px 0px 10px; margin-right: 30px" class="BckgroundInsideContentLayout" onload="HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');">
    
    <form id="form1" runat="server">
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
    <asp:UpdatePanel ID="updPnlMaintenanceCustomerPhoto" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate> 
        <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
       <fieldset>
        <div id="lblPopNotify_pnlMsgPopContentZone" class="Bckgroundreport" style="padding: 5px; width: 100%">
            <input type="button"  runat="server"  id="btnback" value="Back"  style="width:80px"
              onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');" class="cls_button" />
            
            <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" />
            <div style="padding-left:5px">
            <table border="0" cellpadding="5" cellspacing="0" class="cls_label_header" width="90%">
                <tr>
                     <td>
                        <div style="background-color: #ffffdd; border: solid 1px #000000; padding: 5px">
                        Upload new customer picture: <br />
                        <asp:FileUpload ID="fuCustPhoto" runat="server" CssClass="cls_button" Width="200px" />
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" cssclass="cls_button" style="height:16px; font-size:8pt" />
                        <asp:Label ID="lblUploadInfo" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                        </div>
                    </td>
                     
                </tr>  
                <tr>
                     <td>
                        <div style="background-color: #ffffdd; border: solid 1px #000000; padding: 10px">
                        <asp:Label ID="lblCustMsg" runat="server" Text="Label">No picture has been linked to this customer.</asp:Label><br /><br />
                        <asp:Image ID="imgCust" runat="server" ImageUrl="~/images/anonymous.jpg" />    
                        </div>
                    </td>   
                </tr>             
            </table>
            </div>            
        </div>      
       </fieldset>   
    
    </ContentTemplate>
    
    <Triggers>

        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>    
</asp:UpdatePanel>
    </form>
</body>
</html>
