'************************************************************************
'	Author	    :	Alex Chia 
'	Date	    :	19/09/2005
'	Purpose	    :	Show Sales Enquiry List based on the search criteria 
'                   in SalesEnquirySearchPanel in wuc_ctrlpanel.
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data
Partial Class SalesEnquiryList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Local Variable"
    Private intPageSize As Integer
    'Dim aryDataItem As New ArrayList
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem_SalesEnqList")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            _aryDataItem = value
            ViewState("DataItem_SalesEnqList") = value
        End Set
    End Property

    'Dim licItemFigureCollector As ListItemCollection
    Private _licItemFigureCollector As ListItemCollection
    Protected Property licItemFigureCollector() As ListItemCollection
        Get
            If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = Session("ItemFigureCollector_SalesEnqList")
            If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = New ListItemCollection
            Return _licItemFigureCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licItemFigureCollector = value
            Session("ItemFigureCollector_SalesEnqList") = value
        End Set
    End Property


    'Private Property intItemFigureCount() As Integer
    '    Get
    '        Dim intTtlItemFigure As Integer = IIf(IsNumeric(ViewState("ttlItemFigure")), CInt(ViewState("ttlItemFigure")), 0)
    '        Return intTtlItemFigure
    '    End Get
    '    Set(ByVal value As Integer)
    '        ViewState("ttlItemFigure") = value
    '    End Set
    'End Property


    'Private Property GrandTotal() As Double
    '    Get
    '        Dim dblGrandTotal As Double = IIf(IsNumeric(ViewState("GrandTotal")), CDbl(ViewState("GrandTotal")), 0.0)
    '        Return dblGrandTotal
    '    End Get
    '    Set(ByVal value As Double)
    '        ViewState("GrandTotal") = value
    '    End Set
    'End Property


#End Region


    Public ReadOnly Property PageName() As String
        Get
            Return "Sales Enquiry List"
        End Get
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	Sub Page_Load
    ' Purpose	        :	Initial User Control information
    '----------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.SALESENQ) '"Summarized Sales Report"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.SALESENQ
                .DataBind()
                .Visible = True
            End With


            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                Dim strDateStart, strDateEnd, strSalesrepCode, strSMName As String
                strDateStart = Trim(Request.QueryString("DATE_START"))
                strDateEnd = Trim(Request.QueryString("DATE_END"))
                strSalesrepCode = Trim(session("SALESREP_CODE"))
                strSMName = Trim(Session("SALESREP_NAME"))
                If Not String.IsNullOrEmpty(strDateStart) AndAlso IsDate(strDateStart) _
                   AndAlso Not String.IsNullOrEmpty(strDateEnd) AndAlso IsDate(strDateEnd) Then
                    With wuc_ctrlpanel
                        .SearchPanelVisibility = False
                        .DateFrom = strDateStart
                        .Dateto = strDateEnd
                        .SalesmanCode = strSalesrepCode
                        .SalesmanName = strSMName
                    End With
                    btnBack.Visible = True
                Else
                    BindDefault() 'Show blank grid view
                End If
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
        'Call Header

    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

#Region "Event Handler"
    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Sub btnSearchRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SalesEnquiryBtn_Click 'btnSearchRef.Click
        Try
            If Page.IsValid = False Then Exit Sub
            ViewState("dtCurrentView") = Nothing
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnSearchRef_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.EnqResetBtn_Click
        Try
           BindDefault
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "Sales_Enquiry")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrenttable Is Nothing Then
                'With wuc_ctrlpanel
                '    .UpdateSalesEnquirySearchDetails()
                '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
                'End With
                dtCurrenttable = GetRecList()

                ViewState("strSortExpression") = Nothing
                ViewState("dtCurrentView") = dtCurrenttable
                dgList.PageIndex = 0
            End If
            PreRenderMode(dtCurrenttable)
            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

#End Region

#Region "Custom dgList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SalesEnqList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SalesEnqList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SalesEnqList.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SalesEnqList.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SalesEnqList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '    If e.Row.RowType = DataControlRowType.Footer Then
    '        Dim GVR As GridViewRow = e.Row 'New GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Insert)
    '        Dim TC As TableCell
    '        Dim intColumnSpan As Integer = e.Row.Cells.Count - 3
    '        e.Row.Cells.Clear()

    '        TC = New TableCell
    '        TC.Text = "Grand Total "
    '        TC.HorizontalAlign = HorizontalAlign.Right
    '        TC.ColumnSpan = intColumnSpan
    '        GVR.Cells.Add(TC)


    '        TC = New TableCell
    '        TC.Text = GrandTotal.ToString("#,0.00")
    '        TC.HorizontalAlign = HorizontalAlign.Right
    '        TC.ColumnSpan = 1
    '        GVR.Cells.Add(TC)

    '        'GVR.HorizontalAlign = HorizontalAlign.Right

    '        dgList.Controls(0).Controls.AddAt(0, e.Row)

    '    End If
    'End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Footer Then
            'Dim GVR As GridViewRow = e.Row 'New GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Insert)
            'Dim TC As TableCell
            Dim intColumnSpan As Integer = e.Row.Cells.Count - licItemFigureCollector.Count

            For intIndex As Integer = 0 To intColumnSpan - 2
                e.Row.Cells.RemoveAt(0)
            Next
            e.Row.Cells(0).ColumnSpan = intColumnSpan

            e.Row.Cells(0).Text = "Grand Total "
            e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Right

            dgList.Controls(0).Controls.AddAt(0, e.Row)

        End If
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                Case DataControlRowType.Footer
                    'Dim iIndex As Integer
                    For Each TC As TableCell In e.Row.Cells
                        Dim DCF As DataControlFieldCell = TC
                        Dim bfBoundField As BoundField = CType(DCF.ContainingField, BoundField)
                        Dim li As ListItem = licItemFigureCollector.FindByText(bfBoundField.DataField)
                        If bfBoundField IsNot Nothing AndAlso li IsNot Nothing Then
                            TC.Text = String.Format(CF_SalesEnqList.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0)) 'bfBoundField.DataField
                        End If
                    Next
                    'For Each li As ListItem In licItemFigureCollector
                    '    iIndex = aryDataItem.IndexOf(li.Text)
                    '    If iIndex >= 0 Then
                    '        e.Row.Cells(iIndex).Text = String.Format(CF_SalesEnqList.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    '    End If
                    'Next
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Sub BindDefault()
        Try
            wuc_ctrlpanel.SearchPanelVisibility = True
            Dim dt As Data.DataTable = GetRecList("1900-01-01", "1900-01-01")
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList(ByVal strStartDate As String, ByVal strEndDate As String, Optional ByVal strPrdCode As String = "", Optional ByVal strSalesrepCode As String = "", Optional ByVal strCustCode As String = "", Optional ByVal strInvNo As String = "", Optional ByVal intNetValue As Integer = 0) As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")

            'intNetValue = IIf(intNetValue = 0, CInt(Session("NetValue")), intNetValue)
            If intNetValue = 0 Then intNetValue = wuc_ctrlpanel.NetValue
            Dim clsSalesDB As New rpt_Sales.clsSalesQuery
            DT = clsSalesDB.GetSalesEnquiryList(strUserID, strPrincipalId, strPrincipalCode, strStartDate, strEndDate, strPrdCode, strSalesrepCode, strCustCode, strInvNo, intNetValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            With wuc_ctrlpanel
                .UpdateSalesEnquirySearchDetails()
                DT = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .CustomerCode, .InvoiceNumber, .NetValue)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

            aryDataItem = _aryDataItem
            licItemFigureCollector = _licItemFigureCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                '
                '
                If (strColumnName Like "SALES") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next
            'intItemFigureCount = licItemFigureCollector.Count
            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub dgList_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.PreRender
    '    Try
    '        If dgList.Rows.Count = 0 Then Exit Sub
    '        If wuc_ctrlpanel.isShiptoCodeExist Then
    '            'Show Product, hide Customer
    '            'Show Product Code and Description
    '            setColumnVisibility("PRD_CODE", True)
    '            setColumnVisibility("PRD_NAME", True)

    '            'HIDE Customer Code and Name
    '            setColumnVisibility("CUST_CODE", False)
    '            setColumnVisibility("CUST_NAME", False)
    '        Else
    '            'Show Customer, hide Product

    '            'HIDE Product Code and Description
    '            setColumnVisibility("PRD_CODE", False)
    '            setColumnVisibility("PRD_NAME", False)

    '            'Show Customer Code and Name
    '            setColumnVisibility("CUST_CODE", True)
    '            setColumnVisibility("CUST_NAME", True)
    '        End If
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".dgList_PreRender : " & ex.ToString)
    '    Finally

    '    End Try
    'End Sub

    'Private Sub setColumnVisibility(ByVal strFieldName As String, ByVal blnVisible As Boolean)
    '    Try
    '        Dim intIndex As Integer = aryDataItem.IndexOf(strFieldName.ToUpper)
    '        For Each DCF As DataControlField In dgList.Columns
    '            If DCF.SortExpression.ToUpper = strFieldName Then
    '                DCF.Visible = blnVisible
    '                Exit For
    '            End If
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeDgListColumnVisibility : " & ex.ToString)
    '    Finally

    '    End Try
    'End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()

    End Sub
#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_SalesEnqList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "INV_NO"
                strFieldName = "Inv No."
            Case "SALES_QTY"
                strFieldName = "QTY"
            Case "FOC_QTY"
                strFieldName = "FOC"
            Case "SALES"
                strFieldName = "Sales Value"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            'Select Case strColumnName.ToUpper
            '    'Case "MTD_SALES"
            '    '    FCT = FieldColumntype.HyperlinkColumn
            '    Case "SALES_AREA_CODE", "SALESREP_NAME" ', "CUST_NAME", "SHIPTO_CODE"
            '        FCT = FieldColumntype.InvisibleColumn
            '    Case "CUST_CODE", "CUST_NAME"
            '        'if blnshiptoexist=true, show prd, hide cust
            '        FCT = IIf(blnShiptoExist = True, FieldColumntype.InvisibleColumn, FieldColumntype.BoundColumn)
            '    Case "PRD_CODE", "PRD_NAME"
            '        FCT = IIf(blnShiptoExist = False, FieldColumntype.InvisibleColumn, FieldColumntype.BoundColumn)
            '    Case Else
            '        FCT = FieldColumntype.BoundColumn
            'End Select

            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "SALES"
                    strFormatString = "{0:#,0.00}"
                Case "FOC_QTY", "SALES_QTY"
                    strFormatString = "{0:#,0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    'Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
    '    Dim CS As New ColumnStyle
    '    Try
    '        With CS
    '            Dim strColumnName As String = ColumnName.ToUpper
    '            .FormatString = GetOutputFormatString(ColumnName)

    '            Select Case strColumnName
    '                Case "DATE", "SALES_AREA_CODE", "SALESREP_CODE", "CUST_CODE", "SHIPTO_CODE", "PRD_CODE", "INV_NO"
    '                    .HorizontalAlign = HorizontalAlign.Center
    '                Case Else
    '                    If Not String.IsNullOrEmpty(.FormatString) Then
    '                        .HorizontalAlign = HorizontalAlign.Right
    '                    Else
    '                        .HorizontalAlign = HorizontalAlign.Left
    '                    End If
    '            End Select
    '        End With

    '    Catch ex As Exception

    '    End Try
    '    Return CS
    'End Function
    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
