'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	01/10/2007
'	Purpose	    :	SAP List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class iFFMA_SAP_SAPList_V4
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strDocType, strSearchType, strSearchValue, strSearchValue1 As String

        Try
            If Session("UserID") = "" Then
                Dim strScript As String = ""
                strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
            End If

            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "SAP List v4"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request
                strDocType = IIf(Trim(Request.QueryString("doc_type")) <> "", Trim(Request.QueryString("doc_type")), "SO")
                strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "LASTUPDATE")
                strSearchValue = Trim(Request.QueryString("search_value"))
                strSearchValue1 = Trim(Request.QueryString("search_value_1"))

                'Prepare Recordset,Createobject as needed
                ddlDocType.SelectedValue = strDocType
                ddlSearchType.SelectedValue = strSearchType
                ddlSearchType_onChanged()

                GetTeam()
                BindGrid(ViewState("SortCol"))
                If Trim(strSearchType) = "LASTUPDATE" Or Trim(strSearchType) = "txn_timestamp" Then
                    Wuc_txtDate1.Text = IIf(strSearchValue <> "", strSearchValue, Now.ToString("yyyy-MM-dd"))
                    Wuc_txtDate2.Text = IIf(strSearchValue1 <> "", strSearchValue1, Now.ToString("yyyy-MM-dd"))
                ElseIf Trim(strSearchType) = "STATUS" Then
                    ddlList.SelectedValue = IIf(strSearchValue <> "", strSearchValue, "")
                Else
                    txtSearchValue.Text = IIf(strSearchValue <> "", strSearchValue, "")
                    txtSearchValue1.Text = IIf(strSearchValue1 <> "", strSearchValue1, "")
                End If

            End If

        Catch ex As Exception
            ExceptionMsg("SAPList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub GetTeam()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            ddlTeam.Items.Clear()
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.user_code = Trim(Session("UserCode"))
                dt = .GetSAPTeamList
            End With
            If dt.Rows.Count > 0 Then
                ddlTeam.Items.Add(New ListItem("All", "ALL"))
                For Each drRow In dt.Rows
                    ddlTeam.Items.Add(New ListItem(drRow("team").ToString.Trim, drRow("team").ToString.Trim))
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("SAPList")) Then
                objSAPQuery = New mst_SAP.clsSAPQuery
                With objSAPQuery
                    .clsProperties.doc_type = Trim(ddlDocType.SelectedValue)
                    .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                    .clsProperties.team_code = Trim(ddlTeam.SelectedValue)
                    .clsProperties.user_code = Trim(Session("UserCode"))
                    Select Case ddlSearchType.SelectedValue
                        Case "LASTUPDATE", "txn_timestamp"
                            .clsProperties.search_value = Trim(Replace(Wuc_txtDate1.Text, "*", "%"))
                            .clsProperties.search_value_1 = Trim(Replace(Wuc_txtDate2.Text, "*", "%"))
                        Case "STATUS", "txn_status" '"StatusID"   'HL:20070622
                            .clsProperties.search_value = Trim(ddlList.SelectedValue)
                        Case Else
                            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                    End Select
                    dt = .GetSAPList
                End With
                ViewState("SAPList") = dt
            Else
                dt = ViewState("SAPList")
            End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

            'For Each COL As DataControlField In dgList.Columns
            '    Select Case COL.HeaderText.Trim.ToUpper
            '        Case "DELETE"
            '            COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Delete)
            '        Case "EDIT"
            '            COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)
            '    End Select

            'Next

        Catch ex As Exception
            ExceptionMsg("SAPList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objSAPQuery = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"

    Protected Sub dgList_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgList.RowCancelingEdit
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try
            If Trim(dgList.DataKeys(e.RowIndex).Value) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(dgList.Rows(e.RowIndex).Cells(5).Text) <> "P" And Trim(dgList.Rows(e.RowIndex).Cells(6).Text) <> "E" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to cancel.');", True)
                Exit Sub
            ElseIf Replace(Trim(dgList.Rows(e.RowIndex).Cells(6).Text), "&nbsp;", "") <> "" And Trim(dgList.Rows(e.RowIndex).Cells(6).Text) <> "E" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be cancelled.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(ddlDocType.SelectedValue)
                    .clsProperties.txn_no = Trim(dgList.DataKeys(e.RowIndex).Value)
                    .Cancel()
                End With

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(dgList.DataKeys(e.RowIndex).Value) & " has cancelled.');", True)
            End If

            'If Trim(ddlSearchType.SelectedValue.ToString) = "LASTUPDATE" Then
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text), False)
            'ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "STATUS" Then
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
            'Else
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
            'End If

            intCurrentPage = wuc_dgpaging.CurrentPageIndex
            dgList.PageIndex = 0

            BindGrid(ViewState("SortCol"))

            If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            Else
                wuc_dgpaging.CurrentPageIndex = intCurrentPage
            End If
            wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            BindGrid(ViewState("SortCol"), 1)
        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowCancelingEdit : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try
            Dim row As GridViewRow
            Dim index As Integer
            Dim strTxn_no As String
            Dim strTxn_Status As String
            Dim strSavingNo As String 'Modified by Soo Fong - Filtered by Saving No

            Dim strDocType As String = Trim(ddlDocType.SelectedValue.ToString)

            Select Case e.CommandName
                Case "Cancel"

                Case "Details"
                    strTxn_no = dgList.DataKeys(e.CommandArgument)("txn_no")
                    strTxn_Status = Trim(dgList.Rows(e.CommandArgument).Cells(5).Text)
                    strSavingNo = TryCast(dgList.Rows(e.CommandArgument).Cells(9).Controls(0), LinkButton).Text  'Modified by Soo Fong - Filtered by Saving No

                    If (strDocType = "SO") Then
                        If Trim(ddlSearchType.SelectedValue.ToString) = "LASTUPDATE" Or Trim(ddlSearchType.SelectedValue.ToString) = "txn_timestamp" Then
                            Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                        ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "STATUS" Then
                            Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                        Else
                            Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                        End If

                    Else
                        If (strTxn_Status = "P" Or strTxn_Status = "B") Then
                            If Trim(ddlSearchType.SelectedValue.ToString) = "LASTUPDATE" Or Trim(ddlSearchType.SelectedValue.ToString) = "txn_timestamp" Then
                                Response.Redirect("SAPDtlv4.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text) & "&type=2", False)
                            ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "STATUS" Then
                                Response.Redirect("SAPDtlv4.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)
                            Else
                                Response.Redirect("SAPDtlv4.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)
                            End If
                        Else
                            If Trim(ddlSearchType.SelectedValue.ToString) = "LASTUPDATE" Or Trim(ddlSearchType.SelectedValue.ToString) = "txn_timestamp" Then
                                Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                            ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "STATUS" Then
                                Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                            Else
                                Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2&so_no=" & Trim(strSavingNo), False)
                            End If
                        End If
                    End If


                Case "UnBlock", "UnBlockSAP"
                    row = DirectCast(DirectCast(e.CommandSource, ImageButton).NamingContainer, GridViewRow)
                    index = row.RowIndex
                    strTxn_no = Trim(e.CommandArgument)
                    strTxn_Status = Trim(dgList.Rows(index).Cells(5).Text)

                    If strTxn_no = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                        Exit Sub
                    Else
                        objSAP = New mst_SAP.clsSAP
                        With objSAP
                            .clsProperties.user_id = Session("UserID")
                            .clsProperties.user_name = Session("UserName")
                            .clsProperties.doc_type = Trim(ddlDocType.SelectedValue)
                            .clsProperties.txn_no = strTxn_no
                            If e.CommandName = "UnBlockSAP" Then
                                .UnBlockSAP()
                            Else
                                .UnBlock()
                            End If
                        End With

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn No. " & strTxn_no & " has unblocked.');", True)
                    End If

                    intCurrentPage = wuc_dgpaging.CurrentPageIndex
                    dgList.PageIndex = 0

                    BindGrid(ViewState("SortCol"))

                    If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                        wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
                    Else
                        wuc_dgpaging.CurrentPageIndex = intCurrentPage
                    End If
                    wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
                    dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

                    BindGrid(ViewState("SortCol"), 1)
            End Select

            'End If

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowCommand : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim lblStatus As Label
        Dim imgCancel, imgSubmit, imgUnBlock, imgTradeDeal As ImageButton

        Try

            If ddlDocType.SelectedValue = "TRA" Then
                dgList.Columns(7).Visible = True
                dgList.Columns(8).Visible = True
            Else
                dgList.Columns(7).Visible = False
                dgList.Columns(8).Visible = False
            End If
            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)

                'Status
                lblStatus = New Label
                imgSubmit = New ImageButton
                imgCancel = New ImageButton
                'lblStatus = CType(e.Row.FindControl("lblStatus"), Label)
                imgSubmit = CType(e.Row.FindControl("imgResubmit"), ImageButton)
                imgCancel = CType(e.Row.FindControl("imgCancel"), ImageButton)
                imgUnBlock = CType(e.Row.FindControl("imgUnBlock"), ImageButton)
                imgTradeDeal = CType(e.Row.FindControl("imgTradeDeal"), ImageButton)

                'imgSubmit = CType(e.Row.Cells(0).Controls(0), ImageButton)
                'imgEdit = CType(e.Row.Cells(10).Controls(0), ImageButton)

                ' confirm_delete() is a javascript sub placed in html
                'imgSubmit.Attributes.Add("onclick", "javascript:return confirm('Do you want to resubmit?')")

                imgUnBlock.Visible = False
                If IsDBNull(drv("STATUS")) Then
                    'lblStatus.Text = ""
                    imgSubmit.Visible = False

                    If IsDBNull(drv("txn_status")) Then
                        imgCancel.Visible = False
                    Else
                        If drv("txn_status") = "P" And IsDBNull(drv("note")) Then
                            imgCancel.Visible = True
                        ElseIf drv("txn_status") = "P" And IIf(IsDBNull(drv("note")), "", drv("note")) = "Trade Deal Issue" Then
                            imgTradeDeal.Visible = True
                            imgCancel.Visible = True
                        ElseIf drv("txn_status") = "B" Then
                            imgUnBlock.Visible = True

                        Else
                            imgCancel.Visible = False
                        End If
                    End If
                Else
                    If drv("STATUS") = "E" And drv("txn_status") <> "C" Then
                        imgSubmit.Visible = True
                        imgCancel.Visible = True

                    ElseIf drv("STATUS") = "B" Then
                        Dim lnkUnblock As LinkButton = CType(e.Row.FindControl("lnkUnBlock_SAP"), LinkButton)
                        lnkUnblock.Visible = (lnkUnblock IsNot Nothing)
                    End If

                End If

                'imgSubmit.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try
            If Trim(dgList.DataKeys(e.RowIndex).Value) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(dgList.Rows(e.RowIndex).Cells(6).Text) <> "E" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to resubmit.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(ddlDocType.SelectedValue)
                    .clsProperties.txn_no = Trim(dgList.DataKeys(e.RowIndex).Value)
                    .Resubmit()
                End With

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn No. " & Trim(dgList.DataKeys(e.RowIndex).Value) & " has resubmitted.');", True)
            End If

            'If Trim(ddlSearchType.SelectedValue.ToString) = "LASTUPDATE" Then
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text), False)
            'ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "STATUS" Then
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
            'Else
            '    Response.Redirect("SAPList.aspx?txn_no=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
            'End If

            intCurrentPage = wuc_dgpaging.CurrentPageIndex
            dgList.PageIndex = 0

            BindGrid(ViewState("SortCol"))

            If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            Else
                wuc_dgpaging.CurrentPageIndex = intCurrentPage
            End If
            wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowDeleting : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try
            If Trim(dgList.DataKeys(e.NewEditIndex).Value) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(dgList.Rows(e.NewEditIndex).Cells(27).Text) <> "Trade Deal Issue" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to resubmit trade deal.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.txn_no = Trim(dgList.DataKeys(e.NewEditIndex).Value)
                    .ResubmitTradeDeal()
                End With

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn No. " & Trim(dgList.DataKeys(e.NewEditIndex).Value) & " has resubmitted. ');", True)
            End If



            intCurrentPage = wuc_dgpaging.CurrentPageIndex
            dgList.PageIndex = 0

            BindGrid(ViewState("SortCol"))

            If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            Else
                wuc_dgpaging.CurrentPageIndex = intCurrentPage
            End If
            wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowEditing : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub
#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
            UpdateDatagrid.Update()

        Catch ex As Exception
            lblErr.Text = "SAPList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            ddlSearchType_onChanged()
            'UpdateSearch.Update()
            'BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "SAPList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlDocType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Protected Sub ddlDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocType.SelectedIndexChanged
    '    Try
    '        BindGrid(ViewState("SortCol"))

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.ddlDocType_SelectedIndexChanged : " + ex.ToString()
    '    Finally

    '    End Try
    'End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlTeam_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged
    '    Try
    '        BindGrid(ViewState("SortCol"))

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.ddlTeam_SelectedIndexChanged : " + ex.ToString()
    '    Finally

    '    End Try
    'End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            'txtSearchValue.Text = ""
            'txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"
            Wuc_txtDate1.Text = ""
            Wuc_txtDate2.Text = ""

            lblSearchValue.Visible = False
            'lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            Wuc_txtDate1.Visible = False
            Wuc_txtDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "all"
                    btnSearch.Visible = True

                Case "LASTUPDATE", "txn_timestamp"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    lblTo.Visible = True
                    'txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    'txtSearchValue.ReadOnly = True
                    'txtSearchValue1.Visible = True
                    'txtSearchValue1.ReadOnly = True
                    'txtSearchValue1.Text = ""
                    btnSearch.Visible = True
                    Wuc_txtDate1.Visible = True
                    Wuc_txtDate2.Visible = True

                Case "STATUS"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""   'HL:20070622

                    'Get Status Record
                    ddlList.Items.Clear()
                    Dim objSAPQuery As mst_SAP.clsSAPQuery
                    objSAPQuery = New mst_SAP.clsSAPQuery
                    With objSAPQuery
                        dt = .GetSAPStatusList
                    End With
                    objSAPQuery = Nothing
                    If dt.Rows.Count > 0 Then
                        ddlList.Items.Add(New ListItem("Select", 0))
                        For Each drRow In dt.Rows
                            ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
                        Next
                    End If

                Case "txn_status"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""   'HL:20070622

                    'Get Status Record
                    ddlList.Items.Clear()
                    Dim objSAPQuery As mst_SAP.clsSAPQuery
                    objSAPQuery = New mst_SAP.clsSAPQuery
                    With objSAPQuery
                        dt = .GetSAPTxnStatusList
                    End With
                    objSAPQuery = Nothing
                    If dt.Rows.Count > 0 Then
                        ddlList.Items.Add(New ListItem("Select", 0))
                        For Each drRow In dt.Rows
                            ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
                        Next
                    End If

                Case Else
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "SAPList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgList_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgList.RowUpdating

    End Sub
End Class
