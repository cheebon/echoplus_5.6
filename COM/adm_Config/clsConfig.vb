﻿'************************************************************************
'	Author	    :	Cheong Boo Lim
'	Date	    :	6/6/2011
'	Purpose	    :	Class Configuration for device
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data

Public Class clsConfig
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsConfig"
        End Get
    End Property

    Public Function GetCategoryList(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_LIST_CATEGORY"
                .addItem("USER_ID", strUserID, 2)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.GetCategoryList:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetConfiguration(ByVal strType As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_LIST_CONFIGURATION"
                .addItem("TYPE", strType, 2)
                .addItem("USER_ID", strUserID, 2)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.GetConfiguration:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetConfigurationDtl(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strType As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_DTL_LIST"
                .addItem("TEAM_CODE", strTeamCode, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("TYPE", strType, 2)
                .addItem("USER_ID", strUserID, 2)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.GetConfigurationDtl:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetConfigurationDtlTeamSR(ByVal strSettingCode As String, ByVal strDtlType As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_SETTING_DTL_LIST"
                .addItem("SETTING_CODE", strSettingCode, 2)
                .addItem("DTL_TYPE", strDtlType, 2)
                .addItem("USER_ID", strUserID, 2)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.GetConfigurationDtlTeamSR:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#Region "Save"
    Public Sub SaveSetting(ByVal strSettingCode As String, ByVal strValue As String, ByVal strValue2 As String, _
                           ByVal strValue3 As String, ByVal strValue4 As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_SAVE_SETTING"
                .addItem("SETTING_CODE", strSettingCode, 2)
                .addItem("VALUE", strValue, 2)
                .addItem("VALUE2", strValue2, 2)
                .addItem("VALUE3", strValue3, 2)
                .addItem("VALUE4", strValue4, 2)
                .addItem("USER_ID", strUserID, 2)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.SaveSetting:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub SaveSettingDtl(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strSettingCode As String, ByVal strValue As String, ByVal strValue2 As String, _
                           ByVal strValue3 As String, ByVal strValue4 As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_SAVE_SETTING_DTL"
                .addItem("TEAM_CODE", strTeamCode, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("SETTING_CODE", strSettingCode, 2)
                .addItem("VALUE", strValue, 2)
                .addItem("VALUE2", strValue2, 2)
                .addItem("VALUE3", strValue3, 2)
                .addItem("VALUE4", strValue4, 2)
                .addItem("USER_ID", strUserID, 2)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.SaveSettingDtl:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region
#Region "Job"
    Public Sub RunJob(ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_UPD_TBL_AND_SETUP_JOB"
                .addItem("USER_ID", strUserID, 2)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.RubJob:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region


#Region "Delete"
    Public Sub DeleteSettingDtl(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_CONFIG_DEVICE_DTL_DELETE"
                .addItem("TEAM_CODE", strTeamCode, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("USER_ID", strUserID, 2)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Config.clsConfig.DeleteSettingDtl:" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
