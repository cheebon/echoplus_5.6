<%@ Page Language="VB" AutoEventWireup="false" CodeFile="main.aspx.vb" Inherits="iFFMR_SFA_main" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SFA Main</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
<script type="text/javascript" language="javascript">
function resetPnlPos(){fraLeft.frameElement.style.height=Math.max((getViewportSize()[1]),0)+'px';}
function resetPnlPos2(){
//fraContent.frameElement.style.height=Math.max((getViewportSize()[1])-10,0)+'px';
}
function getViewportSize() { var size = [0, 0]; if (typeof window.innerWidth != 'undefined') { size = [ window.innerWidth, window.innerHeight ]; } else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) { size = [ document.documentElement.clientWidth, document.documentElement.clientHeight ]; } else { size = [ document.getElementsByTagName('body')[0].clientWidth, document.getElementsByTagName('body')[0].clientHeight ]; } return size; }

var maxW='320px'; var minW='20px';
function CollapseExpand(){var currentW=fraLeft.frameElement.style.width;fraLeft.frameElement.style.width=(currentW>minW)?minW:maxW;resetPnlPos();}

function NavigatePage(URL){fraContent.frameElement.src=URL;}
</script>
</head>
<body>
<form id="frmMain" runat="server">
        <iframe id="fraLeft" name="fraLeft" frameborder="0" marginwidth="0" marginheight="0" src="left.aspx" onload="setTimeout('resetPnlPos()',1000);"
            width="20px" height="500px" scrolling="no" style="border: 0; position: absolute; top: 0px; padding:0px 0px 0px 0px; z-index:3"></iframe>
        <iframe id="fraContent" name="fraContent" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMR/SFA/RPT/ATChart.aspx"
            width="97%" height="98%" scrolling="no" style="border: 0; position:absolute; top:0px; left:18px; z-index:2; padding: 5px 5px 5px 5px;"></iframe>
</form></body>
</html>
