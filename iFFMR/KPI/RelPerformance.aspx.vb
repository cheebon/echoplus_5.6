Imports System.Data


Partial Class RelPerformance
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_RelPerformance"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try

            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.RELPERFORM) '"Relative Performance"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.RELPERFORM
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                If PreviousPage IsNot Nothing Then
                    With CriteriaCollector
                        Session.Item("Year") = .Year
                        Session.Item("Month") = .Month
                        session("PRINCIPAL_ID") = .PrincipalID
                        Session("PRINCIPAL_CODE") = .PrincipalCode
                        Session("TREE_PATH") = .Tree_Path
                        ViewState("strSortExpression") = .SortExpression
                        wuc_Menu.RestoreSessionState = True
                    End With
                Else
                    CriteriaCollector = Nothing ' New clsSharedValue
                    'SetButtonVisibility(ViewMode.ViewByFigure)
                End If
                'wuc_ctrlpanel.pnlField_InsertPointer(True, 0, "Team")
                TimerControl1.Enabled = True
            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "RelPerformance.aspx"
        End Get
    End Property

#Region "Event Handler"


    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            'wuc_ctrlpanel.RefreshDetails()
            'wuc_ctrlpanel.UpdateControlPanel()
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "CallCoverage1")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    'Private Sub PrepareGridViewForExport(ByRef gvList As GridView)
    '    Dim lb As LinkButton = New LinkButton
    '    Dim l As Literal = New Literal
    '    Dim name As String = String.Empty
    '    Dim i As Integer = 0
    '    While i < gvList.Controls.Count
    '        If gvList.Controls(i).GetType Is GetType(LinkButton) Then
    '            l.Text = (CType(gvList.Controls(i), LinkButton)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(DropDownList) Then
    '            l.Text = (CType(gvList.Controls(i), DropDownList)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(CheckBox) Then
    '            l.Text = (CType(gvList.Controls(i), CheckBox)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        End If

    '        'If gvList.Controls(i).HasControls Then
    '        '    PrepareGridViewForExport(gvList.Controls(i))
    '        'End If
    '        System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End While
    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 18 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"
    'Private Sub CustomHeaderRender(ByVal HTMLWriter As HtmlTextWriter, ByVal ctl As Control)
    '    'intGrpItemCount
    '    'FIXED_ITEM_COUNT
    '    Dim iMTD, iQTD, iYTD, iPYTD As Integer

    '    For iIndex As Integer = intGrpItemCount To intTotalColumnCount - 1
    '        Try
    '            If aryDataItem(iIndex) Like "MTD*" Then
    '                iMTD += 1
    '            ElseIf aryDataItem(iIndex) Like "QTD*" Then
    '                iQTD += 1
    '            ElseIf aryDataItem(iIndex) Like "YTD*" Then
    '                If aryDataItem(iIndex) Like "YTDGROSS" Then
    '                    iPYTD += 1
    '                Else
    '                    iYTD += 1
    '                End If
    '            ElseIf aryDataItem(iIndex) Like "PYTD*" Then
    '                iPYTD += 1
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    '<th  valign='top' scope='col' >
    '    HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & intGrpItemCount & "'>&nbsp;</th>")
    '    If iMTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iMTD & "' align='center'>MTD Sales</th>")
    '    If iQTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iQTD & "' align='center'>QTD Sales</th>")
    '    If iYTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iYTD & "' align='center'>YTD Sales</th>")
    '    If iPYTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iPYTD & "' align='center'>PYTD Sales</th>")
    '    HTMLWriter.Write("</TR>") 'Close the row




    '    'HTMLWriter.Write("<TD border='0' colspan='" & intGrpItemCount & "'></TD>")
    '    'If iMTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iMTD & "' align='center'>MTD Sales</TD>")
    '    'If iQTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iQTD & "' align='center'>QTD Sales</TD>")
    '    'If iYTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iYTD & "' align='center'>YTD Sales</TD>")
    '    'If iPYTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iPYTD & "' align='center'>PYTD Sales</TD>")
    '    'HTMLWriter.Write("</TR>") 'Close the row

    '    dgList.HeaderStyle.AddAttributesToRender(HTMLWriter)
    '    HTMLWriter.RenderBeginTag("TR") 'Reopen for the next row, don't need to close as the renderer will close it

    '    Dim i As Integer
    '    For i = 0 To ctl.Controls.Count - 1
    '        ctl.Controls(i).RenderControl(HTMLWriter)
    '    Next i

    'End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_RELPERFORM.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.TemplateColumn_Percentage
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_RELPERFORM.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataFormatString = CF_RELPERFORM.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_RELPERFORM.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_RELPERFORM.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    e.Row.Cells(0).Text = "Average"
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_RELPERFORM.GetOutputFormatString(li.Text), IIf(intRowCount > 0 AndAlso IsNumeric(li.Value), CDbl(CDbl(li.Value) / CDbl(intRowCount)), 0))
                        End If
                    Next
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim intNetValue As Integer
            Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .GroupField = ""
                .Year = strYear
                .Month = strMonth
                .PrincipalID = strPrincipalId
                .PrincipalCode = strPrincipalCode
                .Tree_Path = Session("TREE_PATH")
                .NetValue = intNetValue
            End With

            Dim clsCallDB As New rpt_KPI.clsRelPerform
            'DT = clsCallDB.GetRelativePerformance(strUserID, strYear, strMonth, strTeamCode, strSalesrepCode)
            DT = clsCallDB.GetRelativePerformance(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList, intNetValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_CustomerHeader()
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            'TEAM, SALESMAN, SMNAME, 
            'MTD_TARGET_*
            'MTD_PI_*
            'YTD_PI_*
            'FTE_*
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                'If strColumnName Like "TEAM_CODE" Then
                '    strColumnName = "Sales Team"
                'ElseIf strColumnName Like "REGION_*" Then
                '    strColumnName = "Region"
                'ElseIf strColumnName Like "SALESREP_*" Then
                '    strColumnName = "Sales Rep."
                If strColumnName Like "TEAM_*" OrElse strColumnName Like "REGION_*" OrElse strColumnName Like "SALESREP_*" Then

                ElseIf strColumnName Like "MTD_SALES" OrElse strColumnName Like "MTD_SALES_TGT" OrElse strColumnName Like "MTD_TGT_SALES_ACH" Then
                    strColumnName = "MTD Sales"
                ElseIf strColumnName Like "MTD_CALL" OrElse strColumnName Like "MTD_CALL_TGT" OrElse strColumnName Like "MTD_TGT_CALL_ACH" Then
                    'strColumnName = "MTD Activity"
                    strColumnName = "MTD Call"
                    'ElseIf strColumnName Like "MTD_TGT_*" Then
                    '    strColumnName = "MTD Target Achievement"
                ElseIf strColumnName Like "MTD_PRFM_IDX_*" Then
                    strColumnName = "MTD Performance Index"
                ElseIf strColumnName Like "YTD_PRFM_IDX_*" Then
                    strColumnName = "YTD Performance Index"
                ElseIf strColumnName Like "FTE_*" Then
                    strColumnName = "FTE"
                Else
                    strColumnName = "UKNOWN"
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper

                If (strColumnName Like "MTD_TGT_*" OrElse _
                 strColumnName Like "MTD_SALES*" OrElse _
                 strColumnName Like "MTD_CALL*" OrElse _
                 strColumnName Like "MTD_PRFM_IDX_*" OrElse _
                 strColumnName Like "YTD_PRFM_IDX_*" OrElse strColumnName Like "FTE_*") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class


Public Class CF_RELPERFORM
    Public Shared Function FilterPrefixName(ByVal columnName As String) As String
        Dim strName As String = ""
        Try
            If Not String.IsNullOrEmpty(columnName) Then
                'strName = columnName.ToUpper.Replace("MTD_TGT_", "").Replace("_ACH", "").Replace("MTD_PRFM_IDX_", "").Replace("YTD_PRFM_IDX_", "").Replace("FTE_", "")
                strName = columnName.ToUpper.Replace("MTD_TGT_", "").Replace("MTD_PRFM_IDX_", "").Replace("YTD_PRFM_IDX_", "").Replace("FTE_", "").Replace("MTD_", "")
            End If
        Catch ex As Exception
        End Try
        Return strName
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)
        Select Case strNewName
            Case "SALES"
                strFieldName = "Sales"
            Case "CALL", "ACTY"
                strFieldName = "Call"
            Case "SALES_TGT", "CALL_TGT"
                strFieldName = "Target"
            Case "SALES_ACH"
                strFieldName = "Sales %"
            Case "CALL_ACH"
                strFieldName = "Call %"
            Case "GROWTH"
                strFieldName = "Growth"
            Case "TTL"
                strFieldName = "Total"
            Case "ACT"
                strFieldName = "Call"
            Case "ACT_HALF"
                strFieldName = "Call"
            Case "TGT"
                strFieldName = "Target"
            Case "ACH_SALES"
                strFieldName = "Sales %"
            Case "ACH_CALL"
                strFieldName = "Call %"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If strColumnName = "REGION_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)
        Try
            Select Case strNewName
                Case "ACT", "TGT", "SALES_TGT", "CALL_TGT", "CALL"
                    strStringFormat = "{0:#,0.#}"
                Case "SALES", "ACTY", "GROWTH", "TTL"
                    strStringFormat = "{0:#,0.00}"
                Case "ACT_HALF"
                    strStringFormat = "{0:0.0}"
                Case Else
                    strStringFormat = ""
            End Select
            If strColumnName Like "*SALES_ACH" OrElse strColumnName Like "*CALL_ACH" Then
                strStringFormat = "{0:0.0}"
            End If

        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class









