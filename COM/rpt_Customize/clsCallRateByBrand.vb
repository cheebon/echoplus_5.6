﻿Option Explicit On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Imports System.Web.HttpContext

Public Class clsCallRateByBrand
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallRateByBrand"
        End Get
    End Property

#Region "ListBox"


    Public Function GetSalesTeam(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_TEAM"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetSalesrep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesrep :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetDistrict(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_DISTRICT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDistrict :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetChannel(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_CHANNEL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetChannel :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetCustClass(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_CUST_CLASS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustClass :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetPrdClass(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_PRD_CLASS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdClass :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetSpecialty(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_SPECIALTY"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSpecialty :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function

    Public Function GetCriteria(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_CRITERIA"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSpecialty :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function
#End Region

    Public Function GetCallRateByBrand(ByVal strYear As String, ByVal strMonth As String, ByVal strTeamList As String, _
    ByVal strSalesrepList As String, ByVal strDistrictList As String, ByVal strChannelList As String, ByVal strCustClassList As String, ByVal strSpecialty As String, _
    ByVal strPrdClassList As String, ByVal strGrouping As String, ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCallRateByBrandDB As clsDB
        Try
            clsCallRateByBrandDB = New clsDB
            With clsCallRateByBrandDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_RATE_BRAND_BY_MONTH"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TEAM_LIST", strTeamList, clsDB.DataType.DBString)
                .addItem("SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString)
                .addItem("DISTRICT_LIST", strDistrictList, clsDB.DataType.DBString)
                .addItem("CHANNEL_LIST", strChannelList, clsDB.DataType.DBString)
                .addItem("CUST_CLASS_LIST", strCustClassList, clsDB.DataType.DBString)
                .addItem("SPECIALTY", strSpecialty, clsDB.DataType.DBString)
                .addItem("PRD_CLASS_LIST", strPrdClassList, clsDB.DataType.DBString)
                .addItem("GROUPING", strGrouping, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsCallRateByBrandDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class

