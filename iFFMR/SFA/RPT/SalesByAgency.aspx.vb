Imports System.Data

Partial Class iFFMR_SFA_RPT_SalesByAgency
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Dim licCustomFooterColl As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblErr.Text = ""
        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True

            wuc_ctrlPnl.SubModuleID = SubModuleType.SFA_SALESBYAGENCY
            wuc_ctrlPnl.DataBind()
        End If
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "SFA_RPT_SalesByAgency"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click, wuc_ctrlPnl.SearchBtn_Click
        RefreshDataBind()
        wuc_ctrlPnl.UpdateControlPanel()
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPnl.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlPnl.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)

        dtCurrentTable = GetRecList()
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_IN.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_IN.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_IN.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub

    '''' <summary>
    '''' Pass in the datatable, and generate the url format string based on the
    '''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    '''' to form the hyperlink fields need.
    '''' </summary>
    '''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    '''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    '''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    '''' <returns>URLFormatString, as url to pass with parameters</returns>
    '''' <remarks></remarks>
    'Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
    '    Dim strUrlFormatString As String = String.Empty
    '    Select Case intIndex
    '        Case 0 'Generate sales info by date links
    '            Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
    '            Dim arrFields As New ArrayList 'store field name used in grouping
    '            Dim licGroupFieldList As New ListItemCollection
    '            licGroupFieldList.Add(New ListItem("TEAM_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("REGION_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHAIN_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHANNEL_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALES_AREA_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SHIPTO_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

    '            For Each liFieldToFind As ListItem In licGroupFieldList
    '                For Each DC As DataColumn In dtToBind.Columns
    '                    If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
    '                        liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    End If
    '                Next
    '            Next

    '            ReDim strUrlFields(arrFields.Count - 1)
    '            For intIndx As Integer = 0 To arrFields.Count - 1
    '                strUrlFields(intIndx) = arrFields.Item(intIndx)
    '            Next

    '            Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
    '            Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value
    '            Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value
    '            strUrlFormatString = "SalesInfoByDate.aspx?" + _
    '            strbFilterGroup.ToString + _
    '           IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
    '           IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
    '           IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "")

    '            Session("SalesInfoByDate_GroupingField") = arrFields
    '        Case Else
    '            strUrlFields = Nothing
    '            strUrlFormatString = ""
    '    End Select
    '    Return strUrlFormatString
    'End Function

    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '    If e.Row.RowType = DataControlRowType.Header Then
    '        Dim oGridView As GridView = dgList 'CType(sender, GridView)
    '        Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
    '        Dim TC As TableHeaderCell
    '        Dim CF As ListItem
    '        Dim intCounter As Integer = 0
    '        Dim intActualIndex As Integer = 0

    '        For Each CF In licHeaderCollector
    '            If CF.Value = 1 Then
    '                Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
    '                intActualIndex = IIf(intCounter > 0, intCounter, 0)
    '                If iIndex >= 0 Then
    '                    e.Row.Cells(intActualIndex).RowSpan = 2
    '                    e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
    '                    GVR.Cells.Add(e.Row.Cells(intActualIndex))
    '                End If
    '            Else
    '                TC = New TableHeaderCell
    '                TC.Text = CF.Text
    '                TC.ColumnSpan = CF.Value
    '                intCounter += CF.Value
    '                GVR.Cells.Add(TC)
    '            End If
    '        Next
    '        oGridView.Controls(0).Controls.AddAt(0, GVR)
    '    End If
    'End Sub
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                Dim iIndex As Integer
                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_IN.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next

                If (aryDataItem.IndexOf("M1_ACH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("M1_ACH")).Text = String.Format(CF_IN.GetOutputFormatString("M1_ACH"), (DIVISION(licItemFigureCollector.FindByText("M1_SALES_AMT"), licItemFigureCollector.FindByText("M1_TGT_AMT")) * 100))
                If (aryDataItem.IndexOf("M2_ACH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("M2_ACH")).Text = String.Format(CF_IN.GetOutputFormatString("M2_ACH"), (DIVISION(licItemFigureCollector.FindByText("M2_SALES_AMT"), licItemFigureCollector.FindByText("M2_TGT_AMT")) * 100))
                If (aryDataItem.IndexOf("M3_ACH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("M3_ACH")).Text = String.Format(CF_IN.GetOutputFormatString("M3_ACH"), (DIVISION(licItemFigureCollector.FindByText("M3_SALES_AMT"), licItemFigureCollector.FindByText("M3_TGT_AMT")) * 100))
                If (aryDataItem.IndexOf("TTL_ACH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("TTL_ACH")).Text = String.Format(CF_IN.GetOutputFormatString("TTL_ACH"), (DIVISION(licItemFigureCollector.FindByText("TTL_SALES_AMT"), licItemFigureCollector.FindByText("TTL_TGT_AMT")) * 100))
                e.Row.Cells(0).Text = "Total"
                e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Right


                'Extra Footer Row
                Dim TBL As Table = e.Row.Parent
                Dim TC As TableCell
                Dim row As New GridViewRow(-1, -1, DataControlRowType.Footer, DataControlRowState.Normal)
                For Each strCol As String In aryDataItem
                    TC = New TableCell()
                    row.Cells.Add(TC)
                    If licCustomFooterColl.FindByText(strCol) IsNot Nothing Then
                        TC.Text = String.Format(CF_IN.GetOutputFormatString("_ACH"), licCustomFooterColl.FindByText(strCol).Value)
                        TC.HorizontalAlign = HorizontalAlign.Right
                    End If
                Next
                row.Cells(0).Text = "Meet Target"
                row.Cells(0).HorizontalAlign = HorizontalAlign.Right
                TBL.Rows.Add(row)
        End Select
    End Sub

#Region "Math Function"
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

#End Region

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsRpt As New rpt_SFA.SFA_RPT
            DT = clsRpt.GET_SalesByAgency(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), Session("Year"), Session("QUARTER"), Session("SALESREP_LIST"), IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue"))))
            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        PreRenderMode(DT)
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_CustomerHeader()
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        Dim sMonth() As String = getQuarterMonth(CInt(Session("QUARTER")))
        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "M1_*" Then
                strColumnName = sMonth(0)
            ElseIf strColumnName Like "M2*" Then
                strColumnName = sMonth(1)
            ElseIf strColumnName Like "M3_*" Then
                strColumnName = sMonth(2)
            ElseIf strColumnName Like "TTL_*" Then
                strColumnName = "Total"
            Else
                strColumnName = CF_IN.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub

    Private Function getQuarterMonth(ByVal iQuarter As Integer) As String()
        Dim strQMonth(2) As String

        Select Case iQuarter
            Case 2
                strQMonth = "April,May,June".Split(",")
            Case 3
                strQMonth = "July,August,September".Split(",")
            Case 4
                strQMonth = "Octorber,November,December".Split(",")
            Case Else
                strQMonth = "January,February,March".Split(",")
        End Select
        Return strQMonth
    End Function

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        licCustomFooterColl = New ListItemCollection
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper

            If (strColumnName Like "*_AMT") Then
                licItemFigureCollector.Add(New ListItem(strColumnName, 0))
            ElseIf (strColumnName Like "*_ACH") Then
                licCustomFooterColl.Add(New ListItem(strColumnName, 0))
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next

            For Each li As ListItem In licCustomFooterColl
                li.Value = SUM(li.Value, IIf(ConvertToDouble(DR(li.Text)) < 100, 0, 1))
            Next
        Next
    End Sub
    'licCustomFooterColl
    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Public Class CF_IN
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            ColumnName = ColumnName.ToUpper
            If ColumnName Like "*_SALES_AMT" Then
                strFieldName = "Actual"
            ElseIf ColumnName Like "*_TGT_AMT" Then
                strFieldName = "Target"
            ElseIf ColumnName Like "*_ACH" Then
                strFieldName = "%"
            Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
            End If

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            'Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            'strColumnName = strColumnName.ToUpper
            'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
            '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            '    FCT = FieldColumntype.InvisibleColumn
            'Else
            '    FCT = FieldColumntype.BoundColumn
            'End If

            FCT = FieldColumntype.BoundColumn

            Return FCT
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strStringFormat As String = ""
            If strColumnName.ToUpper Like "*_AMT" Then
                strStringFormat = "{0:#,0}"
            ElseIf strColumnName.ToUpper Like "*_ACH" Then
                strStringFormat = "{0:#,0.0}"
            End If
            'Select Case strColumnName.ToUpper
            '    Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
            '         "MTD_TGT", "QTD_TGT", "YTD_TGT"
            '        strStringFormat = "{0:#,0.00}"
            '    Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
            '        strStringFormat = "{0:#,0.0}"
            '    Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
            '         "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
            '        strStringFormat = "{0:#,0}"
            '    Case Else
            '        strStringFormat = ""
            'End Select

            Return strStringFormat
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                    .Wrap = False
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

            Return CS
        End Function
    End Class
End Class
