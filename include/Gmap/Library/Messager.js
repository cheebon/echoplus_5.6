﻿/***************************************************************
/ This two functions have the following pre-requisite on the 
/ caller page.
/ 1. A gmap by the id 'map_canvas'
/ 2. A div by the id 'Message'
/ 3. The 'Message' div z-index must be set, so that it can be seen. Current value used is 30.
/ 
/ You can customise where you want the message div to appear inside the map
***************************************************************/
var timer;


function ShowMessage(msg)
{   
    var mapOffset = $("#map_canvas").offset();
    if (mapOffset)
    {
        $("#Message").css({top: mapOffset.top ,left: mapOffset.left});
    } 
   
    if(timer)
    {
        $("#Message").text(msg);
        clearTimeout(timer);
        timer = null;
    }
    else
    {
        $("#Message").text(msg).slideDown();  
    }
    
    timer = setTimeout("HideMessage()", 10000);
}

function HideMessage()
{
    $("#Message").slideUp();
    
    if(timer)
    {
        clearTimeout(timer);
        timer = null;
    }
}
//function ShowMessage(msg)
//{
//    
//    $("#Message").offset({top: $("#map_canvas").offset().top - 2 ,left: $("#map_canvas").offset().left - 2});

//    $("#Message").text(msg).slideDown();
//    
//}

//function HideMessage()
//{
//    $("#Message").slideUp("slow");
//}