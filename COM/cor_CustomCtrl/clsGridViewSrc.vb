'/*
' * Copyright � 2005, Ashley van Gerven (ashley.vg@gmail.com)
' * All rights reserved.
' *
' * Use in source and binary forms, with or without modification, is permitted 
' * provided that the above copyright notice and disclaimer below is not removed.
' * 
' * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
' * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
' * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
' * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
' * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
' * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
' * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
' * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
' * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
' * OF SUCH DAMAGE.
' */
Imports System
Imports System.Drawing
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.ComponentModel

'Namespace BobControls

<ToolboxData("<{0}:ScrollingGrid runat=server></{0}:ScrollingGrid>")> _
<ToolboxBitmap(GetType(clsGridViewSrc), "ScrollingGridIcon.bmp")> _
Public Class clsGridViewSrc
    Inherits Panel
    Private grid As GridView = Nothing
    Public Overflow As String = "scroll"
    Public HeaderWidthReduction As Integer = 0
    Public FooterWidthReduction As Integer = 0
    Public ScrollingEnabled As Boolean = True
    Public StartScrollPos As Point = New Point(0, 0)
    Public ScriptPath As String = ""
    Public FirefoxBorderWorkaround As Boolean = True

    Public Sub New()
        Me.Width = Unit.Percentage(100)
        Me.Height = 200
    End Sub

    Protected Overloads Overrides Sub OnInit(ByVal e As EventArgs)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ScrollingGrid_js", "<script language=JavaScript src=" + ScriptPath + "ScrollingGrid.js></script>")
        'Page.RegisterClientScriptBlock("ScrollingGrid_js", "<script language=JavaScript src=" + ScriptPath + "ScrollingGrid.js></script>")
        For Each c As Control In Me.Controls
            If TypeOf c Is GridView Then
                grid = CType(c, GridView)
                ' break 
            End If
        Next
        If Not (grid Is Nothing) AndAlso ScrollingEnabled Then
            If FirefoxBorderWorkaround Then
                grid.GridLines = GridLines.None
                grid.BorderWidth = 0
            End If
            Dim html As StringBuilder = New StringBuilder
            If grid.ShowHeader Then
                Dim divHdrStyle As String = "overflow:hidden;"
                If HeaderWidthReduction > 0 Then
                    divHdrStyle += String.Format("margin-right:{0}px;", HeaderWidthReduction)
                End If
                html.AppendFormat("<div id={0}$divHdr style='{1}'>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "", Me.ClientID, divHdrStyle)
                Dim border As String = "0"
                Dim style As String = " style='border-collapse:collapse;'"
                If Not grid.BorderWidth.IsEmpty Then
                    border = grid.BorderWidth.Value.ToString
                Else
                    If grid.GridLines = GridLines.Both Then
                        border = "1"
                    End If
                End If
                If grid.CellSpacing > 0 Then
                    style = ""
                End If
                Dim borderColor As String = ""
                If Not grid.BorderColor.IsEmpty Then
                    Dim colorValue As String = grid.BorderColor.Name
                    If colorValue.StartsWith("ff") Then
                        colorValue = "#" + colorValue.Substring(2)
                    End If
                    borderColor = " borderColor='" + colorValue + "'"
                End If
                If grid.CellPadding = -1 Then
                    grid.CellPadding = 2
                End If
                Dim cellpadding As String = String.Format("cellpadding='{0}'", grid.CellPadding)
                html.AppendFormat("<table cellpadding=0 cellspacing=0 id={3}$headerCntr><tr><td><table id={3}$tblHdr border='{0}' {1} cellspacing='{2}' {4}{5}>", border, cellpadding, grid.CellSpacing, Me.ClientID, borderColor, style)
                html.Append(" <tr></tr></table></td></tr></table>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "")
                html.Append("</div>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "")
            End If
            html.AppendFormat("<div id={3}$divContent style='height:{1};overflow:{2};' onscroll='updateScroll(this, ""{3}"")'><table cellpadding=0 cellspacing=0 id={3}$contentCntr><tr><td>", Width, Height, Me.Overflow, Me.ClientID)
            Me.Controls.AddAt(0, New LiteralControl(html.ToString))
            Dim appendHtml As String = ""
            If grid.ShowHeader Then
                appendHtml += "</td></tr></table>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & ""
            End If
            appendHtml += "</div>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & ""
            appendHtml += String.Format("<input type=hidden name={0}$hdnScrollPos id={0}$hdnScrollPos>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "", Me.ClientID)
            Me.Controls.Add(New LiteralControl(appendHtml))
            Dim lastRowIsPager As Boolean = False
            If grid.AllowPaging AndAlso grid.Page.Visible AndAlso (grid.PagerSettings.Position = PagerPosition.Bottom OrElse grid.PagerSettings.Position = PagerPosition.TopAndBottom) Then
                lastRowIsPager = True
                Dim tblPagerStyle As String = "width:100%;"
                If FooterWidthReduction > 0 Then
                    tblPagerStyle += String.Format("margin-right:{0}px;", FooterWidthReduction)
                End If
                Me.Controls.Add(New LiteralControl(String.Format("<table id={0}$tblPager cellpadding={1} cellspacing={2} style='{3}'> <tr></tr></table>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "", Me.ClientID, grid.CellPadding, grid.CellSpacing, tblPagerStyle)))
            End If
            If grid.ShowHeader Then
                Me.Controls.Add(New LiteralControl(String.Format("<script language=javascript>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "<!--" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & " setTimeout(""initScrollingGrid('{0}', '{1}', {2})"", 50) " & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "//--></script>", Me.ClientID, grid.ClientID, lastRowIsPager.ToString.ToLower)))
            End If
        End If
        AddHandler Me.Load, AddressOf clsGridViewSrc_Load
        MyBase.OnInit(e)
    End Sub

    Public Overloads Overrides Sub RenderBeginTag(ByVal writer As HtmlTextWriter)
        Dim style As String = ""
        If Not Me.BackColor.IsEmpty Then
            Dim colorValue As String = Me.BackColor.Name
            If colorValue.StartsWith("ff") Then
                colorValue = "#" + colorValue.Substring(2)
            End If
            style = String.Format("background-color:{0};", colorValue)
        End If
        If ScrollingEnabled Then
            style += String.Format("width:{0};", Me.Width)
        End If
        Dim html As String = String.Format("<table id={0} name=ScrollingGrid style='{1} table-layout:fixed' cellpadding=0 cellspacing=0 border=0", Me.ClientID, style)
        If Not (Me.CssClass Is Nothing) AndAlso Me.CssClass.Length > 0 Then
            html += String.Format(" class='{0}'", Me.CssClass)
        End If
        html += "><tr><td>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & ""
        writer.Write(html)
    End Sub

    Public Overloads Overrides Sub RenderEndTag(ByVal writer As HtmlTextWriter)
        writer.Write("</td></tr></table>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "")
    End Sub

    Public Sub SetStartScrollPosFromPostack()
        Dim key As String = Me.ClientID + "$hdnScrollPos"
        If Not (HttpContext.Current.Request.Form(key) Is Nothing) AndAlso HttpContext.Current.Request.Form(key).Length > 0 Then
            Dim parts As String() = HttpContext.Current.Request.Form(key).Split("-"c)
            Me.Controls.Add(New LiteralControl(String.Format("<script language=javascript>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "<!--" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & " setTimeout(""setContentScrollPos('{0}', {1}, {2})"", 100) " & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "//--></script>", Me.ClientID, parts(0), parts(1))))
        End If
    End Sub

    Private Sub clsGridViewSrc_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Me.StartScrollPos.X > 0 OrElse Me.StartScrollPos.Y > 0 Then
            Me.Controls.Add(New LiteralControl(String.Format("<script language=javascript>" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "<!--" & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & " setTimeout(""setContentScrollPos('{0}', {1}, {2})"", 100) " & Microsoft.VisualBasic.Chr(13) & "" & Microsoft.VisualBasic.Chr(10) & "//--></script>", Me.ClientID, Me.StartScrollPos.X, Me.StartScrollPos.Y)))
        End If
    End Sub
End Class
'End Namespace
