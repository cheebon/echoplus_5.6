﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSalesByChainCust
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesByChainCust"
        End Get
    End Property

    Function GetSalesByChain() As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CHAIN"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.strPrincipalId, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("MONTH", properties.Month, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", properties.strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("NET_VALUE", CStr(properties.intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesByChain :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetSalesByCust() As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.strPrincipalId, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("MONTH", properties.Month, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", properties.strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("NET_VALUE", properties.intNetValue, clsDB.DataType.DBInt)
                .addItem("CHAIN_CODE", properties.strChainCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesByCust :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
