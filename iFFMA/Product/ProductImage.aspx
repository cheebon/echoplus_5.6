﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProductImage.aspx.vb" Inherits="iFFMA_Product_ProductImage" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product Image</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />  
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmPrdImage" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
        <div style="width: 70%">
            <asp:Label ID="lblNewImg" runat="server" Text="Select product image (Format jpg,png,gif) " CssClass="cls_label"></asp:Label>
            <asp:Label ID="lblUploadInfo" runat="server" Text="" CssClass="cls_label_err"></asp:Label>
            <asp:FileUpload ID="fuPhoto" runat="server" CssClass="cls_button" Width="350px" />
            <asp:Button ID="btnLoadImg" runat="server" Text="Load" CssClass="cls_button" />
            <asp:Button ID="btnSaveImg" runat="server" Text="Save" CssClass="cls_button" />
            <br />
            <asp:UpdatePanel ID="UpdPnlImg" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Image ID="imgNewPrd" runat="server" AlternateText="Select a product image to upload" Width="250px"
                        Height="250px" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
