﻿Imports System.Data

Partial Class iFFMR_Customize_MissCallAnalysis_CallByMonthMC
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_MISSCALLANALYSIS")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_MISSCALLANALYSIS") = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_MISSCALLANALYSIS"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region


#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.MISSCALLANALYSIS) '"SALES_CUST_PER_PRD"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.MISSCALLANALYSIS
                .DataBind()
                .Visible = True
            End With

            GroupingValue = String.Empty
            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("TREE_PATH") = .Tree_Path
                    SortingExpression = .SortExpression
                End With
            Else
                CriteriaCollector = Nothing ' New clsSharedValue
            End If

            TimerControl1.Enabled = True
            'Try
            'Catch ex As Exception
            '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
            'End Try
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")


        End If

        lblErr.Text = ""
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "MISSCALLANALYSIS"
        End Get
    End Property

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

#Region "Event Handler"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged

        If IsPostBack Then
            RenewDataBind()
        End If

    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed

        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed

        RefreshDataBind()
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged

        GroupingValue = String.Empty
        RenewDataBind()
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged

        UpdateDatagrid_Update()
    End Sub


#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)


        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        '    ViewState("dtCurrentView") = dtCurrentTable
        '    SortingExpression = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)

            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        Else
            If Session("CUST_CODE") = "" Then
                dgList.GridHeight = 420
            Else
                dgList.GridHeight = 360
            End If
        End If
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MissCallAnalysis.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MissCallAnalysis.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MissCallAnalysis.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MissCallAnalysis.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_MissCallAnalysis.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_MissCallAnalysis.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                  

                    strUrlFormatString = "#"
                    dgColumn.DataNavigateUrlFields = Nothing ' strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.InvisibleColumn

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MissCallAnalysis.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_MissCallAnalysis.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MissCallAnalysis.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_MissCallAnalysis.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_MissCallAnalysis.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub

    ''' <summary>
    ''' Pass in the datatable, and generate the url format string based on the
    ''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    ''' to form the hyperlink fields need.
    ''' </summary>
    ''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    ''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    ''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    ''' <returns>URLFormatString, as url to pass with parameters</returns>
    ''' <remarks></remarks>
    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Select Case intIndex
            Case 0 'Generate sales info by date links
                Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
                Dim arrFields As New ArrayList 'store field name used in grouping
                Dim licGroupFieldList As New ListItemCollection
                licGroupFieldList.Add(New ListItem("TEAM_CODE", -1))
                licGroupFieldList.Add(New ListItem("REGION_CODE", -1))
                licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
                licGroupFieldList.Add(New ListItem("CHAIN_CODE", -1))
                licGroupFieldList.Add(New ListItem("CHANNEL_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
                licGroupFieldList.Add(New ListItem("PRD_GRP_CODE", -1))
                licGroupFieldList.Add(New ListItem("PRD_CODE", -1))
                licGroupFieldList.Add(New ListItem("SALES_AREA_CODE", -1))
                licGroupFieldList.Add(New ListItem("SHIPTO_CODE", -1))
                licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

                For Each liFieldToFind As ListItem In licGroupFieldList
                    For Each DC As DataColumn In dtToBind.Columns
                        If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
                            liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        End If
                    Next
                Next

                ReDim strUrlFields(arrFields.Count - 1)
                For intIndx As Integer = 0 To arrFields.Count - 1
                    strUrlFields(intIndx) = arrFields.Item(intIndx)
                Next

                Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
                Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value
                Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value
                strUrlFormatString = "SalesInfoByDate.aspx?" + _
                strbFilterGroup.ToString + _
               IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
               IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
               IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "")

                Session("SalesInfoByDate_GroupingField") = arrFields
            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        Return strUrlFormatString
    End Function

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                Dim iIndex As Integer
                Dim iDay As Integer
                If licItemFigureCollector Is Nothing Then Exit Select
                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    iDay = CF_MissCallAnalysis.GetDayValue(li.Text)
                    If iDay > 0 And Not IsDBNull(DataBinder.Eval(e.Row.DataItem, li.Text)) Then
                        Dim strCallData As String = DataBinder.Eval(e.Row.DataItem, li.Text) ' & "<BR/><small><b>" & DataBinder.Eval(e.Row.DataItem, li.Text).ToString.Replace(",", "<br>") & "</b></small>"
                        If ViewState("EXPORT") = True Then
                            e.Row.Cells(iIndex).Text = strCallData
                        Else
                            e.Row.Cells(iIndex).Text = "<a href=""#"" onclick=""parent.document.getElementById('DetailBarIframe').src='CallDtlMC.aspx?DAY=" & iDay & _
                                "&amp;MONTH=" & DataBinder.Eval(e.Row.DataItem, "MONTH") & _
                                "&amp;YEAR=" & DataBinder.Eval(e.Row.DataItem, "YEAR") & _
                                "&amp;TEAM_CODE=" & DataBinder.Eval(e.Row.DataItem, "TEAM_CODE") & _
                                "&amp;REGION_CODE=" & DataBinder.Eval(e.Row.DataItem, "REGION_CODE") & _
                                "&amp;SALESREP_CODE=" & DataBinder.Eval(e.Row.DataItem, "SALESREP_CODE") & _
                                "&PAGE_INDICATOR=CALLBYDAY'" & """ >" & _
                                strCallData & "</a>"
                        End If
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                'If aryDataItem.IndexOf("PRD_GRP_NAME") >= 0 Then
                '    Dim strCapCode As String = Trim(e.Row.Cells(aryDataItem.IndexOf("PRD_GRP_NAME")).Text)
                '    If UCase(strCapCode) Like "*TOTAL" Then
                '        e.Row.CssClass = "GridFooter"
                '    End If
                'End If

                'For Each TC As TableCell In e.Row.Cells
                '    If TC.Controls.Count > 0 Then
                '        Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                '        If HYL IsNot Nothing Then
                '            Dim strlink As String = HYL.NavigateUrl
                '            HYL.NavigateUrl = "#"
                '            HYL.Target = String.Empty
                '            HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();")
                '        End If
                '    End If
                'Next
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_MissCallAnalysis.GetOutputFormatString(li.Text), CDbl(li.Value))
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strUserID, strPrincipalCode, strPrincipalID As String

             strYear = Session("YEAR")
            strMonth = Session("MONTH")
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            lblYear.Text = strYear
            lblMonth.Text = strMonth

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .Year = strYear
                .Month = strMonth
                .PrincipalID = strPrincipalID
                .PrincipalCode = strPrincipalCode
                .Tree_Path = Session("TREE_PATH")
            End With

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            Dim clsMissCallAnalysis As New rpt_Customize.clsMissCallAnalysis
            DT = clsMissCallAnalysis.GetMissCallAnalysis(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList)
            'dgList_Init(DT)
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_ItemFigureCollector(ByVal DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String 'OrElse strColumnName Like "*DAY" _
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If (strColumnName Like "DAY_*_CALL" OrElse _
             strColumnName = "TOTAL_CALL" OrElse strColumnName = "DAY_MC" _
             ) _
             And Not (strColumnName Like "DAY_*_DEF") Then 'OrElse strColumnName Like "PERDAY"  OrElse strColumnName Like "AVG" _

                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'licItemFigureCollector = _licItemFigureCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub




End Class

Public Class CF_MissCallAnalysis
    Public Shared Function GetDayValue(ByVal ColumnName As String) As Integer
        '      Try
        Dim strColumnName As String = ColumnName.ToUpper
        Dim intValue As Integer = 0

        strColumnName = strColumnName.Replace("_CALL", "").Replace("_DEF", "").Replace("DAY_", "")
        'strColumnName = strColumnName.Replace("S", "")
        If Not String.IsNullOrEmpty(strColumnName) AndAlso IsNumeric(strColumnName) Then intValue = CInt(strColumnName)
        Return intValue
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "TOTAL_CALL"
                strFieldName = "No. MC"
            Case "DAY_MC"
                strFieldName = "DMC"
            Case Else
                If ColumnName.ToUpper Like "DAY_*_CALL" And Not ColumnName.ToUpper Like "DAY_*_DEF" Then
                    strFieldName = ColumnName.Replace("_CALL", "").Replace("DAY_", "")
                    If IsNumeric(strFieldName) Then strFieldName = CInt(strFieldName)
                Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
                End If
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '  Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If (strColumnName = "YEAR") OrElse (strColumnName = "MONTH") OrElse (strColumnName = "TEAM_CODE") OrElse (strColumnName = "REGION_CODE") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf (strColumnName Like "*_CALL") And Not (strColumnName = "TOTAL_CALL") And Not (strColumnName = "DAY_MC") Then

            'If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CABYDAY, "'1','8'") Then
            FCT = FieldColumntype.HyperlinkColumn
            'End If
        End If

        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALL", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUNE"
                Return 6
            Case "JULY"
                Return 7
            Case "AUG"
                Return 8
            Case "SEPT"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        '  Try
        If strColumnName Like "*_CALL" OrElse strColumnName = "TOTAL_CALL" OrElse strColumnName = "DAY_MC" Then
            strStringFormat = "{0:0.##}"
        Else
            strStringFormat = ""
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        '    Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class

