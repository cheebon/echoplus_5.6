'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	19/02/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSFMSExtract
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFMSExtract"
        End Get
    End Property

    Public Function GetSFMSExtractList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_EXTRACTION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractList :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSFMSExtractList_2(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_EXTRACTION_2"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractList :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSupplier(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SUPPLIER"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSupplier :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSalesTeam(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_TEAM"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSalesrep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesrep :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetCat(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SFMS_CAT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCat :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSubCat(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String, ByVal strCatList As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SFMS_SUB_CAT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSubCat :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function



    Public Function GetPreplanExtractList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strTeamCode As String, _
                                    ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim clsPreplanExtractDB As clsDB
        Try
            clsPreplanExtractDB = New clsDB
            With clsPreplanExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PREPLAN_EXTRACTION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractList :" & ex.Message))
        Finally
            clsPreplanExtractDB = Nothing
        End Try
    End Function

    Public Function GetExCat(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SFMS_SUB_CAT_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetExCat :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSFMSExtractListExtracCat(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strExCatCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_EXTRACTION_EXTRA_CAT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)
                .addItem("EXTRA_CAT_CODE", strExCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractListExtracCat :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSFMSExtractList_3(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strExCatCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_EXTRACTION_3"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)
                .addItem("EXTRA_CAT_CODE", strExCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFMSExtractList_3 :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class


