Imports System.Data

Partial Class PreplanList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _aryAllColumnName As ArrayList
    Protected Property aryAllColumnName() As ArrayList
        Get
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = ViewState("AllDataItem")
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = New ArrayList
            Return _aryAllColumnName
        End Get
        Set(ByVal value As ArrayList)
            ViewState("AllDataItem") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_PreplanList"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "PreplanList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Try
        If Not IsPostBack Then

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.PREPLAN) '"Preplan Call List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.PREPLAN
                .DataBind()
                .Visible = True
            End With

            GroupingValue = ""
            'ViewState("dtCurrentView") = Nothing

            Dim strNodeName As String
            Dim strNodeValue As String

            strNodeName = Session("NODE_GROUP_NAME")
            strNodeValue = Trim(Request.QueryString("TREE_" + strNodeName))

            Report.UpdateTreePath(strNodeName, strNodeValue)

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("TREE_PATH") = .Tree_Path
                    GroupingValue = .GroupField
                    SortingExpression = .SortExpression
                    wuc_ctrlpanel.strRestoreGroupingTextValue = .GroupTextValue
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing
            End If
            wuc_Menu.RestoreSessionState = True
            TimerControl1.Enabled = True

        End If
        lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        'Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        'Try
        'ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        'Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        'Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        '  Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        ' Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlpanel.ExportToFile(dgList, "Preplan")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("GroupingValue") = String.Empty
        '   ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'dgList.ShowFooter = False
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        'End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        ' Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
        Dim aryList As New ArrayList
        ' Try
        For Each DC As DataColumn In dtToBind.Columns
            aryList.Add(DC.ColumnName.ToUpper)
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
        'End Try
        Return aryList
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Dim aryAllColumnName As ArrayList = GenerateAllColumnName(dtToBind)

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_PREPLAN.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    dgColumn.ItemStyle.HorizontalAlign = CF_PREPLAN.ColumnStyle(ColumnName).HorizontalAlign

                    If String.IsNullOrEmpty(CF_PREPLAN.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_PREPLAN.GetOutputFormatString(ColumnName)
                    End If

                    dgColumn.HeaderText = CF_PREPLAN.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                    strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields, ColumnName)

                    'Dim strUrlFields(3) As String
                    ''Pass in 'SALESREP_CODE,DATE,STATUS
                    'strUrlFields(0) = IIf(aryAllColumnName.Contains("SALESREP_CODE"), "SALESREP_CODE", "")
                    'strUrlFields(1) = IIf(aryAllColumnName.Contains("DATE"), "DATE", "")
                    'strUrlFields(2) = ""
                    'strUrlFields(3) = IIf(aryAllColumnName.Contains("REGION_CODE"), "REGION_CODE", "")
                    'Dim strUrlFormatString As String
                    'If (aryAllColumnName.Contains("TEAM_CODE") OrElse aryAllColumnName.Contains("REGION_CODE")) AndAlso (Not (aryAllColumnName.Contains("SALESREP_CODE") OrElse aryAllColumnName.Contains("DATE"))) Then
                    '    strUrlFields(2) = IIf(aryAllColumnName.Contains("TEAM_CODE"), "TEAM_CODE", "")
                    '    strUrlFormatString = "PreplanList.aspx?TEAM_CODE={2}&REGION_CODE={3}&SALESREP_CODE={0}&DATE={1:yyyy-MM-dd}"
                    'Else
                    '    strUrlFields(2) = IIf(aryAllColumnName.Contains("TEAM_CODE"), "TEAM_CODE", "")
                    '    strUrlFields(3) = IIf(aryAllColumnName.Contains("REGION_CODE"), "REGION_CODE", "")
                    '    strUrlFormatString = "PreplanCustList.aspx?STATUS=" & ColumnName & "&SALESREP_CODE={0}&DATE={1:yyyy-MM-dd}&TEAM_CODE={2}&REGION_CODE={3}"
                    '    'If aryDataItem.Contains("TEAM_CODE") AndAlso (aryDataItem.Contains("REGION_CODE")) AndAlso Not (aryDataItem.Contains("SALESREP_CODE") OrElse aryDataItem.Contains("DATE")) Then
                    '    '    strUrlFields(2) = IIf(aryAllColumnName.Contains("TEAM_CODE"), "TEAM_CODE", "")
                    '    '    strUrlFields(3) = IIf(aryAllColumnName.Contains("REGION_CODE"), "REGION_CODE", "")
                    '    '    strUrlFormatString = "PreplanList.aspx?SALESREP_CODE={0}&DATE={1:yyyy-MM-dd}&TEAM_CODE={2}&REGION_CODE={3}"
                    '    'Else
                    '    '    strUrlFields(2) = IIf(aryAllColumnName.Contains("BINDTEAM"), "BINDTEAM", "")
                    '    '    strUrlFormatString = "PreplanCustList.aspx?STATUS=" & ColumnName & "&TEAM_CODE=" & Trim("{2}") & "&SALESREP_CODE={0}&DATE={1:yyyy-MM-dd}"
                    '    'End If

                    'End If

                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString

                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    'dgColumn.ItemStyle.HorizontalAlign = CF_PREPLAN.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.ItemStyle.HorizontalAlign = CF_PREPLAN.ColumnStyle(ColumnName).HorizontalAlign
                    Dim strFormatString As String = CF_PREPLAN.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If


                    dgColumn.HeaderText = CF_PREPLAN.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        aryDataItem = _aryDataItem
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub

    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String, ByVal ColumnName As String) As String
        Dim strUrlFormatString As String = String.Empty
        '   Try
        Select Case intIndex
            Case 0 'Generate sales info by date links
                Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
                Dim arrFields As New ArrayList 'store field name used in grouping

                Dim licGroupFieldList As New ListItemCollection
                licGroupFieldList.Add(New ListItem("DATE", -1))
                licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

                For Each liFieldToFind As ListItem In licGroupFieldList
                    For Each DC As DataColumn In dtToBind.Columns
                        If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)

                            If liFieldToFind.Text.Equals("DATE") Then
                                strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & ":yyyy-MM-dd}")
                            Else
                                strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            End If

                            Exit For
                        ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
                            liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        End If
                    Next
                Next

                Dim blnBackToOwnPage As Boolean
                blnBackToOwnPage = True

                ReDim strUrlFields(arrFields.Count - 1)
                For intIndx As Integer = 0 To arrFields.Count - 1
                    strUrlFields(intIndx) = arrFields.Item(intIndx)

                    If arrFields.Item(intIndx) = "DATE" OrElse _
                        arrFields.Item(intIndx) = "TREE_SALESREP_CODE" Then
                        blnBackToOwnPage = False
                    End If
                Next

                If blnBackToOwnPage Then
                    strUrlFormatString = "PreplanList.aspx?" & strbFilterGroup.ToString
                Else
                    strUrlFormatString = "PreplanCustList.aspx?" & strbFilterGroup.ToString & "&STATUS=" & ColumnName
                End If

                Session("PreplanList_GroupingField") = arrFields

            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        'End Try
        Return strUrlFormatString
    End Function

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header
                Case DataControlRowType.DataRow
                    'Dim iIndex As Integer

                    'For Each li As ListItem In licItemFigureCollector
                    '    iIndex = aryDataItem.IndexOf(li.Text)
                    '    If iIndex > 0 Then
                    '        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
                    '    End If
                    'Next
                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex > 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_PREPLAN.GetOutputFormatString(li.Text), li.Value)
                        End If
                    Next

                    If licItemFigureCollector.Count > 0 Then
                        'Plan%
                        Dim liDividend As ListItem = licItemFigureCollector.FindByText("TTL_CUST")
                        'Dim liDivisor As ListItem = licItemFigureCollector.FindByText("VISITED")
                        Dim liDivisor As ListItem
                        If licItemFigureCollector.FindByText("VISITED") Is Nothing Then
                            liDivisor = licItemFigureCollector.FindByText("VISITED_CALL")
                        Else
                            liDivisor = licItemFigureCollector.FindByText("VISITED")
                        End If

                        If (Not liDividend Is Nothing) AndAlso (Not liDivisor Is Nothing) _
                        AndAlso liDividend.Value > 0 AndAlso (aryDataItem.IndexOf("PLAN_ACH") > 0) _
                        Then
                            e.Row.Cells(aryDataItem.IndexOf("PLAN_ACH")).Text = String.Format(CF_PREPLAN.GetOutputFormatString("PLAN_ACH"), (liDivisor.Value / liDividend.Value * 100))
                        End If

                        'Actual Calls%
                        liDividend = licItemFigureCollector.FindByText("TTL_CUST")
                        If licItemFigureCollector.FindByText("TTL_CALL") Is Nothing Then
                            liDivisor = licItemFigureCollector.FindByText("TTL_CALL_CALL")
                        Else
                            liDivisor = licItemFigureCollector.FindByText("TTL_CALL")
                        End If

                        If (Not liDividend Is Nothing) AndAlso (Not liDivisor Is Nothing) _
                        AndAlso liDividend.Value > 0 AndAlso (aryDataItem.IndexOf("ACT_ACH") > 0) _
                        Then
                            e.Row.Cells(aryDataItem.IndexOf("ACT_ACH")).Text = String.Format(CF_PREPLAN.GetOutputFormatString("ACT_ACH"), (liDivisor.Value / liDividend.Value * 100))
                        End If

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        '  Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        '  Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        'Try
        'strUserID , strYear , strMonth , strTeamCode , strSalesrepCode , strGroupFields ,
        Dim strUserID, strYear, strMonth, strPrincipalCode, strPrincipalID As String
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")

        Dim strGroupCode, strGroupName, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupName = strGroupCode.Replace("_CODE", " (Tree)").Replace("_", " ")
        strSalesRepList = Session("SALESREP_LIST")

        Dim strPointerText As String
        strPointerText = strGroupName 'IIf(String.IsNullOrEmpty(strTeamCode), "Team", IIf(String.IsNullOrEmpty(strRegionCode), "Region", "Sales Rep."))
        'If wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False Then wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        'Add in salesrep_code when first visit, or when grouping is nothing
        If (IsPostBack = False AndAlso wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False) OrElse _
            (wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow) = 0) Then
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        End If

        Dim strGroupField As String = IIf(String.IsNullOrEmpty(GroupingValue), wuc_ctrlpanel.pnlField_GetGroupingValue, GroupingValue)
        If Not String.IsNullOrEmpty(GroupingValue) Then GroupingValue = String.Empty
        'select group field to show
        'Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        'If Not String.IsNullOrEmpty(strTeamCode) Then
        '    If Not String.IsNullOrEmpty(strRegionCode) Then
        '        'group by salesrep
        '        strGroupField = strGroupField.Replace(strPointer, "SALESREP_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")
        '    Else
        '        'Group by Region
        '        strGroupField = strGroupField.Replace(strPointer, "REGION_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Region")
        '    End If
        'Else
        '    'group by Team
        '    strGroupField = strGroupField.Replace(strPointer, "TEAM_CODE")
        '    wuc_ctrlpanel.pnlField_EditPointerText("Team")
        'End If
        If String.IsNullOrEmpty(GroupingValue) Then
            strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
        Else
            strGroupField = GroupingValue
            GroupingValue = String.Empty
        End If

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .Year = strYear
            .Month = strMonth
            .PrincipalID = strPrincipalID
            .PrincipalCode = strPrincipalCode
            .Tree_Path = Session("TREE_PATH")
            .GroupField = strGroupField 'Session("GroupField")
            .GroupTextValue = wuc_ctrlpanel.pnlField_GetGroupingTextValue
        End With

        Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        strGroupField = strGroupField.Replace(strPointer, "TREE_" & strGroupCode)
        wuc_ctrlpanel.pnlField_EditPointerText(strGroupName)

        Dim clsPreplanDB As New rpt_Preplan.clsPreplanQuery
        DT = clsPreplanDB.GetPreplanList(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField)

        aryAllColumnName = GenerateAllColumnName(DT)
        PreRenderMode(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        ' Try
        dgList_Init(DT)
        Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByVal DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        '  Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            Select Case strColumnName
                Case "TTL_CUST", "VISITED", "UNVISIT", "UNPLAN", "TTL_CALL", "VISITED_CALL", "UNVISIT_CALL", "TTL_CALL_CALL"
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
            End Select
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        'Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        CriteriaCollector.SortExpression = strSortExpression
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        'Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

End Class

Public Class CF_PREPLAN
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "BINDTEAM"
                strFieldName = "Team"
            Case "DATE"
                strFieldName = "Date"
            Case "TTL_CUST"
                strFieldName = "Plan"
            Case "VISITED"
                strFieldName = "Visit"
            Case "VISITED_CALL"
                strFieldName = "Call"
            Case "PLAN_ACH"
                strFieldName = "Plan%"
            Case "UNVISIT"
                strFieldName = "Un-Visit"
            Case "UNVISIT_CALL"
                strFieldName = "Un-Call"
            Case "UNPLAN"
                strFieldName = "Un-Plan"
            Case "TTL_CALL"
                strFieldName = "Total Visit"
            Case "TTL_CALL_CALL"
                strFieldName = "Total Call"
            Case "ACT_ACH"
                strFieldName = "Actual%"
            Case "PLANNED"
                strFieldName = "Plan"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '    Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "BINDTEAM"
                FCT = FieldColumntype.InvisibleColumn
            Case "TTL_CUST", "VISITED", "UNVISIT", "UNPLAN", "VISITED_CALL", "UNVISIT_CALL", "PLANNED"
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.PREPLANCUST, "'1','8'") Then
                    FCT = FieldColumntype.HyperlinkColumn
                End If
        End Select

        Return FCT

        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        '    Try
        'TEAM_CODE , DATE , SALESREP_CODE , SALESREP_NAME , 
        'TTL_CUST, VISITED, TOTPLAN, UNVISIT, UNPLAN, TOTCALL, ACT_ACH

        Select Case strNewName
            Case "PLAN_ACH", "ACT_ACH"
                strStringFormat = "{0:0.0}"
            Case "TTL_CUST", "VISITED", "UNVISIT", "UNPLAN", "TTL_CALL", "VISITED_CALL", "UNVISIT_CALL", "TTL_CALL_CALL", "PLANNED"
                strStringFormat = "{0:0}"
            Case "DATE"
                strStringFormat = "{0:yyyy-MM-dd}"
            Case Else
                strStringFormat = ""
        End Select

        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function


    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        'Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum