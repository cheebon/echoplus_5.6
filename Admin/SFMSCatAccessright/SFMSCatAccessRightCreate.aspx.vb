﻿Option Explicit On
Imports System.Data
Imports Portal
Imports System.Threading
Imports System.Globalization

''' <summary>
'''  ************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-08-15
'''	Purpose	    :	Easy Loader Accessright Create
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
''' 
Partial Class Admin_SFMSCatAccessright_SFMSCatAccessRightCreate
    Inherits System.Web.UI.Page
#Region "Local Properties"
    Public Property SessionUser() As String
        Get
            Return Portal.UserSession.UserID
        End Get
        Set(ByVal value As String)
            Portal.UserSession.UserID = value
        End Set
    End Property
    Private Property UrlParams() As String
        Get
            Return ViewState("UrlParams")
        End Get
        Set(ByVal value As String)
            ViewState("UrlParams") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErr.Text = ""
        lblErrDtl.Text = ""
        updPnlErr.Update()
        updPnlAccessright.Update()

        Try
            lblHeader.Title = "Field Activity Category Accessright Create" 'Me.GetLocalResourceObject("PageResource1.Title").ToString
            'lblHeader.Title = "Create Access Right"

            If Not Page.IsPostBack Then
                UrlParams = "search_type=" & Trim(Request.QueryString("search_type")) & "&search_value=" & Trim(Request.QueryString("search_value"))

                'LoadCountry(SessionUser)
                'LoadPrincipal(SessionUser)
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub



    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            If Not IsARExists(0) Then
                Dim obj As New adm_SFMSCatAccessRight.clsSFMSCatAccessRight

                With obj
                    .clsPpt.accessright_name = txtName.Text.Trim
                    .clsPpt.accessright_name_1 = txtName1.Text.Trim
                    .clsPpt.accessright_desc = txtDesc.Text.Trim
                    .clsPpt.accessright_desc_1 = txtDesc1.Text.Trim
                    Dim dt As DataTable = .CreateAR(SessionUser)

                    Dim strARID As String = dt.Rows(0)("ACCESSRIGHT_ID").ToString

                    Response.Redirect("SFMSCatAccessRightEdit.aspx?accessright_id=" & Trim(strARID) & "&ori=create", False)
                End With
            Else
                lblErrDtl.Text = "Accessright name already exists."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateAllPnl()
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("SFMSCatAccessRightList.aspx?" & UrlParams, True)
    End Sub

    Private Function IsARExists(ByVal strARID As String) As Boolean
        Dim blnExists As Boolean = False

        Dim obj As New adm_SFMSCatAccessRight.clsSFMSCatAccessRight

        With obj
            .clsPpt.accessright_name = txtName.Text.Trim
            .clsPpt.accessright_id = strARID
        End With
        Dim dt As DataTable = obj.CheckARDuplicate()

        If Portal.Util.GetValue(Of Integer)(dt.Rows(0)("CNT"), 0) > 0 Then
            blnExists = True
        End If

        Return blnExists
    End Function

    Private Sub UpdateAllPnl()
        UpdateErrPnl()
        UpdateARPnl()
    End Sub

    Private Sub UpdateErrPnl()
        updPnlErr.Update()
    End Sub

    Private Sub UpdateARPnl()
        updPnlAccessright.Update()
    End Sub
End Class
