'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	10/04/2007
'	Purpose	    :	Mobile User Edit
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_UserMobile_UserMobileEdit
    Inherits System.Web.UI.Page
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim objCrypto As cor_Crypto.clsDecryption
        Dim strDecryptPwd, strUserID As String
        Dim dt As DataTable
        'Dim strSessionUserCode As String

        Try
            lblErr.Text = ""

            If Not IsPostBack Then
                'constant
                'strSessionUserCode = Trim(Session("UserCode"))

                'Call Header
                With wuc_lblheader
                    .Title = "Mobile User Edit" 'strTitle
                    .DataBind()
                    .Visible = True
                End With

                'populate ddl
                LoadDDL()

                'request
                strUserID = Trim(Request.QueryString("user_id"))

                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False

                'Get Details
                objUserQuery = New adm_PDAUser.clsPDAUserQuery
                With objUserQuery
                    .clsProperties.strUserID = strUserID
                    dt = .GetUserDetail
                End With

                If dt.Rows.Count > 0 Then
                    'Decrypt Password
                    objCrypto = New cor_Crypto.clsDecryption
                    With objCrypto
                        strDecryptPwd = .TripleDESDecrypt(Trim(dt.Rows(0)("pwd")))
                    End With

                    ddlStatusID.SelectedValue = Trim(dt.Rows(0)("active_flag"))
                    'txtPassword.Text = strDecryptPwd
                    ViewState("pwd") = strDecryptPwd
                    txtPassword.Attributes.Add("value", ViewState("pwd").ToString)
                    txtConfirmedPassword.Attributes.Add("value", ViewState("pwd").ToString)
                    txtUserCode.Text = Trim(dt.Rows(0)("user_mobile_code"))
                    txtUserName.Text = Trim(dt.Rows(0)("user_mobile_name"))
                    txtMobileCode.Text = Trim(dt.Rows(0)("mobile_code"))
                    If IsDBNull(dt.Rows(0)("user_mobile_contactno")) Then
                        txtContactNo.Text = ""
                    Else
                        txtContactNo.Text = Trim(dt.Rows(0)("user_mobile_contactno"))
                    End If
                    If IsDBNull(dt.Rows(0)("user_mobile_address")) Then
                        txtAddress.Text = ""
                    Else
                        txtAddress.Text = Trim(dt.Rows(0)("user_mobile_address"))
                    End If
                    If IsDBNull(dt.Rows(0)("user_mobile_email")) Then
                        txtEmail.Text = ""
                    Else
                        txtEmail.Text = Trim(dt.Rows(0)("user_mobile_email"))
                    End If
                    lblUserIDValue.Text = Trim(dt.Rows(0)("user_mobile_id"))
                    If IsDBNull(dt.Rows(0)("comm_svr_setting_code")) Then
                        ddlSettingCode.SelectedValue = 0
                    Else
                        ddlSettingCode.SelectedValue = Trim(dt.Rows(0)("comm_svr_setting_code"))
                    End If
                End If
                End If
        Catch ex As Exception
            ExceptionMsg("UserMobileEdit.Page_Load : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub LoadDDL
    ' Purpose	    :	Load dropdownlist
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub LoadDDL()
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim dt As DataTable
        Dim intLoopCount As Integer = 0

        objUserQuery = New adm_PDAUser.clsPDAUserQuery
        With objUserQuery
            dt = .GetConnectionString
        End With

        If dt.Rows.Count > 0 Then
            Do While intLoopCount < dt.Rows.Count
                ddlSettingCode.Items.Insert(intLoopCount + 1, dt.Rows(intLoopCount).Item("comm_svr_setting_code").ToString)
                intLoopCount = intLoopCount + 1
            Loop
        End If
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_PDAUser.clsPDAUser
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String = ""
        Dim bUserExist As Boolean

        Try
            'Check login or user code exist or not
            objUserQuery = New adm_PDAUser.clsPDAUserQuery
            With objUserQuery
                .clsProperties.strMobileUserID = trim(lblUserIDValue.Text)
                .clsProperties.strSalesRepCode = Trim(txtUserCode.Text)
                .clsProperties.strMobileCode = Trim(txtMobileCode.Text)
                bUserExist = .UserExist
            End With
            objUserQuery = Nothing

            'If bUserExist = False Then
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('User Code does not exists!');</script>")
            '    Exit Sub
            'End If

            If bUserExist = True Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Login / User Code already exists!');</script>")
                Exit Sub
            End If

            If chkPassword.Checked Then
                'Check Password match
                If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                    Exit Sub
                End If

                'Encrypt Password
                objCrypto = New cor_Crypto.clsEncryption
                With objCrypto
                    strEncryptPwd = .TripleDESEncrypt(Trim(txtPassword.Text))
                End With
            End If

            'Insert into User Table
            objUser = New adm_PDAUser.clsPDAUser
            With objUser
                If chkPassword.Checked Then
                    .clsProperties.strPassword = strEncryptPwd
                    .clsProperties.bChangePwd = True
                Else
                    .clsProperties.bChangePwd = False
                End If
                .clsProperties.strUserID = Trim(lblUserIDValue.Text)
                .clsProperties.strSalesRepCode = Trim(txtUserCode.Text)
                .clsProperties.strSalesRepName = Trim(txtUserName.Text)
                .clsProperties.strMobileCode = Trim(txtMobileCode.Text)
                .clsProperties.strContactNo = Trim(txtContactNo.Text)
                .clsProperties.strAddress = Trim(txtAddress.Text)
                .clsProperties.strEmail = Trim(txtEmail.Text)
                .clsProperties.strConnCode = ddlSettingCode.SelectedValue.Trim
                If Session("UserID") <> "" Then
                    .clsProperties.strChangedUserID = Session("UserID")
                Else
                    .clsProperties.strChangedUserID = ""
                End If
                .clsProperties.strChangedDate = Format(Date.Now, "MM/dd/yyyy hh:mm:ss")
                .clsProperties.strActiveFlag = ddlStatusID.SelectedValue
                .clsProperties.strSyncOperation = "A"
                .Update()
            End With
            Response.Redirect("UserMobileDtl.aspx?user_code=" & Trim(txtUserCode.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserMobileEdit.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub chkPassword_CheckedChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub chkPassword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPassword.CheckedChanged
        Try
            If chkPassword.Checked Then
                txtPassword.Enabled = True
                txtConfirmedPassword.Enabled = True
                rfvPassword.Visible = True
                rfvConfirmedPassword.Visible = True
                lblMark3.Visible = True
                lblMark4.Visible = True
            Else
                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("UserMobileEdit.chkPassword_CheckedChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("UserMobileList.aspx", False)
    End Sub
End Class