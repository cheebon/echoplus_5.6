<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DetailingSubCatList.ascx.vb" Inherits="iFFMA_Detailing_DetailingSubCatList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DetailingSubCatPop" Src="~/iFFMA/Detailing/DetailingSubCatPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DetailingSubPreviewPop" Src="~/iFFMA/Detailing/DetailingSubPreviewPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlDetailingSubCatList" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                        <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }"/>
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <customToolkit:wuc_dgpaging ID="wuc_dgSubCatPaging" runat="server" />
                        <ccGV:clsGridView ID="dgSubCatList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>     
        <asp:HiddenField ID="hdDetCode" runat="server" Value="" />
        <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="This category cannot be deleted." runat="server" />
        <customToolkit:wuc_DetailingSubCatPop ID="wuc_DetailingSubCatPop" Title="Sub Category Maintenance (Detailing)" runat="server" />
        <customToolkit:wuc_DetailingSubPreviewPop ID="wuc_DetailingSubPreviewPop" Title="Sub Detailing Preview" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>