<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ctrlpnl.ascx.vb" Inherits="iFFMR_SFA_USRCTRL_wuc_ctrlpnl" %>
<asp:UpdatePanel ID="UpdateCtrlPanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
    <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0" width="100%" border="0" style="">
    <tr>
        <td style="padding-left: 10px; width: 50%; text-align: left">
<div style="float:left"> 
            <span class="cls_label_header">Map:&nbsp;</span>
            <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
</div>
<div style="float:right">
            <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/iFFMR/SFA/images/icoSearch.png" EnableViewState="false" AlternateText="Search" />
    <asp:ImageButton ID="ImgExport" runat="server" ImageUrl="~/iFFMR/SFA/images/icoExcel.gif" EnableViewState="false" AlternateText="Export" />
</div>
<div style="float:right; padding:3px 10px 0px 5px; white-space:nowrap">
<%  If pnlSupplier_Visible Then%>
                <span class="cls_label_header">Supplier:&nbsp;</span>
                <asp:DropDownList ID="ddlSupplier" CssClass="cls_dropdownlist" runat="server" />    
<% end if %> 
                <span class="cls_label_header">Year:&nbsp;</span>
                <asp:DropDownList ID="ddlYear" CssClass="cls_dropdownlist" runat="server" />
<%  If pnlChannel_Visible Then%> 
                <span class="cls_label_header">Channel:&nbsp;</span>
                <asp:DropDownList ID="ddlChannel" CssClass="cls_dropdownlist" runat="server">              
                </asp:DropDownList>
<% end if %> 
<%  If pnlRegion_Visible Then%> 
                <span class="cls_label_header">Region:&nbsp;</span>
                <asp:DropDownList ID="ddlRegion" CssClass="cls_dropdownlist" runat="server">
                    <asp:ListItem Value="">--- Select All ---</asp:ListItem>                    
                </asp:DropDownList>
<% end if %> 
<%  If pnlMonth_Visible Then%> 
                <span class="cls_label_header">Month:&nbsp;</span>
                <asp:DropDownList ID="ddlMonth" CssClass="cls_dropdownlist" runat="server">
                    <asp:ListItem Value="1">Jan</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Apr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Aug</asp:ListItem>
                    <asp:ListItem Value="9">Sept</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dec</asp:ListItem>
                </asp:DropDownList>
<% end if %>   
<%  If pnlQuarter_Visible Then%>              
                <span class="cls_label_header">Quarter:&nbsp;</span>
                <asp:DropDownList ID="ddlQuarter" CssClass="cls_dropdownlist" runat="server">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                </asp:DropDownList>
<% end if %> 

</div>
        </td>
    </tr>
    </table>
<%--                <asp:Panel ID="pnlYear" runat="server" style="float:left">
            </asp:Panel>
            <asp:Panel ID="pnlMonth" runat="server" style="float:left">
            </asp:Panel>
            <asp:Panel ID="pnlQuarter" runat="server"  style="float:left">
            </asp:Panel>--%>
    </ContentTemplate>
    <Triggers><asp:PostBackTrigger ControlID="ImgExport" /></Triggers>
</asp:UpdatePanel>
