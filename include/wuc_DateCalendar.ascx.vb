Option Explicit On

Imports System.Data

Partial Public Class wuc_DateCalendar
    Inherits System.Web.UI.UserControl

    'RequiredFieldValidator
    Private _strValidationGroup As String
    Private _blnRequiredValidation As Boolean = True
    Private _strValidationErrorMessage As String

    'CompareDateRangeValidator
    Private _blnCompareDateRangeEnabled As Boolean = True
    Private _strCompareDateRangeErrorMessage As String

    'Date Format
    Private _strCurrentFormat As String
    Private _strDefaultDateFormatString As String = "yyyy-MM-dd"

    Private ReadOnly Property ControlName() As String
        Get
            Return "wuc_txtCalendar"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()
        If IsPostBack = False Then
            InsertFunctionScript()
            txtDate.Attributes.Add("onblur", "formatDate('" & txtDate.ClientID & "', '" & DateFormatString & "');")
            txtDate.Attributes.Add("onkeydown", "EnterToTab();")
            rfvFormatDate.ClientValidationFunction = "formatDate('" & txtDate.ClientID & "', '" & DateFormatString & "');"
            rfvCheckDataType.ErrorMessage = "<BR/>Invalid Date!(" & DateFormatString & ")"

            'Set ValidationGoupFiled is exist
            If Not String.IsNullOrEmpty(_strValidationGroup) Then
                rfvDateStart.ValidationGroup = _strValidationGroup
            End If

            'Set Validation Visibility is Exist
            rfvDateStart.Enabled = _blnRequiredValidation
        End If
    End Sub

    Public Sub InsertFunctionScript()
        ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
    End Sub

#Region "Text"
    Public Property DateStart() As String
        Get
            Return Text()
        End Get
        Set(ByVal Value As String)
            Text = Value
        End Set
    End Property
    Public Property Text() As String
        Get
            Return txtDate.Text
        End Get
        Set(ByVal Value As String)
            Dim dtm As DateTime
            If Not String.IsNullOrEmpty(Value) AndAlso IsDate(Value) Then
                dtm = DateTime.Parse(Value)
                txtDate.Text = dtm.ToString(DateFormatString)
            Else
                txtDate.Text = ""
            End If
        End Set
    End Property
#End Region

#Region "Value"
    Public Function FormatedDateStartValue(Optional ByVal strDateFormatString As String = "") As String
        Dim strFormatedDatetimeValue As String = ""
        If String.IsNullOrEmpty(strDateFormatString) Then strDateFormatString = DateFormatString
        Try
            Dim dtmSelectedDateTime As DateTime = DateStartValue
            strFormatedDatetimeValue = dtmSelectedDateTime.ToString(strDateFormatString)
        Catch ex As Exception
            'ExceptionMsg(ControlName + ".FormatedDateStartValue : " & ex.ToString)
        End Try
        Return strFormatedDatetimeValue
    End Function

    Public Function FormatedDateString(Optional ByVal strDateFormatString As String = "") As String
        Dim strFormatedDatetimeValue As String = ""
        If String.IsNullOrEmpty(strDateFormatString) Then strDateFormatString = DateFormatString
        Try
            Dim dtmSelectedDateTime As DateTime = DateStartValue
            strFormatedDatetimeValue = dtmSelectedDateTime.ToString(strDateFormatString)
        Catch ex As Exception
            'ExceptionMsg(ControlName + ".FormatedDateStartValue : " & ex.ToString)
        End Try
        Return strFormatedDatetimeValue
    End Function

    Public ReadOnly Property DateStartValue() As DateTime
        Get
            Dim dtmSelectedDateTime As DateTime
            Try
                dtmSelectedDateTime = DateTime.Parse(Text)

            Catch ex As Exception
                'ExceptionMsg(ControlName + ".DateStartValue : " & ex.ToString)
            End Try
            Return dtmSelectedDateTime
        End Get
    End Property
#End Region

#Region "Date Format"
    Private Sub setDefaultFormat()
        _strCurrentFormat = _strDefaultDateFormatString
    End Sub

    Public Property DateFormatString() As String
        Get
            If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()
            Return _strCurrentFormat
        End Get
        Set(ByVal value As String)
            _strCurrentFormat = value
        End Set
    End Property
#End Region

#Region "Require Field Validators"
    Public Property ValidationErrorMessage() As String
        Get
            Return _strValidationErrorMessage
        End Get
        Set(ByVal value As String)
            _strValidationErrorMessage = value
            rfvDateStart.ErrorMessage = _strValidationErrorMessage
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return _blnRequiredValidation
        End Get
        Set(ByVal blnVisible As Boolean)
            _blnRequiredValidation = blnVisible
            If Not rfvDateStart Is Nothing Then rfvDateStart.Enabled = _blnRequiredValidation
        End Set
    End Property

    Public Property RequiredValidationGroup() As String
        Get
            Return _strValidationGroup
        End Get
        Set(ByVal value As String)
            _strValidationGroup = value
            If Not rfvDateStart Is Nothing Then rfvDateStart.ValidationGroup = _strValidationGroup
        End Set
    End Property

#End Region
End Class