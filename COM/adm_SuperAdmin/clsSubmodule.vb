﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSubmodule
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function SubmoduleList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULE_LIST"
                .addItem("SEARCH_TYPE", strSearchType, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, cor_DB.clsDB.DataType.DBString)
                SubmoduleList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.SubmoduleList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function SubmoduleDtl(ByVal strSubmoduleID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULE_DTL"
                .addItem("SUBMODULE_ID", strSubmoduleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                SubmoduleDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.SubmoduleDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function SubmoduleActionDtl(ByVal strSubmoduleID As String, ByVal dblEnable As Double) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEACTION_LIST"
                .addItem("SUBMODULE_ID", strSubmoduleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("ENABLED", dblEnable, cor_DB.clsDB.DataType.DBDouble)
                SubmoduleActionDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.SubmoduleDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function Create(ByVal strSubmoduleCode As String, ByVal strModuleID As String, ByVal strSubmoduleName As String, ByVal strSubmoduleName1 As String, _
                           ByVal strSubmoduleDesc As String, ByVal strSubmoduleDesc1 As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULE_CREATE"
                .addItem("SUBMODULE_CODE", strSubmoduleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("MODULE_ID", strModuleID, cor_DB.clsDB.DataType.DBString)
                .addItem("SUBMODULE_NAME", strSubmoduleName, cor_DB.clsDB.DataType.DBString)
                .addItem("SUBMODULE_NAME_1", strSubmoduleName1, cor_DB.clsDB.DataType.DBString)
                .addItem("SUBMODULE_DESC", strSubmoduleDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("SUBMODULE_DESC_1", strSubmoduleDesc1, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                Create = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub SubmoduleActionCreate(ByVal strSubmoduleID As String, ByVal strActions As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEACTION_CREATE"
                .addItem("SUBMODULE_ID", strSubmoduleID, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTIONS", strActions, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)

                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.SubmoduleActionCreate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub Update(ByVal strSubmoduleID As String, ByVal strModuleID As String, ByVal strSubmoduleName As String, ByVal strSubmoduleName1 As String, _
                           ByVal strSubmoduleDesc As String, ByVal strSubmoduleDesc1 As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULE_UPDATE"
                .addItem("SUBMODULE_ID", strSubmoduleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("MODULE_ID", strModuleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SUBMODULE_NAME", strSubmoduleName, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SUBMODULE_NAME_1", strSubmoduleName1, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SUBMODULE_DESC", strSubmoduleDesc, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SUBMODULE_DESC_1", strSubmoduleDesc1, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub Delete(ByVal strSubmoduleID As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULE_DELETE"
                .addItem("SUBMODULE_ID", strSubmoduleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodule.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
