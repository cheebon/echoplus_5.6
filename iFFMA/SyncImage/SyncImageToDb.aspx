﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SyncImageToDb.aspx.vb" Inherits="iFFMA_SyncImage_SyncImageToDb" %>

<%@ Register TagPrefix="wuctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="wuclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="wuclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register Assembly="IZ.WebFileManager" Namespace="IZ.WebFileManager" TagPrefix="iz" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Salesteam List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="../../styles/jquery-ui-1.8.14.custom.css" rel="stylesheet" />
    <link href="../../styles/bootstrap.min.css" rel="stylesheet" />
    <link href="../../styles/jquery.ui.plupload.css" rel="stylesheet" />
    <script src="../../scripts/jquery-1.10.2.min.js"></script>
    <script src="../../scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../../scripts/plupload/js/moxie.min.js"></script>
    <script src="../../scripts/plupload/js/plupload.full.min.js"></script>
    <script src="../../scripts/plupload/js/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //LoadHeaderHtml();
            //LoadHeaderStyle();
            //initSession();
            LoadUploader();
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            //function EndRequestHandler(sender, args) {
            //    LoadHeaderHtml();
            //    LoadHeaderStyle();

            //}
        });

        function LoadUploader() {
            // Initialize the widget when the DOM is ready
            $(function () {
                $("#uploader").plupload({
                    // General settings
                    runtimes: 'html5,flash,silverlight,html4',
                    url: 'HandlerSyncImageUpload.ashx',
                    multipart_params: { Path: $('#hfVirtualPath').val() },
                    // Maximum file size
                    max_file_size: '20mb',
                    // Resize images on clientside if we can
                    resize: {
                        width: 200,
                        height: 200,
                        quality: 90,
                        crop: true // crop to exact dimensions
                    },
                    // Specify what files to browse for
                    filters: [
                        { title: "Image files", extensions: "png,jpg,gif" },
                        { title: "Doc files", extensions: "pdf,txt,pptx,xls,xlsx,docx,doc" },
                        { title: "AV files", extensions: "mp4,mp3,wmv" }
                    ],
                    // Rename files by clicking on their titles
                    rename: true,
                    // Sort files
                    sortable: true,
                    // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
                    dragdrop: true,
                    // Views to activate
                    views: {
                        list: true,
                        thumbs: true, // Show thumbs
                        active: 'thumbs'
                    },
                    // Flash settings
                    flash_swf_url: '../../Scripts/plupload-2.1.2/js/Moxie.swf',
                    // Silverlight settings
                    silverlight_xap_url: '../../Scripts/plupload-2.1.2/js/Moxie.xap',
                    preinit: attachCallbacks,
                    unique_names: false,
                    init: {
                        UploadComplete: function (up, files) {
                            up.splice();
                            up.refresh();
                        }
                    }
                });
            });
        }
        function attachCallbacks(Uploader) {
            Uploader.bind('FileUploaded', function (Up, File, Response) {
                if ((Uploader.total.uploaded + 1) == Uploader.files.length) {
                    javascript: WFM_FileManager1_Controller.OnRefresh(WFM_FileManager1_FileView, '');
                }
            });
        }
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesteamList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />

        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport" style="margin-top: 10px;">
                            <tr align="left">
                                <td>
                                    <asp:HiddenField ID="hfUserId" runat="server" />
                                    <asp:HiddenField ID="hfPrincipalId" runat="server" />
                                    <asp:HiddenField ID="hfAppName" runat="server" />
                                    <wuclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <wuclblInfo:wuc_lblInfo ID="lblErr" runat="server" />

                                    <div id="Content" style="width: 100%; padding-left: 20px;">
                                        <asp:UpdatePanel ID="updPnlHdr" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <label id="lblHeader" runat="server" text="Image Synchoniser" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="ContainerDivBlue" style="width: 98%; padding-top: 10px;">
                                            <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <%--<uc1:wuc_lblInfo ID="lblErr" runat="server" title="" />--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="updPnlMsg" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <%--<uc1:wuc_lblInfo ID="lblMsg" runat="server" title="" />--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="UpdPnlSync" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <label id="lblMsg" runat="server" />
                                                    <br />
                                                    <%--<customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />--%>
                                                    <asp:Label ID="lblSyncTypeDecs" runat="server" Text="Sync Type "></asp:Label>
                                                    <asp:DropDownList ID="ddlSyncType" runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem Text="Customer" Value="Customer" Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnLoadImageFromDb" runat="server" Text="Load all images from database and overite existing" CssClass="cls_button" Style="display: none" />
                                                    <asp:Button ID="btnSaveImageTodb" runat="server" Text="Save all images to datatbase and overite existing" CssClass="cls_button" Style="display: none" />
                                                    <asp:Button ID="btnLoadNewImageFromDb" runat="server" Text="Load only new images from database" CssClass="cls_button" Style="display: none" />
                                                    <asp:Button ID="btnSaveNewImageTodb" runat="server" Text="Save only new images to datatbase" CssClass="cls_button" Style="display: none" />
                                                    <asp:Button ID="btnUpdateDocumentLibrary" runat="server" Text="Update document library" CssClass="cls_button" />
                                                    <asp:HiddenField ID="hfTeamAccessStatus" runat="server" />
                                                    <asp:HiddenField ID="hfTeamName" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <br />
                                            <asp:UpdatePanel ID="UpdpnlFileMan" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td style="vertical-align: top;">
                                                                <div style="overflow: auto; height: 330px;">
                                                                    <iz:FileManager ID="FileManager1" runat="server" OnFileUploaded="FileManager1_FileUploaded" OnItemRenaming="FileManager1_FolderRename" OnNewFolderCreating="FileManager1_FolderCreate" OnSelectedItemsActionComplete="FileManager1_SelectedItemsActionComplete" OnSelectedItemsAction="FileManager1_SelectedItemsAction" AllowUpload="false">
                                                                        <RootDirectories>
                                                                        </RootDirectories>
                                                                    </iz:FileManager>
                                                                </div>
                                                            </td>
                                                            <td style="vertical-align: top;">
                                                                <div id="uploader" style="overflow: auto; width: 100%;">
                                                                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                            </tr>

                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
