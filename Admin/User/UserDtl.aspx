<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserDtl.aspx.vb" Inherits="Admin_User_NewUserDtl_UserDtl" %>

<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register Src="~/include/wuc_ddlUserType.ascx" TagName="wuc_ddlUserType" TagPrefix="uc9" %>
<%@ Register Src="~/include/wuc_ddlSalesRep.ascx" TagName="wuc_ddlSalesRep" TagPrefix="uc8" %>
<%@ Register Src="~/include/wuc_ddlBranch.ascx" TagName="wuc_ddlBranch" TagPrefix="uc7" %>
<%@ Register Src="~/include/wuc_ddlSalesTeam.ascx" TagName="wuc_ddlSalesTeam" TagPrefix="uc6" %>
<%@ Register Src="~/include/wuc_ddlPrincipal.ascx" TagName="wuc_ddlPrincipal" TagPrefix="uc5" %>
<%@ Register Src="~/include/wuc_ddlOperationType.ascx" TagName="wuc_ddlOperationType"
    TagPrefix="uc4" %>
<%@ Register Src="~/include/wuc_ddlLanguage.ascx" TagName="wuc_ddlLanguage" TagPrefix="uc3" %>
<%@ Register Src="~/include/wuc_ddlCountry.ascx" TagName="wuc_ddlCountry" TagPrefix="uc2" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script src="../../include/dkshpolicy.min.js" type="text/javascript"></script>

    <style type="text/css">
        .InfoTable {
            width: 510px;
            background-color: #C9D1F9;
            padding-left: 2px;
            padding-top: 2px;
            padding-right: 2px;
            padding-bottom: 2px;
        }

        .row {
            width: 510px;
            height: 100%;
        }

        .header_col {
            width: 20%;
            background-color: #C9D1F9;
            vertical-align: top;
            padding: 1px 2px 1px 7px;
        }

        .content_col {
            width: 30%;
            background-color: #EEEEEE;
            vertical-align: top;
            padding: 1px 2px 1px 7px;
        }

        .ARTableWidth {
            width: 45%;
        }
    </style>

    <script type="text/javascript">
        <% = Portal.Policy.WriteClientPolicy() %>
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmUserDtl" runat="server">
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
        <asp:UpdatePanel ID="UpdatePanelDtl" runat="server">
            <ContentTemplate>
                <table id="tbl1" cellspacing="0" cellpadding="2" width="98%" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BckgroundInsideContentLayout">
                            <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                        </td>
                    </tr>
                    <tr>
                        <td class="BckgroundBenealthTitle" height="5"></td>
                    </tr>
                    <tr class="Bckgroundreport" style="padding: 3px;">
                        <td>
                            <div style="width: 98%; border: solid 1px #b9b8b8; padding: 3px 3px 3px 3px;">
                                <asp:Button ID="cmdEditInfo" runat="server" Text="Edit" CssClass="cls_button" ValidationGroup="Edit"
                                    Width="70px" />
                                <asp:Button ID="cmdCancelInfo" runat="server" Text="Cancel" CssClass="cls_button"
                                    ValidationGroup="Cancel" Visible="False" Width="70px" />
                                <asp:Button ID="cmdSaveInfo" runat="server" CssClass="cls_button" Text="Save" OnClick="cmdSaveInfo_Click"
                                    Visible="False" Width="70px" /><br />
                                <div style="border: solid 1px #5d7b9d; width: 95%">
                                    <asp:Panel ID="pnlUserDtlInfo" runat="server">
                                        <table width="90%" cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="lblDtlEditViolation" runat="server" Text="Please complete current editing section before proceeding."
                                                        Visible="false" CssClass="cls_validator"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblStatusValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblIDPrefix" runat="server" Text="ID Prefix" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblIDPrefixValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLogin" runat="server" Text="Login" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblLoginValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPwdExpiryRmd" runat="server" Text="Password Expiry Reminder (Days)" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblPwdExpiryRmdValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblUserName" runat="server" Text="User Name" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblUserNameValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPasswordExpiry" runat="server" Text="Password Expiry (Days)" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblPasswordExpiryValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblUserType" runat="server" Text="User Type" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblUserTypeValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLoginExpiry" runat="server" Text="Login Expiry" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:CheckBox ID="cbLoginExpiry" runat="server" CssClass="cls_checkbox" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblCountryValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblMaxRetry" runat="server" Text="Max Retry" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblMaxRetryValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPrincipal" runat="server" Text="Principal" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblPrincipalValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPageSize" runat="server" Text="Page Size" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblPageSizeValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblSalesTeam" runat="server" Text="Sales Team" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblSalesTeamValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblNetValue" runat="server" Text="Net Value" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:CheckBox ID="cbNetValue" runat="server" CssClass="cls_checkbox" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblSalesRep" runat="server" Text="Sales Representative" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblSalesRepValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblDefaultNetValue" runat="server" Text="Default Net Value" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblDefaultNetValueValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLanguage" runat="server" Text="Language" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblLanguageValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblOpType" runat="server" Text="Operation Type" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblOpTypeValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblNewCustNo" runat="server" Text="New Customer No" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:Label ID="lblNewCustNoValue" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlUserDtlEdit" runat="server" Visible="false">
                                        <table width="100%" cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblStatusEdit" runat="server" Text="Status" CssClass="cls_label_header" /><asp:Label
                                                        ID="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:DropDownList ID="ddlStatusEdit" runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                        <asp:ListItem Value="0">In-Active</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvStatusID" runat="server" ControlToValidate="ddlStatusEdit"
                                                        ErrorMessage="Please select Status." Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLoginEdit" runat="server" Text="Login" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label1" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtLoginEdit" runat="server" CssClass="cls_textbox"></asp:TextBox><asp:RequiredFieldValidator
                                                        ID="rfvLogin" runat="server" ControlToValidate="txtLoginEdit" ErrorMessage="Login cannot be Blank !"
                                                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator><asp:Label
                                                            ID="lblLoginExists" runat="server" Text="Login Exists!" CssClass="cls_validator"
                                                            Visible="false"></asp:Label>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblIDPrefixEdit" runat="server" Text="ID Prefix" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtIDPrefixEdit" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPasswordEdit" runat="server" Text="Password (Blank if no changes)"
                                                        CssClass="cls_label_header" /><asp:Label ID="Label11" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtPasswordEdit" runat="server" CssClass="cls_textbox" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvPasswordBlank" runat="server" ControlToValidate="txtPasswordEdit"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lblPasswordRepeat" runat="server" Text="New password must be different from last 8 old password."
                                                        Visible="false" CssClass="cls_validator"></asp:Label>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPasswordConfirmEdit" runat="server" Text="Password Confirm (Blank if no changes)"
                                                        CssClass="cls_label_header" /><asp:Label ID="Label12" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtPasswordConfirmEdit" runat="server" CssClass="cls_textbox" TextMode="Password"></asp:TextBox><asp:CompareValidator
                                                        ID="cvPassword" runat="server" ErrorMessage="Password not matched!" Display="Dynamic"
                                                        CssClass="cls_validator" ControlToCompare="txtPasswordEdit" ControlToValidate="txtPasswordConfirmEdit"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPasswordConfirmBlank" runat="server" ControlToValidate="txtPasswordConfirmEdit"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPoorPass" runat="server" ErrorMessage="Minimum 8 alphanumeric character "
                                                        Display="Dynamic" CssClass="cls_validator" ControlToValidate="txtPasswordConfirmEdit"
                                                        OnServerValidate="PolicyCheck" ClientValidationFunction="PolicyCheck"></asp:CustomValidator>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPwdExpiryRmdEdit" runat="server" Text="Password Expiry Reminder (Days)"
                                                        CssClass="cls_label_header" /><asp:Label ID="Label14" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtPwdExpiryRmdEdit" runat="server" CssClass="cls_textbox" Text="<%$ AppSettings:PwdExpiryRmd%>"></asp:TextBox>
                                                    <asp:CompareValidator ID="cfvPwdReminderDays" runat="server" ErrorMessage="Password Expiry Reminder must be numeric"
                                                        ControlToValidate="txtPwdExpiryRmdEdit" Type="Double" Operator="DataTypeCheck" Display="Dynamic"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPwdReminderDays" runat="server" ControlToValidate="txtPwdExpiryRmdEdit"
                                                        ErrorMessage="Password expiry reminder cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cfvPwdReminderDays1" runat="server" Display="Dynamic" ErrorMessage="Password Expiry Reminder must be greater or equal to 0."
                                                        ControlToValidate="txtPwdExpiryRmdEdit" ValueToCompare="0" Operator="GreaterThanEqual"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblUserNameEdit" runat="server" Text="User Name" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label2" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtUserNameEdit" runat="server" CssClass="cls_textbox"></asp:TextBox><asp:RequiredFieldValidator
                                                        ID="rfvUserName" runat="server" ControlToValidate="txtUserNameEdit" ErrorMessage=" User Name cannot be Blank !"
                                                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPasswordExpiryEdit" runat="server" Text="Password Expiry (Days)"
                                                        CssClass="cls_label_header" /><asp:Label ID="Label7" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtPasswordExpiryEdit" runat="server" CssClass="cls_textbox" Text="<%$ AppSettings:PwdExpiryDays%>"></asp:TextBox>
                                                    <asp:CompareValidator ID="cfvPwdExpiry" runat="server" ErrorMessage="Password Expiry must be numeric"
                                                        ControlToValidate="txtPasswordExpiryEdit" Type="Double" Operator="DataTypeCheck"
                                                        Display="Dynamic" CssClass="cls_validator"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPwdExpiry" runat="server" ControlToValidate="txtPasswordExpiryEdit"
                                                        ErrorMessage="Password Expiry cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cfvPwdExpiry1" runat="server" Display="Dynamic" ErrorMessage="Password Expiry must be greater or equal to 0."
                                                        ControlToValidate="txtPasswordExpiryEdit" ValueToCompare="0" Operator="GreaterThanEqual"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblUserTypeEdit" runat="server" Text="User Type" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label3" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <uc9:wuc_ddlUserType ID="Wuc_ddlUserType" runat="server"></uc9:wuc_ddlUserType>
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLoginExpiryEdit" runat="server" Text="Login Expiry" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:CheckBox ID="cbLoginExpiryEdit" runat="server" CssClass="cls_checkbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblCountryEdit" runat="server" Text="Country" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label4" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <uc2:wuc_ddlCountry ID="Wuc_ddlCountry" runat="server" />
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblMaxRetryEdit" runat="server" Text="Max Retry" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label8" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtMaxRetryEdit" runat="server" CssClass="cls_textbox" Text="<%$ AppSettings:MaxRetry%>"></asp:TextBox>
                                                    <asp:CompareValidator ID="cfvMaxRetry" runat="server" ErrorMessage="Max Retry must be numeric"
                                                        ControlToValidate="txtMaxRetryEdit" Type="Double" Operator="DataTypeCheck" Display="Dynamic"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvMaxRetry" runat="server" ControlToValidate="txtMaxRetryEdit"
                                                        ErrorMessage="Max Retry cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cfvMaxRetry1" runat="server" Display="Dynamic" ErrorMessage="Max Retry must be greater or equal to 0."
                                                        ControlToValidate="txtMaxRetryEdit" ValueToCompare="0" Operator="GreaterThanEqual"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPrincipalEdit" runat="server" Text="Principal" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label5" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <uc5:wuc_ddlPrincipal ID="Wuc_ddlPrincipal" runat="server" />
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblPageSizeEdit" runat="server" Text="Page Size" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label9" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtPageSizeEdit" runat="server" CssClass="cls_textbox">20</asp:TextBox>
                                                    <asp:CompareValidator ID="cfvPageSize" runat="server" ErrorMessage="Page Size must be numeric"
                                                        ControlToValidate="txtPageSizeEdit" Type="Double" Operator="DataTypeCheck" Display="Dynamic"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPageSize" runat="server" ControlToValidate="txtPageSizeEdit"
                                                        ErrorMessage="Page Size cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cfvPageSize1" runat="server" Display="Dynamic" ErrorMessage="Page Size must be greater or equal to 1."
                                                        ControlToValidate="txtPageSizeEdit" ValueToCompare="0" Operator="GreaterThan"
                                                        CssClass="cls_validator"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblSalesTeamEdit" runat="server" Text="Sales Team" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <uc6:wuc_ddlSalesTeam ID="Wuc_ddlSalesTeam" runat="server" />
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblNetValueEdit" runat="server" Text="Net Value" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:CheckBox ID="cbNetValueEdit" runat="server" CssClass="cls_checkbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblSalesRepEdit" runat="server" Text="Sales Representative" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <uc8:wuc_ddlSalesRep ID="Wuc_ddlSalesRep" runat="server" />
                                                </td>
                                                <td class="header_col">
                                                    <asp:Label ID="lblDefaultNetValueEdit" runat="server" Text="Default Net Value" CssClass="cls_label_header"></asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <asp:DropDownList ID="ddlDefaultNetValueEdit" runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblLanguageEdit" runat="server" Text="Language" CssClass="cls_label_header" /><asp:Label
                                                        ID="Label6" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td class="content_col">
                                                    <uc3:wuc_ddlLanguage ID="Wuc_ddlLanguage" runat="server" />
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblOpTypeEdit" runat="server" Text="Operation Type" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <uc4:wuc_ddlOperationType ID="Wuc_ddlOperationType" runat="server" />
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                            <tr>
                                                <td class="header_col">
                                                    <asp:Label ID="lblNewCustNoEdit" runat="server" Text="New Customer No" CssClass="cls_label_header" />
                                                </td>
                                                <td class="content_col">
                                                    <asp:TextBox ID="txtNewCustNoEdit" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td>
                                                <td class="header_col"></td>
                                                <td class="content_col"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <asp:HiddenField ID="hfUserID" runat="server" />
                                <asp:HiddenField ID="hfUserCode" runat="server" />
                                <asp:HiddenField ID="hfSearchType" runat="server" />
                                <asp:HiddenField ID="hfSearchValue" runat="server" />
                                <asp:HiddenField ID="hfType" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td>
                            <div style="width: 98%; border: solid 1px #b9b8b8; padding: 3px 3px 3px 3px" runat="server"
                                id="MappingPanelDiv">
                                <asp:Panel ID="pnlMapping" runat="server">
                                    <asp:Label ID="lblMappingEditViolation" runat="server" Text="Please complete current editing section before proceeding."
                                        Visible="false" CssClass="cls_validator"></asp:Label>
                                    <div style="float: left; width: 48%">
                                        <asp:Button ID="cmdAccessRightMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            OnClick="cmdAccessRightMappingEdit_Click" ValidationGroup="dummyvalidation" Width="70px" />&nbsp;&nbsp;<asp:Label
                                                ID="lblAccessRightMapping" runat="server" Text="Access Right" CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lboxAccessRightMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>
                                    <div style="float: left; width: 48%">
                                        <asp:Button ID="cmdSalesRepMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            OnClick="cmdSalesRepMappingEdit_Click" ValidationGroup="dummyvalidation" Width="70px" />&nbsp;&nbsp;<asp:Label
                                                ID="lblSalesRepMapping" runat="server" Text="Sales Representative" CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lboxSalesRepMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>
                                    <div style="float: left; width: 48%" runat="server" id="SRGrpID">
                                        <asp:Button ID="cmdSalesrepGrpMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            ValidationGroup="dummyvalidation" Width="70px" />&nbsp;&nbsp;<asp:Label ID="Label10"
                                                runat="server" Text="Sales Representative Group" CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lboxSalesrepGrpMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>
                                    <div style="float: left; width: 48%" runat="server" id="Div1">
                                        <asp:Button ID="btnAgencyMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            Width="70px" />&nbsp;&nbsp;<asp:Label ID="lblAgencyDesc" runat="server" Text="Agency"
                                                CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lstAgencyMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>
                                    <div style="float: left; width: 48%" runat="server" id="ELAccessRight">
                                        <asp:Button ID="cmdELAccessRightMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            OnClick="cmdELAccessRightMappingEdit_Click" ValidationGroup="dummyvalidation" Width="70px" />&nbsp;&nbsp;<asp:Label
                                                ID="lblELAccessRightMapping" runat="server" Text="Easy Loader V2 Access Right" CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lboxELAccessRightMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>

                                    <div style="float: left; width: 48%" runat="server" id="SFMSCategory">
                                        <asp:Button ID="cmdSFMSCatAccessRightMappingEdit" runat="server" Text="Edit" CssClass="cls_button"
                                            OnClick="cmdSFMSCatAccessRightMappingEdit_Click" ValidationGroup="dummyvalidation" Width="70px" />&nbsp;&nbsp;<asp:Label
                                                ID="lblSFMSCatAccessRightMapping" runat="server" Text="Field Activity Category" CssClass="cls_label"></asp:Label><br />
                                        <asp:ListBox ID="lboxSFMSCatAccessRightMapping" runat="server" CssClass="cls_listbox" Width="80%"
                                            Rows="10"></asp:ListBox>
                                    </div>

                                </asp:Panel>
                                <asp:Panel ID="pnlAccessRight" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdARSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdARSave_Click" Width="70px" />
                                                <asp:Button ID="cmdARCancel" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdARCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxAccessRight" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">&nbsp;<asp:LinkButton ID="IBRight" runat="server" ToolTip="Select" OnClick="IBRight_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBLeft" runat="server" ToolTip="Deselect" OnClick="IBLeft_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="IBRightAll" runat="server" ToolTip="Select All" OnClick="IBRightAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBLeftAll" runat="server" ToolTip="Deselect All" OnClick="IBLeftAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxAccessRightAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlSalesrep" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdSRSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSRSave_Click" Width="70px" />
                                                <asp:Button ID="cmdSRCancel" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSRCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblSelectST" runat="server" Text="Select a sales team:" CssClass="cls_label"></asp:Label>
                                                <asp:DropDownList ID="ddlSalesTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlSalesTeam_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSalesrep" runat="server" CssClass="cls_listbox" Rows="10" SelectionMode="Multiple"
                                                    Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">
                                                <asp:LinkButton ID="IBSRSelect" runat="server" ToolTip="Select" OnClick="IBSRSelect_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRDeselect" runat="server" ToolTip="Deselect" OnClick="IBSRDeselect_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRSelectAll" runat="server" ToolTip="Select All" OnClick="IBSRSelectAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRDeselectAll" runat="server" ToolTip="Deselect All" OnClick="IBSRDeselectAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSalesrepAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlSalesrepGrp" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdSRGrpSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    Width="70px" />
                                                <asp:Button ID="cmdSRGrpCancel" CssClass="cls_button" runat="server" Text="Cancel"
                                                    ValidationGroup="dummyvalidation" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSalesrepGrp" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">
                                                <asp:LinkButton ID="IBSRGrpSelect" runat="server" ToolTip="Select">></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRGrpDeselect" runat="server" ToolTip="Deselect"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRGrpSelectAll" runat="server" ToolTip="Select All">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="IBSRGrDeselectAll" runat="server" ToolTip="Deselect All"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSalesrepGrpAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlAgency" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="btnAgencySave" CssClass="cls_button" runat="server" Text="Save" Width="70px" />
                                                <asp:Button ID="btnAgencyCancel" CssClass="cls_button" runat="server" Text="Cancel"
                                                    Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lstAgency" runat="server" CssClass="cls_listbox" Rows="10" SelectionMode="Multiple"
                                                    Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">
                                                <asp:LinkButton ID="lnkAgencySelect" runat="server" ToolTip="Select">></asp:LinkButton><br />
                                                <asp:LinkButton ID="lnkAgencyDeSelect" runat="server" ToolTip="Deselect"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="lnkAgencySelectAll" runat="server" ToolTip="Select All">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="lnkAgencyDeselectAll" runat="server" ToolTip="Deselect All"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lstAgencyAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlELAccessRight" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdELARSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdELARSave_Click" Width="70px" />
                                                <asp:Button ID="cmdELARCancel" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdELARCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxELAccessRight" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">&nbsp;<asp:LinkButton ID="ELIBRight" runat="server" ToolTip="Select" OnClick="ELIBRight_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="ELIBLeft" runat="server" ToolTip="Deselect" OnClick="ELIBLeft_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="ELIBRightAll" runat="server" ToolTip="Select All" OnClick="ELIBRightAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="ELIBLeftAll" runat="server" ToolTip="Deselect All" OnClick="ELIBLeftAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxELAccessRightAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCategory" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="Button1" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdELARSave_Click" Width="70px" />
                                                <asp:Button ID="Button2" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdELARCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="ListBox1" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">&nbsp;<asp:LinkButton ID="LinkButton1" runat="server" ToolTip="Select" OnClick="ELIBRight_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="LinkButton2" runat="server" ToolTip="Deselect" OnClick="ELIBLeft_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="LinkButton3" runat="server" ToolTip="Select All" OnClick="ELIBRightAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="LinkButton4" runat="server" ToolTip="Deselect All" OnClick="ELIBLeftAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="ListBox2" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlSFMSCategory" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdSFMSCatSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSFMSCatSave_Click" Width="70px" />
                                                <asp:Button ID="cmdSFMSCatCancel" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSFMSCatCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSFMSCatAccessRight" runat="server" CssClass="cls_listbox" Rows="10" SelectionMode="Multiple"
                                                    Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">
                                                <asp:LinkButton ID="SFMSCatRight" runat="server" ToolTip="Select" OnClick="SFMSCatRight_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatLeft" runat="server" ToolTip="Deselect" OnClick="SFMSCatLeft_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatRightAll" runat="server" ToolTip="Select All" OnClick="SFMSCatRightAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatLeftAll" runat="server" ToolTip="Deselect All" OnClick="SFMSCatLeftAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSFMSCatAccessRightAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--<asp:Panel ID="pnlSFMSCategory" runat="server" Visible="false">
                                    <table width="90%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="cmdSFMSCatSave" CssClass="cls_button" runat="server" Text="Save" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSFMSCatSave_Click" Width="70px" />
                                                <asp:Button ID="cmdSFMSCatCancel" CssClass="cls_button" runat="server" Text="Cancel" ValidationGroup="dummyvalidation"
                                                    OnClick="cmdSFMSCatCancel_Click" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSFMSCatAccessRight" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                            <td style="vertical-align: middle" align="center">&nbsp;<asp:LinkButton ID="SFMSCatRight" runat="server" ToolTip="Select" OnClick="SFMSCatRight_Click">></asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatLeft" runat="server" ToolTip="Deselect" OnClick="SFMSCatLeft_Click"><</asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatRightAll" runat="server" ToolTip="Select All" OnClick="SFMSCatRightAll_Click">>></asp:LinkButton><br />
                                                <asp:LinkButton ID="SFMSCatLeftAll" runat="server" ToolTip="Deselect All" OnClick="SFMSCatLeftAll_Click"><<</asp:LinkButton>
                                            </td>
                                            <td class="ARTableWidth">
                                                <asp:ListBox ID="lboxSFMSCatAccessRightAdded" runat="server" CssClass="cls_listbox" Rows="10"
                                                    SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                                <%--</ContentTemplate>--%>
                                <%--</asp:UpdatePanel>--%>
                            </div>
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td></td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td>
                            <asp:Button ID="cmdFFMASetting" runat="server" Text="FFMA Setting" CssClass="cls_button"
                                OnClick="cmdFFMASetting_Click" />
                            <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="cls_button" OnClick="cmdDelete_Click" />
                            <asp:Button ID="cmdReturn" runat="server" Text="Return" CssClass="cls_button" OnClick="cmdReturn_Click"
                                ValidationGroup="none" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
