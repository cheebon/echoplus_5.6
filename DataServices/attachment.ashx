﻿<%@ WebHandler Language="VB" Class="attachment" %>

Imports System
Imports System.Web
Imports System.Linq
Imports System.Web.Script.Serialization
Imports System.IO

Public Class attachment : Implements IHttpHandler, IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim strTxnNo = context.Request.QueryString("strTxnNo")
        Dim Mode = context.Request.QueryString("Mode")
        Dim strFileName = context.Request.QueryString("strFileName")
        
        'Dim strPath As String = ConfigurationManager.AppSettings("UploadPath") + context.Session("CURRENT_PRINCIPAL_CODE") + "\\TRA\\"
        Dim strPath As String = System.Web.HttpContext.Current.Server.MapPath("~/") + "Documents\Root\" + context.Session("CURRENT_PRINCIPAL_CODE") + "\TRA\"
        If Mode = "Upload" Then
            If (Not System.IO.Directory.Exists(strPath)) Then
                System.IO.Directory.CreateDirectory(strPath)
            End If
        
            For Each strFile As String In context.Request.Files
                Dim hpf As HttpPostedFile = TryCast(context.Request.Files(strFile), HttpPostedFile)
                Dim FileName As String = String.Empty
                If HttpContext.Current.Request.Browser.Browser.ToUpper() = "IE" Then
                    Dim files As String() = hpf.FileName.Split(New Char() {"\"})
                    FileName = files(files.Length - 1)
                Else
                    FileName = hpf.FileName.Split("\").Last()
                End If
                If hpf.ContentLength = 0 Then
                    Continue For
                End If
            
                Dim savedFileName As String = strPath + strTxnNo + "_" + FileName
                If (File.Exists(savedFileName)) Then
                    context.Response.Write("{""jquery-upload-file-error"":""" + FileName + " already exists""}")
                Else
                    hpf.SaveAs(savedFileName)
                    context.Response.Write("{""filePath"":""" + FileName + """}")
                End If
            Next
        End If
       
        If Mode = "Delete" Then
            Dim DeleteFileName As String = strPath + strFileName
            Try
                Dim myfile As FileInfo = New FileInfo(DeleteFileName)
                If myfile.Exists Then
                    File.Delete(DeleteFileName)
                End If
            Catch ex As Exception
            Finally
            End Try
        End If

        If Mode = "GetAttachment" Then
            Dim js As New JavaScriptSerializer()
            Dim r As New ArrayList
            For Each nFile In Directory.GetFiles(strPath, strTxnNo + "*", SearchOption.AllDirectories)
                r.Add(Path.GetFileName(nFile))
            Next
           
            Dim uploadedFiles = New With { _
                Key .files = r.ToArray() _
            }
            Dim jsonObj = js.Serialize(uploadedFiles)
            context.Response.Write(jsonObj.ToString())
        End If
        
        If Mode = "Remove" Then
            If strTxnNo <> ""
                Dim file = From fl In IO.Directory.GetFiles(strPath)
                        Where fl.Contains(strTxnNo)
                        Select fl
                For Each fl In file
                    IO.File.Delete(fl)
                Next
            Else
                
            End If
            
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return True
        End Get
    End Property

    <System.Runtime.Serialization.DataContractAttribute()> _
    Public Class UploadedFilesResult
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property file() As String
    End Class
    
End Class
